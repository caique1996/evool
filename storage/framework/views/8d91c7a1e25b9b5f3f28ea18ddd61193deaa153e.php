<?php $__env->startSection('page_css'); ?>
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/rede-scripts/jquerytop.css')); ?>">

<link rel="stylesheet" href="<?php echo e(env('CFURL').('/rede-scripts/jquery.orgchart.css')); ?>">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
Minha Rede
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Minha Rede
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li class="active">Minha Rede</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php

function printPopOver($id, $login, $nome, $status) {
    $status = $status ? 'Ativo' : 'Inativo';
    return "ID:<b>$id</b><br/> Login:<b>$login</b><br/> Nome:<b>$nome</b></br> Status: </b>$status</b>";
}
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div id="orgChartContainer">
        <div id="orgChart"></div>
    </div>
    <div id="consoleOutput">
    </div>

</div>


<!-- Main row -->
<div class="row">

</div><!-- /.row (main row) -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<script src="<?php echo e(env('CFURL').('/rede-scripts/jquery.orgchart.js')); ?>"></script>
<script type="text/javascript">
<?php
$user = new \App\User();

$total_rede = $user->display_children(Auth::user()->id);
if ($user->apto_bonus(Auth::user()->id)) {
    $status_str = 'Ativo';
    $status = 1;
} else {
    $status_str = 'Pendente';
    $status = 0;
}
$desc = 'Status: ' . $status_str . " <br> Rede: $total_rede";
?>
var testData = [
    {id: <?= Auth::user()->id ?>, name: '<?= Auth::user()->username ?>', parent: 0, ativo: <?=$status?>,'description':'<?=$desc?>'},
<?php
$filhos = \App\Referrals::where('system_id', Auth::user()->id)->get();
foreach ($filhos as $value) {
    $usr = $user->userInfo($value['user_id']);
    $total_rede = $user->display_children($value['user_id']);
    if ($user->apto_bonus($value['user_id'])) {
        $status_str = 'Ativo';
        $status = 1;
    } else {
        $status_str = 'Pendete';
        $status = 0;
    }


    $desc = 'Status: ' . $status_str . " <br> Rede: $total_rede";
    echo "{id: {$usr['id']}, name: '{$usr['username']}', parent:{$value['system_id']},ativo:$status,'description':'$desc'},";


}
?>

];
$(function () {
    org_chart = $('#orgChart').orgChart({
        data: testData,
        showControls: false,
        allowEdit: false,
        onAddNode: function (node) {
            //  log('Created new node on node ' + node.data.id);
            // org_chart.newNode(node.data.id);
        },
        onDeleteNode: function (node) {
            //log('Deleted node ' + node.data.id);
            //org_chart.deleteNode(node.data.id);
        },
        onClickNode: function (node) {
                      location.href="<?=url('painel/minha-rede/')?>/"+node.data.id;

        }

    });
});

// just for example purpose
function log(text) {
    $('#consoleOutput').append('<p>' + text + '</p>')
}
</script>
<script>



    $(function () {

        $('[data-toggle="popover"]').popover({
            html: true,
            trigger: 'manual',
            container: $(this).attr('id'),
            placement: 'top',
            content: function () {
                $return = '<div class="hover-hovercard"></div>';
            }
        }).on("mouseenter", function () {
            var _this = this;
            $(this).popover("show");
            $(this).siblings(".popover").on("mouseleave", function () {
                $(_this).popover('hide');
            });
        }).on("mouseleave", function () {
            var _this = this;
            setTimeout(function () {
                if (!$(".popover:hover").length) {
                    $(_this).popover("hide")
                }
            }, 100);
        });

    })

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>