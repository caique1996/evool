<!DOCTYPE html>
<html lang="en">
    <?php echo $__env->make('layouts.partials.htmlheader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body class="<?= App\config::getConf()['tema_estilo'] ?> sidebar-mini">
        <div class="wrapper">
            <?php echo $__env->make('layouts.partials.mainheader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('layouts.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php $renovacao = app('App\Http\Controllers\Admin\VoucherController'); ?>
                <?php
                if ($renovacao->renova(Auth::user()->id)) {
                    echo "<script>alert('A sua conta foi renovada automaticamente.');</script>";
                }
                ?>
                <?php echo $__env->make('layouts.partials.contentheader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Main content -->
                <section class="content">
                    <!-- Your Page Content Here -->
                    <?php echo $__env->yieldContent('main-content'); ?>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
            <?php echo $__env->make('layouts.partials.controlsidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('layouts.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div><!-- ./wrapper -->
        <div class="modal fade" id="modalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalMsgTitle"></h4>
                    </div>
                    <div class="modal-body" id='modalMsgBody'>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-backdrop="static" 
             data-keyboard="false" >
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-sm">
                        <h4 class="modal-title" id="LoadingMsgTitle"></h4>
                    </div>
                    <div class="modal-body" id='LoadingMsgBody'>
                        <div class="text-center">
                            <img src="<?php echo e(asset('sximo/images/carregando.gif')); ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $config = \App\config::all()->first();
        if ($config['tawk_status'] == 'sim' and $config['tawk_token'] <> '') {
            ?>
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
                (function () {
                    var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/<?= $config['tawk_token'] ?>/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0.parentNode.insertBefore(s1, s0);
                })();
            </script>
        <?php } ?>
        <?php echo $__env->make('layouts.partials.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body>
</html>
