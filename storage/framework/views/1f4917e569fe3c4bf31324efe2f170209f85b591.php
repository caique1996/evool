<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <strong><a href="<?php echo e(env('SITE_URL')); ?>"><?= App\config::getConf()['site_name'] ?></a>.</strong>
    </div>
    <!-- Default to the left -->
    &nbsp;
</footer>