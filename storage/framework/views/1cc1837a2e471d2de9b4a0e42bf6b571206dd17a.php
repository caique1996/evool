<?php $__env->startSection('htmlheader_title'); ?>
Meu carrinho
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Loja Virtual > Meu carrinho
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box">
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="ord-carrinho" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th> </th>
                            <th>Produto</th>
                            <th>Preço</th>
                            <th>Quantidade</th>
                            <th>Total</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($produtos as $produto): ?>
                        <tr>
                            <td style="
                                background-image: url('/public/uploads/<?php echo e($produto['produto']['img']); ?>');
                                background-position: center;
                                background-size: cover;
                                height: 77px;">
                            </td>
                            <td><?php echo e($produto['produto']['nome']); ?></td>
                            <td> <?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto['product_id'])) { ?>
                    <strike>R$ <?php echo e(number_format($produto['produto']['preco'],2)); ?></strike> R$ <?= number_format(Auth::user()->meu_desconto($produto['product_id']), 2) ?>
                    <?php } else { ?>
                        R$ <?php echo e(number_format($produto['produto']['preco'],2)); ?>

                    <?php } ?></td>
                    <td>

                        <form method="get" action="<?php echo e(url('/painel/meu-carrinho/qtd/')); ?>/<?php echo e($produto['product_id']); ?>">

                            <input style="width: 50px; text-align: center;" name="qtd" value="<?php echo e($produto['quantidade']); ?>"/>
                            <button type="sumit">Salvar</button>
                        </form>


                    </td>
                    <td>
                        <?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto['product_id'])) { ?>
                        <strike>R$ <?php echo e(number_format($produto['produto']['preco']*$produto['quantidade'],2)); ?></strike> R$ <?= number_format(Auth::user()->meu_desconto($produto['product_id']) * $produto['quantidade'], 2) ?>
                    <?php } else { ?>
                        R$ <?php echo e(number_format($produto['produto']['preco']*$produto['quantidade'],2)); ?>

                    <?php } ?></h4></td>
                    <td><a href="<?php echo e(url('/painel/meu-carrinho/remove/')); ?>/<?php echo e($produto['product_id']); ?>"><i style="color:#F37171;" class="fa fa-times"></i></a></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-4">
                        <form method="get" action="<?php echo e(url('/painel/meu-carrinho/')); ?>">
                            <p class="lead" style="margin: 50px 0px 6px 0px;">Calcular frete</p>
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <p>CEP</p>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="cep" placeholder="Insira sue CEP" value="<?php echo e($cep); ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>PAC</p>
                                    </td>
                                    <td>
                                        <input type="radio" name="codigo" value="41106" <?php echo e($codigo == 41106 ? 'checked' : ''); ?>>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>SEDEX</p>
                                    </td>
                                    <td>
                                        <input type="radio" name="codigo" value="40010" <?php echo e($codigo == 40010 ? 'checked' : ''); ?>>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>SEDEX 10</p>
                                    </td>
                                    <td>
                                        <input type="radio" name="codigo" value="40215" <?php echo e($codigo == 40215 ? 'checked' : ''); ?>>
                                    </td>
                                </tr>
                            </table>
                            <a href="/painel/produtos">
                                <button type="submit" style="margin-bottom: 5px; color: black;" class="btn btn-flat btn-responsive btn-block">
                                    Calcular frete
                                </button>
                            </a>
                        </form>
                    </div>

                    <div class="col-md-offset-4 col-md-4">
                        <p class="lead" style="margin: 50px 0px 6px 0px;">Total no carrinho</p>
                        <table class="table table-bordered">

                            <tr>
                                <td>
                                    <p >Subtotal</p>
                                </td>
                                <td>
                                    <?php echo e(number_format($subtotal,2)); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p >Frete</p>
                                </td>
                                <td>
                                    <?php echo e(empty($frete) ? 'Indisponivel' : 'R$ '.$frete); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p ><b>Total</b></p>
                                </td>
                                <td>
                                    <b>R$ <?php echo e(number_format($total,2)); ?></b>
                                </td>
                            </tr>
                        </table>
                        <a href="/painel/produtos">
                            <button style="margin-bottom: 5px; color: black;" type="button" class="btn btn-flat btn-responsive btn-block" name="button">
                                Comprar mais produtos
                            </button>
                        </a>
                        <?php if(sizeof($produtos) > 0): ?>
                        <a href="/painel/meu-carrinho/endereco">
                            <button type="button" style="color: black;" class="btn btn-flat btn-responsive btn-block" name="button">Realizar pedido</button>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

<div class="modal fade" id="modal_desconto" data-backdrop="static" 
     data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalMsgTitle">Pararabéns ,você está fazendo uma grande economia!</h4>
            </div>
            <div class="modal-body" id='modalMsgBody'>
                <p>Normalmente você pagaria R$ <?php echo e(number_format($subtotal2,2)); ?> mas você irá pagar apenas <strong><?php echo e(number_format($subtotal,2)); ?></strong>
                <h3>Você economizou  R$ <?php echo e(number_format($subtotal2-$subtotal,2)); ?>

            </div>
            <div id="mensagem_anuncio"></div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>

$(function () {
<?php if ($subtotal2 > $subtotal) { ?>
        $("#modal_desconto").modal();
<?php }
?>
    $('#ord-carrinho').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "oLanguage": {"sZeroRecords": "Não existem produtos em seu carrinho",
            "sEmptyTable": "Não existem produtos em seu carrinho"},
        "order": [[1, "desc"]]
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>