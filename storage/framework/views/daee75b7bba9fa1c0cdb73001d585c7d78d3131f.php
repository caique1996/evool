<!-- Left side column. contains the logo and sidebar -->
<?php
global $request;
$uriPainel = $request->segment(1);
$uriPage = $request->segment(2);
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <?php if(! Auth::guest() && !Auth::user()->ativo): ?>
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo e(Auth::user()->photo); ?>" width="64" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php echo e(Auth::user()->getShortName()); ?></p>
                <!-- Status -->
                <a href="#"><?= Auth::user()->minhaGraduacao() ?></a>
            </div>
        </div>
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" disabled placeholder="Pesquisar..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="<?php echo e($uriPage == '' ? 'active' : ''); ?>"><a href="javascript:$('#enviarComprovante').modal();"><i class='fa fa-upload'></i> <span>Enviar comprovante</span></a></li>

        </ul>

        <?php endif; ?>
        <?php if(! Auth::guest() && Auth::user()->ativo): ?>
        <div class="user-panel">
            <div class="pull-left image">
                <img onclick="" class="img-circle" src="<?php echo e(Auth::user()->photo); ?>" width="128"">
            </div>
            <div class="pull-left info">
                <p><?php echo e(Auth::user()->getShortName()); ?></p>
                <!-- Status -->
                <a href="#"><?= Auth::user()->minhaGraduacao() ?></a>


            </div>
        </div>
        <?php endif; ?>


        <?php if(Auth::user()->ativo): ?>
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" disabled placeholder="Pesquisar..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>
            <!-- Optionally, you can add icons to the links -->

            <li class="<?php echo e($uriPage == 'home' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/home')); ?>"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>

            <?php if(! Auth::guest()  && $uriPainel == 'painel'): ?>
            <li class="<?php echo e($uriPage == 'meus-dados' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/meus-dados')); ?>"><i class='fa fa-user'></i> <span>Meus Dados</span></a></li>
            <li class="treeview"  class="<?php echo e($uriPage == 'minha-rede' ? 'active' : ''); ?><?php echo e($uriPage == 'meus-indicados' ? 'active' : ''); ?><?php echo e($uriPage == 'unilevel' ? 'active' : ''); ?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Minha Rede</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e($uriPage == 'minha-rede' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/minha-rede')); ?>"><i class='fa fa-circle-o'></i> <span>Minha rede</span></a></li>

                    <li class="<?php echo e($uriPage == 'meus-indicados' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/meus-indicados')); ?>"><i class='fa fa-circle-o'></i> <span>Indicados Diretos</span></a></li>

                    <li class="<?php echo e($uriPage == 'unilevel' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/unilevel')); ?>"><i class='fa fa-circle-o'></i> <span>Rede Unilevel </span></a></li>
                </ul>
            </li>
            <?php if (env('lv_BTNvZfXgG4b5JPnD') == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-shopping-cart "></i> <span>Loja Virtual</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo e($uriPage == 'produtos' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/produtos')); ?>"><i class="fa fa-circle-o"></i> Produtos</a></li>
                        <li class="<?php echo e($uriPage == 'meu-carrinho' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/meu-carrinho')); ?>"><i class="fa fa-circle-o"></i> Meu carrinho</a></li>
                        <li class="<?php echo e($uriPage == 'meus-pedidos' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/meus-pedidos')); ?>"><i class='fa fa-truck '></i> <span>Meus pedidos</span></a></li>
                    </ul>
                </li>
            <?php } ?>



            <li class="treeview"  class="<?php echo e($uriPage == 'transacoes' ? 'active' : ''); ?><?php echo e($uriPage == 'faturas' ? 'active' : ''); ?><?php echo e($uriPage == 'saques' ? 'active' : ''); ?><?php echo e($uriPage == 'gerenciar_saldo' ? 'active' : ''); ?>">
                <a href="#">
                    <i class="fa fa-dollar"></i> <span>Financeiro</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e($uriPage == 'vouchers' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/vouchers')); ?>"><i class='fa fa-circle-o'></i> <span>Vouchers</span></a></li>

                    <li class="<?php echo e($uriPage == 'transacoes' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/transacoes')); ?>"><i class='fa fa-circle-o'></i> <span>Extrato financeiro</span></a></li>
                    <li class="<?php echo e($uriPage == 'pontos' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/pontos')); ?>"><i class='fa fa-circle-o'></i> <span>Extrato de Pontos</span></a></li>
                 <!--   <li class="<?php echo e($uriPage == 'faturas' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/faturas')); ?>"><i class='fa fa-circle-o'></i> <span>Faturas</span></a></li>-->
                    <li class="<?php echo e($uriPage == 'saques' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/saques')); ?>"><i class='fa fa-circle-o'></i> <span>Saques</span></a></li>
                    <li class="<?php echo e($uriPage == 'gerenciar_saldo' ? 'active' : ''); ?>"><a href="javascript:$('#converterSaldo').modal();""><i class='fa fa-circle-o'></i> <span>Converter Saldo</span></a></li>
                    <li class="<?php echo e($uriPage == 'gerenciar_saldo' ? 'active' : ''); ?>"><a href="javascript:$('#transferirSaldo').modal();""><i class='fa fa-circle-o'></i> <span>Transferir Saldo</span></a></li>

                    <li class=""><a  href="javascript:$('#ativarUsuario').modal();"><i class='fa fa-circle-o'></i> <span>Ativar Usuário</span></a></li>
                    <li class="<?php echo e($uriPage == 'gerenciar_saldo' ? 'active' : ''); ?>"><a href="javascript:$('#pagarSaldo').modal();""><i class='fa fa-circle-o'></i> <span>Pagar fatura com saldo</span></a></li>

                </ul>
            </li>

            <li class="<?php echo e($uriPage == 'landingpage' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/landingpage')); ?>"><i class='fa fa-trophy'></i> <span>Meu site</span></a></li>



            <li class="<?php echo e($uriPage == 'upgrade' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/upgrade')); ?>"><i class='fa fa-trophy'></i> <span>Upgrade</span></a></li>

            <li class="<?php echo e($uriPage == 'materiais' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/materiais')); ?>"><i class='fa fa-folder'></i> <span>Materiais</span></a></li>
            <?php
            $config = \App\config::all()->first();
            if ($config['tawk_status'] == 'sim' and $config['tawk_token'] <> '') {
                ?>
                <li ><a href="javascript:Tawk_API.toggle();"><i class='fa fa-support'></i> <span>Suporte</span></a></li>
            <?php } else { ?>

                <li ><a href="mailto:<?= $config['email_suporte'] ?>"><i class='fa fa-support'></i> <span>Suporte</span></a></li>

            <?php } ?>
            <li class="<?php echo e($uriPage == 'graduacoes' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/graduacoes')); ?>"><i class='fa fa-graduation-cap'></i> <span>Graduações</span></a></li>

            <li class="<?php echo e($uriPage == '' ? 'active' : ''); ?>"><a href="javascript:$('#enviarComprovante').modal();"><i class='fa fa-upload'></i> <span>Enviar comprovante</span></a></li>




            <?php endif; ?>
            <?php if(! Auth::guest() && Auth::user()->isAdmin() && $uriPainel == 'admin'): ?>
            <?php if (env('lv_BTNvZfXgG4b5JPnD') == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-shopping-cart "></i> <span>Loja Virtual</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo e($uriPage == 'produtos/add' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/produtos/add')); ?>"><i class="fa fa-circle-o"></i> Adicionar Produtos</a></li>
                        <li class="<?php echo e($uriPage == 'produtos/list' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/produtos/list')); ?>"><i class="fa fa-circle-o"></i> Lista de Produtos</a></li>
                        <li class="<?php echo e($uriPage == 'produtos' ? 'active' : ''); ?>"><a href="<?php echo e(url('/painel/produtos')); ?>"><i class="fa fa-circle-o"></i> Produtos</a></li>
                        <li class="<?php echo e($uriPage == 'meu-carrinho' ? 'active' : ''); ?>"><a href="<?php echo e(url('/painel/meu-carrinho')); ?>"><i class="fa fa-circle-o"></i> Meu carrinho</a></li>
                        <li class="<?php echo e($uriPage == 'pedidos' ? 'active' : ''); ?>"><a href="<?php echo e(url('/admin/pedidos')); ?>"><i class="fa fa-circle-o"></i>Pedidos</a></li>
                    </ul>
                </li>
            <?php } ?>

            <li class="treeview"  class="<?php echo e($uriPage == 'transacoes' ? 'active' : ''); ?><?php echo e($uriPage == 'faturas' ? 'active' : ''); ?><?php echo e($uriPage == 'saques' ? 'active' : ''); ?><?php echo e($uriPage == 'gerenciar_saldo' ? 'active' : ''); ?>">
                <a href="#">
                    <i class="fa fa-dollar"></i> <span>Financeiro</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e($uriPage == 'relatorios' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/relatorios')); ?>"><i class='fa fa-circle-o'></i> <span>Relatórios</span></a></li>
                    <li class="<?php echo e($uriPage == 'saque' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/saque')); ?>"><i class='fa fa-circle-o'></i> <span>Saques</span></a></li>
                    <li class="<?php echo e($uriPage == 'faturas' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/faturas')); ?>"><i class='fa fa-circle-o'></i> <span>Faturas</span></a></li>
                    <li class="<?php echo e($uriPage == 'comprovantes' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/comprovantes')); ?>"><i class='fa fa-upload'></i> <span>Comprovantes</span></a></li>

                </ul>
            </li>



            <li class="<?php echo e($uriPage == 'usuarios' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/usuarios')); ?>"><i class='fa fa-users'></i> <span>Gerenciar Usuários</span></a></li>
            <li class="<?php echo e($uriPage == 'vouchers' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/vouchers')); ?>"><i class='fa fa-users'></i> <span>Gerenciar Vouchers</span></a></li>
            <li class="<?php echo e($uriPage == 'materiais' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/materiais')); ?>"><i class='fa fa-folder'></i> <span>Materiais</span></a></li>
            <li class="<?php echo e($uriPage == 'pacote' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/pacote')); ?>"><i class='fa fa-folder'></i> <span>Pacotes</span></a></li>
            <li class="<?php echo e($uriPage == 'graduacoes' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/graduacoes')); ?>"><i class='fa fa-graduation-cap'></i> <span>Graduações</span></a></li>
            <li class="<?php echo e($uriPage == 'config' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.$uriPainel.'/config')); ?>"><i class='fa fa-gears'></i> <span>Configurações Gerais</span></a></li>
                <li><a href="javascript:$('#lucro').modal();"><i class='fa fa-dollar'></i> <span>Renda Variável</span></a></li>
            <li><a href="javascript:$('#addSaldo').modal();"><i class='fa fa-plus'></i> <span>Divisão Manager Direct</span></a></li>

            <?php
            if (isset($_GET['infoBin'])) {
                echo "<script> alert('Operação realizada com sucesso')</script>";
            }
            ?>

            <?php endif; ?>
            <!-- Sidebar Menu -->

            <?php endif; ?>



        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
