<?php $__env->startSection('htmlheader_title'); ?>
Pedido número <?php echo e($pedido['id']); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Loja Virtual > Meus pedidos > Pedido número <?php echo e($pedido['id']); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<div class="container-fluid">

    <div class="box">
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">

                    <div id="mensagemAdicionarVouchers">

                    </div>

                    <div class="col-md-5">
                        <h4>Endereco de entrega</h4>
                        <div class="row">
                            <div class="box box-info">
                                <div class="box-body">
                                    <p><?php echo e($endereco['pais']); ?></p>
                                    <p><?php echo e($endereco['endereco']); ?></p>
                                    <p>CEP <?php echo e($endereco['cep']); ?> - <?php echo e($endereco['cidade']); ?>, <?php echo e($endereco['estado']); ?></p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-offset-1 col-md-6">

                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    ID do pedido
                                </td>
                                <td>
                                    <?php echo e($pedido['id']); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Status
                                </td>
                                <td>
                                    <?php echo e($pedido['status']); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Preco + frete
                                </td>
                                <td>
                                    R$ <?php echo e(number_format($pedido['preco'],2)); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Data
                                </td>
                                <td>
                                    <?php echo e(Carbon\Carbon::parse($pedido['date'])->format('d/m/Y')); ?>

                                </td>
                            </tr>
                        </table>

                    </div>
                    <?php
                    define('MY_SHOP_LOGIN', env("MY_SHOP_LOGIN"));
                    define('MY_SHOP_SECRET', env("MY_SHOP_SECRET"));

                    $shop_login = MY_SHOP_LOGIN;
                    $shop_secret = MY_SHOP_SECRET;

                    $amount = $pedido['preco'];
                    $currency = 'BRL';
                    $reference = md5(time() . rand(0, 999999999999999));

                    $invoice_id = $reference;


                    $payment_description = '#pedido_' . $pedido['id'] . '_' . \Auth::user()->id;
                    $signature = '';
                    $message = 'S' . $shop_login . $amount . $currency . $invoice_id;
                    $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));
                    if ($pedido['status'] == 'Pendente') {

                        echo '
    <form id="form" action="' . env("BITZPAYER_URL") . '" method="POST">
                        <input type="hidden" name="shop_login"          value="' . $shop_login . '"/>
                        <input type="hidden" name="amount"              value="' . $amount . '"/>
                        <input type="hidden" name="currency"            value="' . $currency . '"/>
                        <input type="hidden" name="invoice_id"          value="' . $invoice_id . '"/>
                        <input type="hidden" name="payment_description" value="' . $payment_description . '"/>
                        <input type="hidden" name="signature"           value="' . $signature . '"/>
                                        <button type="submit"  class="btn btn-lg btn-primary">Pagar Via Bitzpayer<img src="http://sandbox.bitzpayer.com/images/whitelabels/bitzpayer.png" width="40"></button>

                    </form>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
                    <script>
                ///$("#form").submit();
                    </script>';
                    }
                    ?>
                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                    <table id="ord-carrinho" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> </th>
                                <th>Produto</th>
                                <th>Preço</th>
                                <th>Quantidade</th>
                                <th>Total</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <?php
                        $produtos = json_decode($pedido['produtos'])
                        ?>
                        <tbody>

                            <?php foreach($produtos as $produto): ?>
                            <tr>
                                <td style="
                                    background-image: url('/<?php echo e($produto->img); ?>');
                                    background-position: center;
                                    background-size: cover;
                                    height: 77px;">
                                </td>
                                <td><?php echo e($produto->nome); ?></td>
                                <td> <?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto->product_id)) { ?>
                                    R$ <?php echo e(number_format(Auth::user()->meu_desconto($produto->product_id), 2)); ?>

                                    <?php } else { ?>R$ <?php echo e(number_format($produto->preco,2)); ?>

                                    <?php } ?></td>
                                <td><?php echo e($produto->quantidade); ?></td>
                                <td><?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto->product_id)) { ?>
                                    R$ <?php echo e(number_format(Auth::user()->meu_desconto($produto->product_id)*$produto->quantidade, 2)); ?>

                                    <?php } else { ?> R$ <?php echo e(number_format($produto->preco*$produto->quantidade,2)); ?>

                                    <?php }?>
                                   </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>



                </div>
            </div>
        </div>


    </div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>