<!-- Content Header (Page header) -->

<div class="row">
    <div class="col-md-12">
        <?php if(!Auth::user()->ativado()): ?>
            <div class="alert alert-warning alert-dismissible flat no-margin">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Atenção!
                    <small class="text-bold text-black"> Sua conta encontra-se inativa, insira seu voucher de ativação abaixo ou efetue o pagamento para poder utilizar o sistema!</small>
                </h4>
                <form action="<?php echo e(url('painel/ativar-conta')); ?>" method="post">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('POST')); ?>

                    <div class="input-group">
                        <input type="text" name="ativarVoucher" class="form-control" placeholder="Insira seu voucher de ativação">
                          <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Ativar Conta!</button>
                          </span>
                    </div>
                </form>
            </div>
        <?php endif; ?>

        <?php if(session('success')): ?>
            <div class="alert alert-success fade in alert-dismissible flat  no-margin">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                <?php echo e(session('success')); ?>

            </div>
        <?php endif; ?>

        <?php if(isset($errors) && count($errors) > 0): ?>
            <div class="alert alert-danger alert-dismissible flat no-margin">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Whoops!</strong><br>
                <ul>
                    <?php foreach($errors->all() as $error): ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>


<section class="content-header">
    <h1>
        <?php echo $__env->yieldContent('contentheader_title', 'Page Header here'); ?>
        <small><?php echo $__env->yieldContent('contentheader_description'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
        <?php echo $__env->yieldContent('breadcrumb', '<li class="active">Dashboard</li>'); ?>
    </ol>
</section>