<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
Saques
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Saques
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li class="active">Saques</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<!-- Main row -->
<!-- Small boxes (Stat box) -->
<div id='saqueModal' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>
           
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> Lista de Saques</h3>

            </div>
            <div class="box-body">
                <div id="mensagemAjax">

                </div>
                <table id="example2" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>Mensagem</th>
                            <th>Data de solicitação</th>
                            <th>Status</th>
                            <th>valor</th>
                            <td>Data de deposito</td>
                            <td>Conta</td>

                        </tr>
                    </thead>
                    <tbody>
                        <?php $User = app('App\User'); ?>

                        <?php

                        function getStatus($id) {
                            switch ($id) {
                                case 0:
                                    $res = 'Pendente';
                                    break;
                                case 1:
                                    $res = 'Pago';
                                    break;

                                case 2:
                                    $res = 'Pendente';
                                    break;
                            }
                            return $res;
                        }
                        ?>
                        <?php $Saque = app('App\Saque'); ?>
                        <?php foreach($Saque->where('user_id',Auth::user()->id)->orderBy('id')->get() as $saque): ?>

                        <tr>
                            <td><?php echo e($saque->id); ?></td>
                            <td><?php echo e($saque->mensagem); ?></td>
                            <td><?php echo e($saque->created_at); ?></td>
                            <td><?php echo e(getStatus($saque->status)); ?></td>
                            <td>R$<?php echo e(@number_format($saque->valor, 2, ',', '.')); ?></td>
                            <td><?php echo e($saque->data_deposito); ?></td>
                            <td><?php echo e(strip_tags($saque->conta)); ?></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.Left col -->

    <?php echo e(method_field('PUT')); ?>

</div><!-- /.row (main row) -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php $__env->stopSection(); ?>


<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(asset('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/form/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>


<script>
                                        $(function () {

                                        function fBlockUi() {
                                        $.blockUI({
                                        message: "<h4>Por favor aguarde...</h4>",
                                                css: {
                                                border: 'none',
                                                        padding: '5px',
                                                        backgroundColor: '#000',
                                                        '-webkit-border-radius': '5px',
                                                        '-moz-border-radius': '5px',
                                                        opacity: .5,
                                                        color: '#fff'
                                                }
                                        });
                                        }

                                        $(".delete").click(function () {
                                        fBlockUi();
                                                $.ajax({
                                                url: '<?php echo e(url(' / admin / pacote')); ?>' + '/' + $(this).attr('data-id'),
                                                        method: 'DELETE',
                                                        data: {_token: "<?php echo csrf_token(); ?>"},
                                                        type: 'DELETE',
                                                        success: function (result) {
                                                        $("#mensagemAjax").html(result);
                                                                $.unblockUI();
                                                                window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                                                        }
                                                });
                                        });
                                                $(".openModal").on('click', function () {

                                        $('#myModal').removeData('bs.modal');
                                                $('#myModal').modal(
                                        {
                                        remote: $(this).attr('data-href')
                                        }
                                        );
                                                $('#myModal').modal('show');
                                                $('#myModal').on('loaded.bs.modal', function (e) {

                                        // bind form using ajaxForm
                                        var form = $('.formAjax').ajaxForm({
                                        // target identifies the element(s) to update with the server response
                                        target: '#mensagemAjax',
                                                beforeSubmit: function () {
                                                fBlockUi();
                                                        $('#myModal').modal('hide');
                                                        $('input').attr('disabled', true);
                                                },
                                                // success identifies the function to invoke when the server response
                                                // has been received; here we apply a fade-in effect to the new content
                                                success: function () {
                                                $('.formAjax').clearForm();
                                                        $('#mensagemAjax').fadeIn('slow');
                                                        $.unblockUI();
                                                        $('input').attr('disabled', false);
                                                        window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                                                }
                                        });
                                        });
                                        });
                                                $('#example2').DataTable({
                                                    "order": [ 0, 'desc' ],
                                        "paging": true,
                                                "lengthChange": true,
                                                "searching": true,
                                                "ordering": true,
                                                "info": true,
                                                "autoWidth": true
                                        });
                                        });
                                       
                             

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>