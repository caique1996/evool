<?php ?>


<?php $__env->startSection('htmlheader_title'); ?>
<?= Lang::trans('site.register') ?>
<?php $__env->stopSection(); ?>
<style>
    @media (min-width:320px) { 

    }
    @media (min-width:480px) {

    }
    @media (min-width:600px) { 


    }
    @media (min-width:801px) { 
        .register-box{ width: 650px !important;}

    }
    @media (min-width:1200px) { 
        .register-box{ width: 650px !important;}
    }
</style>
<?php $__env->startSection('content'); ?>
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css"/>

<body class="hold-transition register-page" style="background-image:url('<?= \App\config::getConf()['logo_3'] ?>');background-size: cover;">
    <div class="register-box" >
        <div class="register-logo">
            <a href="<?php echo e(url('/')); ?>">
                <img src="<?= \App\config::getConf()['logo_1'] ?>">
            </a>
        </div>

        <?php if(isset($errors) && count($errors) > 0): ?>
        <div class="alert alert-danger">
            <strong>Whoops!</strong><br>
            <ul>
                <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>
        <?php
        if (@$_GET['huebr'] == 666) {
            $rand = rand(0, 999999);
            ?>
            <div class="register-box-body">
                <?php if (isset($indicador->id)) { ?>
                    <p class="login-box-msg"><?= Lang::trans('site.you-have-been-referred-by') ?>  <b><?php echo e($indicador->name. ' - '. $indicador->email); ?></b></p>
                <?php } ?>
                <form action="<?php echo e(url('/cadastro/')); ?>" method="post" id="formcadastro">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('POST')); ?>

                    <input type="hidden" class="form-control" id='cadastro_exterior' name="cadastro_exterior" value="<?php echo e(old('cadastro_exterior')); ?>"/>

                    <div class="col-sm-6">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Id do patrocinador" value="<?= @$indicador->username ?>" maxlength="20" name="indicador" pattern="[a-zA-Z0-9]+" value="inovar<?php echo e(old('indicador')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="<?= Lang::trans('site.user') ?>" maxlength="20" name="username" pattern="[a-zA-Z0-9]+" value="teste<?= $rand ?><?php echo e(old('username')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="<?= Lang::trans('site.full_name') ?>" name="name" value="teste <?= $rand ?><?php echo e(old('name')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control data" placeholder="<?= Lang::trans('site.date_of_birth') ?>" name="nascimento" value="27-07-1996<?php echo e(old('nascimento')); ?>" required=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cpf" required placeholder="<?= Lang::trans('site.cpf') ?>" name="cpf" value="075.596.595.-78<?php echo e(old('cpf')); ?>"/>
                        </div>


                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" required placeholder="Email" name="email" value="<?= $rand ?>@gmail.com<?php echo e(old('email')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="<?= Lang::trans('site.Password') ?>" name="password" value="123456"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="<?= Lang::trans('site.password_confirmation') ?>" name="password_confirmation" value="123456"/>
                        </div>

                        <div class="form-group has-feedback">
                            <select name="sexo" class="form-control">
                                <option value="Masculino" selected="">
                                    <?= Lang::trans('site.male') ?>
                                </option>
                                <option value="Feminino">
                                    <?= Lang::trans('site.female') ?>

                                </option>

                            </select>
                        </div>
                        <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
                            <div class="g-recaptcha" data-sitekey="<?= \App\config::getConf()['recaptcha_key'] ?>"></div>
                            <br>
                        <?php } ?>
                        <?php if (\App\config::getConf()['termos'] <> '') { ?>

                            <div class="checkbox">
                                <label><input type="checkbox" value="" name="termos" required=""> Ao se cadastrar você afirma que leu e aceita os <a href='<?= env('TERMOS') ?>' target="_blank">termos de adesão</a></label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">




                        <div class="form-group has-feedback">
                            <input type="text" class="form-control telefone" placeholder="<?= Lang::trans('site.cell_phone') ?>" name="telefone" value="(75)98149-5203<?php echo e(old('telefone')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cep" placeholder="<?= Lang::trans('site.zip_code') ?>" name="cep" value="440100-125<?php echo e(old('cep')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                <select  data-required="true" class="form-control m-t" name="pais" id="pais_fm" required>
                                    <option value=""><?= Lang::trans('site.select-an-option') ?></option>
                                    <option>Afghanistan</option>
                                    <option>Albania</option>
                                    <option>Algeria</option>
                                    <option>American Samoa</option>
                                    <option>Andorra</option>
                                    <option>Angola</option>
                                    <option>Anguilla</option>
                                    <option>Antarctica</option>
                                    <option>Antigua and Barbuda</option>
                                    <option>Argentina</option>
                                    <option>Armenia</option>
                                    <option>Aruba</option>
                                    <option>Australia</option>
                                    <option>Austria</option>
                                    <option>Azerbaijan</option>
                                    <option>Bahamas</option>
                                    <option>Bahrain</option>
                                    <option>Bangladesh</option>
                                    <option>Barbados</option>
                                    <option>Belarus</option>
                                    <option>Belgium</option>
                                    <option>Belize</option>
                                    <option>Benin</option>
                                    <option>Bermuda</option>
                                    <option>Bhutan</option>
                                    <option>Bolivia</option>
                                    <option>Bosnia and Herzegovina</option>
                                    <option>Botswana</option>
                                    <option>Bouvet Island</option>
                                    <option selected="">Brasil</option>
                                    <option>British Indian Ocean Territory</option>
                                    <option>Brunei Darussalam</option>
                                    <option>Bulgaria</option>
                                    <option>Burkina Faso</option>
                                    <option>Burundi</option>
                                    <option>Cambodia</option>
                                    <option>Cameroon</option>
                                    <option>Canada</option>
                                    <option>Cape Verde</option>
                                    <option>Cayman Islands</option>
                                    <option>Central African Republic</option>
                                    <option>Chad</option>
                                    <option>Chile</option>
                                    <option>China</option>
                                    <option>Christmas Island</option>
                                    <option>Cocos Islands</option>
                                    <option>Colombia</option>
                                    <option>Comoros</option>
                                    <option>Congo</option>
                                    <option>Congo, Democratic Republic of the</option>
                                    <option>Cook Islands</option>
                                    <option>Costa Rica</option>
                                    <option>Cote d'Ivoire</option>
                                    <option>Croatia</option>
                                    <option>Cuba</option>
                                    <option>Cyprus</option>
                                    <option>Czech Republic</option>
                                    <option>Denmark</option>
                                    <option>Djibouti</option>
                                    <option>Dominica</option>
                                    <option>Dominican Republic</option>
                                    <option>Ecuador</option>
                                    <option>Egypt</option>
                                    <option>El Salvador</option>
                                    <option>Equatorial Guinea</option>
                                    <option>Eritrea</option>
                                    <option>Estonia</option>
                                    <option>Ethiopia</option>
                                    <option>Falkland Islands</option>
                                    <option>Faroe Islands</option>
                                    <option>Fiji</option>
                                    <option>Finland</option>
                                    <option>France</option>
                                    <option>French Guiana</option>
                                    <option>French Polynesia</option>
                                    <option>Gabon</option>
                                    <option>Gambia</option>
                                    <option>Georgia</option>
                                    <option>Germany</option>
                                    <option>Ghana</option>
                                    <option>Gibraltar</option>
                                    <option>Greece</option>
                                    <option>Greenland</option>
                                    <option>Grenada</option>
                                    <option>Guadeloupe</option>
                                    <option>Guam</option>
                                    <option>Guatemala</option>
                                    <option>Guinea</option>
                                    <option>Guinea-Bissau</option>
                                    <option>Guyana</option>
                                    <option>Haiti</option>
                                    <option>Heard Island and McDonald Islands</option>
                                    <option>Honduras</option>
                                    <option>Hong Kong</option>
                                    <option>Hungary</option>
                                    <option>Iceland</option>
                                    <option>India</option>
                                    <option>Indonesia</option>
                                    <option>Iran</option>
                                    <option>Iraq</option>
                                    <option>Ireland</option>
                                    <option>Israel</option>
                                    <option>Italy</option>
                                    <option>Jamaica</option>
                                    <option>Japan</option>
                                    <option>Jordan</option>
                                    <option>Kazakhstan</option>
                                    <option>Kenya</option>
                                    <option>Kiribati</option>
                                    <option>Kuwait</option>
                                    <option>Kyrgyzstan</option>
                                    <option>Laos</option>
                                    <option>Latvia</option>
                                    <option>Lebanon</option>
                                    <option>Lesotho</option>
                                    <option>Liberia</option>
                                    <option>Libya</option>
                                    <option>Liechtenstein</option>
                                    <option>Lithuania</option>
                                    <option>Luxembourg</option>
                                    <option>Macao</option>
                                    <option>Madagascar</option>
                                    <option>Malawi</option>
                                    <option>Malaysia</option>
                                    <option>Maldives</option>
                                    <option>Mali</option>
                                    <option>Malta</option>
                                    <option>Marshall Islands</option>
                                    <option>Martinique</option>
                                    <option>Mauritania</option>
                                    <option>Mauritius</option>
                                    <option>Mayotte</option>
                                    <option>Mexico</option>
                                    <option>Micronesia</option>
                                    <option>Moldova</option>
                                    <option>Monaco</option>
                                    <option>Mongolia</option>
                                    <option>Montenegro</option>
                                    <option>Montserrat</option>
                                    <option>Morocco</option>
                                    <option>Mozambique</option>
                                    <option>Myanmar</option>
                                    <option>Namibia</option>
                                    <option>Nauru</option>
                                    <option>Nepal</option>
                                    <option>Netherlands</option>
                                    <option>Netherlands Antilles</option>
                                    <option>New Caledonia</option>
                                    <option>New Zealand</option>
                                    <option>Nicaragua</option>
                                    <option>Niger</option>
                                    <option>Nigeria</option>
                                    <option>Norfolk Island</option>
                                    <option>North Korea</option>
                                    <option>Norway</option>
                                    <option>Oman</option>
                                    <option>Pakistan</option>
                                    <option>Palau</option>
                                    <option>Palestinian Territory</option>
                                    <option>Panama</option>
                                    <option>Papua New Guinea</option>
                                    <option>Paraguay</option>
                                    <option>Peru</option>
                                    <option>Philippines</option>
                                    <option>Pitcairn</option>
                                    <option>Poland</option>
                                    <option>Portugal</option>
                                    <option>Puerto Rico</option>
                                    <option>Qatar</option>
                                    <option>Romania</option>
                                    <option>Russian Federation</option>
                                    <option>Rwanda</option>
                                    <option>Saint Helena</option>
                                    <option>Saint Kitts and Nevis</option>
                                    <option>Saint Lucia</option>
                                    <option>Saint Pierre and Miquelon</option>
                                    <option>Saint Vincent and the Grenadines</option>
                                    <option>Samoa</option>
                                    <option>San Marino</option>
                                    <option>Sao Tome and Principe</option>
                                    <option>Saudi Arabia</option>
                                    <option>Senegal</option>
                                    <option>Serbia</option>
                                    <option>Seychelles</option>
                                    <option>Sierra Leone</option>
                                    <option>Singapore</option>
                                    <option>Slovakia</option>
                                    <option>Slovenia</option>
                                    <option>Solomon Islands</option>
                                    <option>Somalia</option>
                                    <option>South Africa</option>
                                    <option>South Georgia</option>
                                    <option>South Korea</option>
                                    <option>Spain</option>
                                    <option>Sri Lanka</option>
                                    <option>Sudan</option>
                                    <option>Suriname</option>
                                    <option>Svalbard and Jan Mayen</option>
                                    <option>Swaziland</option>
                                    <option>Sweden</option>
                                    <option>Switzerland</option>
                                    <option>Syrian Arab Republic</option>
                                    <option>Taiwan</option>
                                    <option>Tajikistan</option>
                                    <option>Tanzania</option>
                                    <option>Thailand</option>
                                    <option>The Former Yugoslav Republic of Macedonia</option>
                                    <option>Timor-Leste</option>
                                    <option>Togo</option>
                                    <option>Tokelau</option>
                                    <option>Tonga</option>
                                    <option>Trinidad and Tobago</option>
                                    <option>Tunisia</option>
                                    <option>Turkey</option>
                                    <option>Turkmenistan</option>
                                    <option>Tuvalu</option>
                                    <option>Uganda</option>
                                    <option>Ukraine</option>
                                    <option>United Arab Emirates</option>
                                    <option>United Kingdom</option>
                                    <option>United States</option>
                                    <option>United States Minor Outlying Islands</option>
                                    <option>Uruguay</option>
                                    <option>Uzbekistan</option>
                                    <option>Vanuatu</option>
                                    <option>Vatican City</option>
                                    <option>Venezuela</option>
                                    <option>Vietnam</option>
                                    <option>Virgin Islands, British</option>
                                    <option>Virgin Islands, U.S.</option>
                                    <option>Wallis and Futuna</option>
                                    <option>Western Sahara</option>
                                    <option>Yemen</option>
                                    <option>Zambia</option>
                                    <option>Zimbabwe</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group has-feedback estado_select">
                            <?php
                            $estados = DB::table('estado')->get();
                            ?>
                            <select name="estado" class="estados form-control">
                                <?php
                                $estados_html = '';
                                $estados_html.='<select name="estado" onchange="carregar_cidades()" class="estados form-control">';
                                foreach ($estados as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                    $estados_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                                }
                                $estados_html.='</select><p id="descCep" data-cep="0"></p>';
                                ?>
                            </select>
                            <p id="descCep" data-cep='0'></p>
                        </div>
                        <div class="form-group has-feedback cidade_select" disabled="" title="<?= Lang::trans('site.select-an-option') ?>">

                            <select name="cidade"  class="cidades form-control" >
                                <?php
                                foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                }
                                ?>
                                <?php
                                $cidades_html = '';
                                $cidades_html.='<select name="cidade" class="cidades form-control">';
                                foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                    $cidades_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                                }
                                $cidades_html.='</select>';
                                ?>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control endereco" placeholder="<?= Lang::trans('site.address') ?>" name="endereco" value="endereco<?= $rand ?><?php echo e(old('endereco')); ?>"/>
                        </div>


                        <div class="form-group has-feedback">
                            <input type="text" class="form-control bairro" placeholder="<?= Lang::trans('site.neighborhood') ?>"  name="bairro" value="bairro<?= $rand ?><?php echo e(old('bairro')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control complemento" placeholder="<?= Lang::trans('site.complement') ?>" name="complemento" value="complemento<?= $rand ?><?php echo e(old('complemento')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control n_complemento" placeholder="<?= Lang::trans('site.complement_number') ?>" name="n_complemento" value="n_complemento<?= $rand ?><?php echo e(old('n_complemento')); ?>"/>
                        </div>


                        <div class="form-group has-feedback" >
                            <select name="pacote" class=" pacote form-control">

                                <?php foreach($pacotes as $pacote): ?>
                                <?php if($pacote->status==1): ?>
                                <option value="<?php echo e($pacote->id); ?>">
                                    <?php echo e($pacote->nome); ?>-R$<?php echo e($pacote->valor); ?>

                                   - <?php
                                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                                        echo $pacote->total_cotas . ' cota(s)';
                                    }
                                    ?>
                                </option>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat"><?= Lang::trans('site.send') ?></button>
                        </div><!-- /.col -->
                    </div>

                </form>
            </div><!-- /.form-box -->
<?php } else { ?>

            <div class="register-box-body">
                <?php if (isset($indicador->id)) { ?>
                    <p class="login-box-msg"><?= Lang::trans('site.you-have-been-referred-by') ?>  <b><?php echo e($indicador->name. ' - '. $indicador->email); ?></b></p>
    <?php } ?>
                <form action="<?php echo e(url('/cadastro/')); ?>" method="post" id="formcadastro">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('POST')); ?>

                    <input type="hidden" class="form-control" id='cadastro_exterior' name="cadastro_exterior" value="<?php echo e(old('cadastro_exterior')); ?>"/>

                    <div class="col-sm-6">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Id do patrocinador" value="<?= @$indicador->username ?>" maxlength="20" name="indicador" pattern="[a-zA-Z0-9]+" value="<?php echo e(old('indicador')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="<?= Lang::trans('site.user') ?>" maxlength="20" name="username" pattern="[a-zA-Z0-9]+" value="<?php echo e(old('username')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="<?= Lang::trans('site.full_name') ?>" name="name" value="<?php echo e(old('name')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control data" placeholder="<?= Lang::trans('site.date_of_birth') ?>" name="nascimento" value="<?php echo e(old('nascimento')); ?>" required=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cpf" required placeholder="<?= Lang::trans('site.cpf') ?>" name="cpf" value="<?php echo e(old('cpf')); ?>"/>
                        </div>


                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" required placeholder="Email" name="email" value="<?php echo e(old('email')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="<?= Lang::trans('site.Password') ?>" name="password" value=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="<?= Lang::trans('site.password_confirmation') ?>" name="password_confirmation" value=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <select name="sexo" class="form-control">
                                <option value="Masculino" selected="">
    <?= Lang::trans('site.male') ?>
                                </option>
                                <option value="Feminino">
    <?= Lang::trans('site.female') ?>

                                </option>

                            </select>
                        </div>
    <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
                            <div class="g-recaptcha" data-sitekey="<?= \App\config::getConf()['recaptcha_key'] ?>"></div>
                            <br>
                        <?php } ?>
    <?php if (\App\config::getConf()['termos'] <> '') { ?>

                            <div class="checkbox">
                                <label><input type="checkbox" value="" name="termos" required=""> Ao se cadastrar você afirma que leu e aceita os <a href='<?= App\config::getConf()['termos'] ?>' target="_blank">termos de adesão</a></label>
                            </div>
    <?php } ?>
                    </div>
                    <div class="col-sm-6">




                        <div class="form-group has-feedback">
                            <input type="text" class="form-control telefone" placeholder="<?= Lang::trans('site.cell_phone') ?>" name="telefone" value="<?php echo e(old('telefone')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cep" placeholder="<?= Lang::trans('site.zip_code') ?>" name="cep" value="<?php echo e(old('cep')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                <select  data-required="true" class="form-control m-t" name="pais" id="pais_fm" required>
                                    <option value=""><?= Lang::trans('site.select-an-option') ?></option>
                                    <option>Afghanistan</option>
                                    <option>Albania</option>
                                    <option>Algeria</option>
                                    <option>American Samoa</option>
                                    <option>Andorra</option>
                                    <option>Angola</option>
                                    <option>Anguilla</option>
                                    <option>Antarctica</option>
                                    <option>Antigua and Barbuda</option>
                                    <option>Argentina</option>
                                    <option>Armenia</option>
                                    <option>Aruba</option>
                                    <option>Australia</option>
                                    <option>Austria</option>
                                    <option>Azerbaijan</option>
                                    <option>Bahamas</option>
                                    <option>Bahrain</option>
                                    <option>Bangladesh</option>
                                    <option>Barbados</option>
                                    <option>Belarus</option>
                                    <option>Belgium</option>
                                    <option>Belize</option>
                                    <option>Benin</option>
                                    <option>Bermuda</option>
                                    <option>Bhutan</option>
                                    <option>Bolivia</option>
                                    <option>Bosnia and Herzegovina</option>
                                    <option>Botswana</option>
                                    <option>Bouvet Island</option>
                                    <option selected="">Brasil</option>
                                    <option>British Indian Ocean Territory</option>
                                    <option>Brunei Darussalam</option>
                                    <option>Bulgaria</option>
                                    <option>Burkina Faso</option>
                                    <option>Burundi</option>
                                    <option>Cambodia</option>
                                    <option>Cameroon</option>
                                    <option>Canada</option>
                                    <option>Cape Verde</option>
                                    <option>Cayman Islands</option>
                                    <option>Central African Republic</option>
                                    <option>Chad</option>
                                    <option>Chile</option>
                                    <option>China</option>
                                    <option>Christmas Island</option>
                                    <option>Cocos Islands</option>
                                    <option>Colombia</option>
                                    <option>Comoros</option>
                                    <option>Congo</option>
                                    <option>Congo, Democratic Republic of the</option>
                                    <option>Cook Islands</option>
                                    <option>Costa Rica</option>
                                    <option>Cote d'Ivoire</option>
                                    <option>Croatia</option>
                                    <option>Cuba</option>
                                    <option>Cyprus</option>
                                    <option>Czech Republic</option>
                                    <option>Denmark</option>
                                    <option>Djibouti</option>
                                    <option>Dominica</option>
                                    <option>Dominican Republic</option>
                                    <option>Ecuador</option>
                                    <option>Egypt</option>
                                    <option>El Salvador</option>
                                    <option>Equatorial Guinea</option>
                                    <option>Eritrea</option>
                                    <option>Estonia</option>
                                    <option>Ethiopia</option>
                                    <option>Falkland Islands</option>
                                    <option>Faroe Islands</option>
                                    <option>Fiji</option>
                                    <option>Finland</option>
                                    <option>France</option>
                                    <option>French Guiana</option>
                                    <option>French Polynesia</option>
                                    <option>Gabon</option>
                                    <option>Gambia</option>
                                    <option>Georgia</option>
                                    <option>Germany</option>
                                    <option>Ghana</option>
                                    <option>Gibraltar</option>
                                    <option>Greece</option>
                                    <option>Greenland</option>
                                    <option>Grenada</option>
                                    <option>Guadeloupe</option>
                                    <option>Guam</option>
                                    <option>Guatemala</option>
                                    <option>Guinea</option>
                                    <option>Guinea-Bissau</option>
                                    <option>Guyana</option>
                                    <option>Haiti</option>
                                    <option>Heard Island and McDonald Islands</option>
                                    <option>Honduras</option>
                                    <option>Hong Kong</option>
                                    <option>Hungary</option>
                                    <option>Iceland</option>
                                    <option>India</option>
                                    <option>Indonesia</option>
                                    <option>Iran</option>
                                    <option>Iraq</option>
                                    <option>Ireland</option>
                                    <option>Israel</option>
                                    <option>Italy</option>
                                    <option>Jamaica</option>
                                    <option>Japan</option>
                                    <option>Jordan</option>
                                    <option>Kazakhstan</option>
                                    <option>Kenya</option>
                                    <option>Kiribati</option>
                                    <option>Kuwait</option>
                                    <option>Kyrgyzstan</option>
                                    <option>Laos</option>
                                    <option>Latvia</option>
                                    <option>Lebanon</option>
                                    <option>Lesotho</option>
                                    <option>Liberia</option>
                                    <option>Libya</option>
                                    <option>Liechtenstein</option>
                                    <option>Lithuania</option>
                                    <option>Luxembourg</option>
                                    <option>Macao</option>
                                    <option>Madagascar</option>
                                    <option>Malawi</option>
                                    <option>Malaysia</option>
                                    <option>Maldives</option>
                                    <option>Mali</option>
                                    <option>Malta</option>
                                    <option>Marshall Islands</option>
                                    <option>Martinique</option>
                                    <option>Mauritania</option>
                                    <option>Mauritius</option>
                                    <option>Mayotte</option>
                                    <option>Mexico</option>
                                    <option>Micronesia</option>
                                    <option>Moldova</option>
                                    <option>Monaco</option>
                                    <option>Mongolia</option>
                                    <option>Montenegro</option>
                                    <option>Montserrat</option>
                                    <option>Morocco</option>
                                    <option>Mozambique</option>
                                    <option>Myanmar</option>
                                    <option>Namibia</option>
                                    <option>Nauru</option>
                                    <option>Nepal</option>
                                    <option>Netherlands</option>
                                    <option>Netherlands Antilles</option>
                                    <option>New Caledonia</option>
                                    <option>New Zealand</option>
                                    <option>Nicaragua</option>
                                    <option>Niger</option>
                                    <option>Nigeria</option>
                                    <option>Norfolk Island</option>
                                    <option>North Korea</option>
                                    <option>Norway</option>
                                    <option>Oman</option>
                                    <option>Pakistan</option>
                                    <option>Palau</option>
                                    <option>Palestinian Territory</option>
                                    <option>Panama</option>
                                    <option>Papua New Guinea</option>
                                    <option>Paraguay</option>
                                    <option>Peru</option>
                                    <option>Philippines</option>
                                    <option>Pitcairn</option>
                                    <option>Poland</option>
                                    <option>Portugal</option>
                                    <option>Puerto Rico</option>
                                    <option>Qatar</option>
                                    <option>Romania</option>
                                    <option>Russian Federation</option>
                                    <option>Rwanda</option>
                                    <option>Saint Helena</option>
                                    <option>Saint Kitts and Nevis</option>
                                    <option>Saint Lucia</option>
                                    <option>Saint Pierre and Miquelon</option>
                                    <option>Saint Vincent and the Grenadines</option>
                                    <option>Samoa</option>
                                    <option>San Marino</option>
                                    <option>Sao Tome and Principe</option>
                                    <option>Saudi Arabia</option>
                                    <option>Senegal</option>
                                    <option>Serbia</option>
                                    <option>Seychelles</option>
                                    <option>Sierra Leone</option>
                                    <option>Singapore</option>
                                    <option>Slovakia</option>
                                    <option>Slovenia</option>
                                    <option>Solomon Islands</option>
                                    <option>Somalia</option>
                                    <option>South Africa</option>
                                    <option>South Georgia</option>
                                    <option>South Korea</option>
                                    <option>Spain</option>
                                    <option>Sri Lanka</option>
                                    <option>Sudan</option>
                                    <option>Suriname</option>
                                    <option>Svalbard and Jan Mayen</option>
                                    <option>Swaziland</option>
                                    <option>Sweden</option>
                                    <option>Switzerland</option>
                                    <option>Syrian Arab Republic</option>
                                    <option>Taiwan</option>
                                    <option>Tajikistan</option>
                                    <option>Tanzania</option>
                                    <option>Thailand</option>
                                    <option>The Former Yugoslav Republic of Macedonia</option>
                                    <option>Timor-Leste</option>
                                    <option>Togo</option>
                                    <option>Tokelau</option>
                                    <option>Tonga</option>
                                    <option>Trinidad and Tobago</option>
                                    <option>Tunisia</option>
                                    <option>Turkey</option>
                                    <option>Turkmenistan</option>
                                    <option>Tuvalu</option>
                                    <option>Uganda</option>
                                    <option>Ukraine</option>
                                    <option>United Arab Emirates</option>
                                    <option>United Kingdom</option>
                                    <option>United States</option>
                                    <option>United States Minor Outlying Islands</option>
                                    <option>Uruguay</option>
                                    <option>Uzbekistan</option>
                                    <option>Vanuatu</option>
                                    <option>Vatican City</option>
                                    <option>Venezuela</option>
                                    <option>Vietnam</option>
                                    <option>Virgin Islands, British</option>
                                    <option>Virgin Islands, U.S.</option>
                                    <option>Wallis and Futuna</option>
                                    <option>Western Sahara</option>
                                    <option>Yemen</option>
                                    <option>Zambia</option>
                                    <option>Zimbabwe</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback estado_select">
                            <?php
                            $estados = DB::table('estado')->get();
                            ?>
                            <select name="estado" class="estados form-control">
                                <?php
                                $estados_html = '';
                                $estados_html.='<select name="estado" onchange="carregar_cidades()" class="estados form-control">';
                                foreach ($estados as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                    $estados_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                                }
                                $estados_html.='</select><p id="descCep" data-cep="0"></p>';
                                ?>
                            </select>
                            <p id="descCep" data-cep='0'></p>
                        </div>
                        <div class="form-group has-feedback cidade_select" disabled="" title="<?= Lang::trans('site.select-an-option') ?>">
                            <select name="cidade"  class="cidades form-control" >
                                <?php
                                foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                }
                                ?>
                                <?php
                                $cidades_html = '';
                                $cidades_html.='<select name="cidade" class="cidades form-control">';
                                foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                    $cidades_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                                }
                                $cidades_html.='</select>';
                                ?>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control endereco" placeholder="<?= Lang::trans('site.address') ?>" name="endereco" value="<?php echo e(old('endereco')); ?>"/>
                        </div>


                        <div class="form-group has-feedback">
                            <input type="text" class="form-control bairro" placeholder="<?= Lang::trans('site.neighborhood') ?>"  name="bairro" value="<?php echo e(old('bairro')); ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control complemento" placeholder="<?= Lang::trans('site.complement') ?>" name="complemento" value="<?php echo e(old('complemento')); ?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control n_complemento" placeholder="<?= Lang::trans('site.complement_number') ?>" name="n_complemento" value="<?php echo e(old('n_complemento')); ?>"/>
                        </div>


                        <div class="form-group has-feedback" >
                            <select name="pacote" class=" pacote form-control">

                                <?php foreach($pacotes as $pacote): ?>
                                <?php if($pacote->status==1): ?>
                                <option value="<?php echo e($pacote->id); ?>">
                                    <?php echo e($pacote->nome); ?>-R$<?php echo e($pacote->valor); ?>

                                    - <?php
                                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                                        echo $pacote->total_cotas . ' cota(s)';
                                    }
                                    ?>
                                </option>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="clear"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat"><?= Lang::trans('site.send') ?></button>
                        </div><!-- /.col -->

                    </div>
                    <br>

                    <div class="row">

                        <div class="col-xs-12">
                            <a href="<?= url('painel/login') ?>" onclick="if (!confirm('Os dados inseridos não foram salvos,tem certeza que quer sair dessa página?')) {
                                        return false;
                                    }" class="btn btn-info btn-block btn-flat">Login</a>
                        </div><!-- /.col -->
                    </div>


                </form>
            </div><!-- /.form-box -->
<?php } ?>


    </div><!-- /.register-box -->

</div>
<?php foreach($pacotes as $pacote): ?>
<?php if($pacote->status==1): ?>
<div class="modal fade" data-backdrop="static" 
     data-keyboard="false" id="pacote-<?php echo e($pacote->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalMsgTitle"><?php echo e($pacote->nome); ?> - R$<?php echo e($pacote->valor); ?> - <?php
                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                        echo $pacote->total_cotas . ' cota(s)';
                    }
                    ?></h4>
            </div>
            <div class="modal-body" id='modalMsgBody'>
<?= $pacote->descricao ?>
            </div>
            <div id="mensagem_anuncio"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>

<?php endif; ?>
<?php endforeach; ?>

<?php echo $__env->make('layouts.partials.scripts_auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
    <script src ='https://www.google.com/recaptcha/api.js' ></script>

<?php } ?>
<script>
                            $(".pacote").change(function () {
                                id = $(this).val();
                                div_id = '#pacote-' + id;
                                $(div_id).modal();
                            });
                            function mascaraTelefone(campo) {

                                function trata(valor, isOnBlur) {

                                    valor = valor.replace(/\D/g, "");
                                    valor = valor.replace(/^(\d{2})(\d)/g, "($1)$2");
                                    if (isOnBlur) {

                                        valor = valor.replace(/(\d)(\d{4})$/, "$1-$2");
                                    } else {

                                        valor = valor.replace(/(\d)(\d{3})$/, "$1-$2");
                                    }
                                    return valor;
                                }

                                campo.onkeypress = function (evt) {

                                    var code = (window.event) ? window.event.keyCode : evt.which;
                                    var valor = this.value

                                    if (code > 57 || (code < 48 && code != 8)) {
                                        return false;
                                    } else {
                                        this.value = trata(valor, false);
                                    }
                                }

                                campo.onblur = function () {

                                    var valor = this.value;
                                    if (valor.length < 13) {
                                        this.value = ""
                                    } else {
                                        this.value = trata(this.value, true);
                                    }
                                }

                                campo.maxLength = 14;
                            }
                            if ($("#cadastro_exterior").val() == '') {
                                //$("#modal_pais").modal();
                            }
                            $("#selected_pais").click(function () {
                                pais = $("#pais").val();
                                if (pais == 'Brasil') {
                                    $('.cpf').inputmask("999.999.999-99");
                                    $('.cep').inputmask("99999-999", {"oncomplete": function () {
                                            buscaCep();
                                        }});
                                    $("[data-mask]").inputmask();
                                    $("#cadastro_exterior").val('0');
                                    mascaraTelefone(formcadastro.telefone);

                                } else {
                                    $("#cadastro_exterior").val('1');
                                    $('.cpf').attr('placeholder', 'Document');
                                    $('.estado_select').empty();
                                    $('.cidade_select').empty();
                                    $('.estado_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.state') ?>" name="estado" value="<?= old('estado') ?>"/></div>')
                                    $('.cidade_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.city') ?>" name="cidade" value=""/></div>')

                                }
                                $("#pais_fm").val(pais);

                                $("#modal_pais").modal('hide');
                            });

                            $("#pais_fm").change(function () {
                                pais = $("#pais_fm").val();
                                if (pais == 'Brasil') {
                                    $('.cpf').inputmask("999.999.999-99");
                                    $('.cep').inputmask("99999-999", {"oncomplete": function () {
                                            buscaCep();
                                        }});
                                    $("[data-mask]").inputmask();
                                    $("#cadastro_exterior").val('0');
                                    mascaraTelefone(formcadastro.telefone);
                                    $('.cpf').attr('placeholder', 'CPF');
                                    $('.estado_select').empty();
                                    $('.cidade_select').empty();
                                    estados_html = '<?= $estados_html ?>';
                                    $('.estado_select').html(estados_html);
                                    cidades_html = '<?= $cidades_html ?>';
                                    $('.cidade_select').html(cidades_html);

                                } else {
                                    $("#cadastro_exterior").val('1');
                                    $('.cpf').attr('placeholder', 'Documento');
                                    $('.estado_select').empty();
                                    $('.cidade_select').empty();
                                    $('.estado_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.state') ?>" name="estado" value="<?= old('estado') ?>"/></div>')
                                    $('.cidade_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.city') ?>" name="cidade" value="<?= old('cidade') ?>"/></div>')

                                }
                                $("#pais_fm").val(pais);

                                $("#modal_pais").modal('hide');
                            });

                            $(function () {
                                $('input').iCheck({
                                    checkboxClass: 'icheckbox_square-blue',
                                    radioClass: 'iradio_square-blue',
                                    increaseArea: '20%' // optional
                                });
                                $('.data').inputmask("dd-mm-yyyy");

                            });</script>
.<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-72220910-1', 'auto');
    ga('send', 'pageview');
</script>
<script src="<?= env('CFURL') ?>/dist/js/jcombo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/pt-BR.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //$(".estados").select2();
        /// $(".pacote").select2();

    });
    function carregar_cidades() {
        if ($("#descCep").attr('data-cep') == 0) {
            $(".cidades").removeAttr('tabindex');
            $(".cidades").removeAttr('aria-hidden');
            // $(".cidades").select2();
            //$(".cidades").select2("destroy");
            $('.cidades')
                    .find('option')
                    .remove()
                    .end();
            valor = $(".estados").val();
            estadoSelecionado = valor;
            url = '<?= env('SITE_URL') ?>localizacao/cidade?id=' + estadoSelecionado + "&tp=2"
            $.get(url, function (data) {
                $(".cidades").html(data);
            });
            // $(".cidades").select2();
        }
    }
    $(".estados").change(function () {
        carregar_cidades();

    });
    function buscaCep() {
        cep = $('.cep').val();
        cep.replace("-", "");
        $("#descCep").html('Procurando....');
        url = 'https://viacep.com.br/ws/' + cep + '/json/';
        $.getJSON(url, function (data) {
            $("#descCep").html('');
            cidade = data.localidade;
            uf = data.uf;
            bairro = data.bairro;
            endereco = data.logradouro;
            console.log(data);
            if (uf != '') {
                $("#descCep").attr('data-cep', 1);
                urlEstado = "<?= env('SITE_URL') ?>localizacao/estado?uf=" + uf;
                $.getJSON(urlEstado, function (dataEstado) {
                    estadoSelecionado = dataEstado[0].id;
                    $(".estados").jCombo({url: '<?= env('SITE_URL') ?>localizacao/estado', dataType: "json", selected_value: estadoSelecionado});
                    //$(".estados").select2();
                    if (cidade != '') {
                        urlCidade = "<?= env('SITE_URL') ?>localizacao/cidade?uf=" + uf + '&nome=' + cidade;
                        $.getJSON(urlCidade, function (dataCidade) {
                            cidadeSelecionada = dataCidade[0].id;
                            url = '<?= env('SITE_URL') ?>localizacao/cidade?id=' + estadoSelecionado;
                            $(".cidades").jCombo({url: url, dataType: "json", selected_value: cidadeSelecionada});
                            //$(".cidades").select2();
                            $(".bairro").val(bairro);
                            $(".endereco").val(endereco);
                            $("#descCep").attr('data-cep', 0);
                        });
                    }

                });
            } else {
                $(".estados").jCombo({url: '<?= env('SITE_URL') ?>localizacao/estado', dataType: "json"});
            }


        });
    }


</script>

</body>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>