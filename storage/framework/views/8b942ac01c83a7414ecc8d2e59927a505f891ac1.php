<?php $__env->startSection('htmlheader_title'); ?>
Meus pedidos
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Meus pedidos > Lista de pedidos
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box">
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="ord-carrinho" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th> </th>
                            <th>ID</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Status</th>
                            <th>Mensagem</th>
                            <th>Detalhes</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($pedidos as $pedido): ?>
                        <tr>
                            <td style="
                                background-image: url(<?php echo url(''.$pedido['img']); ?>);
                                background-position: center;
                                background-size: cover;">
                            </td>
                            <td><?php echo e($pedido['id']); ?></td>
                            <td><?php echo e(Carbon\Carbon::parse($pedido['date'])->format('d/m/Y')); ?></td>
                            <td><?php echo e(number_format($pedido['preco'],2)); ?></td>
                            <td><?php echo e($pedido['status']); ?></td>
                            <td><?=$pedido['info']?></td>

                            <td><a href="<?php echo url('painel/meus-pedidos/pedido/'.$pedido['id']); ?>" class="btn btn-info">Detalhes</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>
$(function () {

    $('#ord-carrinho').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": false,
        "autoWidth": true,
        "oLanguage": {"sZeroRecords": "Você ainda não realizou nenhum pedido",
            "sEmptyTable": "Você ainda não realizou nenhum pedido"},
        "order": [[1, "desc"]]
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>