<?php $__env->startSection('htmlheader_title'); ?>
Gerenciar Usuários
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(asset('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Gerenciar Usuários
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->
<?php

function isValidMd5($md5 = '') {
    return preg_match('/^[a-f0-9]{32}$/', $md5);
}
?>
<div class="row">

    <section class="col-lg-10">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Usuarios</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>
                <div id="mensagemAjax">

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <?php
                            $total = \App\User::all()->count();
                            $ativos = \App\User::where('ativo', 1)->count();
                            ?>
                            Total&nbsp;<button class="btn btn-warning" data-widget="collapse"><?= $total ?></button>&nbsp;&nbsp;
                            Ativo(s)&nbsp;<button class="btn btn-success" data-widget="collapse"><?= $ativos ?></button>&nbsp;&nbsp;
                            Inativo(s)&nbsp;<button class="btn btn-danger" data-widget="collapse"><?= ($total - $ativos) ?></button>&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
                <br>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Username</th>

                            <th>Saldo</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        function getStatus($id) {
                            switch ($id) {
                                case 1:
                                    $res = 'Ativo';
                                    break;
                                default :
                                    $res = 'Inativo';
                            }
                            return $res;
                        }

                        $pacoteAll = App\Pacote::where('status', 1)->get();
                        ?>

                        <?php foreach($usuarios as $usuario): ?>
                        <?php
                        $pacoteData = App\Pacote::where('id', $usuario->pacote)->first();
                        ?>


                        <tr>
                            <td><?php echo e($usuario->id); ?></td>
                            <td><?php echo e($usuario->name); ?></td>
                            <td><?php echo e($usuario->username); ?></td>
                            <td><?php echo e($usuario->saldo); ?></td>
                            <td><?php echo e($usuario->email); ?></td>
                            <td class="text-center"><?php echo e(getStatus($usuario->ativo)); ?></td>
                            <td>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <button data-href="<?php echo e(url('/admin/usuario/'.$usuario->id)); ?>" class="editarUsr btn btn-success">Editar Usuário</button>

                                        <?php if (getStatus($usuario->ativo) == 'Inativo') { ?>
                                            <button onclick="ativarUsr(<?php echo e($usuario->id); ?>, 1, this);" class="ativarusr btn btn-success">Ativar Usuário</button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php if ($usuario->saque == 'inativo') { ?>
                                            <button onclick="saqueUsr(<?php echo e($usuario->id); ?>, 1, this);" class="ativarusr btn btn-success">Ativar saque</button>

                                        <?php } else { ?>
                                            <button onclick="saqueUsr(<?php echo e($usuario->id); ?>, 2, this);" class="ativarusr btn btn-danger">Desativar  saque</button>
                                        <?php } ?> 
                                    </div>
                                    <div class="col-lg-6">
                                        <a href="<?php echo e(url('/admin/acessar/'.$usuario->id)); ?>" onclick="if (!confirm('Ao acessar um novo painel, a sessão atual será cancelada.Continuar com a ação?')){ return false; }" class="btn btn-success">Acessar painel</a>

                                    </div>
                                    <div class="col-lg-6">
                                        <select class="mudarPacote" data-usr='<?php echo e($usuario->id); ?>'>
                                            <option value="<?php echo e($usuario->pacote); ?>" data-usr='<?php echo e($usuario->id); ?>' selected=""><?php echo e($pacoteData['nome']); ?>-<?php echo e($pacoteData['valor']); ?>-</option>
                                            <?php
                                            foreach ($pacoteAll as $value) {
                                                echo "<option value = '{$value['id']}' >{$value['nome']}-{$value['valor']}</option>";
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>




                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Adicionar Vouchers</h4>

                </div>
                <div class="modal-body">
                    <div class="te"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary">Salvar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


</div>

<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">


    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(asset('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/form/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>


<script>

                                                    function fBlockUi() {
                                                    $.blockUI({
                                                    message: "<h4>Por favor aguarde...</h4>",
                                                            css: {
                                                            border: 'none',
                                                                    padding: '5px',
                                                                    backgroundColor: '#000',
                                                                    '-webkit-border-radius': '5px',
                                                                    '-moz-border-radius': '5px',
                                                                    opacity: .5,
                                                                    color: '#fff'
                                                            }
                                                    });
                                                    }

                                            $(function () {



                                            $(".adicionarVouchers").on('click', function () {

                                            $('#myModal').removeData('bs.modal');
                                                    $('#myModal').modal(
                                            {
                                            remote: $(this).attr('data-href')
                                            }
                                            );
                                                    $('#myModal').modal('show');
                                                    $('#myModal').on('loaded.bs.modal', function (e) {

                                            // bind form using ajaxForm
                                            var form = $('.formAdicionarVouchers').ajaxForm({
                                            // target identifies the element(s) to update with the server response
                                            target: '#mensagemAdicionarVouchers',
                                                    beforeSubmit: function () {
                                                    $('input').attr('disabled', true);
                                                    },
                                                    // success identifies the function to invoke when the server response
                                                    // has been received; here we apply a fade-in effect to the new content
                                                    success: function () {
                                                    $('.formAdicionarVouchers').clearForm();
                                                            $('#mensagemformAdicionarVouchers').fadeIn('slow');
                                                            $('#myModal').modal('hide');
                                                            $('input').attr('disabled', false);
                                                    }
                                            });
                                            });
                                            });
                                                    $('#example2').DataTable({
                                            "order": [ 0, 'desc' ],
                                                    "paging": true,
                                                    "lengthChange": true,
                                                    "searching": true,
                                                    "ordering": true,
                                                    "info": true,
                                                    "autoWidth": true,
                                                    "language": {
                                                    "sEmptyTable": "Nenhum registro encontrado",
                                                            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                                            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                                                            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                                            "sInfoPostFix": "",
                                                            "sInfoThousands": ".",
                                                            "sLengthMenu": "_MENU_ resultados por página",
                                                            "sLoadingRecords": "Carregando...",
                                                            "sProcessing": "Processando...",
                                                            "sZeroRecords": "Nenhum registro encontrado",
                                                            "sSearch": "Pesquisar",
                                                            "oPaginate": {
                                                            "sNext": "Próximo",
                                                                    "sPrevious": "Anterior",
                                                                    "sFirst": "Primeiro",
                                                                    "sLast": "Último"
                                                            },
                                                            "oAria": {
                                                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                                                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                                            }
                                                    }
                                            });
                                            });
                                                    function ativarUsr(id, status, elemento){
                                                    if (status == 1){
                                                    msg = "Deseja realmente ativar esse usuário?";
                                                    }
                                                    else if (status == 0){
                                                    msg = "Deseja realmente desativar esse usuário?";
                                                    } else{
                                                    alert("Operação Desconhecida");
                                                            return false;
                                                    }
                                                    if (confirm(msg)){
                                                    $(elemento).attr('disabled', true);
                                                            var dados = $(this).serialize();
                                                            var here = $(this); // alert div for show alert message
                                                            $.ajax({
                                                            'url': 'ativarUsr',
                                                                    'type': 'GET',
                                                                    data:'id=' + id, // serialize form data 
                                                                    dataType: "html",
                                                                    beforeSend: function () {
                                                                    here.html('Carregando...');
                                                                    },
                                                                    'success': function (dados) {
                                                                    alert(dados);
                                                                    }
                                                            });
                                                            return false;
                                                    } else{
                                                    return false;
                                                    }
                                                    }

                                            function saqueUsr(id, status, elemento){
                                            msg = "Você tem certeza?";
                                                    if (confirm(msg)){
                                            $(elemento).attr('disabled', true);
                                                    var dados = $(this).serialize();
                                                    var here = $(this); // alert div for show alert message
                                                    $.ajax({
                                                    'url': 'statusSaque',
                                                            'type': 'GET',
                                                            data:'id=' + id, // serialize form data 
                                                            dataType: "html",
                                                            beforeSend: function () {
                                                            here.html('Carregando...');
                                                            },
                                                            'success': function (dados) {
                                                            alert(dados);
                                                            }
                                                    });
                                                    return false;
                                            } else{
                                            return false;
                                            }
                                            }


                                            $(".mudarPacote").change(function(){
                                            val = $(this).val();
                                                    id = $(this).attr('data-usr');
                                                    if (confirm("Tem certeza?")){
                                            $.ajax({
                                            'url': 'mudarPacote',
                                                    'type': 'GET',
                                                    data:'id=' + id + "&pacote=" + val, // serialize form data 
                                                    dataType: "html",
                                                    beforeSend: function () {
                                                    alert('Carregando...');
                                                    },
                                                    'success': function (dados) {
                                                    alert(dados);
                                                    }
                                            });
                                            }

                                            });
                                                    $(".editarUsr").on('click', function () {

                                            $('#myModal').removeData('bs.modal');
                                                    $('#myModal').modal(
                                            {
                                            remote: $(this).attr('data-href')
                                            }
                                            );
                                                    $('#myModal').modal('show');
                                                    $('#myModal').on('loaded.bs.modal', function (e) {

                                            // bind form using ajaxForm
                                            var form = $('.formAjax').ajaxForm({
                                            // target identifies the element(s) to update with the server response
                                            target: '#mensagemAjax',
                                                    beforeSubmit: function () {
                                                    fBlockUi();
                                                            $('#myModal').modal('hide');
                                                            $('input').attr('disabled', true);
                                                    },
                                                    // success identifies the function to invoke when the server response
                                                    // has been received; here we apply a fade-in effect to the new content
                                                    success: function () {
                                                    $('.formAjax').clearForm();
                                                            $('#mensagemAjax').fadeIn('slow');
                                                            $.unblockUI();
                                                            $('input').attr('disabled', false);
                                                            //window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                                                    }
                                            });
                                            });
                                            });
                                                    //editarUsr

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>