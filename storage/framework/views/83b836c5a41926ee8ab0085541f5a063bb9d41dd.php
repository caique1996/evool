<?php $__env->startSection('htmlheader_title'); ?>
Selecionar endereço
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Loja Virtual > Meu carrinho > Endereços
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<div class="container-fluid">

    <div class="box">
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">

                    <div id="mensagemAdicionarVouchers">

                    </div>
                    <form action="index.html" method="get">
                        <div class="col-md-5">
                            <h4>Selecione o serviço para envio do pedido *</h4>
                            <div class="row">
                                <div class="box">
                                    <div class="box-body">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>
                                                    <p>CEP</p>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="cep" placeholder="Insira sue CEP" required>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>PAC</p>
                                                </td>
                                                <td>
                                                    <input type="radio" name="codigo" value="41106" required>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>SEDEX</p>
                                                </td>
                                                <td>
                                                    <input type="radio" name="codigo" value="40010" required>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>SEDEX 10</p>
                                                </td>
                                                <td>
                                                    <input type="radio" name="codigo" value="40215" required>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <h4>Selecione o endereço para realizar o pedido *</h4>
                            <?php foreach($enderecos as $endereco): ?>
                            <div class="row">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <p>Endereço para entrega</p>
                                        <p><?php echo e($endereco['pais']); ?></p>
                                        <p><?php echo e($endereco['endereco']); ?></p>
                                        <p>CEP <?php echo e($endereco['cep']); ?> - <?php echo e($endereco['cidade']); ?>, <?php echo e($endereco['estado']); ?></p>
                                        <a href="/painel/meus-pedidos/add/<?php echo e($endereco['id']); ?>/41106?pagamento=1"><button type="button" class="btn btn-info">Pagar com saldo</button></a>
                                        <br>
                                        <a href="/painel/meus-pedidos/add/<?php echo e($endereco['id']); ?>/41106?pagamento=2"><button type="button" class="btn btn-info">Pagar com BitzPayer(boleto,bitocoin e cartão de crédito)</button></a>

                                    </div>
                                </div>
                            </div>

                            <?php endforeach; ?>

                            <?php if(sizeof($enderecos) == 0): ?>
                            <div class="callout callout-info">
                                <h4>Não existem endereços cadastrados!</h4>
                                <p>Para prosseguir, cadastre um endereço que poderá receber o produto.</p>
                            </div>
                            <?php endif; ?>

                        </div>

                    </form>

                    <div class="col-md-offset-1 col-md-6">
                        <form method="post" action="/painel/meu-carrinho/endereco/add">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <fieldset>

                                <!-- Form Name -->
                                <legend>Adicionar novo endereço</legend>
                                <?php if(!empty($error)): ?>
                                <div class="callout callout-danger">
                                    <h4><?php echo e($error); ?></h4>
                                </div>
                                <?php endif; ?>
                                <!-- Select Basic -->
                                <div class="form-group">
                                    <label class="control-label" for="Country">País</label>
                                    <select id="Country" name="pais" class="form-control">
                                        <option value="Brasil">Brasil</option>
                                    </select>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label" for="State">Estado</label>
                                    <input id="State" name="estado" type="text" placeholder="Estado" class="form-control input-md" required="">
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label" for="city">Cidade</label>
                                    <input id="city" name="cidade" type="text" placeholder="Sua cidade" class="form-control input-md" required="">

                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label" for="address1">Endereço</label>
                                    <input id="address1" name="endereco" type="text" placeholder="" class="form-control input-md">
                                    <span class="help-block">Endereço da sua casa, compania ou outro...</span>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label" for="cep">CEP</label>
                                    <input id="cep" name="cep" type="text" placeholder="Insira seu cep" class="form-control input-md" required="">
                                </div>

                            </fieldset>
                            <button type="submit" class="btn btn-info pull-right">Adicionar novo</button>
                        </form>


                    </div>

                </div>
            </div>
        </div>


    </div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>