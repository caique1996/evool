<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
Lancamentos
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Lancamentos
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li class="active">Lancamentos</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> Lista de Lancamentos</h3>
                <div class="box-tools pull-right">
                    <a href="javascript:void(0);" data-href="<?php echo e(url('/admin/lancamentos/create')); ?>" class="btn btn-sm btn-primary openModal"><i class="fa fa-plus"></i> Adicionar</a>
                </div>
            </div>
            <div class="box-body">
                <div id="mensagemAjax">

                </div>
                <table id="example2" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>Nome</th>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th>Data</th>
                            <th>Tipo</th>


                            <th width="280px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $lancamentos = App\Lancamentos::all();
                        ?>
                        <?php foreach($lancamentos as $lancamento): ?>
                        <tr>
                            <td><?php echo e($lancamento->id); ?></td>
                            <td><?php echo e($lancamento->nome); ?></td>
                            <td><?php echo e($lancamento->descricao); ?></td>
                            <td><?php echo e($lancamento->valor); ?></td>
                            <td><?php echo e($lancamento->data); ?></td>
                            <td><?php echo e($lancamento->tipo); ?></td>

                            <td>
                                <a href="javascript:void(0);" data-id="<?php echo e($lancamento->id); ?>" class="btn btn-sm btn-danger delete"><i class="fa fa-times"></i> Remover</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.Left col -->

    <?php echo e(method_field('PUT')); ?>

</div><!-- /.row (main row) -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php $__env->stopSection(); ?>


<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(asset('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/form/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>


<script>
$(function () {

    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    $(".delete").click(function () {
        fBlockUi();
        $.ajax({
            url: '<?php echo e(url('/admin/lancamentos')); ?>'+'/'+$(this).attr('data-id'),
            method: 'DELETE',
            data: {_token: "<?php echo csrf_token(); ?>"},
            type: 'DELETE',
            success: function (result) {
                $("#mensagemAjax").html(result);
                $.unblockUI();
                window.setTimeout('location.reload()', 1500); //Reloads after three seconds
            }
        });
    });

    $(".openModal").on('click', function () {

        $('#myModal').removeData('bs.modal');

        $('#myModal').modal(
                {
                    remote: $(this).attr('data-href')
                }
        );

        $('#myModal').modal('show');

        $('#myModal').on('loaded.bs.modal', function (e) {

            // bind form using ajaxForm
            var form = $('.formAjax').ajaxForm({
                // target identifies the element(s) to update with the server response
                target: '#mensagemAjax',
                beforeSubmit: function () {
                    fBlockUi();
                    $('#myModal').modal('hide');
                    $('input').attr('disabled', true);
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAjax').clearForm();
                    $('#mensagemAjax').fadeIn('slow');
                    $.unblockUI();
                    $('input').attr('disabled', false);
                    window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                }
            });

        });


    });

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "allowHTML": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>