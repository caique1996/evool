<?php $__env->startSection('htmlheader_title'); ?>
Relatórios
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Relatórios
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->
<?php $extratos = app('App\extratos'); ?>
<style>
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    tfoot {
        display: table-header-group;
    }
</style>
<?php
if(!@$_GET['tipo']){
$extract = App\extratos::where('beneficiado', Auth::user()->id);
if (!isset($_GET['de']) or ! isset($_GET['ate'])) {
    $ganho = App\extratos::where('beneficiado', Auth::user()->id)->where('tipo', 5)->orWhere('tipo', 6)->orWhere('tipo', 7)->orWhere('tipo', 9)->sum('valor');

    $saida = App\extratos::where('beneficiado', Auth::user()->id)->where('tipo', 1)->orWhere('tipo', 2)->orWhere('tipo', 11)->orWhere('tipo', 12)->sum('valor');
     
    $allExtratos = App\extratos::where('beneficiado', Auth::user()->id)->get();
   
} else {
    $extract = App\extratos::where('beneficiado', Auth::user()->id)->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate']);
    $ganho = App\extratos::where('beneficiado', Auth::user()->id)->where('tipo', 4)->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate'])->orWhere('tipo', 5)->orWhere('tipo', 6)->orWhere('tipo', 7)->orWhere('tipo', 9)->sum('valor');
    $saida = App\extratos::where('beneficiado', Auth::user()->id)->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate'])->orWhere('tipo', 2)->where('tipo', 1)->orWhere('tipo', 11)->orWhere('tipo', 12)->sum('valor');
    $allExtratos = App\extratos::where('beneficiado', Auth::user()->id)->get();
    $allExtratos = $extract->get();
}

$total = $ganho - $saida;
}

?>
<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Relatórios</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>
                <?php if(!@$_GET['tipo']){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            Saída&nbsp;<button class="btn btn-danger" data-widget="collapse">R$<?= number_format($saida, 2, ',', '.') ?></button>&nbsp;&nbsp;
                            Entrada&nbsp;<button class="btn btn-warning" data-widget="collapse">R$<?= number_format($ganho, 2, ',', '.') ?></button>&nbsp;&nbsp;
                            Total&nbsp;<button class="btn btn-success" data-widget="collapse">R$<?php if($saida<0){$saida=$saida*(-1);}?><?= number_format($ganho - $saida, 2, ',', '.') ?></button>&nbsp;&nbsp;
                        </div>
                        <div class="col-md-6">
                            <form class="form-inline" role="form">
                                <div class="form-group">
                                    <label for="de">De :</label>
                                    <input type="date" required="" class="form-control" id="de" value="<?= @$_GET['de'] ?>" name="de">
                                </div>
                                <div class="form-group">
                                    <label for="de">Até :</label>
                                    <input type="date" required="" class="form-control" value="<?= @$_GET['ate'] ?>" id="ate" name="ate">
                                </div>
                                <button type="submit" class="btn btn-default">Exibir</button>
                            </form>
                        </div>
                    </div>

                </div>
                <br>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th >Id</th>
                            <th>Usuario</th>
                            <th>Descrição</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>tipo</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th >Id</th>
                            <th>Usuario</th>
                            <th>Descrição</th>
                            <th>Data</th>
                            <th>Valor</th>

                            <th>tipo</th>


                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        function extrato_tipo($tipo) {
                            switch ($tipo) {
                                case 1:

                                    $tipo = 'Bônus de indicação';
                                    break;
                                case 2:

                                    $tipo = 'Bônus de equiparação';
                                    break;
                                case 4:

                                    $tipo = 'Pagamento de fatura';
                                    break;
                                case 5:

                                    $tipo = 'Ativação';
                                    break;
                                case 6:

                                    $tipo = 'Renovação';
                                    break;
                                case 7:

                                    $tipo = 'Compra';
                                    break;
                                case 8:

                                    $tipo = 'Bônus graduação';
                                    break;
                                case 9:
                                    $tipo = 'Transferência de saldo';
                                    break;
                                case 11:
                                    $tipo = 'Indicação direta';
                                    break;
                                case 12:
                                    $tipo = 'Divisão de lucros';
                                    break;


                                default:
                                    $tipo = 'Outros';

                                    break;
                            }
                            return $tipo;
                        }
                        ?>

                        <?php foreach($allExtratos as $extrato): ?>
                        <tr>
                            <td ><?php echo e($extrato['id']); ?></td>
                            <td><?php if($extrato['descricao']=='Pagamento de binário'): ?>
                                <?php echo e('Administração'); ?>

                                <?php endif; ?>
                                <?php if($extrato['descricao']!='Pagamento de binário'): ?>
                                <?php echo e($extrato->userName($extrato['user_id'])); ?>

                                <?php endif; ?>


                            </td>
                            <td><?= $extrato['descricao'] ?></td>

                            <td><?php echo e($extrato['data']); ?></td>
                            <td>R$<?php echo e(number_format($extrato['valor'],2)); ?></td>
                            <td><?php echo e(extrato_tipo($extrato['tipo'])); ?></td>


                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php } ?>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>
$(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#example2 tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    });

    // DataTable
    var table = $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[0, "desc"]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });

    // Apply the search
    table.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                        .search(this.value)
                        .draw();
            }
        });
    });
});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>