<?php $__env->startSection('htmlheader_title'); ?>
Analytics
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Analytics
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div id="embed-api-auth-container"></div>
        <div id="view-selector-container"></div>
        <div id="data-chart-1-container"></div>
        <div id="date-range-selector-1-container"></div>
        <div id="data-chart-2-container"></div>
        <div id="date-range-selector-2-container"></div>

    </section>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/form/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<script>
function status_recusado(elemento) {
    status = $(elemento).val();
    if (status == 3) {
        $("#anotacoes").attr('required', true);
        $("#anuncio_recu").show();
    } else {
        $("#anotacoes").hide();
        $(elemento).removeAttr('required');



    }
}
$("#myModal").mouseover(function () {
    tinymce.init({selector: 'textarea', plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ], setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
});
$(function () {

    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    $(".delete").click(function () {
        fBlockUi();
        $.ajax({
            url: '<?php echo e(url(' / admin / pacote')); ?>' + '/' + $(this).attr('data-id'),
            method: 'DELETE',
            data: {_token: "<?php echo csrf_token(); ?>"},
            type: 'DELETE',
            success: function (result) {
                $("#mensagemAjax").html(result);
                $.unblockUI();
                window.setTimeout('location.reload()', 1500); //Reloads after three seconds
            }
        });
    });
    $(".openModal").on('click', function () {

        $('#myModal').removeData('bs.modal');
        $('#myModal').modal(
                {
                    remote: $(this).attr('data-href')
                }
        );
        $('#myModal').modal('show');
        $('#myModal').on('loaded.bs.modal', function (e) {

// bind form using ajaxForm
            var form = $('.formAjax').ajaxForm({
// target identifies the element(s) to update with the server response
                target: '#mensagemAjax',
                beforeSubmit: function () {
                    fBlockUi();
                    $('#myModal').modal('hide');
                    $('input').attr('disabled', true);
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAjax').clearForm();
                    $('#mensagemAjax').fadeIn('slow');
                    $.unblockUI();
                    $('input').attr('disabled', false);
                    window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                }
            });
        });
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});


</script>
<script>
    (function (w, d, s, g, js, fs) {
        g = w.gapi || (w.gapi = {});
        g.analytics = {q: [], ready: function (f) {
                this.q.push(f);
            }};
        js = d.createElement(s);
        fs = d.getElementsByTagName(s)[0];
        js.src = 'https://apis.google.com/js/platform.js';
        fs.parentNode.insertBefore(js, fs);
        js.onload = function () {
            g.load('analytics');
        };
    }(window, document, 'script'));
</script>
<!-- Include the ViewSelector2 component script. -->
<script src="/public/javascript/embed-api/components/view-selector2.js"></script>

<!-- Include the DateRangeSelector component script. -->
<script src="/public/javascript/embed-api/components/date-range-selector.js"></script>
<script>

    gapi.analytics.ready(function () {

        /**
         * Authorize the user immediately if the user has already granted access.
         * If no access has been created, render an authorize button inside the
         * element with the ID "embed-api-auth-container".
         */
        gapi.analytics.auth.authorize({
            container: 'embed-api-auth-container',
            clientid: 'REPLACE WITH YOUR CLIENT ID'
        });


        /**
         * Store a set of common DataChart config options since they're shared by
         * both of the charts we're about to make.
         */
        var commonConfig = {
            query: {
                metrics: 'ga:sessions',
                dimensions: 'ga:date'
            },
            chart: {
                type: 'LINE',
                options: {
                    width: '100%'
                }
            }
        };


        /**
         * Query params representing the first chart's date range.
         */
        var dateRange1 = {
            'start-date': '14daysAgo',
            'end-date': '8daysAgo'
        };


        /**
         * Query params representing the second chart's date range.
         */
        var dateRange2 = {
            'start-date': '7daysAgo',
            'end-date': 'yesterday'
        };


        /**
         * Create a new ViewSelector2 instance to be rendered inside of an
         * element with the id "view-selector-container".
         */
        var viewSelector = new gapi.analytics.ext.ViewSelector2({
            container: 'view-selector-container',
        }).execute();


        /**
         * Create a new DateRangeSelector instance to be rendered inside of an
         * element with the id "date-range-selector-1-container", set its date range
         * and then render it to the page.
         */
        var dateRangeSelector1 = new gapi.analytics.ext.DateRangeSelector({
            container: 'date-range-selector-1-container'
        })
                .set(dateRange1)
                .execute();


        /**
         * Create a new DateRangeSelector instance to be rendered inside of an
         * element with the id "date-range-selector-2-container", set its date range
         * and then render it to the page.
         */
        var dateRangeSelector2 = new gapi.analytics.ext.DateRangeSelector({
            container: 'date-range-selector-2-container'
        })
                .set(dateRange2)
                .execute();


        /**
         * Create a new DataChart instance with the given query parameters
         * and Google chart options. It will be rendered inside an element
         * with the id "data-chart-1-container".
         */
        var dataChart1 = new gapi.analytics.googleCharts.DataChart(commonConfig)
                .set({query: dateRange1})
                .set({chart: {container: 'data-chart-1-container'}});


        /**
         * Create a new DataChart instance with the given query parameters
         * and Google chart options. It will be rendered inside an element
         * with the id "data-chart-2-container".
         */
        var dataChart2 = new gapi.analytics.googleCharts.DataChart(commonConfig)
                .set({query: dateRange2})
                .set({chart: {container: 'data-chart-2-container'}});


        /**
         * Register a handler to run whenever the user changes the view.
         * The handler will update both dataCharts as well as updating the title
         * of the dashboard.
         */
        viewSelector.on('viewChange', function (data) {
            dataChart1.set({query: {ids: data.ids}}).execute();
            dataChart2.set({query: {ids: data.ids}}).execute();

            var title = document.getElementById('view-name');
            title.innerHTML = data.property.name + ' (' + data.view.name + ')';
        });


        /**
         * Register a handler to run whenever the user changes the date range from
         * the first datepicker. The handler will update the first dataChart
         * instance as well as change the dashboard subtitle to reflect the range.
         */
        dateRangeSelector1.on('change', function (data) {
            dataChart1.set({query: data}).execute();

            // Update the "from" dates text.
            var datefield = document.getElementById('from-dates');
            datefield.innerHTML = data['start-date'] + '&mdash;' + data['end-date'];
        });


        /**
         * Register a handler to run whenever the user changes the date range from
         * the second datepicker. The handler will update the second dataChart
         * instance as well as change the dashboard subtitle to reflect the range.
         */
        dateRangeSelector2.on('change', function (data) {
            dataChart2.set({query: data}).execute();

            // Update the "to" dates text.
            var datefield = document.getElementById('to-dates');
            datefield.innerHTML = data['start-date'] + '&mdash;' + data['end-date'];
        });

    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>