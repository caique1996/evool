<?php $__env->startSection('htmlheader_title'); ?>
Upgrade
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Upgrade
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Upgrade</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="container">
                    <h2>Upgrade</h2>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select name="qntd_cotas" id="qntd_cotas" class="form-control"><?php $i = 0;
?>                                    <option value="" selected="">Selecione um pacote</option>
                                <?php foreach($pacotes as $pacote): ?>
                                <?php
                                $pacoteValor = $pacote['valor'];
                                $pacoteValor = str_replace('.', '', $pacoteValor);
                                $pacoteValor = str_replace(',', '.', $pacoteValor);
                                //$totalTitulo = Auth::user()->totalCotas() + (floor($pacoteValor / 200));
                                if ($pacote['total_cotas'] >$pacoteAtual['total_cotas']) {
                                    ?>
                                    <option value="<?php echo e($pacote['id']); ?>"><?php echo e($pacote['nome']); ?></option>
                                    <?php $i++; ?>
                                <?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </div>


                        <a class="upgrade" data-pacote="<?php echo e($pacote['id']); ?>" data-metodo='3'  style="color: #fff; cursor: pointer;">
                            <div class="panel-footer  bg-blue-gradient">
                                <span id="euQuero" class="pagar">Eu quero este pacote(pagar via Bitzpayer(Boleto,cartão e Bitcoin)!</span> <i class="fa fa-arrow-circle-right"></i>

                            </div>
                        </a>
                     <?php if ($config['gnt_status'] == 'sim' and $config['gnt_client'] <> '' and $config['gnt_secret'] <> '') {
                                    ?>
                                                        <a class="upgrade" data-pacote="<?php echo e($pacote['id']); ?>" data-metodo='2'  style="color: #fff; cursor: pointer;">
                                                            <div class="panel-footer  bg-blue-gradient">
                                                                <span id="euQuero" class="pagar">Eu quero este pacote(pagar via boleto)!</span> <i class="fa fa-arrow-circle-right"></i>

                                                            </div>
                                                        </a>
                        <?php } ?>
                        <?php if ($config['iugu_status'] == 'sim' and $config['iugu_token'] <> '') { ?>
                                                        <a class="upgrade" data-pacote="<?php echo e($pacote['id']); ?>" data-metodo='1'  style="color: #fff; cursor: pointer;">
                                                            <div class="panel-footer  bg-red-gradient">
                                                                <span id="euQuero" class="pagar">Eu quero este pacote(pagar via Iugu(Cartão e boleto))!</span> <i class="fa fa-arrow-circle-right"></i>
                                                            </div>
                                                        </a>
                        <?php } ?>
                        <?php if ($config['deposito_status'] == 'sim') { ?>
                            <a class="upgrade" data-pacote="<?php echo e($pacote['id']); ?>" data-metodo='3'  style="color: #fff; cursor: pointer;">
                                <div class="panel-footer  bg-green-gradient">
                                    <span id="euQuero" class="pagar">Eu quero este pacote(pagar via transferência bancaria)!</span> <i class="fa fa-arrow-circle-right"></i>

                                </div>
                            </a>
                        <?php } ?>
                        <br>
                        <p>Essa fatura será armazenada e pode ser paga posteriormente .Para acessar suas faturas ou pagar faturas com saldo <a href='faturas'>Clique Aqui</a>.</p>

                    </div>

                </div>
                <?php $i++; ?>
                <div class="clearfix"></div>

            </div>
            <br>





        </div>
</div>


</div><!-- /.box-body -->
</div><!-- /.box -->

</section>

</div>
<div id='transferenciaBan' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dados das contas</h4>

            </div>
            <div class="modal-body">
                <?= $config['deposito_contas'] ?>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('page_scripts'); ?>
    <!-- DataTables -->
    <script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
    <script>
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
});
$(".deposito").click(function () {
    $("#transferenciaBan").modal();
});

$(".upgrade").click(function () {
    pacote = $("#qntd_cotas").val();
    if (pacote == '') {
        alert('Selecione um pacote');
    } else {
        ele = this;
        $(ele).find('#euQuero').html('Por favor aguarde...');
        $(ele).find('.panel-footer').removeClass('bg-blue-gradient');
        $(ele).find('.panel-footer').removeClass('bg-red-gradient');
        $(ele).find('.panel-footer').removeClass('bg-green-gradient');
        $(ele).find('.panel-footer').addClass('bg-orange');
        if ($(ele).attr('data-metodo') == 4) {
            $(ele).find('.panel-footer').removeClass('bg-orange');
            $(ele).find('.panel-footer').addClass('bg-green');
            $(ele).find('#euQuero').html('Redirecionando...');
            setTimeout(function () {
                url='<?= url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=2&pacote=") ?>' + pacote
                location.href =url ;
            }, 2000);
        } else {
            $.ajax({
                'url': "?novopagamento=1&metodo=" + $(ele).attr('data-metodo') + "&pacote=" + pacote,
                dataType: 'html',
                'success': function (txt) {
                    if (txt == '') {
                        alert('Houve uma falha ao executar a operação.Verifique se seus dados foram inseridos corretamente.');
                    } else if (txt == 'ok') {
                        $("#transferenciaBan").modal();
                        $(ele).find('.panel-footer').removeClass('bg-orange');
                        $(ele).find('.panel-footer').addClass('bg-green');
                        $(ele).find('#euQuero').html('Fatura gerada com sucesso');

                    } else {
                        $(ele).find('.panel-footer').removeClass('bg-orange');
                        $(ele).find('.panel-footer').addClass('bg-green');
                        $(ele).find('#euQuero').html('Redirecionando...');
                        setTimeout(function () {
                            location.href = txt;
                        }, 2000);
                    }


                }
            });
        }
    }

});


    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>