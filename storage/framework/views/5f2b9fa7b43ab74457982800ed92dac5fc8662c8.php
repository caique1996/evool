<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('htmlheader_title'); ?>
Materiais
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Materiais
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<li class="active">Materiais</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Lista de Materiais
            </div>
            <div class="panel-body">
                <table id="example2" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>Material</th>
                            <th width="225px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $materiais = app('App\Materiais'); ?>
                        <?php foreach($materiais->get() as $material): ?>
                        <tr>
                            <td><?php echo e($material->id); ?></td>
                            <td><?php echo e($material->description); ?></td>
                            <td>
                                <a href="javascript:void(0);" data-href="<?php echo e(url('/painel/materiais/'.$material->id)); ?>" class="btn btn-sm btn-info openModal"><i class="fa fa-eye"></i> Vizualizar</a>
                                <a href="<?php echo e($material->download); ?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-download"></i> Download</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php $__env->stopSection(); ?>


<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(asset('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>

<script>
$(function () {

$(".openModal").on('click', function () {

    $('#myModal').removeData('bs.modal');

    $('#myModal').modal(
            {
                remote: $(this).attr('data-href')
            }
    );

    $('#myModal').modal('show');

});

$('#example2').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    }
});
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>