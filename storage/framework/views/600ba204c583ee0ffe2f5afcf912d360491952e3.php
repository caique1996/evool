<form class="formAjax" method="post" action="<?php echo e(url('/painel/landingpage/salvar')); ?>">

    <?php echo e(csrf_field()); ?>

    <?php

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar</h4>

    </div>
    <div class="modal-body">
        <input type="hidden" class="form-control"  name="id" value="<?= $dados['id'] ?>"/>
        <div class="form-group">
            <label>Pilar 1 (título)</label>
            <input type="text" class="form-control" value="<?= $dados['pilar_1_titulo'] ?>"  name="pilar_1_titulo" />
        </div>
        <div class="form-group">
            <label>Pilar 1 (conteúdo)</label>
            <textarea class="form-control" rows="7" value="<?= $dados['pilar_1_conteudo'] ?>"  name="pilar_1_conteudo" ><?= $dados['pilar_1_conteudo'] ?></textarea>
        </div>

        <div class="form-group">
            <label>Pilar 2 (título)</label>
            <input type="text" class="form-control" value="<?= $dados['pilar_2_titulo'] ?>"  name="pilar_2_titulo" />
        </div>
        <div class="form-group">
            <label>Pilar 2 (conteúdo)</label>
            <textarea class="form-control" rows="7" value="<?= $dados['pilar_2_conteudo'] ?>"  name="pilar_2_conteudo" ><?= $dados['pilar_2_conteudo'] ?></textarea>
        </div>
        <div class="form-group">
            <label>Pilar 3 (título)</label>
            <input type="text" class="form-control" value="<?= $dados['pilar_3_titulo'] ?>"  name="pilar_3_titulo" />
        </div>
        <div class="form-group">
            <label>Pilar 3 (conteúdo)</label>
            <textarea class="form-control" rows="7" value="<?= $dados['pilar_3_conteudo'] ?>"  name="pilar_3_conteudo" ><?= $dados['pilar_3_conteudo'] ?></textarea>
        </div>
        <div class="form-group">
            <label>Pilar 4 (título)</label>
            <input type="text" class="form-control" value="<?= $dados['pilar_4_titulo'] ?>"  name="pilar_4_titulo" />
        </div>
        <div class="form-group">
            <label>Pilar 4 (conteúdo)</label>
            <textarea class="form-control" rows="7" value="<?= $dados['pilar_4_conteudo'] ?>"  name="pilar_4_conteudo" ></textarea>
        </div>

            <label>Título do video</label>
            <textarea class="form-control" rows="7" value="<?= $dados['video_titulo'] ?>"  name="video_titulo" ><?= $dados['video_titulo'] ?></textarea>
        </div>
        <div class="form-group">
            <label>Descrição do vídeo (conteúdo)</label>
            <textarea class="form-control" rows="7" value=""  name="video_descricao" ><?= $dados['video_descricao'] ?></textarea>
        </div>
        <div class="form-group">
            <label>Link da Conferência </label>
            <textarea class="form-control" rows="7"   name="conferencia_link" ><?= $dados['conferencia_link'] ?></textarea>
        </div>
     
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>

