<?php $__env->startSection('htmlheader_title'); ?>
Admin
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<style>

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
Painel do Usuario
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<style>
    .box.box-warning {
        border-top-color: #fff;
    }
</style>
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">

<?php
$conf = new App\config();
$conf = $conf->getConfig();
?>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">lucro</span>
                <span class="info-box-number">R$ 
                    <?php
                    $trinDias = date('Y-m-d', strtotime('- 1 month', strtotime(date('Y-m-d'))));
                    $lucro = \App\extratos::where('tipo', 5)->orWhere('tipo', 6)->orWhere('tipo', 7)->orWhere('tipo', 15)->orWhere('tipo', 9)->sum('valor');
                    if ($lucro == '') {
                        $lucro = 0;
                    }
                    echo e(number_format($lucro, 2, ',', '.'));
                    ?></span>
                <p style="font-size:10px;">lucro de <?= $trinDias ?> até <?= date('Y-m-d') ?>

            </div>
        </div>
    </div> 
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-dollar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">A pagar</span>
                <span class="info-box-number">R$ <?php
                    echo e(number_format(Auth::user()->all()->sum('saldo'), 2, ',', '.'));
                    ?></span>
                <p style="font-size:10px;">*Soma de saques pendentes.</p>
            </div>
        </div>
    </div> 
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-dollar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Dívida total</span>
                <span class="info-box-number"><?= e(number_format(Auth::user()->where('ativo', 1)->sum('saldo'), 2, ',', '.')); ?></span>
                <p style="font-size:10px;">*Soma do saldo de todos os usuários.</p>

            </div>
        </div>
    </div> 
    <div class="col-md-3 col-sm-6 col-xs-12">

        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-dollar"></i></span>
            <div class="info-box-content">

                <span class="info-box-text"><a  class="small-box-footer" style="color:#333;">Saída </a></span>
                <span class="info-box-number"> <?php echo e(number_format(\App\Saque::where('status', 1)->sum('valor'), 2, ',', '.')); ?></span>
                <p style="font-size:10px;">*Soma de  todos os saques pagos.</p>

            </div>


        </div>
    </div> 




</div><!-- /.row -->

<div class="row">
    <div  class="col-lg-6">


        <?php
        $cadastros = App\User::all();
        $total = 0;
        $i = 0;
        foreach ($cadastros as $value) {
            // echo strtotime($value['created_at']).' '.strtotime(strtotime('- 5 days', strtotime(date('Y-m-d'))));
            if (strtotime($value['created_at']) > strtotime(date('Y-m-d', strtotime('- 5 days', strtotime(date('Y-m-d')))))) {
                $i++;
            }
        }
        $de = date('Y-m-d', strtotime('- 5 days', strtotime(date('Y-m-d'))));
        $at = date('Y-m-d');
        ?>
        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title "><?= $i ?> novo(s) cadastro(s)</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <table id="example2" class="table table-hover">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Username</th>
                            <th>Data de cadastro</th>

                            <th>Situação</th>

                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        foreach ($cadastros as $value) {
                            // echo strtotime($value['created_at']).' '.strtotime(strtotime('- 5 days', strtotime(date('Y-m-d'))));
                            if (strtotime($value['created_at']) > strtotime(date('Y-m-d', strtotime('- 5 days', strtotime(date('Y-m-d')))))) {
                                echo "<tr><th>" . $value['id'] . "</th>";
                                echo "<th>" . $value['username'] . "</th>";
                                echo "<th>" . $value['created_at'] . "</th>";
                                echo "<th>" . Auth::user()->getStatus($value['status']) . "</th></tr>";


                                $i++;
                            }
                        }
                        ?>

                    </tbody>



                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div  class="col-lg-6">

        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title ">Ativações recentes </h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <table id="example2" class="table table-hover">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Username</th>
                            <th>Data</th>



                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $cadastros = App\extratos::where('beneficiado', Auth::user()->id)->where('tipo', 7)->where('data', '>=', $de)->where('data', '<=', $at)->get();


                        foreach ($cadastros as $value) {
                            // echo strtotime($value['created_at']).' '.strtotime(strtotime('- 5 days', strtotime(date('Y-m-d'))));
                            echo "<tr><th>" . $value['id'] . "</th>";
                            echo "<th>" . Auth::user()->userInfo($value['user_id'])['username'] . "</th>";
                            echo "<th>" . $value['data'] . "</th>";


                            $i++;
                        }
                        ?>

                    </tbody>



                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    <div  class="col-lg-6">

        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title ">Solicitações de saques </h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <table id="example2" class="table table-hover">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Username</th>
                            <th>Data</th>
                            <th>Valor</th>




                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $cadastros = App\Saque::where('status', 0)->get();

                            // echo strtotime($value['created_at']).' '.strtotime(strtotime('- 5 days', strtotime(date('Y-m-d'))));
                            foreach ($cadastros as $value) {
                                // echo strtotime($value['created_at']).' '.strtotime(strtotime('- 5 days', strtotime(date('Y-m-d'))));
                                if (strtotime($value['created_at']) > strtotime(date('Y-m-d', strtotime('- 5 days', strtotime(date('Y-m-d')))))) {

                                    echo "<tr><th>" . $value['id'] . "</th>";
                                    echo "<th>" . Auth::user()->userInfo($value['user_id'])['username'] . "</th>";
                                    echo "<th>" . $value['created_at'] . "</th>";
                                    echo "<th>R$" . number_format($value['valor'], 2, ',', '.') . "</th>";

                                    $i++;
                                }
                            }
                  
                        ?>

                    </tbody>



                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">


    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_scripts'); ?>
<!-- InputMask -->
<script src="<?php echo e(env('CFURL') . ('/plugins/form/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>

<script>
$(document).ready(function () {
    $("#example2").DataTable();
// bind form using ajaxForm
    $('#formSuporteDashboard').ajaxForm({
        // target identifies the element(s) to update with the server response
        target: '#mensagemSuporteDashboard',
        beforeSubmit: function () {
            $('#sendEmail').attr('disabled', true);
            Pace.restart();
        },
        // success identifies the function to invoke when the server response
        // has been received; here we apply a fade-in effect to the new content
        success: function () {
            $('#formSuporteDashboard').clearForm();
            $('#mensagemSuporteDashboard').fadeIn('slow');
            setTimeout(function () {
                Pace.restart();
                location.reload();
            }, 1500);
        }
    });
});
$(document).ready(function () {
    var ladoBin = "<?php echo e(Auth::user()->direcao); ?>";
    if (ladoBin == 'direita') {
        $("#ladobinario").attr('checked', true);
    } else {
        $("#ladobinario2").attr('checked', true);

    }

});
$(".change_dir").click(function () {
    var lado = $(this).val();
    $.ajax({
        'url': "muda_lado?lado=" + lado,
        dataType: 'html',
        'success': function (txt) {
            if (txt == 'ok') {
                alert('O lado binário foi alterado com sucesso.');
            }
            if (txt == 'fail') {
                alert('O lado não binário foi alterado com sucesso.');

            }
        }});
});
$("#btnSaque").click(function () {
    var valor = $("#valorSaque").val();
    var pinSaque = $("#pinSaque").val();

    url = "saque?valor=" + valor;
    if (confirm("Você realmente deseja solicitao saque de R$" + valor + "?")) {
        $.ajax({
            'url': "saque?valor=" + valor + "&pin=" + pinSaque,
            dataType: 'html',
            'success': function (txt) {
                alert(txt);
                if (txt == 'ok') {
                    alert('O lado saque foi solicitado com sucesso.');
                }
                if (txt == 'fail') {
                    alert('O saque não  foi solicitado com sucesso.');

                }
            }});
    } else {
        alert('Operação cancelada');
    }

    return false;
});
</script>
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>