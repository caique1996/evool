<?php
$extesionsImage = array('jpg', 'JPG', 'png', 'PNG', 'jpeg', 'JPEG', 'gif', 'GIF');

if (in_array($extension, $extesionsImage)):
?>
<img src="<?php echo e($link); ?>">
<?php
endif;

$extensionsVideo = array('mp4');
if (in_array($extension, $extensionsVideo)) {
?>
<video width="100%" height="100%" controls>
    <source src="<?php echo e($link); ?>">
    Your browser does not support the video tag.
</video>

<?php
}
?>

