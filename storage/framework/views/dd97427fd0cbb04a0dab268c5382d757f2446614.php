<?php $__env->startSection('htmlheader_title'); ?>
Faturas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Faturas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Transações</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="example2" class="table table-bordered table-hover ">
                    <thead>
                        <tr>
                            <th >Id</th>
                            <th>Usuario</th>
                            <th>Valor</th>
                            <th>Descrição</th>
                            <th>Expiração</th>
                            <th>Situação</th>
                            <th>Ação</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        function getStatus($status) {
                            if ($status == 1) {
                                return 'Ativa';
                            } else {
                                return 'Inativa/Expirada';
                            }
                        }

                        $usr = new \App\User();
                        ?>
                        <?php foreach($faturas as $fatura): ?>
                        <?php
                        $payInfo = App\Pagamentos::where('id', $fatura->pagamento_id)->first();
                        if ($payInfo['tipo'] == 'Compra') {
                            $valor = (App\Pedidos::where('id', $payInfo['pacote'])->first()['preco'] ? App\Pedidos::where('id', $payInfo['pacote'])->first()['preco'] : 0);
                        } else {
                            $valor = App\Pacote::where('id', $payInfo['pacote'])->first()['valor'] ? App\Pacote::where('id', $payInfo['pacote'])->first()['valor'] : 0;
                        }
                        ?>
                        <?php
                        $userInfo = $usr->userInfo($fatura->user_id);
                        ?>
                        <tr>
                            <td ><?php echo e($fatura->id); ?></td>
                            <td><?php echo e($userInfo['username']); ?></td>
                            <td>R$<?= number_format($valor, 2) ?></td>
                            <td><?= $payInfo['tipo'] ?></td>

                            <td><?php echo e($fatura->validade); ?></td>
                            <td><?php echo e(getStatus($fatura->status)); ?></td>
                         
                                    <?php
                            $payInfo = App\Pagamentos::where('id', $fatura->pagamento_id)->first();

                            echo "<td> <a  class='btn btn-warning' href='fatura/liberar/{$fatura->id}' >Liberar Fatura</a>
<a  class='btn btn-danger' href='fatura/excluir/{$fatura->id}' >Cancelar</a></td>";
                            ?>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>
<div id='transferenciaBan' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dados das contas</h4>

            </div>
            <div class="modal-body">
                <?= $config['deposito_contas'] ?>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('page_scripts'); ?>
    <!-- DataTables -->
  <script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/form/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>
    <script>
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[0, "desc"]],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
$("#pagarTrans").click(function () {
    $("#transferenciaBan").modal();
});
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>