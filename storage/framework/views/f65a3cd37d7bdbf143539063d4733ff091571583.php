<form class="formAjax" method="post" action="<?php echo e(url('/admin/materiais')); ?>">
    <?php echo e(csrf_field()); ?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Material</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Descrição</label>
            <input type="text" class="form-control" required placeholder="Descrição" name="description"/>
        </div>
        <div class="form-group">
            <label>Vizualizar (URL)</label>
            <input type="url" class="form-control" required placeholder="Link de Vizualização" name="link"/>
        </div>
        <div class="form-group">
            <label>Download (URL)</label>
            <input type="url" class="form-control" required placeholder="Link de Download" name="download"/>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>