<?php $__env->startSection('htmlheader_title'); ?>
<?= Lang::trans('site.login') ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php
$config = App\config::getConf();
?>
<body class="hold-transition login-page" style="background-image:url('<?= $config['logo_3'] ?>');background-size: cover;">
    <div class="login-box">
        <div class="login-logo">
            <a href="<?php echo e(url('/')); ?>">
                <img src="<?= $config['logo_1'] ?>">
            </a>
        </div><!-- /.login-logo -->
<?php if(session('success')): ?>
            <div class="alert alert-success fade in alert-dismissible flat  no-margin">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                <?php echo e(session('success')); ?>

            </div>
        <?php endif; ?>
        <?php if(isset($errors) && count($errors) > 0): ?>
        <div class="alert alert-danger">
            <strong>Whoops!</strong> <?= Lang::trans('site.an_erro_as_ocurred') ?><br><br>
            <ul>
                <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="login-box-body">
            <form action="<?php echo e(url('/admin/login')); ?>" method="post">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="<?= Lang::trans('site.user') ?>" name="username"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="<?= Lang::trans('site.Password') ?>" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                 <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
                <br>
                    <div class="g-recaptcha" data-sitekey="<?=\App\config::getConf()['recaptcha_key']?>"></div>
                    <br>

                <?php } ?>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" id="icheck" name="remember" class="minimal"> <?= Lang::trans('site.remember') ?>
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><?= Lang::trans('site.login') ?></button>
                    </div><!-- /.col -->
                </div>
            </form>

            <a href="<?php echo e(url('/painel/password/reset')); ?>"><?= Lang::trans('site.forgot_password') ?></a><br>
            <br>


        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    <?php echo $__env->make('layouts.partials.scripts_auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
        <script src ='https://www.google.com/recaptcha/api.js' ></script>

    <?php } ?>
    <script>
        $(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-72220910-1', 'auto');
        ga('send', 'pageview');

    </script>
</body>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>