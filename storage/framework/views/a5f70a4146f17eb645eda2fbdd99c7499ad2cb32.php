<?php $__env->startSection('htmlheader_title'); ?>
Extrato Financeiro
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Transações
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->
<?php $extratos = app('App\extratos'); ?>

<?php
if (!isset($_GET['de']) or ! isset($_GET['ate'])) {
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('tipo', '<>', '3');
    $allExtratos = $extract->get();
} else {
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('tipo', '<>', '3')->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate']);
    $allExtratos = $extract->get();
}
?>
<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Extrato Financeiro</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                <label for="de">De :</label>
                                <input type="date" required="" class="form-control" id="de" value="<?= @$_GET['de'] ?>" name="de">
                            </div>
                            <div class="form-group">
                                <label for="de">Até :</label>
                                <input type="date" required="" class="form-control" value="<?= @$_GET['ate'] ?>" id="ate" name="ate">
                            </div>
                            <button type="submit" class="btn btn-default">Exibir</button>
                        </form>
                    </div>
                </div>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Usuario</th>
                            <th>Descrição</th>
                            <th>Data</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($allExtratos as $extrato): ?>
                        <tr>
                            <td><?php echo e($extrato['id']); ?></td>
                            <td><?php if($extrato['descricao']=='Pagamento de binário'): ?>
                                <?php echo e('Administração'); ?>

                                <?php endif; ?>
                                <?php if($extrato['descricao']!='Pagamento de binário'): ?>
                                <?php echo e($extrato->userName($extrato['user_id'])); ?>

                                <?php endif; ?>


                            </td>
                            <td><?php echo e($extrato['descricao']); ?></td>

                            <td><?php echo e($extrato['data']); ?></td>
                            <td>R$<?php echo e(number_format($extrato['valor'],2)); ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script>
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[0, "desc"]],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>