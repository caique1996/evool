<?php $__env->startSection('htmlheader_title'); ?>
Comprar produtos
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Loja Virtual > Produtos disponíveis
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="container-fluid">

    <div class="row">

        <div id="mensagemAdicionarVouchers">

        </div>

        <form method="get" action="<?php echo e(url('/painel/produtos/')); ?>">
            <!-- Input de pesquisa -->
            <div class="col-md-12">
                <div  style="
                      margin-bottom: 10px;
                      ">
                    <div class="input-group"
                         style="
                         border-radius: 0px;
                         height: 30px;
                         ">

                        <span class="input-group-addon" id="basic-addon1"
                              style="
                              background-color: #222D32;
                              color: white;
                              border-style: none;
                              ">
                            <i class="fa fa-search"></i>
                        </span>
                        <input type="text" value="<?php echo e($busca); ?>" name="busca" class="form-control" placeholder="Por qual tipo de produto deseja buscar?"
                               style="
                               height: 50px;
                               color: #B8C7CE;
                               font-size: 20px;
                               background-color: #222D32;
                               border-style: none;
                               ">
                    </div>
                </div>
        </form>
        <!-- Fim do Input de pesquisa -->

        <!-- Informação da busca -->
        <?php if(!empty($busca)): ?>
        <div class="container-fluid"
             style="
             margin: 20px 0px;
             ">
            <h1>Busca por "<?php echo e($busca); ?>"</h1>
        </div>
        <?php endif; ?>
        <!-- Fim da Informação da busca -->

        <!-- Tiles de produtos -->
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="box box-widget widget-user-2" style="margin-left: -5.5%;">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-green">

                        <!-- /.widget-user-image -->
                        <h6 class="widget-user-username " style="margin-left: -3%;">Categorias</h6>

                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <?php
                            $categorias = \App\Produtos::where('estoque', '>', 0)->where('status', 1)->select('categoria')
                                    ->groupBy('categoria')
                                    ->get();
                            ?>
                            <?php
                            foreach ($categorias as $ctg) {
                                $qntd = \App\Produtos::where('estoque', '>', 0)->where('status', 1)->where('categoria', $ctg['categoria'])->count();
                                ?>
                                <li><a href="<?= url('painel/produtos?ctg=' . $ctg['categoria']) ?>"><?= $ctg['categoria'] ?> <span class="pull-right badge bg-blue"><?= $qntd ?></span></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div><div class="col-md-9">
                <?php foreach($produtos as $produto): ?>
                <?php if ($produto['estoque'] > 0 and $produto['status'] == 1) { ?>

                    <div class="col-md-3">
                        <div class="box box-solid text-center"
                             style="
                             border-style: solid;
                             border-radius: 0px;
                             border-width: 1px;
                             border-color: #C5C5C5;
                             ">
                            <div class="box-header with-border"
                                 style="
                                 background-image: url('<?php echo e(url($produto['img'])); ?>');
                                 background-position: center;
                                 background-size: cover;
                                 height: 200px;">

                            </div>
                            <div class="box-body">
                                <h4><?php echo e($produto['nome']); ?></h4>
                                <h5><?php echo e($produto['categoria']); ?></h5>
                                <h4>
                                    <?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto['id'])) { ?>
                                        <strike>R$ <?php echo e(number_format($produto['preco'],2)); ?></strike> R$ <?= Auth::user()->meu_desconto($produto['id']) ?>
                                    <?php } else { ?>
                                        R$ <?php echo e(number_format($produto['preco'],2)); ?>

                                    <?php } ?>
                                </h4>
                                <a href="/painel/meu-carrinho/add/<?php echo e($produto['id']); ?>"><button type="button" class="btn btn-flat btn-responsive btn-success" 
                                                                                              name="button"><i class="fa fa-plus"></i>Adicionar</button></a><a  produto-id="<?php echo e($produto['id']); ?>" class="ver_produto" ><button type="button" produto-id="<?php echo e($produto['id']); ?>" class="ver_produto btn btn-flat btn-responsive btn-primary" 
                                                                                                                                                               name="button"><i class="fa fa-eye"></i>ver</button></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php endforeach; ?>

                <?php if(sizeof($produtos) == 0): ?>

                <h1 class="text-center"
                    style="
                    margin-top: 170px;
                    color: #D6D6D6;
                    font-size: 100px;
                    font-weight: bold;
                    ">
                    <i class="fa fa-shopping-cart"></i>
                </h1>
                <h1 class="text-center"
                    style="
                    color: #D6D6D6;
                    font-size: 50px;
                    font-weight: bold;
                    ">
                    Sem produtos disponíveis.
                </h1>

                <?php endif; ?>
            </div>
            <!-- Fim das tiles de produtos -->
        </div>
    </div>
</div>

<?php foreach($produtos as $produto): ?>
<?php if ($produto['estoque'] > 0 and $produto['status'] == 1) { ?>

    <div class="modal fade" data-backdrop="static" 
         data-keyboard="false" id="produto-<?php echo e($produto->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalMsgTitle"><?php echo e($produto->nome); ?> -   <?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto['id'])) { ?>
                            <strike>R$ <?php echo e(number_format($produto['preco'],2)); ?></strike> R$ <?= number_format(Auth::user()->meu_desconto($produto['id']), 2) ?>
                        <?php } else { ?>
                            R$ <?php echo e(number_format($produto['preco'],2)); ?>

                        <?php } ?></h4>
                </div>
                <div class="modal-body" id='modalMsgBody'>
                    <img src="<?php echo e(url($produto['img'])); ?>" class="img-responsive" title="<?php echo e($produto->nome); ?> "/> 
                    <?= $produto->descricao ?>
                </div>
                <div id="mensagem_anuncio"></div>
                <div class="modal-footer">

                    <a href="/painel/meu-carrinho/add/<?php echo e($produto['id']); ?>"><button type="button" class="btn btn-success" 
                                                                                  name="button"><i class="fa fa-plus"></i>Adicionar ao carrinho</button></a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>

                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php endforeach; ?>
<?= $paginate ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>