<?php $__env->startSection('htmlheader_title'); ?>
Editar Produto
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Editar Produto
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">
    <div class="box"><div class="box-body">
            <div class="register-box" style="width: 850px; margin-top:0px;
                 margin-left:0px">

                <form action="<?php echo e(url('/admin/produtos/update')); ?>" enctype="multipart/form-data" method="POST">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('POST')); ?>

                    <div class="col-sm-9">

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control"  placeholder="Nome" maxlength="20" name="nome" value="<?= $dados['nome'] ?>" required=""/>
                        </div>
                        <input type="hidden" class="form-control"  name="id" value="<?= $dados['id'] ?>" required=""/>

                        <div class="form-group">
                            <label>Descrição </label>
                            <textarea class="form-control" required placeholder="Descrição" name="descricao">
                                <?= $dados['descricao'] ?>
                            </textarea>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control money"  placeholder="Preco" name="preco" value="<?= $dados['preco'] ?>"  required=""/>
                        </div>
                        <div class="form-group">
                            <label>Status </label>
                            <select class="form-control" name="status">
                                <option value="<?= $dados['status'] ?>"><?= Auth::user()->getStatus($dados['status']) ?></option>

                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="number" class="form-control "  placeholder="Quantidade disponivel" name="estoque" value="<?= $dados['estoque'] ?>"  required=""/>
                        </div>
                        <?php
                        $pacotes = \App\Pacote::where('status', 1)->get();
                        $descontos = json_decode($dados['descontos']);
                        foreach ($pacotes as $value) {
                            ?>
                            <div class="form-group has-feedback">
                                Quanto os usuários do pacote <?= $value['nome'] ?> irá pagar?
                                <input type="text" class="form-control money"  placeholder="Preco" name="pre_pacote[<?= $value['id'] ?>]"  required="" value="<?= @$descontos->$value['id'] ?>"/>
                            </div>
                        <?php }
                        ?>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control "  placeholder="Peso" name="peso" value="<?= $dados['peso'] ?>"  required=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="file" class="form-control "  name="img" value=""/> 
                            <a href="<?= url($dados['img']) ?>">Imagem Atual</a>
                        </div>



                        <div class="form-group has-feedback">
                            <input type="text" class="form-control "  placeholder="Categoria" name="categoria" value="<?= $dados['categoria'] ?>"  required=""/>
                        </div>
                          <div class = "form-group  has-feedback">
                            <label>Pontuação direta</label>
                            <input  type='number'  class = 'form-control'  placeholder = 'pontuação direta' value='<?= $dados['pontos_diretos'] ?>' name ='pontos_diretos'/>
                        </div>
                        <?php
                        for ($i = 1; $i <= env('NIVEIS'); $i++) {
                            $key = 'pontos' . $i;
                            $valor=$dados[$key];
                            echo '<div class = "form-group  has-feedback">';
                            echo"<label>Pontuação nível {$i}</label>";
                            echo "<input  type='number'  class = 'form-control'  placeholder = '{$i}º Nível' value='$valor' name ='pontos{$i}'/>
            </div";
                        }
                        ?>

                        <div class="row">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
                            </div><!-- /.col -->
                        </div>

                </form>
            </div><!-- /.form-box --></div><!-- /.form-box -->
    </div><!-- /.register-box -->


</div>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
tinymce.init({selector: 'textarea', plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ], setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
});
</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>