<?php $__env->startSection('htmlheader_title'); ?>
Minha LandingPage
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Minha LandingPage
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Minha Landing Page
                </h3>
                <div class="col-lg-10">


                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div id="mensagemAjax">

                </div>
                <div id="mensagemAdicionarVouchers">

                </div>
                <?php

                function getStatus($status) {
                    if ($status == 1) {
                        return "Publicado";
                    }if ($status == 0) {
                        return "Desativado";
                    }
                }

                $usr = new App\User();
                ?>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Visitas</th>
                            <th>Status
                            </th>

                            <th>Ação
                            </th>



                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $site = App\Landingpages::where('user_id', Auth::user()->id)->first();
                        ?>

                        <tr>
                            <td><?php echo e($site['visitas']); ?></td>
                            <td><?php echo e(getStatus($site['status'])); ?>

                            </td>

                            <td>
                                <a href="javascript:void(0);" data-href='<?php echo e(url('/painel/landingpage/')); ?>/<?php echo e($site['id']); ?>'  target="_blank"  class='btn btn-sm btn-info openModal' ><i class='fa fa-edit'></i>Editar</a>
                                <a href='<?php echo e(url('/site')); ?>/<?= Auth::user()->username ?>' target="_blank"  class='btn btn-sm btn-info ' ><i class='fa fa-eye'></i>Visitar</a>


                            </td>


                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- DataTables -->
<script src="<?php echo e(env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('/plugins/form/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>

<script>
$(function () {

    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    $(".delete").click(function () {
        fBlockUi();
        $.ajax({
            url: '<?php echo e(url(' / admin / pacote')); ?>' + '/' + $(this).attr('data-id'),
            method: 'DELETE',
            data: {_token: "<?php echo csrf_token(); ?>"},
            type: 'DELETE',
            success: function (result) {
                $("#mensagemAjax").html(result);
                $.unblockUI();
                window.setTimeout('location.reload()', 1500); //Reloads after three seconds
            }
        });
    });
    $(".openModal").on('click', function () {




        $('#myModal').removeData('bs.modal');
        $('#myModal').modal(
                {
                    remote: $(this).attr('data-href')
                }
        );
        $('#myModal').modal('show');
        $('#myModal').on('loaded.bs.modal', function (e) {

// bind form using ajaxForm
            var form = $('.formAjax').ajaxForm({
// target identifies the element(s) to update with the server response
                target: '#mensagemAjax',
                beforeSubmit: function () {
                    fBlockUi();
                    $('#myModal').modal('hide');
                    $('input').attr('disabled', true);
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAjax').clearForm();
                    $('#mensagemAjax').fadeIn('slow');
                    $.unblockUI();
                    $('input').attr('disabled', false);
                    window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                }
            });
        });
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>