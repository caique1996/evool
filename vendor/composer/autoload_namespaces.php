<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor' => array($vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'laravel\\pagseguro\\' => array($vendorDir . '/michael/laravelpagseguro/src'),
    'Snowplow\\RefererParser' => array($vendorDir . '/snowplow/referer-parser/php/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'JakubOnderka\\PhpConsoleColor' => array($vendorDir . '/jakub-onderka/php-console-color/src'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
    'Gerencianet' => array($vendorDir . '/gerencianet/gerencianet-sdk-php/src'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'Detection' => array($vendorDir . '/mobiledetect/mobiledetectlib/namespaced'),
    'Cron' => array($vendorDir . '/mtdowling/cron-expression/src'),
    'Contrib\\Component' => array($vendorDir . '/satooshi/php-coveralls/src'),
    'Contrib\\Bundle' => array($vendorDir . '/satooshi/php-coveralls/src'),
    'CodeClimate\\Component' => array($vendorDir . '/codeclimate/php-test-reporter/src'),
    'CodeClimate\\Bundle' => array($vendorDir . '/codeclimate/php-test-reporter/src'),
    'Bllim\\Datatables' => array($vendorDir . '/pragmarx/datatables/src'),
);
