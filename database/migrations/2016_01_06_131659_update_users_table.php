<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('username', 20)->unique();

            $table->string('cpf')->nullable();
            $table->date('nascimento')->nullable();
            $table->string('telefone')->nullable();
            $table->string('sexo')->nullable();
            $table->string('endereco')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();



            $table->boolean('admin')->default(false);
            $table->boolean('ativo')->default(false);
            $table->boolean('ativo_bortoleto')->default(false);
            $table->boolean('pago')->default(false);

            $table->integer('pai_id')->nullable();

            $table->string('banco')->nullable();
            $table->string('agencia')->nullable();
            $table->string('conta')->nullable();
            $table->string('tipo_conta')->nullable();
            $table->string('operacao')->nullable();

            $table->enum('direcao', ['esquerda', 'direita'])->default('esquerda');

            $table->float('saldo', 10, 2)->nullable()->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('cpf');
            $table->dropColumn('nascimento');
            $table->dropColumn('telefone');
            $table->dropColumn('endereco');
            $table->dropColumn('bairro');
            $table->dropColumn('cidade');
            $table->dropColumn('estado');
            $table->dropColumn('sexo');

            $table->dropColumn('admin');
            $table->dropColumn('ativo');
            $table->dropColumn('ativo_bortoleto');
            $table->dropColumn('pago');

            $table->dropColumn('pai_id');

            $table->dropColumn('banco');
            $table->dropColumn('agencia');
            $table->dropColumn('conta');
            $table->dropColumn('tipo_conta');
            $table->dropColumn('operacao');

        });
    }
}
