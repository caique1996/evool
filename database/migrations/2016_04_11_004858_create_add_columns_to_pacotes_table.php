<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnsToPacotesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('pacotes', function (Blueprint $table) {
            $table->integer('valorBin');
            $table->string('valorIndicacao');
        });
    }

}
