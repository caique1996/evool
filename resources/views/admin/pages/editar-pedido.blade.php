@extends('layouts.app')

@section('htmlheader_title')
Editar pedido
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Editar pedido
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">
    <div class="box"><div class="box-body">
            <div class="register-box" style="width: 850px; margin-top:0px;
                 margin-left:0px">

                <form action="{{ url('/admin/pedido/update') }}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="col-sm-9">
                        <input type="hidden" name="id"  value="<?= $dados['id'] ?>" />
                        <div class="form-group">
                            <label>Campo para informações </label>
                            <textarea class="form-control" required placeholder="Ex: Erro nos dados,código de acompanhamento" name="info">
                                <?= $dados['info'] ?>
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label>Status </label>
                            <select class="form-control" name="status">

                                <option value="<?= $dados['status'] ?>"><?= $dados['status'] ?></option>
                                <option value="Pendente">Pendente</option>
                                <option value="Pago">Pago</option>
                                <option value="Encaminhado">Encaminhado</option>
                                <option value="Entregue">Entregue</option>


                            </select>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
                            </div><!-- /.col -->
                        </div>



                </form>
            </div><!-- /.form-box --></div><!-- /.form-box -->
    </div><!-- /.register-box -->


</div>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
tinymce.init({selector: 'textarea', plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ], setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
});
</script>

@endsection

