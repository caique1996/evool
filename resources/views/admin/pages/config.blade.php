@extends('layouts.app')

@section('htmlheader_title')
Configurações Gerais
@endsection

@section('contentheader_title')
Configurações Gerais
@endsection

@section('contentheader_description')

@endsection


@section('main-content')

<?php
/*
  Array ( [id] => 1 [site_name] => Seven7club [site_url] => [logo_1] =>
 * [logo_2] => [logo_3] => [binario_valor] => 0.00 [indicacao] => 0.00 [limite_binario] => 6 [smtp_port] => 0 [smtp_host] => [smtp_user] => [smtp_pass] => 
 * [permitir_saque] => [saque_minimo] => [iugu_token] => [iugu_ambiente] => [gnt_status] => [iugu_status] => [deposito_status] =>
 */
?>

<div class="row">

    <section class="col-lg-6">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Geral</a></li>
                <li ><a href="#tab_2" data-toggle="tab" aria-expanded="false">Saque</a></li>
                <li ><a href="#tab_3" data-toggle="tab" aria-expanded="false">Meios de pagamentos</a></li>
                <li ><a href="#tab_4" data-toggle="tab" aria-expanded="false">Email</a></li>
                <li ><a href="#tab_5" data-toggle="tab" aria-expanded="false">Segurança</a></li>



            </ul>

            <form role="form" method="post" action="" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="box-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group has-feedback">
                                <p>Nome da empresa</p>
                                <input type="text" class="form-control" placeholder="" name="site_name" value="{{old('site_name') ? old('site_name') : $config['site_name']}}" required=""/>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Logo(150x41)</p>
                                <input type="file" class="form-control" placeholder="" name="logo_1" value="{{old('logo_1') ? old('logo_1') : $config['logo_1']}}"/>
                                <p><a  target="_blank" href="<?= $config['logo_1'] ?>">Imagem atual</a>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Favicon(32X32)</p>
                                <input type="file" class="form-control" placeholder="" name="logo_2" value="{{old('logo_2') ? old('logo_2') : $config['logo_2']}}"/>
                                <p><a  target="_blank" href="<?= $config['logo_2'] ?>">Imagem atual</a>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Background(1900x1280)</p>
                                <input type="file" class="form-control" placeholder="" name="logo_3" value="{{old('logo_3') ? old('logo_3') : $config['logo_3']}}"/>
                                <p><a  target="_blank" href="<?= $config['logo_3'] ?>">Imagem atual</a>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Termos de adesão</p>
                                <input type="file" class="form-control" placeholder="" name="termos" value="{{old('termos') ? old('termos') : $config['termos']}}"/>
                                <p><a  target="_blank" href="<?= $config['termos'] ?>">Arquivo atual</a>
                            </div>
                            <div class="form-group has-feedback">
                                <p> Exibir erros?</p>
                                <select name="exibir_erros" required="">
                                    <option value="<?= $config['exibir_erros'] ?>" selected=""><?= $config['exibir_erros'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>
                                </select>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Número de cadastros por CPF</p>
                                <input type="number" class="form-control" placeholder="" name="cpfCadastros" value="{{old('cpfCadastros') ? old('cpfCadastros') : $config['cpfCadastros']}}" required=""/>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Estilo do tema</p>
                                <ul class="list-unstyled clearfix"><li style="float:left; width: 33.33333%; padding: 5px;">
                                        <a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                            <div>
                                                <span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span>
                                                <span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                            </div>
                                            <div>
                                                <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>
                                                <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                            </div>
                                        </a>
                                        <p class="text-center no-margin">Blue</p>
                                    </li>
                                    <li style="float:left; width: 33.33333%; padding: 5px;">
                                        <a href="javascript:void(0);"  class="chgTema" class="chgTema" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                            <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
                                                <span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe;"></span>
                                                <span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe;"></span>
                                            </div>
                                            <div><span style="display:block; width: 20%; float: left; height: 20px; background: #222;"></span>
                                                <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div>
                                        </a><p class="text-center no-margin">Black</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin">Purple</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin">Green</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin">Red</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin">Yellow</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Blue Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe;"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Black Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Purple Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Green Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Red Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0);" class="chgTema" class="chgTema" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span></div></a><p class="text-center no-margin" style="font-size: 12px;">Yellow Light</p></li></ul>
                                <input type="hidden" name="tema_estilo" id="tema_estilo">
                            </div>

                            <div class="form-group has-feedback">
                                <p> Permitir suporte via chat?</p>
                                <select name="tawk_status" required="">
                                    <option value="<?= $config['tawk_status'] ?>" selected=""><?= $config['tawk_status'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>

                                </select>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Token Tawk</p>
                                <input type="text" class="form-control" placeholder="" name="tawk_token" value="{{old('tawk_token') ? old('tawk_token') : $config['tawk_token']}}" required=""/>
                                <p>Clique <a href="https://www.tawk.to/" target="_blank">aqui</a> para criar sua conta e obter um token</p>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Email do suporte</p>
                                <input type="email" class="form-control" placeholder="" name="email_suporte" value="{{old('email_suporte') ? old('email_suporte') : $config['email_suporte']}}" required=""/>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="form-group has-feedback">
                                <p>Permitir saque?</p>
                                <select name="permitir_saque" required="">
                                    <option value="<?= $config['permitir_saque'] ?>" selected=""><?= $config['permitir_saque'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>

                                </select>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Valor mínimo do saque</p>
                                <input type="number" class="form-control" placeholder="" name="saque_minimo" value="{{old('saque_minimo') ? old('saque_minimo') : $config['saque_minimo']}}" required=""/>
                                <p></p>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Valor máximo do saque</p>
                                <input type="number" class="form-control" placeholder="" name="saque_max" value="{{old('saque_max') ? old('saque_max') : $config['saque_max']}}" required=""/>
                                <p></p>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">

                            <div class="form-group has-feedback">
                                <p>Receber Pagamentos Via deposito?</p>
                                <select name="deposito_status" required="">
                                    <option value="<?= $config['deposito_status'] ?>" selected=""><?= $config['deposito_status'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>

                                </select>
                                <br>
                                <label>Dados das contas:</label>
                                <textarea name="deposito_contas">
                                    <?= $config['deposito_contas'] ?>
                                </textarea>
                                <br><br>
                                <p>Receber Pagamentos Via <a href="http://c.iugu.com/" target="_blank">Iugu</a>?</p>
                                <select name="iugu_status" required="">
                                    <option value="<?= $config['iugu_status'] ?>" selected=""><?= $config['iugu_status'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>
                                </select>
                                <div class="form-group has-feedback">
                                    <p>token<a href="http://c.iugu.com/" target="_blank">Iugu</a></p>
                                    <input type="text" class="form-control" placeholder="" name="iugu_token" value="{{old('iugu_token') ? old('iugu_token') : $config['iugu_token']}}"/>
                                    <p></p>
                                </div>

                                <p>Ambiente de teste ou de produção?<a href="http://c.iugu.com/" target="_blank">Iugu</a></p>
                                <select name="iugu_ambiente" required="">
                                    <option value="<?= $config['iugu_ambiente'] ?>" selected=""><?= $config['iugu_ambiente'] ?></option>
                                    <option value="teste">teste</option>
                                    <option value="produção">produção</option>
                                </select>
                                <br><br>
                                <p>Receber Pagamentos Via gerencianet?</p>
                                <select name="gnt_status" required="">
                                    <option value="<?= $config['gnt_status'] ?>" selected=""><?= $config['gnt_status'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>
                                </select>
                                <div class="form-group has-feedback">
                                    <p>Cliente id<a href="https://gerencianet.com.br/" target="_blank">Gerencianet</a></p>
                                    <input type="text" class="form-control" placeholder="" name="gnt_client" value="{{old('gnt_client') ? old('gnt_client') : $config['gnt_client']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Secret<a href="https://gerencianet.com.br/" target="_blank">Gerencianet</a></p>
                                    <input type="text" class="form-control" placeholder="" name="gnt_secret" value="{{old('gnt_secret') ? old('gnt_secret') : $config['gnt_secret']}}"/>
                                    <p></p>
                                </div>
                                <p>Ambiente de teste ou de produção?<a href="https://gerencianet.com.br/" target="_blank">Gerencianet</a></p>
                                <select name="gnt_ambiente" required="">
                                    <option value="<?= $config['gnt_ambiente'] ?>" selected=""><?= $config['gnt_ambiente'] ?></option>
                                    <option value="teste">teste</option>
                                    <option value="produção">produção</option>
                                </select>
                                <br><br>
                                <p>Receber Pagamentos Via <a href="https://bitzpayer.com/" target="_blank">Bitzpayer</a>?</p>
                                <select name="bitzpayer_status" required="">
                                    <option value="<?= $config['bitzpayer_status'] ?>" selected=""><?= $config['bitzpayer_status'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>
                                </select>
                                <div class="form-group has-feedback">
                                    <p>Login<a href="https://bitzpayer.com/" target="_blank">Bitzpayer</a></p>
                                    <input type="text" class="form-control" placeholder="" name="bitzpayer_login" value="{{old('bitzpayer_login') ? old('bitzpayer_login') : $config['bitzpayer_login']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Secret<a href="https://bitzpayer.com/" target="_blank">Bitzpayer</a></p>
                                    <input type="text" class="form-control" placeholder="" name="bitzpayer_secret" value="{{old('bitzpayer_secret') ? old('bitzpayer_secret') : $config['bitzpayer_secret']}}"/>
                                    <p></p>
                                </div>
                                <p>Ambiente de teste ou de produção?<a href="https://bitzpayer.com/" target="_blank">Bitzpayer</a></p>
                                <select name="bitzpayer_ambiente" required="">
                                    <option value="<?= $config['bitzpayer_ambiente'] ?>" selected=""><?= $config['bitzpayer_ambiente'] ?></option>
                                    <option value="teste">teste</option>
                                    <option value="produção">produção</option>
                                </select>
                                <br><br>





                            </div>


                        </div>
                        <div class="tab-pane" id="tab_4">

                            <div class="form-group has-feedback">
                                <p>Drive de email</p>
                                <select name="drive_email" required="">
                                    <option value="<?= $config['drive_email'] ?>" selected=""><?= $config['drive_email'] ?></option>
                                    <option value="smtp">smtp</option>
                                    <option value="mailgun">mailgun</option>
                                    <option value="mandrill">mandrill</option>
                                    <option value="ses">ses</option>
                                    <option value="mail">mail</option>

                                </select>

                                <div class="form-group has-feedback">
                                    <p>Email host</p>
                                    <input type="text" class="form-control" placeholder="" name="email_host" value="{{old('email_host') ? old('email_host') : $config['email_host']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Email port</p>
                                    <input type="text" class="form-control" placeholder="" name="email_port" value="{{old('email_port') ? old('email_port') : $config['email_port']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>From address</p>
                                    <input type="email" class="form-control" placeholder="" name="from_address" value="{{old('from_address') ? old('from_address') : $config['from_address']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>From name</p>
                                    <input type="text" class="form-control" placeholder="" name="from_name" value="{{old('from_name') ? old('from_name') : $config['from_name']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Cripitografia</p>
                                    <input type="text" class="form-control" placeholder="" name="email_encryption" value="{{old('email_encryption') ? old('email_encryption') : $config['email_encryption']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Email username</p>
                                    <input type="text" class="form-control" placeholder="" name="email_username" value="{{old('email_username') ? old('email_username') : $config['email_username']}}"/>
                                    <p></p>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Email pass</p>
                                    <input type="text" class="form-control" placeholder="" name="email_password" value="{{old('email_password') ? old('email_password') : $config['email_password']}}"/>
                                    <p></p>
                                </div>




                            </div>


                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_5">
                            <div class="form-group has-feedback">
                                <p>Permitir captcha?</p>
                                <select name="recaptcha_status" required="">
                                    <option value="<?= $config['recaptcha_status'] ?>" selected=""><?= $config['recaptcha_status'] ?></option>
                                    <option value="sim">sim</option>
                                    <option value="não">não</option>

                                </select>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Recaptcha site secret</p>
                                <input type="text" class="form-control" placeholder="" name="recaptcha_secret" value="{{old('recaptcha_secret') ? old('recaptcha_secret') : $config['recaptcha_secret']}}" required=""/>
                                <p></p>
                            </div>
                            <div class="form-group has-feedback">
                                <p>Recaptcha site key</p>
                                <input type="text" class="form-control" placeholder="" name="recaptcha_key" value="{{old('recaptcha_key') ? old('recaptcha_key') : $config['recaptcha_key']}}" required=""/>
                                <p></p>
                            </div>
                        </div>

                    </div>

                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Atualizar</button>
        </div>
        </form>
</div><!-- /.box -->

</section>

</div>

<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">


    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

@endsection

@section('page_scripts')
<!-- InputMask -->
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
$(function () {
    $('.data').inputmask("99-99-9999");
    $('.cpf').inputmask("999.999.999-99");
    $("[data-mask]").inputmask();
});
$(".chgTema").click(function () {
    $("#tema_estilo").val($(this).attr('data-skin'));
});
</script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({selector: 'textarea'});</script>
@endsection
