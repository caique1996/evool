@extends('layouts.app')

@section('htmlheader_title')
Anúncios
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Anúncios
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Anúncios
                </h3>
                <div class="col-lg-10">
                    <div class="col-md-2">
                        <div class="box-tools">
                            <div class="box-tools">
                                <a  href="{{url('/admin/anuncios?status=1')}}" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Anúncios Publicados</a>
                            </div>
                        </div><!-- /.box-tools -->
                    </div>
                    <div class="col-md-2">
                        <div class="box-tools">
                            <div class="box-tools">
                                <a  href="{{url('/admin/anuncios?status=2')}}" class="btn btn-sm btn-warning"><i class="fa fa-clock-o"></i>Anúncios Pendentes</a>
                            </div>
                        </div><!-- /.box-tools -->
                    </div>
                    <div class="col-md-2">
                        <div class="box-tools">
                            <div class="box-tools">
                                <a  href="{{url('/admin/anuncios?status=3')}}" class="btn btn-sm btn-danger"><i class="fa fa-close"></i>Anúncios Recusados</a>
                            </div>
                        </div><!-- /.box-tools -->
                    </div>

                    <div class="col-md-2">
                        <div class="box-tools">
                            <div class="box-tools">
                                <a  href="{{url('/admin/anuncios?status=4')}}" class="btn btn-sm btn-warning"><i class="fa fa-clock-o"></i>Anúncios Pausados</a>
                            </div>
                        </div><!-- /.box-tools -->
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAjax">

                </div>
                <?php
                $status = @$_GET['status'];
                if ($status == '') {
                    $status = 1;
                }
                $anuncios = \App\anuncios::where('status', $status)->get();

                function getStatus($status) {
                    if ($status == 1) {
                        return "<a   class='btn btn-sm btn-success'><i class='fa fa-check'></i>Publicado</a>";
                    }if ($status == 2) {
                        return "<a  class='btn btn-sm btn-warning'><i class='fa fa-clock-o'></i>Pendente</a>";
                    }
                    if ($status == 3) {
                        return "<a   class='btn btn-sm btn-danger' data-toggle='tooltip' title='Provavelmente esse anúncio foi recusado por não seguir as regras dos termos de uso.Leia as regras novamente'><i class='fa fa-close'></i>Recusado</a>";
                    }if ($status == 2) {
                        return "<a  class='btn btn-sm btn-warning'><i class='fa fa-clock-o'></i>Pausado</a>";
                    }
                }

                function getAcao($status) {
                    if ($status == 1) {
                        $res = "<a  class='btn btn-sm btn-info'><i class='fa fa-external-link'>Visualização</i></a>";
                        return $res;
                    }if ($status == 2) {
                        $res = "<a   class='btn btn-sm btn-info'><i class='fa fa-facebook'></i>Compartilhamento</a>";
                        return $res;
                    }
                }

                $usr = new App\User();
                ?>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Propietário</th>
                            <th>Título</th>
                            <th>Link</th>
                            <th>Visualizações</th>
                            <th>Ação</th>
                            <th>Status</th>
                            <th>Operação</th>


                        </tr>
                    </thead>
                    <tbody>
                        @foreach($anuncios as $anuncio)
                        <?php
                        $usrInfo = $usr->userInfo($anuncio['user_id']);
                        $pacoteInf = App\pacoteViews::where('id', $anuncio['pacote_id'])->first();
                        ?>
                        <tr>
                            <td>{{$anuncio['id']}}</td>
                            <td>

                                {{$usrInfo['username']}}
                            </td>
                            <td>
                                {{$anuncio['nome']}}
                            </td>
                            <td>
                                <a href='{{$anuncio['url']}}' target="_blank">Visualizar</a>
                            </td>
                            <td>
                                {{$anuncio['visualizacoes']}} de {{$anuncio['visualizacoes_restante']}}
                            </td>
                            <td><?php echo getAcao($anuncio['acao']); ?></td>
                            <td><?php echo getStatus($anuncio['status']); ?></td>
                            <td>
                                <a href="javascript:void(0);" data-href='{{url('/admin/anuncio/')}}/{{$anuncio['id']}}' target="_blank"  class='btn btn-sm btn-info openModal' ><i class='fa fa-edit'></i>Editar</a>
                            </td>



                            @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('/plugins/form/jquery.form.min.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<script>
function status_recusado(elemento) {
    status = $(elemento).val();
    if (status == 3) {
        $("#anotacoes").attr('required', true);
        $("#anuncio_recu").show();
    } else {
        $("#anotacoes").hide();
        $(elemento).removeAttr('required');



    }
}
$("#myModal").mouseover(function () {
    tinymce.init({selector: 'textarea', plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ], setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
});
$(function () {

    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    $(".delete").click(function () {
        fBlockUi();
        $.ajax({
            url: '{{url(' / admin / pacote')}}' + '/' + $(this).attr('data-id'),
            method: 'DELETE',
            data: {_token: "<?php echo csrf_token(); ?>"},
            type: 'DELETE',
            success: function (result) {
                $("#mensagemAjax").html(result);
                $.unblockUI();
                window.setTimeout('location.reload()', 1500); //Reloads after three seconds
            }
        });
    });
    $(".openModal").on('click', function () {

        $('#myModal').removeData('bs.modal');
        $('#myModal').modal(
                {
                    remote: $(this).attr('data-href')
                }
        );
        $('#myModal').modal('show');
        $('#myModal').on('loaded.bs.modal', function (e) {

// bind form using ajaxForm
            var form = $('.formAjax').ajaxForm({
// target identifies the element(s) to update with the server response
                target: '#mensagemAjax',
                beforeSubmit: function () {
                    fBlockUi();
                    $('#myModal').modal('hide');
                    $('input').attr('disabled', true);
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAjax').clearForm();
                    $('#mensagemAjax').fadeIn('slow');
                    $.unblockUI();
                    $('input').attr('disabled', false);
                    window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                }
            });
        });
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});

</script>
@endsection
