@extends('layouts.app')

@section('htmlheader_title')
Pedidos
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Todos os pedidos
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box">
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="ord-carrinho" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Dados do usuário</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Status</th>
                            <th>Mensagem</th>
                            <th>Ação</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $usr = new App\User();
                        $pedidos = App\Pedidos::all();
                        ?>
                        @foreach($pedidos as $pedido)
                        <?php
                        $userInfo = $usr->userInfo($pedido['user_id']);
                        $endereco = App\Enderecos::where('id', $pedido['id_endereco'])->first();
                        ?>
                        <tr>
                                                        <td>{{$pedido['id']}}</td>

                            <td>Nome: {{$userInfo['name']}}<br>Email:{{$userInfo['email']}}<br>Telefone:{{$userInfo['telefone']}}<br>
                                <br>Endereço de entrega: {{$endereco['pais']}},{{$endereco['estado']}},{{$endereco['cidade']}},{{$endereco['endereco']}}-{{$endereco['cep']}}<br>

                            <td>{{Carbon\Carbon::parse($pedido['date'])->format('d/m/Y')}}</td>
                            <td>{{number_format($pedido['preco'], 2)}}</td>
                            <td>{{$pedido['status']}}</td>
                            <td><?= $pedido['info'] ?></td>

                            <td><a href = "{!! url('admin/pedido/edit/'.$pedido['id']) !!}" class = "btn btn-info">Editar</a><a href="{!! url('painel/meus-pedidos/pedido/'.$pedido['id']) !!}" class="btn btn-info">Detalhes</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!--/.box-body -->
        </div><!--/.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!--DataTables -->
<script src = "{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function () {

    $('#ord-carrinho').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "oLanguage": {"sZeroRecords": "Você ainda não realizou nenhum pedido",
            "sEmptyTable": "Você ainda não realizou nenhum pedido"},
        "order": [[1, "desc"]]
    });
});
</script>
@endsection
