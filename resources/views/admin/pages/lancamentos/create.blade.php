<form class="formAjax" method="post" action="{{url('/admin/lancamentos')}}">
    {{ csrf_field() }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lancamento</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Ex: Pagamento de Maria" name="nome"/>
        </div>
        <div class="form-group">
            <label>Descrição</label>
            <textarea class="form-control" name="descricao">
                
            </textarea>
        </div>
        <div class="form-group">
            <label>Valor</label>
            <input type="number" step="any" class="form-control" required placeholder="Ex: 500" name="valor"/>
        </div>
     
        <div class="form-group">
            <label>Tipo</label>

            <select class="form-control"  name="tipo">
                <option value="gasto" selected="">gasto</option>
                <option value="ganho" selected="">ganho</option>

            </select>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>