<form class="formAjax" method="post" action="{{url('/admin/anuncios')}}">

    {{ csrf_field() }}

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Anuncio</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="nome"/>
        </div>
        <div class="form-group">
            <label>Descrição </label>
            <textarea class="form-control" required placeholder="Descrição" name="descricao">
                
            </textarea>
        </div>

        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>Binário </label>
            <select class="form-control" name="binario">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>Pontos Binários</label>
            <input type="number" class="form-control" step="any" required placeholder="Pontos Binários" name="porcentagem_bin"/>
            <p>* os valores são em porcentagem</p>

        </div>

        <div class="form-group">
            <label>Pontos de consumo</label>
            <input type="text" class="form-control" step="any" required placeholder="Pontos de consumo" name="pontos_consumo"/>

        </div>
        <div class="form-group">
            <label>Valor da indicação</label>
            <input type="text" class="form-control" step="any" required placeholder="Valor da indicação" name="porcentagem"/>
            <p>* os valores são em porcentagem</p>
        </div>

        <?php
        for ($i = 1; $i <= 14; $i++) {
            echo '<div class = "form-group">';
            echo"<label>{$i}º Nível</label>";
            echo "<input  type='number' step='any' class = 'form-control money'  placeholder = '{$i}º Nível' name ='nivel{$i}'/>
            </div";
        }
        ?>




        <div class="form-group">
            <label>Valor</label>
            <input type="number" step="any" class="form-control" required placeholder="Valor" name="valor" id='valor'/>
        </div>
        <div class="form-group">
            <label>Validade em meses</label>
            <input type="number" class="form-control" required  name="validade" id='validade'/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>


