<form class="formAjax" method="post" action="{{url('/admin/anuncio/salvar')}}">

    {{ csrf_field() }}
    <?php

    function getAcao($status) {
        if ($status == 1) {
            return 'Visualização';
        }if ($status == 2) {
            return 'Compartilhamento';
        }
    }

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Publicado';
            }if ($status == 2) {
                return 'Pendente';
            }if ($status == 3) {
                return 'Recusado';
            }
            if ($status == 4) {
                return 'Pausado';
            }
        } else {
            if ($status == 1) {
                return 'Publicado';
            }if ($status == 2) {
                return 'Pendente';
            }if ($status == 3) {
                return 'Recusado';
            }
            if ($status == 4) {
                return 'Pausado';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Anuncio</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <input type="hidden" class="form-control"  name="id" value="<?= $dados['id'] ?>"/>

            <label>Título</label>
            <input type="text" class="form-control" required placeholder="Nome" name="nome" value="<?= $dados['nome'] ?>"/>
        </div>
        <div class="form-group">
            <input type="hidden" class="form-control"  name="id" value="<?= $dados['id'] ?>"/>
            <label>Link</label>
            <input type="url" class="form-control" required placeholder="Link" name="url" value="<?= $dados['url'] ?>"/>
        </div>

        <div class="form-group">
            <label>Visualizações restantes</label>
            <input type="number" class="form-control" required placeholder="Visualizações" name="visualizacoes_restante" value="<?= $dados['visualizacoes_restante'] ?>"/>
        </div>

        <div class="form-group">
            <label>Ação </label>
            <select class="form-control" name="acao" id="acao" >
                <option selected="" value="<?= $dados['acao'] ?>"><?= getAcao($dados['acao']) ?></option>
                <option value="1">Visualização</option>
                <option value="2">Compartilhamento</option>
            </select>
        </div>
        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status" id="status" onchange="status_recusado(this)">
                <option selected="" value="<?= $dados['status'] ?>"><?= getStatus($dados['status']) ?></option>
                <option value="1">Publicado</option>
                <option value="2">Pendente</option>
                <option value="3">Recusado</option>
                <option value="4">Pausado</option>

            </select>
        </div>
        <div class="form-group" id="anuncio_recu" style="display: none;">
            <div class="form-group">
                <label>Por que esse anúncio esta sendo recusado?</label>
                <textarea name="anotacao" >
                    <?= $dados['anotacoes'] ?> 
                </textarea>
            </div>
        </div>



    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </div>
</form>
<script>

</script>


