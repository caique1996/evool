<form class="formAjax" method="post" action="{{url('/admin/trade')}}">

    {{ csrf_field() }}

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Trade</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control"  placeholder="Username" name="username" required=""/>
        </div>
        <div class="form-group">
            <label>Nome de exibição</label>
            <input type="text" class="form-control"  placeholder="Nome de exibição" name="nome_exibicao" required=""/>
        </div>


        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>

