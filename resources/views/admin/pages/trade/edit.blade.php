<form class="formAjax" method="post" action="{{url('/admin/trade/salvar')}}">

    {{ csrf_field() }}
    <?php

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Trade</h4>

    </div>
    <div class="modal-body">
        <input type="hidden" class="form-control"  name="id" value="<?= $dados['id'] ?>"/>

        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" value="<?= $dados['username'] ?>"  placeholder="Username" name="username" required=""/>
        </div>
        <div class="form-group">
            <label>Nome de exibição</label>
            <input type="text" class="form-control" value="<?= $dados['nome_exibicao'] ?>"  placeholder="Nome de exibição" name="nome_exibicao" required=""/>
        </div>


        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option selected="" value="<?= $dados['status'] ?>"><?= getStatus($dados['status']) ?></option>
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>

