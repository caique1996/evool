@extends('layouts.app')
@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('htmlheader_title')
Graduações
@endsection

@section('contentheader_title')
Graduações
@endsection

@section('breadcrumb')
<li class="active">Graduações</li>
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> Lista de Graduações</h3>
                <div class="box-tools pull-right">
                    <a href="javascript:void(0);" data-href="{{url('/admin/graduacao/create')}}" class="btn btn-sm btn-primary openModal"><i class="fa fa-plus"></i> Adicionar</a>
                </div>
            </div>
            <div class="box-body">
                <div id="mensagemAjax">

                </div>
                <table id="example2" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>Ícone</th>
                            <th>Nome</th>
                            <th>Status</th>
                            <th>Pontuação</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($graduacoes as $graduacao)
                        <tr>
                            <td>{{$graduacao->id}}</td>               
                            <td><img src="{{$graduacao->icone}}" width="40"></td>

                            <td>{{$graduacao->name}}</td>
                            <td><?=Auth::user()->getStatus($graduacao->status)?></td>
                            <td>{{$graduacao->pontuacao}}</td>
                            <td><a href="javascript:void(0);" data-href="{{url('/admin/graduacao/'.$graduacao->id)}}" class="btn btn-sm btn-info openModal"><i class="fa fa-edit"></i> Editar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.Left col -->

    {{ method_field('PUT') }}
</div><!-- /.row (main row) -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection


@section('page_scripts')
<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('/plugins/form/jquery.form.min.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
$(function () {

    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    $(".delete").click(function () {
        fBlockUi();
        $.ajax({
            url: '{{url(' / admin / pacote')}}' + '/' + $(this).attr('data-id'),
            method: 'DELETE',
            data: {_token: "<?php echo csrf_token(); ?>"},
            type: 'DELETE',
            success: function (result) {
                $("#mensagemAjax").html(result);
                $.unblockUI();
                window.setTimeout('location.reload()', 1500); //Reloads after three seconds
            }
        });
    });

    $(".openModal").on('click', function () {

        $('#myModal').removeData('bs.modal');

        $('#myModal').modal(
                {
                    remote: $(this).attr('data-href')
                }
        );
        $('#myModal').modal('show');

        $('#myModal').on('loaded.bs.modal', function (e) {

            // bind form using ajaxForm
            var form = $('.formAjax').ajaxForm({
                // target identifies the element(s) to update with the server response
                target: '#mensagemAjax',
                beforeSubmit: function () {
                    tinyMCE.triggerSave();
                    fBlockUi();
                    $('#myModal').modal('hide');
                    $('input').attr('disabled', true);
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAjax').clearForm();
                    $('#mensagemAjax').fadeIn('slow');
                    $.unblockUI();
                    $('input').attr('disabled', false);
                    window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                }
            });

        });


    });
    $("#myModal").mouseover(function () {
        tinymce.init({selector: 'textarea', plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ], setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            }
        });
    });

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
</script>
@endsection