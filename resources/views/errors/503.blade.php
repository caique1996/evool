<!DOCTYPE html>
<html class="csstransforms csstransforms3d csstransitions no-js skrollr skrollr-desktop" lang="en">
<head>
    <title>Seven - Marketing & Network</title>
    <meta charset="utf-8"/>
    <meta property="og:image" content="http://www.sevenbrasil.co/site/images/facebook.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- favicon -->
    <link rel="shortcut icon" href="{{'/site/images/favicon.png'}}"/>
    <!-- google font -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"/>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald:300,400,700"/>
    <!-- font awesome-->
    <link rel="stylesheet" type="text/css" href="{{'/site/css/font-awesome.min.css'}}"/>
    <!-- Animation -->
    <link rel="stylesheet" type="text/css" id="animationcss" href="{{'/site/css/animation.css'}}"/>
    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{'/site/css/bootstrap.css'}}"/>
    <!-- custom style -->
    <link rel="stylesheet" type="text/css" href="{{'/site/css/style.css'}}"/>
    <!-- custom scrollbar -->
    <link rel="stylesheet" type="text/css" href="{{'/site/css/custom-scrollbar.cs'}}s">
    <!-- color style -->
    <link rel="stylesheet" type="text/css" href="{{'/site/css/colors/yellow.css'}}"/>
    <!-- responsive -->
    <link rel="stylesheet" type="text/css" href="{{'/site/css/responsive.css'}}">
    <!-- yellow-responsive -->
    <link rel="stylesheet" type="text/css" href="{{'/site/css/colors/yellow-responsive.css'}}">
    <style>
        .bg_video {
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: -1000;
            background: url(site/images/preto.jpg) no-repeat;
            background-size: cover;
        }

        .body {
            padding: 20px;
            background: rgba(255, 255, 255, 0.9);
            margin: 30% auto 20px auto;
            max-width: 960px;
            border-radius: 10px;
        }</style>
</head>

<body>
<video autoplay class="bg_video">
    <source src="https://s3-sa-east-1.amazonaws.com/seven-mn/bg.mp4" type="video/mp4">
</video>
<!-- main page -->
<div class="main">
    <!-- header-->
    <header id="home" data-stellar-background-ratio="0.5" class="header animated no-background">
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav" role="navigation">
            <div class="container main-navigation">
                <div class="col-md-3 float-left"><a class="logo-dark" href="#home"><img alt="logo-dark" src="/site/images/logo.png" class="logo-dark"/></a> <a class="logo-light" href="#home"><img alt="logo-white" src="/site/images/logo-white.png" class="logo-white"/></a></div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="sr-only">Menu</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                </div>
                <div class="col-md-9 text-left float-right collapse-navation">
                    <div class="navbar-collapse collapse navbar-inverse no-transition">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="scroll-menu" href="#sobre">CONCEITO</a></li>
                            <li><a class="scroll-menu" href="#mercado">O MERCADO</a></li>
                            <li><a class="scroll-menu" href="#produtos">PRODUTOS</a></li>
                            <li><a class="scroll-menu" href="#oportunidade">OPORTUNIDADE</a></li>
                            <li><a class="redirect-link" href="https://sevenbrasil.co/painel">PLATAFORMA VIRTUAL</a></li>
                            <li><a class="scroll-menu" href="#contato">CONTATO</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="home-slider full-screen">
            <div class="black-overlay-dark"></div>
        </div>
    </header>
    <!-- header end -->

    <!-- about -->
    <section id="sobre" class="gray-bg">
        <div class="container">
            <div class="row border-bottom os-animation text-center" data-os-animation="fadeInUp">
                <div class="col-md-6 col-sm-6 title-text border-right">
                    <h2 class="title">INSTITUCIONAL</h2>
                </div>
                <div class="col-md-6 col-sm-6 simple-text">
                    <p class="description text-left">Através da junção de dois grandes mercados, criamos um sistema de comissionamento inteligente, onde oferecemos uma oportunidade de transformação financeira e social para nossos associados.</p>
                </div>
            </div>
            <div class="row border-bottom os-animation" data-os-animation="fadeInUp">
                <div class="col-md-3 col-sm-3 service-box border-right text-center">
                    <div class="service-icon"><i class="fa fa-bell-o"></i></div>
                    <h3>Oportunidade</h3>
                    <br>
                    <p class="content">Esta é a sua grande oportunidade de conquistar a liberdade financeira ajudando outras pessoas a terem uma vida ainda melhor.</p>
                    <br/>
                </div>
                <div class="col-md-3 col-sm-3 service-box border-right text-center">
                    <div class="service-icon"><i class="fa fa-check"></i></div>
                    <h3>Segurança</h3>
                    <br>
                    <p class="content">Conte com uma empresa segura, que maior objetivo é presar por
                        um modelo de negócios sustentável, visando a segurança de nossos associados. </p>
                </div>
                <div class="col-md-3 col-sm-3 service-box border-right text-center">
                    <div class="service-icon"><i class="fa fa-cog"></i></div>
                    <h3>Competência</h3>
                    <br/>
                    <p class="content">Uma organização sólida, formada por profissionais competentes,
                        com vasta dedicados e comprometidos com o seu sucesso do seu negócio. </p>
                    <br/>
                </div>
                <div class="col-md-3 col-sm-3 service-box text-center">
                    <div class="service-icon"><i class="fa fa-arrow-up"></i></div>
                    <h3>Resultado</h3>
                    <br>
                    <p class="content">Criamos um negócio perfeito, com o objetivo e a capacidade de trazer resultado e mudança de vida aos nossos associados. </p>
                    <br/>
                </div>
            </div>
        </div>
    </section>
    <!-- about end -->

    <!-- about2 -->
    <section id="mercado" class="gray-bg">
        <div class="container">
        </div>
        <div class="row margin-top-70 about-details os-animation animated" data-os-animation="fadeInUp">
            <div class="no-padding-right about-details-img"></div>
            <div class="col-md-6 col-sm-6 dark-gray-bg no-padding-left padding-70 float-right">
                <div class="col-md-9 col-sm-9">
                    <h6 class="white-text"><font color="#ffcc00">O mercado anti-crise</font></h6>
                    <div class="about-con margin-top">
                        <ul class="white-text">
                            <li>Conhecido por ser o mercado que muda vidas, o modelo de negócios do Marketing de Relacionamentos não exige diploma, e não se importa com sua classe social, formação ou ocupação principal. <br>
                                Através de um sistema inovador de distribuição de produtos e serviços, pagamos pessoas para falar sobre nós, trazendo diversos benefócios para a empresa, e para nossos associados.
                                <br>
                                <br>
                                <b>FLEXIBILIDADE:</b> Você não irá precisar parar sua ocupação principal para construir uma renda extra
                                <br>
                                <br>
                                <b>MODELO EM ALTA:</b> Representa certa de 25% do PIB dos Estados Unidos, e 16% do PIB do Japão
                                <br>
                                <br>
                                <b>MUDANÇA SOCIAL:</b> 25% dos milionários dos EUA construiram sua fortuna através desse mercado
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- about2 end -->

    <!-- about3 -->
    <section class="gray-bg">
        <div class="container">
        </div>
        <div class="row margin-top-70 about-details os-animation animated" data-os-animation="fadeInUp">
            <div class="no-padding-right about-details-img-2 float-right"></div>
            <div class="col-md-6 col-sm-6 dark-gray-bg no-padding-right padding-70 float-left">
                <div class="col-md-9 col-sm-9">
                    <h6 class="white-text"><font color="#ffcc00">O futuro do marketing</font></h6>
                    <div class="about-con margin-top">
                        <ul class="white-text">
                            <li>O marketing digital para empresas aumenta o poder competitivo das corporações, abrindo frentes para atingir e conseguir novos clientes. <br>
                                Dessa forma, empresas que utilizam estratégias e ferramentas do marketing digital, mesmo que seja uma empresa de pequeno porte, torna-se mais competitiva no mercado, do que as que não o fazem.
                                As campanhas online poupam dinheiro, geram mais resultados, e trazem o consumidor mais perto, sendo possível interagir diretamente com o cliente e ter um feedback em tempo real, além de métricas precisas, que indicam
                                exatamente o comportamento do público em relação a determinada campanha.
                                <br><br>
                                Através das nossas ferramentas, você poderá levar a sua empresa para dentro da casa do seu cliente.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- about3 end -->


    <!-- expertise -->
    <section id="produtos" class="contact gray-bg">
        <div class="container">
            <div class="row border-bottom os-animation text-center" data-os-animation="fadeInUp">
                <h2 align="center">PRODUTOS</h2>
                <p>"Em algum tempo existirão dois tipos de empresas. As que fazem negócios pela internet, e as que não fazem negócios." - Bill Gates</p>
                <br>
                <br>
            </div>
            <div class="row feature-content os-animation" data-os-animation="fadeInUp">
                <div class="col-md-4 col-sm-4 text-justify no-padding">
                    <div class="feature">
                        <div class="icon-container"><i class="fa fa-thumbs-up"></i><br></div>
                        <div class="fetaure-details">

                            <h5 class="title">Seven <span>likes e views.</span></h5><br>
                            <p class="content">Diariamente nossos associados realizam a divulgação e visualização de fanpages e websites de nossos parceiros, dando a possibilidade do anunciante escolher o estado ou cidade onde o anuncio irá vincular.</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 text-justify no-padding">
                    <div class="feature">
                        <div class="icon-container"><i class="fa fa-facebook"></i><br></div>
                        <div class="fetaure-details">

                            <h5 class="title">Automação <span>em facebook.</span></h5><br>
                            <p class="content">O SevenSoft funciona como um funcionário virtual. Através dele você poderá divulgar em milhares de grupos diariamente, e enviar mensagens inbox direto na caixa de entrada dos membros.</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 text-justify no-padding">
                    <div class="feature">
                        <div class="icon-container"><i class="fa fa-mobile"></i><br></div>
                        <div class="fetaure-details">

                            <h5 class="title">Plataforma <span>sms marketing.</span></h5><br>
                            <p class="content">A SevenSMS permite que o cliente envie mensagens de texto de forma simples e prática, para moradores de um estado, cidade, ou bairro, trazendo uma alta taxa de segmentação de público.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- expertise end -->

        <!-- about -->
        <section id="oportunidade" class="white-bg">
            <div class="container">
                <div class="row border-bottom os-animation text-center" data-os-animation="fadeInUp">
                    <div class="col-md-6 col-sm-6 title-text border-right">
                        <h2 class="title">OPORTUNIDADE</h2>
                    </div>
                    <div class="col-md-6 col-sm-6 simple-text">
                        <p class="description text-left">O rio da oportunidade passa ao menos uma vez na vida de cada pessoa. O que diferencia uns aos outros, é que alguns o recebem com uma colher, enquanto outros, com um balde.</p>
                    </div>
                </div>
                <div class="row feature-content os-animation" data-os-animation="fadeInUp">
                    <div class="col-md-6 col-sm-6">
                        <div class="feature">
                            <div class="icon-container"><i class="fa fa-file-pdf-o"></i></div>
                            <div class="fetaure-details">
                                <h4 class="title">Apresentação <span>de negócios.</span></h4>
                                <p class="content"><a target="_blank" href="https://s3-sa-east-1.amazonaws.com/seven-mn/apresentacao-de-negocios.pdf">Clique aqui para acessar a apresentação de negócios Seven, um modelo que vem mudando vidas em todo o mundo, e pode mudar a
                                        sua!</a></p>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="feature">
                            <div class="icon-container"><i class="fa fa-lock"></i></div>
                            <div class="fetaure-details">
                                <h4 class="title">Acesso a <span>platafomra virtual.</span></h4>
                                <p class="content"><a target="_blank" href="https://sevenbrasil.co/painel">Você já faz parte do nosso time de associados Seven? Para acessar sua plataforma virtual, basta clicar aqui! </a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about end -->


        <!-- counter -->
        <section id="conter-box" class="conter-box">
            <div class="color-overlay" style="background-image: url('/site/images/img.png'); background-size: cover; position: relative; min-height: 520px;">
                <div class="container">
                    <div class="row os-animation" data-os-animation="fadeInUp">
                    </div>
                </div>
            </div>
        </section>
        <!-- counter -->


        <!-- contact -->
        <section id="contato" class="contact gray-bg">
            <div class="container">
                <div class="row border-bottom os-animation text-center" data-os-animation="fadeInUp">
                    <div class="col-md-6 col-sm-6 title-text border-right">
                        <h2 class="title">FALE CONOSCO</h2>
                    </div>
                    <div class="col-md-6 col-sm-6 simple-text">
                        <p class="description text-left">Através do formulário abaixo você poderá entrar em contato com a nossa equipe de suporte. Use-o para enviar soluções, criticas e sugestões.</p>
                    </div>
                </div>
                <div class="row contact-info os-animation" data-os-animation="fadeInUp">
                    <div class="col-md-6 col-sm-6 left-part">
                        <div class="head"><span class="contact-title">Estamos prontos para lhe atender!</span>
                            <p class="content contact-text">Nossa equipe de suporte funciona em horário comercial. Caso envie sua mensagem após as 19:00hs UTF-Brasilia, responderemos no dia seguinte.</p>
                        </div>
                        <ul class="icon-list">
                            <li class="divider"></li>
                            <li><i class="fa fa-envelope-o"></i><a href="mailto:contato@sevenbrasil.co">contato@sevenbrasil.co</a></li>
                            <li class="divider"></li>
                            <li><i class="fa fa-globe"></i><a href="javascript:;">www.sevenbrasil.co</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 right-part">
                        <div class="form-group">
                            <form action="javascript:void(0)" method="post">
                                <input class="form-control" name="name" placeholder="Nome" type="text">
                                <input class="form-control" name="email" placeholder="Email" type="text">
                                <input class="form-control" name="website" placeholder="Telefone" type="text">
                                <textarea class="form-control" placeholder="Mensagem" name="comment"></textarea>
                                <div align="center">
                                    <button id="submit-button" class="small-button text-left">Enviar mensagem</button>
                                </div>
                            </form>
                        </div>
                        <div id="success"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- contact end -->


        <!-- map -->
        <section class="map">
            <div class="map-overlay">
                <button class="map-button" value="Show map">AONDE ESTAMOS?</button>
            </div>
            <div id="googlemap">
                <div id="map" data-address="Alameda Jaú, 4003" data-text="<p><strong>Seven Brasil.</strong><br>São Paulo, Brasil.</p>"></div>
            </div>
        </section>
        <!-- map end -->


        <!-- scroll to top -->
        <a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
        <!-- scroll to top End... -->

</div>
<!-- main page end -->
<!-- javascript  -->
<!-- Placed at the End of the document so the pages load faster -->
<!-- jQuery -->
<script src="{{env('CFURL').'/site/js/jquery.js'}}"></script>
<!-- page loading -->
<script src="{{env('CFURL').'/site/js/loader.min.js'}}"></script>
<!-- smoth hover -->
<script src="{{env('CFURL').'/site/js/hover.min.js'}}"></script>
<!-- parallax background -->
<script src="{{env('CFURL').'/site/js/parallel.min.js'}}"></script>
<!-- jQuery UI -->
<script src="{{env('CFURL').'/site/js/jquery-ui.min.js'}}"></script>
<!-- modernizr -->
<script src="{{env('CFURL').'/site/js/modernizr.custom.js'}}"></script>
<!-- smooth page scrolling -->
<script src="{{env('CFURL').'/js/smoothscroll.js'}}"></script>
<!-- custom javascript  -->
<script src="{{env('CFURL').'/site/js/custom.js'}}"></script>
<!-- counter  -->
<script src="{{env('CFURL').'/site/js/conter.js'}}"></script>
<!-- bootstrap  -->
<script src="{{env('CFURL').'/site/js/bootstrap-custom.js'}}"></script>
<!-- custom scrollbar  -->
<script src="{{env('CFURL').'/site/js/custom-scrollbar.min.js'}}"></script>
<!-- google map API -->
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=true"></script>
<script src="{{env('CFURL').'/site/js/map.js'}}"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-72220910-1', 'auto');
    ga('send', 'pageview');

</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?3hy7UH0L8U2FzidJDmQA1Wcfv1FB1rjh";z.t=+new Date;$.
                type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>