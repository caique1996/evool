<!-- Left side column. contains the logo and sidebar -->
<?php
global $request;
$uriPainel = $request->segment(1);
$uriPage = $request->segment(2);
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest() && !Auth::user()->ativo)
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{Auth::user()->photo}}" width="64" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->getShortName()}}</p>
                <!-- Status -->
                <a href="#"><?= Auth::user()->minhaGraduacao() ?></a>
            </div>
        </div>
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" disabled placeholder="Pesquisar..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{$uriPage == '' ? 'active' : ''}}"><a href="javascript:$('#enviarComprovante').modal();"><i class='fa fa-upload'></i> <span>Enviar comprovante</span></a></li>

        </ul>

        @endif
        @if (! Auth::guest() && Auth::user()->ativo)
        <div class="user-panel">
            <div class="pull-left image">
                <img onclick="" class="img-circle" src="{{Auth::user()->photo}}" width="128"">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->getShortName()}}</p>
                <!-- Status -->
                <a href="#"><?= Auth::user()->minhaGraduacao() ?></a>


            </div>
        </div>
        @endif


        @if(Auth::user()->ativo)
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" disabled placeholder="Pesquisar..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>
            <!-- Optionally, you can add icons to the links -->

            <li class="{{$uriPage == 'home' ? 'active' : ''}}"><a href="{{ url('/'.$uriPainel.'/home') }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>

            @if (! Auth::guest()  && $uriPainel == 'painel')
            <li class="{{$uriPage == 'meus-dados' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-dados')}}"><i class='fa fa-user'></i> <span>Meus Dados</span></a></li>
            <li class="treeview"  class="{{$uriPage == 'minha-rede' ? 'active' : ''}}{{$uriPage == 'meus-indicados' ? 'active' : ''}}{{$uriPage == 'unilevel' ? 'active' : ''}}">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Minha Rede</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{$uriPage == 'minha-rede' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/minha-rede')}}"><i class='fa fa-circle-o'></i> <span>Minha rede</span></a></li>

                    <li class="{{$uriPage == 'meus-indicados' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-indicados')}}"><i class='fa fa-circle-o'></i> <span>Indicados Diretos</span></a></li>

                    <li class="{{$uriPage == 'unilevel' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/unilevel')}}"><i class='fa fa-circle-o'></i> <span>Rede Unilevel </span></a></li>
                </ul>
            </li>
            <?php if (env('lv_BTNvZfXgG4b5JPnD') == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-shopping-cart "></i> <span>Loja Virtual</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{$uriPage == 'produtos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos')}}"><i class="fa fa-circle-o"></i> Produtos</a></li>
                        <li class="{{$uriPage == 'meu-carrinho' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meu-carrinho')}}"><i class="fa fa-circle-o"></i> Meu carrinho</a></li>
                        <li class="{{$uriPage == 'meus-pedidos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-pedidos')}}"><i class='fa fa-truck '></i> <span>Meus pedidos</span></a></li>
                    </ul>
                </li>
            <?php } ?>



            <li class="treeview"  class="{{$uriPage == 'transacoes' ? 'active' : ''}}{{$uriPage == 'faturas' ? 'active' : ''}}{{$uriPage == 'saques' ? 'active' : ''}}{{$uriPage == 'gerenciar_saldo' ? 'active' : ''}}">
                <a href="#">
                    <i class="fa fa-dollar"></i> <span>Financeiro</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{$uriPage == 'vouchers' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/vouchers')}}"><i class='fa fa-circle-o'></i> <span>Vouchers</span></a></li>

                    <li class="{{$uriPage == 'transacoes' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/transacoes')}}"><i class='fa fa-circle-o'></i> <span>Extrato financeiro</span></a></li>
                    <li class="{{$uriPage == 'pontos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/pontos')}}"><i class='fa fa-circle-o'></i> <span>Extrato de Pontos</span></a></li>
                 <!--   <li class="{{$uriPage == 'faturas' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/faturas')}}"><i class='fa fa-circle-o'></i> <span>Faturas</span></a></li>-->
                    <li class="{{$uriPage == 'saques' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/saques')}}"><i class='fa fa-circle-o'></i> <span>Saques</span></a></li>
                    <li class="{{$uriPage == 'gerenciar_saldo' ? 'active' : ''}}"><a href="javascript:$('#converterSaldo').modal();""><i class='fa fa-circle-o'></i> <span>Converter Saldo</span></a></li>
                    <li class="{{$uriPage == 'gerenciar_saldo' ? 'active' : ''}}"><a href="javascript:$('#transferirSaldo').modal();""><i class='fa fa-circle-o'></i> <span>Transferir Saldo</span></a></li>

                    <li class=""><a  href="javascript:$('#ativarUsuario').modal();"><i class='fa fa-circle-o'></i> <span>Ativar Usuário</span></a></li>
                    <li class="{{$uriPage == 'gerenciar_saldo' ? 'active' : ''}}"><a href="javascript:$('#pagarSaldo').modal();""><i class='fa fa-circle-o'></i> <span>Pagar fatura com saldo</span></a></li>

                </ul>
            </li>

            <li class="{{$uriPage == 'landingpage' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/landingpage')}}"><i class='fa fa-trophy'></i> <span>Meu site</span></a></li>



            <li class="{{$uriPage == 'upgrade' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/upgrade')}}"><i class='fa fa-trophy'></i> <span>Upgrade</span></a></li>

            <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/materiais')}}"><i class='fa fa-folder'></i> <span>Materiais</span></a></li>
            <?php
            $config = \App\config::all()->first();
            if ($config['tawk_status'] == 'sim' and $config['tawk_token'] <> '') {
                ?>
                <li ><a href="javascript:Tawk_API.toggle();"><i class='fa fa-support'></i> <span>Suporte</span></a></li>
            <?php } else { ?>

                <li ><a href="mailto:<?= $config['email_suporte'] ?>"><i class='fa fa-support'></i> <span>Suporte</span></a></li>

            <?php } ?>
            <li class="{{$uriPage == 'graduacoes' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/graduacoes')}}"><i class='fa fa-graduation-cap'></i> <span>Graduações</span></a></li>

            <li class="{{$uriPage == '' ? 'active' : ''}}"><a href="javascript:$('#enviarComprovante').modal();"><i class='fa fa-upload'></i> <span>Enviar comprovante</span></a></li>




            @endif
            @if (! Auth::guest() && Auth::user()->isAdmin() && $uriPainel == 'admin')
            <?php if (env('lv_BTNvZfXgG4b5JPnD') == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-shopping-cart "></i> <span>Loja Virtual</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{$uriPage == 'produtos/add' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos/add')}}"><i class="fa fa-circle-o"></i> Adicionar Produtos</a></li>
                        <li class="{{$uriPage == 'produtos/list' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos/list')}}"><i class="fa fa-circle-o"></i> Lista de Produtos</a></li>
                        <li class="{{$uriPage == 'produtos' ? 'active' : ''}}"><a href="{{url('/painel/produtos')}}"><i class="fa fa-circle-o"></i> Produtos</a></li>
                        <li class="{{$uriPage == 'meu-carrinho' ? 'active' : ''}}"><a href="{{url('/painel/meu-carrinho')}}"><i class="fa fa-circle-o"></i> Meu carrinho</a></li>
                        <li class="{{$uriPage == 'pedidos' ? 'active' : ''}}"><a href="{{url('/admin/pedidos')}}"><i class="fa fa-circle-o"></i>Pedidos</a></li>
                    </ul>
                </li>
            <?php } ?>

            <li class="treeview"  class="{{$uriPage == 'transacoes' ? 'active' : ''}}{{$uriPage == 'faturas' ? 'active' : ''}}{{$uriPage == 'saques' ? 'active' : ''}}{{$uriPage == 'gerenciar_saldo' ? 'active' : ''}}">
                <a href="#">
                    <i class="fa fa-dollar"></i> <span>Financeiro</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{$uriPage == 'relatorios' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/relatorios')}}"><i class='fa fa-circle-o'></i> <span>Relatórios</span></a></li>
                    <li class="{{$uriPage == 'saque' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/saque')}}"><i class='fa fa-circle-o'></i> <span>Saques</span></a></li>
                    <li class="{{$uriPage == 'faturas' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/faturas')}}"><i class='fa fa-circle-o'></i> <span>Faturas</span></a></li>
                    <li class="{{$uriPage == 'comprovantes' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/comprovantes')}}"><i class='fa fa-upload'></i> <span>Comprovantes</span></a></li>
                    <li class="{{$uriPage == 'lancamentos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/lancamentos')}}"><i class='fa fa-upload'></i> <span>Lancamentos</span></a></li>

                </ul>
            </li>



            <li class="{{$uriPage == 'usuarios' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/usuarios')}}"><i class='fa fa-users'></i> <span>Gerenciar Usuários</span></a></li>
            <li class="{{$uriPage == 'vouchers' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/vouchers')}}"><i class='fa fa-users'></i> <span>Gerenciar Vouchers</span></a></li>
            <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/materiais')}}"><i class='fa fa-folder'></i> <span>Materiais</span></a></li>
            <li class="{{$uriPage == 'pacote' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/pacote')}}"><i class='fa fa-folder'></i> <span>Pacotes</span></a></li>
            <li class="{{$uriPage == 'graduacoes' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/graduacoes')}}"><i class='fa fa-graduation-cap'></i> <span>Graduações</span></a></li>
            <li class="{{$uriPage == 'config' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/config')}}"><i class='fa fa-gears'></i> <span>Configurações Gerais</span></a></li>
                <li><a href="javascript:$('#lucro').modal();"><i class='fa fa-dollar'></i> <span>Renda Variável</span></a></li>
            <li><a href="javascript:$('#addSaldo').modal();"><i class='fa fa-plus'></i> <span>Divisão Manager Direct</span></a></li>

            <?php
            if (isset($_GET['infoBin'])) {
                echo "<script> alert('Operação realizada com sucesso')</script>";
            }
            ?>

            @endif
            <!-- Sidebar Menu -->

            @endif



        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
