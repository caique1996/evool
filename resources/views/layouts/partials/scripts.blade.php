<!-- REQUIRED JS SCRIPTS -->
<!-- REQUIRED JS SCRIPTS -->
<div id='criarApp' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <p>
            <form id="formSite" method="get" action="{{url('/painel/app-vouchers')}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Criar Aplicativo</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Código do voucher </label>
                        <input class="form-control"  type="text" name="code" id="code" required>
                        <p>  Você pode pegar o seus vouchers clicando <a href="{{url('/painel/app-vouchers')}}" target='_blank'>aqui</a>  </p>
                    </div>
                    <div class="modal-footer">
                        <div id="resUpdate">
                        </div>
                        <a href='<?= env('BUILDER_APP') ?>' target='_blank' class="btn btn-default" >Ir para o criador de aplicativos.</a>
                        <button type="submit" id="distribuir" class="btn btn-primary">OK</button>
                        </p>
                    </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
<div id='enviarComprovante' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <p>
            <form id="formSite" method="post" action="{{url('/painel/Enviarcomprovante')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Enviar comprovante</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Arquivo </label>
                        <input class="form-control"  type="file"  name="image" id="arquivoComprovante" required accept="image/*">
                    </div>
                    <?php
                    $faturas = DB::table('faturas')->where("user_id", Auth::user()->id)->where('status', 0);
                    if ($faturas->count() > 0) {
                        ?>

                        <div class="form-group">
                            <label>Faturas</label>
                            <select class="form-control" name="faturasComprovante" id="faturasComprovante">
                                <?php
                                foreach ($faturas->get() as $value) {
                                    if (isset($value->id)) {
                                        echo "<option value ='{$value->id}'>{$value->id}</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <?php } else { ?>
                        <div class="form-group">
                            <label>Informações da fatura</label>
                            <textarea class="form-control" name="infoComprovante" name="infoComprovante">
                                                        
                            </textarea>


                        </div>
                    <?php } ?>
                    <div class="modal-footer">

                        <button type="submit" id="distribuir" class="btn btn-primary">Enviar</button>
                        </p>
                    </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
<div id='addSaldo' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>
            <form id="formSaldo" method="post" action="{{url('/admin/saque')}}">


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Adicionar Saldo</h4>

                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Valor </label>
                        <input class="form-control"  type="number"  name="valorDist2" step="any" id="valorDist2" required>

                    </div>
                    <?php
                    $graduacoes = App\graduacoes::all();
                    ?>
                    <div class="form-group">
                        <label>Graduação Requerida </label>
                        <select class="form-control" name="grad_required" id="grad_required">
                            <?php
                            foreach ($graduacoes as $value) {
                                echo "<option value ='{$value['id']}'>{$value['name']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <div id="resUpdate">
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="addSaldoBtn" class="btn btn-primary">Adicionar</button>
                        </p>
                    </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
<div id='transferirSaldo' class="modal"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <p>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Transferir Saldo</h4>
            </div>
            <div class="modal-body">
                <?php
                $saldo = Auth::user()->saldo;
                if ($saldo > 0) {
                    ?>

                    <h3>Saldo disponível para transferência : R$<?php echo e(number_format($saldo, 2, ',', '.')); ?> </h3>
                    <br>
                    <div class="form-group">
                        <label>Código de segurança </label>
                        <input class="form-control"  type="password"  name="pin_transferir" id="pin_transferir" required>
                    </div>
                    <div class="form-group">
                        <label>Valor </label>
                        <input class="form-control"  type="text"  name="valor_transferir" id="valor_transferir" required>
                    </div>
                    <div class="form-group">
                        <label>Username </label>
                        <input class="form-control"  type="text"  name="username_transferir" id="username_transferir" required>
                        <p id="infoUsuario_transferir">
                        </p>
                    </div>
                    <p>*Não será possível sacar esse saldo</p>

                    <div class="modal-footer">
                        <button  class="btn btn-primary">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                    <?php
                } else {
                    echo '<h4>Você não possui saldo para transferência</h4>';
                }
                ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<div id='anuncio' class="modal"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <p>

            <div class="modal-header">
                <button type="button" class="close" data-dpismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Criar Anúncio</h4>
            </div>
            <div class="modal-body">
                <?php
                $saldo = Auth::user()->carteira_b;
                if ($saldo > 0) {
                    ?>

                    <h3>Saldo disponível: <?= $saldo ?> </h3>
                    <br>
                    <form id="form_anuncio" method="post" action="criar_anuncio">

                        <div class="form-group">
                            <label>Código de segurança </label>
                            <input class="form-control"  type="password"  name="pin_anuncio" id="pin_anuncio" required>
                        </div>
                        <div class="form-group">
                            <label>Titulo </label>
                            <input class="form-control"  type="text"  name="nome" id="nome" required>
                        </div>
                        <?php
                        $pacotes_views = \App\pacoteViews::where('status', 1)->get();
                        ?>
                        <div class="form-group">
                            <label>Total </label>
                            <select name="pacotes_views" id="pacotes_views" class="form-control" required="">
                                <?php
                                foreach ($pacotes_views as $value) {
                                    echo "<option value='{$value->id}'>{$value->valor} ponto(s) - {$value->views} visualizações/compartilhamentos</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Link </label>
                            <input class="form-control"  type="url"  name="link_anuncio" id="link_anuncio" required>
                        </div>
                        <div class="form-group">
                            <label>Ação</label>
                            <select name="anuncio_acao" id="anuncio_acao" class="form-control" required="">
                                <option value='1'>Visitar esse link</option>
                                <option value='2'>Compartilhar esse link</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="check" data-required="true" required><i></i>Li e aceito os termos de uso.
                            </label>

                        </div>
                        <div id="mensagem_anuncio">
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  class="btn btn-primary">Criar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                    <?php
                } else {
                    echo '<h4>Você não possui saldo para criar novos anúncios</h4>';
                }
                ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>



<div id='converterSaldo' class="modal" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <p>
            <form id="formConvert" method="post" action="{{url('/painel/converter_saldo')}}">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Converter Saldo</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Código de segurança </label>
                        <input class="form-control"  type="password"  name="pin_convert" id="pin_convert" required>
                    </div>
                    <div class="form-group">
                        <label>Valor </label>
                        <input class="form-control"  type="text"  name="valor_conver" id="valor_conver" required>
                    </div>



                    <div class="modal-footer">
                        <button type="submit" id="converterBtn" class="btn btn-primary">Converter</button>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
<div id='pagarSaldo' class="modal"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <p>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Pagar fatura com saldo</h4>
            </div>
            <div class="modal-body">
                <?php
                $saldo = Auth::user()->carteira_b;
                if ($saldo > 0) {
                    ?>

                    <h3>Saldo disponível: R$<?php echo e(number_format($saldo, 2, ',', '.')); ?> </h3>
                    <br>
                    <form id="formPagarSaldo" method="post">

                        <div class="form-group">
                            <label>Código de segurança </label>
                            <input class="form-control"  type="password"  name="pin_pagar" id="pin_pagar" required>
                        </div>
                        <div class="form-group">
                            <label>Id da fatura </label>
                            <input class="form-control"  type="number"  name="id_fatura" id="id_fatura" required>
                        </div>
                        <div class="form-group">
                            <label>Username do proprietário da fatura</label>
                            <input class="form-control"  type="text"  name="username" id="username_pagar" required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  class="btn btn-primary">Ok</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                    <?php
                } else {
                    echo '<h4>Você não possui saldo para pagar faturas</h4>';
                }
                ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<div id='ativarUsuario' class="modal"  >
    <div class="modal-dialog">
        <div class="modal-content">
            <p>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ativar Usuário</h4>
            </div>
            <div class="modal-body">
                <?php
                $saldo = Auth::user()->carteira_b;
                if ($saldo > 0) {
                    ?>

                    <h3>Saldo para ativação: R$<?php echo e(number_format($saldo, 2, ',', '.')); ?> </h3>
                    <br>
                    <div class="form-group">
                        <label>Código de segurança </label>
                        <input class="form-control"  type="password"  name="pin_ativar" id="pin_ativar" required>
                    </div>
                    <div class="form-group">
                        <label>Username </label>
                        <input class="form-control"  type="text"  name="username" id="username_ativar" required>
                        <p id="infoUsuario">
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button  class="btn btn-primary">Verificar Usuário</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                    <?php
                } else {
                    echo '<h4>Você não possui saldo para ativação</h4>';
                }
                ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<div id='lucro' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>
            <form id="formLucro" method="post" action="{{url('/admin/saque')}}">
                <input type="hidden" name="advanced" value="1">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Dividir Lucro</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Valor </label>
                        <input class="form-control"  type="number"  step="any" name="valorDist" id="valorDist" required>
                        <?php
                        $pacotes = App\Pacote::all();
                        ?>
                        <p><?php
                            $totalCotas = 0;
                            foreach (App\User::where('ativo', 1)->get() as $value) {
                                $pct = App\Pacote::where('id', $value['pacote'])->first();
                                $totalCotas = $totalCotas + $pct['total_cotas'];
                            }
                            ?>
                        <div class="form-group">
                            <label> Ação</label>
                            <select class="form-control" name="simulacao" id="simulacao" requiered="">
                                <option value ='1' selected="">Simular</option>
                                <option value ='0'>Dividir</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> </label>
                            <select class="form-control" name="pacote_div" id="pacote_div" requiered="">
                                <option value ='todos'>Usuários de todos</option>
                                <?php
                                foreach ($pacotes as $value) {

                                    echo "<option value ='{$value['id']}'>{$value['nome']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <p>
                            <?php
                            $usrTotal = App\User::where('ativo', 1)->count();
                            $lucro = App\User::where('id', 1)->first();
                            $meuLucro = number_format($lucro['saldo'] * 0.7, 2, ',', '.');
                            $valorSeguro2 = $lucro['saldo'] / $usrTotal;
                            $valorSeguro = number_format($valorSeguro2, 2, ',', '.');
                            ?>
                            Total de usuários:<b>{{$usrTotal}}</b> Meu lucro Aproximado: <b>R${{$meuLucro}}</b>                            Total de cotas ativas: <?=$totalCotas?>
  </p>
                    </div>
                    <div class="modal-footer">
                        <div id="resUpdate">
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="distribuir" class="btn btn-primary">Distribuir</button>
                        </p>
                    </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
<?php
$novaGraduacao = Auth::user()->verificar_graduacao();
if ($novaGraduacao) {
    ?>

    <div id='novaGraduacao' class="modal" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <p>

                <div class="modal-header">
                    <h4 class="modal-title">Parabéns você conseguiu  uma nova graduação</h4>
                </div>
                <div class="modal-body">

                    <center> <img src="<?= $novaGraduacao['icone'] ?>" width="40" /><br><?= $novaGraduacao['name'] ?>
                        <br><?= $novaGraduacao['boas_vindas'] ?> </center>


                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>

    <?php
}

$pin = Auth::user()->pin;
if ($pin == '') {
    ?>
    <div id='pin' class="modal" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <p>
                <form id="formPin" method="post" action="{{url('/painel/pin')}}">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="modal-header">
                        <h4 class="modal-title">Para continuar usando o sistema você precisa inserir um código  de segurança.</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Código </label>
                            <input class="form-control"  type="password"  name="pin" id="pin" required>
                        </div>
                        <div class="form-group">
                            <label>Insira novamente </label>
                            <input class="form-control"  type="password"  name="pin_confirmation" id="pin_confirmation" required>
                        </div>
                        <div class="modal-footer">
                            @if(session('success'))
                            <div class="alert alert-success fade in alert-dismissible flat  no-margin">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                                {{session('success')}}
                            </div>
                            @endif

                            @if (isset($errors) && count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible flat no-margin">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Whoops!</strong><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <button type="submit" id="distribuir" class="btn btn-primary">Salvar</button>
                            </p>
                        </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    </div>
<?php } ?>
<!-- jQuery 2.1.4 -->
<script src="{{env('CFURL').('/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{env('CFURL').('/js/jquery-ui.min.js')}}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="{{env('CFURL').('/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<?php /*
  <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js')}}"></script>
  <script src="{{asset('/plugins/morris/morris.min.js')}}"></script>
 */
?>
<!-- Sparkline -->
<script src="{{env('CFURL').('/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{env('CFURL').('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{env('CFURL').('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{env('CFURL').('/plugins/knob/jquery.knob.js')}}"></script>
<!-- PACE -->
<script src="{{env('CFURL').('/plugins/pace/pace.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{env('CFURL').('/plugins/moment.js/moment.min.js')}}"></script>
<script src="{{env('CFURL').('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{env('CFURL').('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{env('CFURL').('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{env('CFURL').('/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{env('CFURL').('/plugins/fastclick/fastclick.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{env('CFURL').('/dist/js/app.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{env('CFURL').('/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{env('CFURL').('/dist/js/demo.js')}}"></script>
<script src="{{env('CFURL').('/js/scripts-custom.js')}}"></script>

<script>
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-72220910-1', 'auto');
ga('send', 'pageview');

</script>
<script>

    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        Pace.restart();
    });

    $('.ajax').click(function () {
        $.ajax({
            url: '#', success: function (result) {
                $('.ajax-content').html('<hr>Ajax Request Completed !');
            }
        });
    });
</script>


@yield('page_scripts')
<script src="{{env('CFURL').('/js/jquery.maskMoney.min.js')}}"></script>

<script>
    function distribuirLucro(valor, senha) {
        alert('Por favor aguarde');
        $('#distribuir').html('Por favor aguarde...');
        url = "divisaoLucro?" + "valor=" + valor + "&_token=<?php echo csrf_token(); ?>" + "&simulacao=" + $("#simulacao").val() + "&pacote_div=" + $("#pacote_div").val();
        console.log(url);
        $.ajax({
            'url': "divisaoLucro",
            "type": "POST",
            "dataType": 'html',
            "data": "valor=" + valor + "&_token=<?php echo csrf_token(); ?>" + "&simulacao=" + $("#simulacao").val() + "&pacote_div=" + $("#pacote_div").val(),
            'success': function (txt) {
                $('#distribuir').html('Distribuir');
                alert(txt);
            }});
    }
    //add saldo
    function adicionarSado(valor, grad) {
        alert('Por favor aguarde');
        $('#adicionarSado').html('Por favor aguarde...');
        var url = "adicionarSaldo?valor=" + valor + "&_token=<?php echo csrf_token(); ?>" + "&grad_required=" + grad;
        $.ajax({
            'url': url,
            "dataType": 'html',
            'success': function (txt) {
                $('#addSaldoBtn').html('Adicionar');
                alert(txt);
            }});
    }
    //converter Saldo
    function converterSaldo(valor, usr, pin) {
        alert('Por favor aguarde');
        $('#converterBtn').html('Por favor aguarde...');
        var url = "converter_saldo?valor_conver=" + valor + "&_token=<?php echo csrf_token(); ?>" + "&trade=" + usr + "&pin=" + pin;
        $.ajax({
            'url': url,
            "dataType": 'html',
            'success': function (txt) {
                $('#converterBtn').html('Converter');
                alert(txt);
            }});
    }
    $("#converterSaldo").submit(function () {
        var valor = $("#valor_conver").val();
        var trade = $("#trade").val();
        var pin = $("#pin_convert").val();
        if (confirm("Deseja realmente realizar essa operação?")) {
            converterSaldo(valor, trade, pin);
        }
        return false;
    });
    //transferir Saldo
    function transferirSaldo(valor, usr, pin) {
        $('#transferirBtn').html('Por favor aguarde...');
        var url = "transferir_saldo?valor_transferir=" + valor + "&_token=<?php echo csrf_token(); ?>" + "&user=" + usr + "&pin=" + pin;
        $.ajax({
            'url': url,
            "dataType": 'html',
            'success': function (txt) {
                $('#transferirBtn').html('transferir');
                alert(txt);
            }});
    }

    function transferir() {
        var valor = $("#valor_transferir").val();
        var username = $("#username_transferir").val();
        var pin = $("#pin_transferir").val();
        if (confirm("Deseja realmente realizar essa operação?")) {
            transferirSaldo(valor, username, pin);
        }
        return false;
    }

    $("#formPagarSaldo").submit(function () {
        var url = "pagar_saldo?username=" + $("#username_pagar").val() + "&_token=<?php echo csrf_token(); ?>" + '&pin=' + $("#pin_pagar").val() + '&fatura=' + $("#id_fatura").val();
        $.ajax({
            'url': url,
            "dataType": 'html',
            'success': function (txt) {
                alert(txt);

            }});
        return false;
    });

    $("#formLucro").submit(function () {
        var valorDist = $("#valorDist").val();

        if (confirm("Deseja realmente realizar essa operação?")) {
            if (valorDist > 100) {
                if (confirm("O valor da transferência é maior que seu lucro,tem certeza que deseja fazer isso?")) {
                    distribuirLucro(valorDist);
                }
            } else {
                distribuirLucro(valorDist);
            }
        }
        return false;

    });
    //add saldo
    $("#formSaldo").submit(function () {
        var valorDist = $("#valorDist2").val();
        var graduacao = $("#grad_required").val();
        var pin = $("#pin_convert").val();


        if (confirm("Deseja realmente realizar essa operação?")) {
            adicionarSado(valorDist, graduacao);

        }
        return false;

    });
    //add anuncio
    $("#form_anuncio").submit(function () {
        enviarForm($(this));
        return false;

    });
    $(document).ready(function () {
        //$('#valorDist').maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });


    $(document).ready(function () {
        $('#valorSaque').maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $(document).ready(function () {
        $('#valor_conver').maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $(document).ready(function () {
        $('#valor_transferir').maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });


<?php
global $request;

$uriPainel = $request->segment(1);
$uriPage = $request->segment(2);
if ($pin == '' and $uriPage <> 'inativo') {
    ?>
        $(document).ready(function () {
            $("#pin").modal();
        });
<?php } ?>
<?php if ($novaGraduacao) {
    ?>
        $(document).ready(function () {
            $("#novaGraduacao").modal();
        });
<?php } ?>

    $("#username_ativar").focusout(function () {
        var url = "ver_user?username=" + $("#username_ativar").val() + "&_token=<?php echo csrf_token(); ?>" + '&pin=' + $("#pin_ativar").val();
        $.ajax({
            'url': url,
            "dataType": 'html',
            'success': function (txt) {
                $('#infoUsuario').html('');
                $('#infoUsuario').html(txt);

            }});
    });
    $("#username_transferir").focusout(function () {
        var url = "ver_user_transferencia?username=" + $("#username_transferir").val() + "&_token=<?php echo csrf_token(); ?>" + '&pin=' + $("#pin_transferir").val() + '&valor=' + $("#valor_transferir").val();
        $.ajax({
            'url': url,
            "dataType": 'html',
            'success': function (txt) {
                $('#infoUsuario_transferir').html('');
                $('#infoUsuario_transferir').html(txt);

            }});
    });

    function ativar_user() {
        var url = "ativar_user?username=" + $("#username_ativar").val() + "&_token=<?php echo csrf_token(); ?>" + '&pin=' + $("#pin_ativar").val();
        $.ajax({
            'url': url,
            "dataType": 'html',
            'success': function (txt) {
                $('#infoUsuario').html('');
                $('#infoUsuario').html(txt);

            }});
    }
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
    function selecionarFoto() {

        $("#photo").trigger('click');
        $("#salvarFoto").show();
    }
    function enviarForm(form) {
        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                $("#mensagem_anuncio").html(response);
            }
        });
        return false;
    }
</script>
<script src="http://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.js" type="text/javascript"></script>

<script>
    ///$(".percent").mask("9?9%");
    $('.money').maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});

    $(".ver_produto").click(function () {
        id = $(this).attr('produto-id');
        div_id = '#produto-' + id;
        $(div_id).modal();
        return false;
    });
</script>