<!DOCTYPE html>
<html lang="en">
    @include('layouts.partials.htmlheader')
    <body class=" sidebar-mini">
        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">
                <?php
                global $request;
                $uriPainel = $request->segment(1);
                $idTarefa = $request->segment(4);
                ?>

                <a href="{{ url('/'.$uriPainel.'/home') }}" class="logo" style="background: #FFFFFF;">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"> <img src="{{url('/img/logo-escritorio.png')}}" width='90'/></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="{{url('/img/logo-escritorio.png')}}" width='70'/></span>
                </a>

                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="" style="float: left; margin-top: 10px;">
                        <a  class="btn btn-sm btn-warning">Você pode fecha esse anúncio em <span id="tempo"></span> <i class="fa fa-clock-o"></i></a>
                    </div>
                </nav>

            </header>
            <div class="embed-responsive embed-responsive-4by3">
                <iframe class="embed-responsive-item" src="<?= $anuncio['url'] ?>" ></iframe>
            </div>


        </div>

        <div class="modal fade" data-backdrop="static" 
             data-keyboard="false" id="modalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalMsgTitle">Para contabilizar essa visualização é necessário acertar o desafio</h4>
                    </div>
                    <div class="modal-body" id='modalMsgBody'>

                        <form id='enviarView' action="{{url('/'.$uriPainel.'')}}/validar_visualizacao">
                            <input type="hidden" name='id' value="<?= $idTarefa ?>" >
                            <center><div class='g-recaptcha'  data-sitekey='6LdPNRsTAAAAAAPrpcPLZmHFzAwKOv_kMpV0ivd2'></div></center>
                    </div>
                    <div id="mensagem_anuncio"></div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Validar Atividade</button>
                        <a href="<?=$anuncio['url']?>" target="_blank" class="btn btn-default">Continuar vendo esse link</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery 2.1.4 -->
        <script src="{{env('CFURL').('/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{env('CFURL').('/js/jquery-ui.min.js')}}"></script>

        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
$.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.5 -->
        <script src="{{env('CFURL').('/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- Morris.js charts -->
        <?php /*
          <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js')}}"></script>
          <script src="{{asset('/plugins/morris/morris.min.js')}}"></script>
         */
        ?>
        <!-- Sparkline -->
        <script src="{{env('CFURL').('/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <!-- jvectormap -->
        <script src="{{env('CFURL').('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{env('CFURL').('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{env('CFURL').('/plugins/knob/jquery.knob.js')}}"></script>
        <!-- PACE -->
        <script src="{{env('CFURL').('/plugins/pace/pace.min.js')}}"></script>
        <!-- daterangepicker -->
        <script src="{{env('CFURL').('/plugins/moment.js/moment.min.js')}}"></script>
        <script src="{{env('CFURL').('/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <!-- datepicker -->
        <script src="{{env('CFURL').('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{env('CFURL').('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <!-- Slimscroll -->
        <script src="{{env('CFURL').('/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <!-- FastClick -->
        <script src="{{env('CFURL').('/plugins/fastclick/fastclick.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{env('CFURL').('/dist/js/app.min.js')}}"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{env('CFURL').('/dist/js/pages/dashboard.js')}}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{env('CFURL').('/dist/js/demo.js')}}"></script>
        <script src="{{env('CFURL').('/js/scripts-custom.js')}}"></script>

        <script>
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-72220910-1', 'auto');
ga('send', 'pageview');

        </script>
        <script>

            // To make Pace works on Ajax calls
            $(document).ajaxStart(function () {
                Pace.restart();
            });

            $('.ajax').click(function () {
                $.ajax({
                    url: '#', success: function (result) {
                        $('.ajax-content').html('<hr>Ajax Request Completed !');
                    }
                });
            });
        </script>


        @yield('page_scripts')
        <script src="{{env('CFURL').('/js/jquery.maskMoney.min.js')}}"></script>

        <script>

            function enviarForm(form) {
                console.log(form.serialize());
                $.ajax({
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        $("#mensagem_anuncio").html(response);
                    }
                });
                return false;
            }
            $("#enviarView").submit(function () {
                enviarForm($(this));
                return false;
            })
        </script>
    </script>

    <script type="text/javascript">

        var tempo = new Number();
        // Tempo em segundos
        tempo = 60;

        function startCountdown() {

            // Se o tempo não for zerado
            if ((tempo - 1) >= 0) {

                // Pega a parte inteira dos minutos
                var min = parseInt(tempo / 60);
                // Calcula os segundos restantes
                var seg = tempo % 60;

                // Formata o número menor que dez, ex: 08, 07, ...
                if (min < 10) {
                    min = "0" + min;
                    min = min.substr(0, 2);
                }
                if (seg <= 9) {
                    seg = "0" + seg;
                }

                // Cria a variável para formatar no estilo hora/cronômetro
                horaImprimivel = '00:' + min + ':' + seg;
                //JQuery pra setar o valor
                $("#tempo").html(horaImprimivel);

                // Define que a função será executada novamente em 1000ms = 1 segundo
                setTimeout('startCountdown()', 1000);

                // diminui o tempo
                tempo--;

                // Quando o contador chegar a zero faz esta ação
            } else {
                $("#modalMsg").modal();                    //window.open('../controllers/logout.php', '_self');
            }

        }

        // Chama a função ao carregar a tela
        $(document).ready(function () {
            startCountdown();
        });

    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

</body>
</html>
