@extends('layouts.auth')

@section('htmlheader_title')
Recuperar Senha
@endsection

@section('content')

<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/admin/home') }}"><b>Recuperar Senha</b></a>
        </div><!-- /.login-logo -->

        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="login-box-body">
            <p class="login-box-msg">Você recebera um email com um link, para redefinir a senha senha.</p>
            <form action="{{ url('/painel/password/email') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" required placeholder="Email" name="email" value="{{ old('email') }}"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
   <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
                            <div class="g-recaptcha" data-sitekey="<?= \App\config::getConf()['recaptcha_key'] ?>"></div>
                            <br>
                        <?php } ?>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar Link</button>
                    </div><!-- /.col -->
                </div>
            </form>

            <br/>
            <a class="btn btn-default btn-block btn-flat" href="{{ url('/painel/login') }}">Login</a>

        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    @include('layouts.partials.scripts_auth')
    <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
        <script src ='https://www.google.com/recaptcha/api.js' ></script>

    <?php } ?>
    <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
    </script>
</body>

@endsection
