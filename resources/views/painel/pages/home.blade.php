<?php $__env->startSection('htmlheader_title'); ?>
Dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<style>

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
Painel do Usuario
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<style>
    .box.box-warning {
        border-top-color: #fff;
    }
</style>
<!-- Small boxes (Stat box) -->
<div id='saqueModal' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Solicitar Saque</h4>
            </div>
            <div class="modal-body">
                <?php
                $config = new \App\config();
                $config = $config->getConfig();
                if ($config['permitir_saque'] == 'sim') {
                    ?>
                    <p>
                    <form>
                        <label for="pinSaque">Código de segurança:</label>

                        <input type="password" name="pinSaque" id='pinSaque' class="form-control" />
                        <label for="valorSaque">Valor:</label>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success" id="btnSaque">Solicitar</button>
                            </div><!-- /btn-group -->
                            <input type="text" name="valorSaque" id='valorSaque' class="form-control" />


                        </div>
                        <p>*Taxa  administrativa de 10%</p>
                    </form>
                    </p>
                <?php } else { ?>
                    <p>Saque indisponível no momento.</p>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">fechar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!--
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Seu Saldo</span>
                <span class="info-box-number">R$ <?php
                    $saldo = Auth::user()->saldo;

                    echo e(number_format($saldo, 2, ',', '.'));
                    ?></span>
                <a style="cursor:pointer;"  data-toggle="modal" data-target="#saqueModal">Solicitar Saque</a>
            </div>
        </div>
    </div> 
    <?php if (env('ct_nU9jppZkP8vbcK6a') == 1) { ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-network"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total de cotas</span>
                    <span class="info-box-number"><?php echo e(Auth::user()->totalCotas()); ?></span>
                </div>
            </div>
        </div> 
    <?php } else { ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-network"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Indicados</span>
                    <span class="info-box-number"><?php echo e(Auth::user()->nIndicados()); ?></span>
                </div>
            </div>
        </div> 
    <?php } ?>
    <div class="col-md-3 col-sm-6 col-xs-12">

        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-network"></i></span>
            <div class="info-box-content">

                <span class="info-box-text"><a  class="small-box-footer" style="color:#333;">Total ativos</a></span>
                <span class="info-box-number"> <?php echo e(Auth::user()->display_children_active()); ?></span>
            </div>


        </div>
    </div> 



    <div class="col-md-3 col-sm-6 col-xs-12">

        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-pie-graph"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Minha Graduação</span>
                <span class="info-box-number"><?php
                    $Minhagraduacao = Auth::user()->minhaGraduacao(Auth::user()->id);
                    if ($Minhagraduacao <> 'Sem graduação') {
                        echo "<span class='label label-success'>$Minhagraduacao</span>";
                    } else {
                        echo '<span class="label label-danger">Sem graduação</span>';
                    }
                    ?></span>
            </div>


        </div>
    </div> 
</div><!-- /.row -->

<div class="row">
    <div  class="col-lg-6">

        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title ">Link de indicação</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <h4><a href="<?php echo e(url('/cadastro/' . Auth::user()->username)); ?>" target="_blank"><?php echo e(url('/cadastro/' . Auth::user()->username)); ?></a></h4>
            </div><!-- /.box-body -->
        </div><!-- /.box -->


    </div>
    <div  class="col-lg-6">

        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title ">Link de indicação</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <h4><a href="<?php echo e(url('/cadastro/' . Auth::user()->username)); ?>" target="_blank"><?php echo e(url('/cadastro/' . Auth::user()->username)); ?></a></h4>
            </div><!-- /.box-body -->
        </div><!-- /.box -->


    </div>
    <div  class="col-lg-6">
        @if(Auth::user()->ativo)
        <!--<div class="pull-left image">
           <img src="{{Auth::user()->getAvatar()}}" class="img-circle" alt="User Image"/>
       </div>
       <div class="pull-left info">
           <p>{{ Auth::user()->name }}</p>
        <!-- Status -->
         <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
        -->
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
                <h3 class="widget-user-username">{{Auth::user()->getShortName()}}</h3>
                <h5 class="widget-user-desc">{{Auth::user()->minhaGraduacao()}}</h5>
            </div>
            <div class="widget-user-image">
                <img onclick="" class="img-circle" src="{{Auth::user()->photo}}" width="128">

            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header">{{Auth::user()->total_bin_dir}}</h5>
                            <span class="description-text">PONTOS</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4">
                        <div class="description-block">
                            <h5 class="description-header">{{Auth::user()->validade_pacote}}</h5>
                            <span class="description-text">EXPIRAÇÃO DA CONTA</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <?php
                    $equiparaca = (\App\Pacote::where('id', Auth::user()->pacote)->first()['bonus_equiparacao']) * 100
                    ?>
                    <div class="col-sm-4">
                        <div class="description-block">
                            <h5 class="description-header"><?= $equiparaca ?>%</h5></h5>
                            <span class="description-text">EQUIPARAÇÃO DE REDE</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.widget-user -->
        </div>
        @endif

    </div>
</div>

<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">


    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_scripts'); ?>
<!-- InputMask -->
<script src="<?php echo e(env('CFURL') . ('/plugins/form/jquery.form.min.js')); ?>"></script>

<script>
$(document).ready(function () {
// bind form using ajaxForm
    $('#formSuporteDashboard').ajaxForm({
        // target identifies the element(s) to update with the server response
        target: '#mensagemSuporteDashboard',
        beforeSubmit: function () {
            $('#sendEmail').attr('disabled', true);
            Pace.restart();
        },
        // success identifies the function to invoke when the server response
        // has been received; here we apply a fade-in effect to the new content
        success: function () {
            $('#formSuporteDashboard').clearForm();
            $('#mensagemSuporteDashboard').fadeIn('slow');
            setTimeout(function () {
                Pace.restart();
                location.reload();
            }, 1500);
        }
    });
});
$(document).ready(function () {
    var ladoBin = "<?php echo e(Auth::user()->direcao); ?>";
    if (ladoBin == 'direita') {
        $("#ladobinario").attr('checked', true);
    } else {
        $("#ladobinario2").attr('checked', true);

    }

});
$(".change_dir").click(function () {
    var lado = $(this).val();
    $.ajax({
        'url': "muda_lado?lado=" + lado,
        dataType: 'html',
        'success': function (txt) {
            if (txt == 'ok') {
                alert('O lado binário foi alterado com sucesso.');
            }
            if (txt == 'fail') {
                alert('O lado não binário foi alterado com sucesso.');

            }
        }});
});
$("#btnSaque").click(function () {
    var valor = $("#valorSaque").val();
    var pinSaque = $("#pinSaque").val();

    url = "saque?valor=" + valor;
    if (confirm("Você realmente deseja solicitao saque de R$" + valor + "?")) {
        $.ajax({
            'url': "saque?valor=" + valor + "&pin=" + pinSaque,
            dataType: 'html',
            'success': function (txt) {
                alert(txt);
                if (txt == 'ok') {
                    alert('O lado saque foi solicitado com sucesso.');
                }
                if (txt == 'fail') {
                    alert('O saque não  foi solicitado com sucesso.');

                }
            }});
    } else {
        alert('Operação cancelada');
    }

    return false;
});
</script>
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
