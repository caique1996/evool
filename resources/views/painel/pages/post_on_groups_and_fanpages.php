<?php

################################################################################################
#  Caíque Marcelino Souza - contato@caiquemarcelino.com  - All rights reserved    #
################################################################################################

class ControllerModulePostOnGroupsAndFanpages extends Controller {

    private $error = array();
    private $meuModulo = 'post_on_groups_and_fanpages';

    function antisql($sql) {
        $sql = get_magic_quotes_gpc() == 0 ? addslashes($sql) : $sql;
        $sql = trim($sql);
        $sql = strip_tags($sql);
        $sql = mysql_escape_string($sql);
        return preg_replace("@(--|#|*|;|=)@s", '', $sql);
    }

    private function replaceArray($nameG, $arr) {
        foreach ($arr as $key => $value) {
            $novakey = str_replace($nameG . '_', '', $key);
            $resultado[$novakey] = $value;
        }
        return $resultado;
    }

    function totalVisitas($id) {
        $this->load->model("module/{$this->meuModulo}");
        $resultado = $this->model_module_post_on_groups_and_fanpages->totalVisitas($id);
        if ($resultado == '') {
            $resultado = 'nops';
        }
        return $resultado;
    }

    function getLang($id) {
        $this->load->model('setting/setting');
        $this->load->model("module/{$this->meuModulo}");
        $res = $this->model_module_post_on_groups_and_fanpages->langById($id);
        return $res[0]['name'];
    }

    function exibir_alert($mensagem, $tipo = '', $fechar = '') {
        if ($tipo == 'vermelho') {
            if ($fechar == '') {
                $button = "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
            } else {
                $button = "";
            }
            echo "<div class='alert alert-danger alert-dismissable'>
            $button
            $mensagem
            </div>";
        } else if ($tipo == 'laranja') {
            if ($fechar == '') {
                $button = "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
            } else {
                $button = "";
            }
            echo "<div class='alert alert-warning alert-dismissable'>
            $button
            $mensagem
            </div>";
        } else if ($tipo == 'verde') {
            if ($fechar == '') {
                $button = "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
            } else {
                $button = "";
            }
            echo "<div class='alert alert-success alert-dismissable'>
            $button
            $mensagem
            </div>";
        } else {
            if ($fechar == '') {
                $button = "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
            } else {
                $button = "";
            }
            echo "<div class='alert alert-info alert-dismissable'>
            $button
            $mensagem
            </div>";
        }
    }

    public function index() {
        $this->language->load("module/{$this->meuModulo}");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');
        $this->load->model("module/{$this->meuModulo}");
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if ($this->request->post['app_id']) {
                $nameG = $this->meuModulo . 'conf';
                $this->request->post['step_1'] = 'ok';
                foreach ($this->request->post as $key => $value) {
                    $dataPost[$nameG . '_' . $key] = $value;
                }
                $this->model_setting_setting->editSetting($nameG, $dataPost);
                $data['success'] = $this->language->get('tra_success');
            } elseif ($this->request->post['gp']) {
                if ($this->request->post['gp']['id'][0] == '' and $this->request->post['gp']['pages'][0] == '') {
                    $data['erro'] = 'Você deve adicionar no minimo um grupo ou página';
                } else {
                    $this->request->post[0]['gp'] = json_encode($this->request->post['gp']);
                    $nameG = $this->meuModulo . 'gp';
                    foreach ($this->request->post as $key => $value) {
                        $dataPost[$nameG . '_' . $key] = $value;
                    }
                    $this->model_setting_setting->editSetting($nameG, $dataPost);
                    unset($this->request->post[0]['gp']);
                    unset($this->request->post[0]);
                    $this->request->post['step_2'] = 'ok';
                    $nameG = $this->meuModulo . 'conf2';
                    foreach ($this->request->post as $key => $value) {
                        $dataPost[$nameG . '_' . $key] = $value;
                    }
                    $this->model_setting_setting->editSetting($nameG, $dataPost);
                    $data['success'] = $this->language->get('tra_success');
                }
            }
        }
        $data['suporte'] = json_decode(file_get_contents("http://caiquemarcelino.com/home/suportePiGF?dominio=" . HTTP_CATALOG));
        $text_strings = array(
            'heading_title',
            'text_enabled',
            'text_disabled',
            'text_content_top',
            'text_content_bottom',
            'text_column_left',
            'text_column_right',
            'entry_layout',
            'entry_limit',
            'entry_image',
            'entry_position',
            'entry_status',
            'entry_sort_order',
            'button_save',
            'button_cancel',
            'button_add_module',
            'button_remove',
            'entry_app_id',
            'secret_key',
            'entry_app_id_def',
            'secret_key_def',
            'entry_config_def',
            'entry_post_fanpageid',
            'entry_yes',
            'entry_no',
            'entry_fanpage_status',
            'entry_group_status',
            'config_fb_app',
            'how_to_create',
            'sign_in_facebook',
            'click_here',
            'and_then_clique_in',
            'click_basic_setup',
            'groups_pages',
            'please_wait',
            'post_config',
            'post_your_product',
            'data_posts',
            'custom_post',
            'loading',
            'title',
            'group',
            'language',
            'unavailable',
            'see',
            'description',
            'image',
            'clear',
            'close',
            'add_and_edit_gp',
            'login_continue',
            'add_more_gp',
            'how_to_get',
            'select_def_fanpages',
            'select_def_lang',
            'post_model',
            'product_desc_link',
            'yes',
            'no',
            'post_your_product',
            'select',
            'product',
            'select_all',
            'select_your_groups',
            'post_selecteds',
            'tra_model',
            'tra_views',
            'tra_edit',
            'not_posted',
            'error_short_url',
            'tra_code',
            'tra_information',
            'you_should_group_fanpage',
            'tra_success',
            'the_group',
            'has_been_sucess',
            'tra_error',
            'only_open_groups',
            'error_occurred',
            'max_selecte_products',
            'tra_products',
            'select_group_or_page_post',
            'logout',
            'you_are_not_logged',
            'was_not_posted_with',
            'tra_view',
            'was_not_posted_with',
            'has_been_successfully_group',
            'was_not_posted_with_page',
            'has_been_successfully_page',
            'this_link_is_not',
            'all_fields_req',
            'browse',
            'product_desc_img_link',
            'tra_default',
            'posting',
            'count_click'
        );
        foreach ($text_strings as $text) {
            $data[$text] = $this->language->get($text);
        }
        $config_data = array(
            "{$this->meuModulo}_example" //this becomes available in our view by the foreach loop just below.
        );
        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $data[$conf] = $this->request->post[$conf];
            } else {
                $data[$conf] = $this->config->get($conf);
            }
        }
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        $confSeo = $this->model_setting_setting->getSetting('config');
        $data['seo'] = $confSeo['config_seo_url'];

        $data['configPost'] = $this->model_setting_setting->getSetting($this->meuModulo);
        $nameG = $this->meuModulo . 'conf';
        $data['config'] = $this->model_setting_setting->getSetting($nameG);
        $data['config'] = $this->replaceArray($nameG, $data['config']);
        $gp[0] = '';
        $nameG = $this->meuModulo . 'gp';
        $gp = @$this->model_setting_setting->getSetting($this->meuModulo . 'gp');
        $gp = $this->replaceArray($nameG, $gp);
        if (VERSION == '1.5.0' or VERSION == '1.5.0.2' or VERSION == '1.5.0.3' or VERSION == '1.5.0.4' or VERSION == '1.5.0.5' or VERSION == '1.5.1' or VERSION == '1.5.1.2' or VERSION == '1.5.1.3' or VERSION == '1.5.1.3.1' or VERSION == '1.5.1.2' or VERSION == '1.5.1.3' or VERSION == '1.5.1.3.1') {
            @$data['gp2'] = $gp;
        } else {
            @$data['gp2'] = $gp[0];
        }
        $data['moduleFunctions'] = $this->model_module_post_on_groups_and_fanpages;
        $data['objeto'] = $this;
        $data['conf2'] = $this->model_setting_setting->getSetting($this->meuModulo . 'conf2');
        $nameG = $this->meuModulo . 'conf2';
        $data['conf2'] = $this->replaceArray($nameG, $data['conf2']);
        $data['configLangs'] = $this->model_module_post_on_groups_and_fanpages->getData(DB_PREFIX . "language");
        $GP = json_decode($data['gp2']['gp']);
        @$lang = @$GP->lang[0];
        $data['defLang'] = $lang;
        $this->load->model('tool/image');
        if (is_numeric($lang)) {
            $data['products'] = $this->model_module_post_on_groups_and_fanpages->getProducts($lang);
        } else {
            $data['defLang'] = $this->model_module_post_on_groups_and_fanpages->getLang();
            $data['defLang'] = $data['defLang'][0]['value'];
            $this->model_module_post_on_groups_and_fanpages->langByCode($data['defLang']);
            $data['products'] = $this->model_module_post_on_groups_and_fanpages->getProducts($this->model_module_post_on_groups_and_fanpages->langByCode($data['defLang']));
        }
        $i = 0;
        foreach ($data['products'] as $result) {
            if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                $image = $result['image'];
            } else {
                $image = 'no_image.jpg';
            }
            $data['products'][$i]['description'] = substr(strip_tags(html_entity_decode($result['description'])), 0, 200);
            $data['products'][$i]['image'] = $image;
            $i++;
        }
        $data['visitas'] = $this->model_module_post_on_groups_and_fanpages->getVisitas();
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link("module/{$this->meuModulo}", 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $data['action'] = $this->url->link("module/{$this->meuModulo}", 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link("extension/{$this->meuModulo}", 'token=' . $this->session->data['token'], 'SSL');
        $data['modules'] = array();
        if (isset($this->request->post["{$this->meuModulo}"])) {
            $data['modules'] = $this->request->post["{$this->meuModulo}"];
        } elseif ($this->config->get("{$this->meuModulo}")) {
            $data['modules'] = $this->config->get("{$this->meuModulo}");
        }
        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();
        $this->template = "module/{$this->meuModulo}.php";
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['totalGroups'] = '';
        $data['totalPg'] = '';
        $this->response->setOutput($this->load->view($this->template, $data));
    }

    public function newPost() {
        $this->load->model("module/{$this->meuModulo}");
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            echo $this->model_module_post_on_groups_and_fanpages->newPost($this->request->post);
        } else {
            echo 'no';
        }
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', "module/{$this->meuModulo}")) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function data() {
        $this->language->load("module/{$this->meuModulo}");
        $this->load->model('setting/setting');
        $this->load->model("module/{$this->meuModulo}");
        $this->load->model('tool/image');
        $text_strings = array(
            'heading_title',
            'text_enabled',
            'text_disabled',
            'text_content_top',
            'text_content_bottom',
            'text_column_left',
            'text_column_right',
            'entry_layout',
            'entry_limit',
            'entry_image',
            'entry_position',
            'entry_status',
            'entry_sort_order',
            'button_save',
            'button_cancel',
            'button_add_module',
            'button_remove',
            'entry_app_id',
            'secret_key',
            'entry_app_id_def',
            'secret_key_def',
            'entry_config_def',
            'entry_post_fanpageid',
            'entry_yes',
            'entry_no',
            'entry_fanpage_status',
            'entry_group_status',
            'config_fb_app',
            'how_to_create',
            'sign_in_facebook',
            'click_here',
            'and_then_clique_in',
            'click_basic_setup',
            'groups_pages',
            'please_wait',
            'post_config',
            'post_your_product',
            'data_posts',
            'custom_post',
            'loading',
            'title',
            'group',
            'language',
            'unavailable',
            'see',
            'description',
            'image',
            'clear',
            'close',
            'add_and_edit_gp',
            'login_continue',
            'add_more_gp',
            'how_to_get',
            'select_def_fanpages',
            'select_def_lang',
            'post_model',
            'product_desc_link',
            'yes',
            'no',
            'post_your_product',
            'select',
            'product',
            'select_all',
            'select_your_groups',
            'post_selecteds',
            'tra_model',
            'tra_views',
            'tra_edit',
            'not_posted',
            'error_short_url',
            'tra_code',
            'tra_information',
            'you_should_group_fanpage',
            'tra_success',
            'the_group',
            'has_been_sucess',
            'tra_error',
            'only_open_groups',
            'error_occurred',
            'max_selecte_products',
            'tra_products',
            'select_group_or_page_post',
            'logout',
            'you_are_not_logged',
            'was_not_posted_with',
            'tra_view',
            'was_not_posted_with',
            'has_been_successfully_group',
            'was_not_posted_with_page',
            'has_been_successfully_page',
            'this_link_is_not',
            'all_fields_req',
            'browse',
            'product_desc_img_link',
            'tra_default',
            'posting',
            'app_id_not_set_red',
            'on_fb',
            'you_should_gp_fp_red',
            'view_inf_about_crea'
        );
        foreach ($text_strings as $text) {
            $data['trad_lang'][$text] = $this->language->get($text);
        }
        $nameG = $this->meuModulo . 'gp';
        $gp = $this->model_setting_setting->getSetting($this->meuModulo . 'gp');
        $gp = $this->replaceArray($nameG, $gp);
        $confSeo = $this->model_setting_setting->getSetting('config');
        $data['seo'] = $confSeo['config_seo_url'];
        if (VERSION == '1.5.0' or VERSION == '1.5.0.2' or VERSION == '1.5.0.3' or VERSION == '1.5.0.4' or VERSION == '1.5.0.5' or VERSION == '1.5.1' or VERSION == '1.5.1.2' or VERSION == '1.5.1.3' or VERSION == '1.5.1.3.1' or VERSION == '1.5.1.2' or VERSION == '1.5.1.3' or VERSION == '1.5.1.3.1') {
            $lang = json_decode($gp['gp']);
            $lang = $lang->lang[0];
            $gp = json_decode($gp['gp']);
        } else {
            $lang = json_decode($gp[0]['gp']);
            $lang = $lang->lang[0];
            $gp = json_decode($gp[0]['gp']);
        }
        $nameG = $this->meuModulo . 'conf';
        $data['configPost'] = $this->model_setting_setting->getSetting($this->meuModulo . 'conf');
        $data['configPost'] = $this->replaceArray($nameG, $data['configPost']);
        $resultado = $this->model_module_post_on_groups_and_fanpages->data($lang);
        $i = 0;
        foreach ($resultado['product'] as $result) {
            if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                $image = $result['image'];
            } else {
                $image = 'no_image.jpg';
            }
            $resultado['product'][$i]['description'] = substr(strip_tags(html_entity_decode($result['description'])), 0, 200) . '...';
            $resultad['product'][$i]['image'] = $image;
            $i++;
        }
        $url = $this->model_module_post_on_groups_and_fanpages->productUrl($resultado['lastProduct'], $confSeo['config_seo_url']);
        $shorturl = '';
        if ($gp->shortUrl[0] == 1) {
            $linkProduto = HTTP_CATALOG . $url;
            $linkProduto = urlencode($linkProduto);
            $json = file_get_contents("http://is.gd/create.php?callback=&url=" . $linkProduto . "&format=json");
            $obj = json_decode($json);
            if ($obj->shorturl) {
                $shorturl = $obj->shorturl;
            } else {
                $shorturl = $linkProduto;
            }
        }
        $data2 = array('trad_lang' => $data['trad_lang'], 'gp' => $gp, 'produto' => $resultado, 'url' => $url, 'configPost' => $data['configPost'], 'shortUrl' => $shorturl);
        echo json_encode($data2);
    }

    public function alertProduct() {
        $this->load->model('setting/setting');

        $this->load->model("module/{$this->meuModulo}");

        $resultado = $this->model_module_post_on_groups_and_fanpages->alertStatus($_POST['id']);
    }

    public function install() {
        $json = file_get_contents("http://caiquemarcelino.com/home/suportePiGF?dominio=" . HTTP_CATALOG);
        $this->load->model("module/{$this->meuModulo}");
        $this->model_module_post_on_groups_and_fanpages->createModuleTables();
    }

    public function uninstall() {
        $this->load->model('setting/setting');
        $this->load->model("module/{$this->meuModulo}");
        $resultado = $this->model_module_post_on_groups_and_fanpages->removeTables();
    }

}

?>