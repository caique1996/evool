@extends('layouts.app')
@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
@section('htmlheader_title')
Materiais
@endsection

@section('contentheader_title')
Materiais
@endsection

@section('breadcrumb')
<li class="active">Materiais</li>
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Lista de Materiais
            </div>
            <div class="panel-body">
                <table id="example2" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>Material</th>
                            <th width="225px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('materiais', 'App\Materiais')
                        @foreach($materiais->get() as $material)
                        <tr>
                            <td>{{$material->id}}</td>
                            <td>{{$material->description}}</td>
                            <td>
                                <a href="javascript:void(0);" data-href="{{url('/painel/materiais/'.$material->id)}}" class="btn btn-sm btn-info openModal"><i class="fa fa-eye"></i> Vizualizar</a>
                                <a href="{{$material->download}}" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-download"></i> Download</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection


@section('page_scripts')
<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script>
$(function () {

$(".openModal").on('click', function () {

    $('#myModal').removeData('bs.modal');

    $('#myModal').modal(
            {
                remote: $(this).attr('data-href')
            }
    );

    $('#myModal').modal('show');

});

$('#example2').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    }
});
});
</script>
@endsection