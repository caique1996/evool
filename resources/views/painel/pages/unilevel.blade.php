@extends('layouts.app')

@section('htmlheader_title')
Rede Unilevel
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Indicados Diretos
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Usuarios</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Username</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>Nível</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $nivel = 1;
                        ?>
                        <?php
                        $usr = new App\User();
                        foreach ($rede as $value) {
                            $user = $usr->userInfo($value->user);
                            $status = Auth::user()->getStatus($user->status);
                            echo" <tr>
                        <td>{$user->id}</td>
                        <td><b>{$user->name}</b></td>
                  <td><b>{$user->username}</b></td>
                                        <td><b>$status</b></td>
                        <td>{$user->email}</td>
                        <td>{$value->level}</td>
                        </tr>";
                        }
                        ?>


                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Adicionar Vouchers</h4>

                </div>
                <div class="modal-body">
                    <div class="te"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary">Salvar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


</div>

<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">


    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/form/jquery.form.min.js') }}"></script>


<script>
$(function () {

    $(".adicionarVouchers").on('click', function () {

        $('#myModal').removeData('bs.modal');

        $('#myModal').modal(
                {
                    remote: $(this).attr('data-href')
                }
        );

        $('#myModal').modal('show');

        $('#myModal').on('loaded.bs.modal', function (e) {

            // bind form using ajaxForm
            var form = $('.formAdicionarVouchers').ajaxForm({
                // target identifies the element(s) to update with the server response
                target: '#mensagemAdicionarVouchers',
                beforeSubmit: function () {
                    $('input').attr('disabled', true);
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAdicionarVouchers').clearForm();
                    $('#mensagemformAdicionarVouchers').fadeIn('slow');
                    $('#myModal').modal('hide');
                    $('input').attr('disabled', false);
                }
            });

        });


    });

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true
    });
});
</script>
@endsection