@extends('layouts.app')

@section('htmlheader_title')
Extrato Financeiro
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Transações
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->
@inject('extratos', 'App\extratos')

<?php
if (!isset($_GET['de']) or ! isset($_GET['ate'])) {
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('tipo', '<>', '3');
    $allExtratos = $extract->get();
} else {
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('tipo', '<>', '3')->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate']);
    $allExtratos = $extract->get();
}
?>
<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Extrato Financeiro</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                <label for="de">De :</label>
                                <input type="date" required="" class="form-control" id="de" value="<?= @$_GET['de'] ?>" name="de">
                            </div>
                            <div class="form-group">
                                <label for="de">Até :</label>
                                <input type="date" required="" class="form-control" value="<?= @$_GET['ate'] ?>" id="ate" name="ate">
                            </div>
                            <button type="submit" class="btn btn-default">Exibir</button>
                        </form>
                    </div>
                </div>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Usuario</th>
                            <th>Descrição</th>
                            <th>Data</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allExtratos as $extrato)
                        <tr>
                            <td>{{$extrato['id']}}</td>
                            <td>@if($extrato['descricao']=='Pagamento de binário')
                                {{'Administração'}}
                                @endif
                                @if($extrato['descricao']!='Pagamento de binário')
                                {{$extrato->userName($extrato['user_id'])}}
                                @endif


                            </td>
                            <td>{{$extrato['descricao']}}</td>

                            <td>{{$extrato['data']}}</td>
                            <td>R${{number_format($extrato['valor'],2)}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[0, "desc"]],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
</script>
@endsection
