@extends('layouts.app')

@section('htmlheader_title')
Extrato de Pontos
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Extrato de Pontos
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Extrato de Pontos</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th >Id</th>
                            <th>Usuario</th>

                            <th>Data</th>
                            <th>Total de pontos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('extratos', 'App\extratos')
                        @foreach($extratos->where('beneficiado', Auth::user()->id)->where('tipo','3')->get() as $extrato)
                        <tr>
                            <td >{{$extrato['id']}}</td>
                            <td>@if($extrato['descricao']=='Pagamento de binário')
                                {{'Administração'}}
                                @endif
                                @if($extrato['descricao']!='Pagamento de binário')
                                {{$extrato->userName($extrato['user_id'])}}
                                @endif

                            </td>

                            <td>{{$extrato['data']}}</td>
                            <td>{{$extrato['valor']}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
        $(function () {

        $('#example2').DataTable({
        "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]]
                "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "_MENU_ resultados por página",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                        "sNext": "Próximo",
                                "sPrevious": "Anterior",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                        },
                        "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                        }
                }
        });
                });
</script>
@endsection
