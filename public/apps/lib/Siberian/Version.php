<?php

class Siberian_Version
{
    const TYPE = 'PE';
    const NAME = 'Platform Edition';
    const VERSION = '4.1.0';

    static function is($type) {
        return self::TYPE == strtoupper($type);
    }
}
