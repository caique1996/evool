<?php

class Admin_Model_Admin extends Admin_Model_Admin_Abstract
{
    public function getApplications() {

        if(!$this->_applications) {
            $application = new Application_Model_Application();
            $this->_applications = $application->findAllByAdmin($this->getId());
        }

        return $this->_applications;
    }

    public function isAllowedToAddPages($app_id = null) {
        return (bool) $this->getTable()->isAllowedToAddPages($this->getId(), $app_id);
    }

    public function getSubaccounts() {

        if(!$this->_subaccounts) {
            $classname = get_class($this);
            $subaccount = new $classname();
            $this->_subaccounts = $subaccount->findAll(array('parent_id' => $this->getId()));
        }

        return $this->_subaccounts;

    }
}