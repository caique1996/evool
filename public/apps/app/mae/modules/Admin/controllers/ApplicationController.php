<?php

class Admin_ApplicationController extends Admin_Controller_Default {

    public function indexAction() {
        $this->_forward('list');
        return $this;
    }

    public function listAction() {
        $this->loadPartials();
    }

    public function createpostAction() {
        $datas = $this->getRequest()->getPost();
        //$datas = $_GET;

        if ($datas) {
            if ($datas['acao'] == 2) {




                if (empty($datas['code'])) {
                    throw new Exception($this->_('Insira um código'));
                } else {
                    $parameters = array(
                        'host' => 'localhost',
                        'username' => 'root',
                        'password' => '81495203',
                        'dbname' => 'adsply'
                    );
                    try {
                        $db = Zend_Db::factory('Pdo_Mysql', $parameters);
                        $db->getConnection();
                    } catch (Zend_Db_Adapter_Exception $e) {
                        echo $e->getMessage();
                        throw new Exception('Could not connect to database.');
                    } catch (Zend_Exception $e) {
                        echo $e->getMessage();
                        throw new Exception('Could not connect to database.');
                    }
                    Zend_Registry::set('db2', $db);
                    $this->db2 = Zend_Registry::get('db2');
                    $code = $this->db2->quote($datas['code']);
                    $data = $this->db2->fetchRow("SELECT * FROM `vouchers_site` WHERE code=$code");
                    if (isset($data['id'])) {
                        $id_amd = $this->getSession()->getAdminId();
                        $this->db2->fetchRow("SELECT * FROM `vouchers_site` WHERE code=$code");
                        //$this->db2->fetchRow("DELETE  FROM `vouchers_site` WHERE code=$code");
                        $db3 = Zend_Db_Table_Abstract::getDefaultAdapter();
                        $db3->query("UPDATE `admin` SET `qntd_app`=(qntd_app)+1 where `admin_id`=$id_amd");
                    } else {
                        throw new Exception($this->_('Voucher não encontrado'));
                    }
                }
            } else {
                try {
                    if (empty($datas['name'])) {
                        throw new Exception($this->_('Please, enter an application name'));
                    }

                    $admin = new Admin_Model_Db_Table_Admin();
                    $admin2 = new Admin_Model_Admin();
                    $application = new Application_Model_Application();
                    $application->setName($datas['name'])
                            ->setAdminId($this->getSession()->getAdminId())
                            ->addAdmin($this->getSession()->getAdmin())
                    ;
                    $admin_data = $admin->findById($this->getSession()->getAdminId())->toArray();
                    if ($admin_data['qntd_app'] == 0) {
                        throw new Exception($this->_('Essa operação não pode ser realizada.Talvez vocẽ precise adiquirir mais vouchers.'));
                    }



                    $free_trial_period = System_Model_Config::getValueFor("application_free_trial");

                    if (is_numeric($free_trial_period)) {
                        $date = new Zend_Date();
                        $date = $date->addDay($free_trial_period);
                        $application->setFreeUntil($date->toString("y-MM-dd HH:mm:ss"));
                    }
                    $application->save();
//remove quantidade de apps
                    $where = $admin->getAdapter()->quoteInto('admin_id = ?', $admin_data['admin_id']);
                    $data_adm['qntd_app'] = $admin_data['qntd_app'] - 1;
                    $admin->update($data_adm, $where);


                    //$admin->setQntd_app($admin_data['qntd_app'] - 1)->
                    $this->getSession()->showTemplates = true;

                    $html = array(
                        'url' => $this->getUrl('admin/application/set', array('id' => $application->getId(), 'redirect_to' => 'edit'))
                    );
                } catch (Exception $e) {
                    $html = array(
                        'message' => $e->getMessage(),
                        'message_button' => 1,
                        'message_loader' => 1
                    );
                }

                $this->_sendHtml($html);
            }
        }

        return $this;
    }

    public function deleteAction() {

        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $admin = new Admin_Model_Admin();
                $application = new Application_Model_Application();
                $status_atual = $application->find($id)->getIsActive();
                $application->find($id)->setIsActive(0)->save();
                $total = $admin->find($this->getSession()->getAdminId())->getQntd_app();
                if ($status_atual == 1) {
                    $admin->find($this->getSession()->getAdminId())->setQntd_app($total + 1)->save();
                }
                $html = array('success' => '1');
            } catch (Exception $e) {
                $html = array(
                    'message' => "Une erreur s'est produite lors de la suppression de votre application",
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->_sendHtml($html);
        }
    }

    public function setAction() {

        if ($id = $this->getRequest()->getParam('id')) {

            $application = new Application_Model_Application();
            $application->find($id);
            $admin_ids = $application->getTable()->getAdminIds($application->getId());
            if (!$application->getId() OR ! in_array($this->getSession()->getAdmin()->getId(), $admin_ids)) {
                $this->_redirect('admin/application/list');
            } else {
                // Passe l'id de l'appli en session
                $this->getSession()->setAppId($id);
                $this->_redirect('application/customization');
            }

            return $this;
        }
    }

    public function duplicateAction() {

        $is_allowed = $this->_getAcl()->isAllowed('application_create', null);

        if (($app_id = $this->getRequest()->getParam('id')) && $is_allowed) {
            $application = new Application_Model_Application();
            $application->find($app_id);
            $admin_ids = array();

            foreach ($application->getAdmins() as $admin)
                $admin_ids[] = $admin;

            if (in_array($this->getSession()->getAdminId(), $admin_ids)) {

                try {
                    $result = $application->duplicate();
                    $html = array(
                        'url' => $this->getUrl('admin/application/list')
                    );
                    $this->_sendHtml($html);
                } catch (Exception $e) {
                    $html = array(
                        'message' => $this->_("Sorry but an error occurred whiled duplicating your application."),
                        'message_button' => 1,
                        'message_loader' => 1
                    );
                    $this->_sendHtml($html);
                }
            }
        }
    }

    public function voucherAction() {
        echo 'hue';
    }

}
