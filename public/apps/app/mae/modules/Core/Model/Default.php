<?php

class Core_Model_Default extends Core_Model_Default_Abstract
{
    public function getApplication() {
        return self::$_application;
    }

    public static function setApplication($application) {
        self::$_application = $application;
    }
}