<?php

class Core_Controller_Default extends Core_Controller_Default_Abstract
{
    public function getApplication() {
        return self::$_application;
    }

    public static function setApplication($application) {
        self::$_application = $application;
    }

}