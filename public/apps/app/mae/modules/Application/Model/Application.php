<?php

class Application_Model_Application extends Application_Model_Application_Abstract {

    public function getUrl($url = '', array $params = array(), $locale = null, $forceKey = false) {

        $is_ionic_url = false;
        if(!empty($params["use_ionic"])) {
            $is_ionic_url = true;
            unset($params["use_ionic"]);
        }

        if(!$this->getDomain()) $forceKey = true;

        if($is_ionic_url) {
            $url = Core_Model_Url::create($url, $params, $locale);
        } else if($forceKey) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $oldKey = $request->getApplicationKey();
            $request->setApplicationKey($this->getKey());
            $url = Core_Model_Url::create($url, $params, $locale);
            $request->setApplicationKey($oldKey);
        } else {
            $domain = rtrim($this->getDomain(), "/")."/";
            $url = Core_Model_Url::createCustom('http://'.$domain, $url, $params, $locale);
        }

        if(substr($url, strlen($url) -1, 1) != "/") {
            $url .= "/";
        }

        return $url;

    }

    public function getIonicUrl($url = '', array $params = array(), $locale = null, $forceKey = false) {

        $request = Zend_Controller_Front::getInstance()->getRequest();
        $params["use_ionic"] = true;

        $oldKey = $request->getApplicationKey();
        $ionic_path = $request->getIonicPath();

        $request->setApplicationKey($this->getKey());
        $request->setIonicPath(self::getIonicPath());

        $url = $this->getUrl($url, $params, $locale, $forceKey);

        $request->setApplicationKey($oldKey);
        $request->setIonicPath($ionic_path);

        return $url;
    }

}
