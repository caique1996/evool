App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/subscription/backoffice_application_list", {
        controller: 'SubscriptionListController',
        templateUrl: BASE_URL+"/subscription/backoffice_application_list/template"
    }).when(BASE_URL+"/subscription/backoffice_application_edit", {
        controller: 'SubscriptionnEditController',
        templateUrl: BASE_URL+"/subscription/backoffice_application_edit/template"
    }).when(BASE_URL+"/subscription/backoffice_application_edit/subscription_app_id/:subscription_app_id", {
        controller: 'SubscriptionEditController',
        templateUrl: BASE_URL+"/subscription/backoffice_application_edit/template"
    });

}).controller("SubscriptionListController", function($scope, $location, Header, Subscription, SectionButton) {

    $scope.header = new Header();
    $scope.header.button.left.is_visible = false;
    $scope.content_loader_is_visible = true;

    $scope.button = new SectionButton(function() {
        $location.path("subscription/backoffice_application_edit");
    });

    Subscription.loadListData().success(function(data) {
        $scope.header.title = data.title;
        $scope.header.icon = data.icon;
    });

    Subscription.findAll().success(function(subscriptions) {
        $scope.subscriptions = subscriptions;
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

}).controller("SubscriptionEditController", function($scope, $location, $routeParams, Header, Label, Subscription) {

    $scope.header = new Header();
    $scope.header.button.left.is_visible = false;
    $scope.header.button.left.action = function() {
        $location.path(Url.get("admin/backoffice_list"));
    };
    $scope.content_loader_is_visible = true;

    $scope.datepicker_visible = false;

    Subscription.loadEditData().success(function(data) {
        $scope.header.title = data.title;
        $scope.header.icon = data.icon;
        $scope.none_label = data.none_label;
    });

    Subscription.find($routeParams.subscription_app_id).success(function(data) {
        $scope.subscription = data.subscription ? data.subscription : {};

        $scope.is_active_checkbox_disabled = $scope.subscription.payment_method == '2checkout' && $scope.subscription.is_active == '0';
        $scope.section_title = data.section_title;

    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    $scope.saveSubscription = function() {

        $scope.form_loader_is_visible = true;


        Subscription.save($scope.subscription).success(function(data) {
            $location.path("subscription/backoffice_application_list");
            $scope.message.setText(data.message)
                .isError(false)
                .show()
            ;
        }).error(function(data) {
            var message = Label.save.error;
            if(angular.isObject(data) && angular.isDefined(data.message)) {
                message = data.message;
            }

            $scope.message.setText(message)
                .isError(true)
                .show()
            ;
        }).finally(function() {
            $scope.form_loader_is_visible = false;
        });
    };

    $scope.onSetTime = function (newDate, oldDate) {
        console.log(newDate);
        console.log(oldDate);
    }

});
