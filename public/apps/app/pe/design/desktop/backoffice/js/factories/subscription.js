
App.factory('Subscription', function($http, Url) {

    var factory = {};

    factory.loadListData = function() {
        return $http({
            method: 'GET',
            url: Url.get("subscription/backoffice_application_list/load"),
            cache: true,
            responseType:'json'
        });
    };

    factory.loadEditData = function(subscription_id) {
        return $http({
            method: 'GET',
            url: Url.get("subscription/backoffice_application_edit/load", {id: subscription_id}),
            cache: true,
            responseType:'json'
        });
    };

    factory.findAll = function() {

        return $http({
            method: 'GET',
            url: Url.get("subscription/backoffice_application_list/findall"),
            cache: false,
            responseType:'json'
        });
    };

    factory.find = function(subscription_app_id) {

        var param = {};
        if(subscription_app_id) {
            param = {subscription_app_id: subscription_app_id};
        }

        return $http({
            method: 'GET',
            url: Url.get("subscription/backoffice_application_edit/find", param),
            cache: false,
            responseType:'json'
        });
    };


    factory.save = function(subscription) {

        return $http({
            method: 'POST',
            data: subscription,
            url: Url.get("subscription/backoffice_application_edit/save"),
            responseType:'json'
        });
    };

    return factory;
});
