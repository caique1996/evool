App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/subscription/backoffice_list", {
        controller: 'PlanListController',
        templateUrl: BASE_URL+"/subscription/backoffice_list/template"
    }).when(BASE_URL+"/subscription/backoffice_edit", {
        controller: 'PlanEditController',
        templateUrl: BASE_URL+"/subscription/backoffice_edit/template"
    }).when(BASE_URL+"/subscription/backoffice_edit/plan_id/:id", {
        controller: 'PlanEditController',
        templateUrl: BASE_URL+"/subscription/backoffice_edit/template"
    });

}).controller("PlanListController", function($scope, $location, Header, Plan, Message, SectionButton) {

    $scope.header = new Header();
    $scope.header.button.left.is_visible = false;
    $scope.content_loader_is_visible = true;

    $scope.button = new SectionButton(function() {
        $location.path("subscription/backoffice_edit");
    });

    Plan.loadListData().success(function(data) {
        $scope.header.title = data.title;
        $scope.header.icon = data.icon;
        $scope.delete_confirmation_message = data.delete_confirmation_message;
    });

    Plan.findAll().success(function(plans) {
        $scope.plans = plans;
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    $scope.delete = function(plan) {

        if(!confirm($scope.delete_confirmation_message)) {
            return;
        }

        if(plan.loader_is_visible) {
            return;
        }

        plan.loader_is_visible = true;

        Plan.delete(plan).success(function(data) {
            var plans = new Array();
            for(var i = 0; i < $scope.plans.length; i++) {
                if($scope.plans[i].id != data.id) {
                    plans.push($scope.plans[i]);
                }
            }
            $scope.plans = plans;
        }).error(function(data) {
            $scope.message = new Message();
            $scope.message.isError(true)
                .setText(data.message)
                .show()
            ;
        }).finally(function() {
            plan.loader_is_visible = false;
        });
    }

}).controller("PlanEditController", function($scope, $location, $routeParams, Header, Label, Plan) {

    $scope.header = new Header();
    $scope.header.button.left.is_visible = false;
    $scope.content_loader_is_visible = true;
    $scope.payment_frequencies = { Monthly: "Monthly", Yearly: "Yearly" };
    $scope.plan = {};

    Plan.loadEditData($routeParams.id).success(function(data) {
        $scope.header.title = data.title;
        $scope.header.icon = data.icon;
    });

    Plan.find($routeParams.id).success(function(data) {
        $scope.plan = data.plan ? data.plan : {};
        $scope.plan.resources = data.resources ? data.resources : {};
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    $scope.save = function() {

        $scope.form_loader_is_visible = true;
;
        Plan.save($scope.plan).success(function(data) {
            $location.path("subscription/backoffice_list");
            $scope.message.setText(data.message)
                .isError(false)
                .show()
            ;
        }).error(function(data) {
            var message = Label.save.error;
            if(angular.isObject(data) && angular.isDefined(data.message)) {
                message = data.message;
            }

            $scope.message.setText(message)
                .isError(true)
                .show()
            ;
        }).finally(function() {
            $scope.form_loader_is_visible = false;
        });
    }

});
