App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/tax/backoffice_list", {
        controller: 'TaxListController',
        templateUrl: BASE_URL+"/tax/backoffice_list/template"
    });

}).controller("TaxListController", function($scope, Header, Label, Tax, SectionButton) {

    $scope.header = new Header();
    $scope.header.button.left.is_visible = false;
    $scope.content_loader_is_visible = true;

    $scope.button = new SectionButton(function() {
        $scope.add();
    });

    Tax.loadListData().success(function(data) {
        $scope.header.title = data.title;
        $scope.header.icon = data.icon;
    });

    Tax.findAll().success(function(data) {
        $scope.taxes = data.taxes;
        $scope.countries = data.countries;
        $scope.regions = data.regions;
        $scope.prepareCountries();
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    $scope.add = function() {
        $scope.taxes.push({
            id: null,
            country_code: null,
            rates: new Array({"rate": null, "region_code": null})
        });
        $scope.prepareCountries();
    };

    $scope.add_region = function(tax) {
        var index = $scope.taxes.indexOf(tax);

        if(angular.isDefined(index)) {
            tax.rates.push({
                region_code: null,
                rate: null
            });
        }
        $scope.prepareCountries();
    };

    $scope.change = function(tax) {
        $scope.prepareCountries();
    };

    $scope.save = function() {

        $scope.content_loader_is_visible = true;

        //Just "cleaning" the object to send to make it lighter
        var taxes_to_send = new Array();
        angular.copy($scope.taxes,taxes_to_send);
        angular.forEach(taxes_to_send, function(tax) {
            delete tax.countries;
            delete tax.regions;
        });
        taxes_to_send = {"taxes": taxes_to_send};

        Tax.save(taxes_to_send).success(function(data) {
            if(angular.isObject(data)) {
                $scope.message.setText(data.message)
                    .isError(false)
                    .show()
                ;
            } else {
                $scope.message.setText(Label.save.error)
                    .isError(true)
                    .show()
                ;
            }
        }).error(function(data) {

            var message = Label.save.error;
            if(angular.isObject(data)) {
                message = data.message;
            }
            $scope.message.setText(message)
                .isError(true)
                .show()
            ;

        }).finally(function() {
            $scope.content_loader_is_visible = false;
        });
    };

    $scope.delete = function(tax) {

        var index = $scope.taxes.indexOf(tax);

        if(angular.isDefined(index)) {
                $scope.taxes.splice(index, 1);
        }

    };

    $scope.delete_region = function(tax, rate) {

        var index = tax.rates.indexOf(rate);

        if(angular.isDefined(index)) {
            tax.rates.splice(index, 1);
        }

    };

    $scope.prepareCountries = function() {
        $scope.filtered_countries = {};
        var countries_taken = new Array();

        angular.forEach($scope.taxes, function(tax) {
            countries_taken.push(tax.country_code);
        });

        for(var i in $scope.countries) {
            if(countries_taken.indexOf(i) == -1) {
                $scope.filtered_countries[i] = $scope.countries[i];
            }
        }

        angular.forEach($scope.taxes, function(tax) {
            var countries = {};
            angular.extend(countries, $scope.filtered_countries);

            var code = tax.country_code;
            countries[code] = $scope.countries[code];
            tax.countries = countries;

            if(angular.isDefined($scope.regions[code])) {
                tax.regions = $scope.regions[code];
            }
        });

    };

});
