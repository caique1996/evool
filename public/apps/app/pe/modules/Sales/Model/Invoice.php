<?php

class Sales_Model_Invoice extends Core_Model_Default {

    protected $_lines = array();
    protected $_pdf;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Sales_Model_Db_Table_Invoice';
        return $this;
    }

    public function getStats() {
        return $this->getTable()->getStats();
    }

    public function findByAppId($app_id) {

        $row = $this->getTable()->findByAppId($app_id);
        if ($row) {
            $this->setData($row->getData())
                    ->setId($row->getId())
            ;
        }

        return $this;
    }

    public function addLine(Sales_Model_Invoice_Line $line) {
        $this->_lines[] = $line;
        return $this;
    }

    public function getLines() {
        if (empty($this->_lines)) {
            $line = new Sales_Model_Invoice_Line();
            $this->_lines = $line->findAll(array('invoice_id = ?' => $this->getId()));
        }

        return $this->_lines;
    }

    public function save() {

        if (!$this->getId()) {

            if(!$this->getNumber()) {
                $this->setNumber($this->_getNewInvoiceNumber());
            }
        }

        parent::save();

        if (!empty($this->_lines)) {
            foreach ($this->_lines as $line) {
                $line->setInvoiceId($this->getId())
                        ->save()
                ;
            }
        }
    }

    public function calcTotal() {

        // Calcul le sous-total et le total HT
        $subtotal_excl_tax = 0;
        $total_excl_tax = 0;
        $real_discount_amount = 0;

        foreach($this->getLines() as $line) {
            $subtotal_excl_tax += $line->getTotalPriceExclTax();
            $total_excl_tax += $line->getTotalPriceExclTax();
        }

        $this->setDiscountAmount($real_discount_amount);

        // Calcul la TVA
        $tax = $total_excl_tax * ($this->getTaxRate() / 100);

        // Affecte les totaux à l'abonnement
        $this->setSubtotalExclTax($subtotal_excl_tax)
            ->setTotalExclTax($total_excl_tax)
            ->setTotalTax($tax)
            ->setTotal($total_excl_tax + $tax)
        ;

        return $this;

    }

    public function getTaxes() {
        try {
            $taxes = @unserialize($this->getData("taxes"));
        } catch(Exception $e) {
            $taxes = array();
        }

        if(!$taxes) {
            $taxes = array();
        }
        return $taxes;
    }

    protected function _getNewInvoiceNumber() {

        $number = $this->getTable()->getNextIncrement();
        $number = str_pad($number, 3, '0', STR_PAD_LEFT);
        $now = Siberian_Date::now()->toString('ddMMy');

        return "I-$now-$number";
    }

    //la fonction qui permet d'afficher la facture en pdf
    public function getPdf() {

        $filename = 'logo_xtraball.png';
        $filepath = BASE_PATH . '/media/bliniz/';
        $file = $filepath . $filename;
        $pdf = new Ad_Pdf();
        $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
        $pdf->pages[] = $page;
//        $image = Zend_Pdf_Image::imageWithPath($file);
//        $page->drawImage($image, 50, 741, 110, 800);
        // Set font
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 20);
        $page->setLineColor(new Zend_Pdf_Color_Rgb(0, 0, 0));

        //En-Tete de la facture
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $page->setFont($font, 12);
        $y = 730;
        $page->drawText('Xtraball', 50, $y); $y-=15;
        $page->drawText('SAS', 50, $y);$y-=15;
        $page->drawText('au Capital de 58.000 €', 50, $y);$y-=15;
        $page->drawText('RCS Toulouse 751 984 360', 50, $y);$y-=15;
        $page->drawText('Identifiant TVA : FR 36 751984360', 50, $y);$y-=15;
        $page->drawText('28 rue Jean Chaptal', 50, $y);$y-=15;
        $page->drawText('31400 Toulouse, France', 50, $y);$y-=15;
        $page->drawText('contact@xtraball.com', 50, $y);


        //Adresse Client
        $y = 730;
        $page->setFont($font, 12);
        $page->drawText($this->getSignName(), 400, $y);$y-=15;
        $page->drawText($this->getSignStreet(), 400, $y);$y-=15;
        $page->drawText(sprintf("%s %s", $this->getSignPostcode(), $this->getSignCity()), 400, $y);

        //text pour le numero facture
        $page->drawLine(50, 568, 50, 588)
                ->drawLine(50, 568, 540, 568)
                ->drawLine(540, 568, 540, 588)
                ->drawLine(50, 588, 540, 588)
                ->drawLine(200, 568, 200, 588)
                ->drawLine(360, 568, 360, 588)
        ;
        $page->drawText('Facture n° ', 54, 574)
                ->setFont($font, 8)
                ->drawText($this->getNumber(), 114, 574);
        $page->setFont($font, 12)
                ->drawText('Date  ', 204, 574)
                ->setFont($font, 8)
                ->drawText(' '.$this->getFormattedCreatedAt('dd MMMM y'), 234, 574)
        ;

        //ligne de la commande je suis vraiment null en informatique
        $page->setFont($font, 12);
        // creation rectangle
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.9))
                ->drawRectangle(50, 540, 540, 560)
                ->drawRectangle(300, 360, 540, 380)
        ;
        //la colonne nom
        $page->drawLine(200, 540, 200, 560)
                ->setFillColor(new Zend_Pdf_Color_Html('#000000'))
                //clonne réference
                ->drawLine(300, 540, 300, 560)
                //colonne prix unitaire
                ->drawLine(430, 540, 430, 560)
                ->drawLine(480, 540, 480, 560)
                //colonne quantite
                ->drawLine(390, 540, 390, 560)
                //la postion $x et $y de colonnes
                ->drawText('Nom', 52, 544)
                ->drawText('Réf', 202, 544)
                ->drawText('Prix Unitaire HT', 302, 544)
                ->drawText('Qté', 392, 544)
                ->drawText('TVA', 440, 544)
                ->drawText('Total HT', 490, 544);

        $y = 530;
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        foreach ($this->getLines() as $line) {

            $y_ref = $y;
            $longchamp = 32;
            $name = $line->getName();
            if (strlen($name) > $longchamp) {

                $words = explode(' ', $name);
                $ligne = "";
                for ($i = 0; $i < count($words); $i++) {

                    $word = $words[$i];
                    if (strlen($ligne) + strlen($word) < $longchamp) {
                        $ligne .= " $word";
                    } else {
                        $page->setFont($font, 8)->drawText($ligne, 51, $y_ref);
                        $ligne = $word;
                        $y_ref -= 10;
                    }
                }
                $page->setFont($font, 8)->drawText($ligne, 51, $y_ref);
            } else {
                $page->setFont($font, 8)
                        ->drawText($line->getName(), 51, $y_ref);
            }

            $page->setFont($font, 8)
                    ->drawText($line->getRef(), 202, $y)
                    ->drawText(html_entity_decode($line->getFormattedPriceExclTax(Core_Model_Language::DEFAULT_CURRENCY), ENT_COMPAT, "UTF-8"), 330, $y)
                    ->drawText($line->getQty(), 407, $y)
                    ->drawText(html_entity_decode($line->getTaxRate() . ' %', ENT_COMPAT, "UTF-8"), 441, $y)
                    ->drawText(html_entity_decode($line->getFormattedTotalPriceExclTax(Core_Model_Language::DEFAULT_CURRENCY), ENT_COMPAT, "UTF-8"), 493, $y)
            ;

            $y = $y_ref - 20;
        }

        //html_entity_decode("€", ENT_COMPAT, "UTF-8"), $x, $y);
        //contenu Totale Facture
        $page->drawLine(300, 360, 300, 380)
                ->drawLine(540, 360, 540, 380)
                ->drawLine(300, 380, 540, 380)
                //la ligne de bas
                ->drawLine(300, 360, 540, 360)
                ->setFont($font, 12)
                ->drawText('Total', 310, 365)
        ;


        $y = 350;
        $y1 = 360;
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $page->setFont($font, 8);

        if ($this->getDiscountId()) {
            $page->drawText('Sous Total HT', 302, $y);
            $page->drawText(html_entity_decode($this->getFormattedSubtotalExclTax(Core_Model_Language::DEFAULT_CURRENCY), ENT_COMPAT, "UTF-8"), 502, $y)
                    ->drawLine(300, $y-=6, 540, $y)
                    ->drawLine(300, $y1, 300, $y1-=80)
                    ->drawLine(540, $y1, 540, $y1+=80)
                    ->drawText('Réduction', 302, $y-=10)
                    ->drawText(html_entity_decode($this->getFormattedDiscountAmount(Core_Model_Language::DEFAULT_CURRENCY), ENT_COMPAT, "UTF-8"), 502, $y)
                    ->drawLine(300, $y-=6, 540, $y)

            ;
            $y-=10;
        }
        $page->drawText('Total HT', 302, $y);
        $page->drawText(html_entity_decode($this->getFormattedTotalExclTax(Core_Model_Language::DEFAULT_CURRENCY), ENT_COMPAT, "UTF-8"), 502, $y)
                ->drawLine(300, $y-=6, 540, $y)
                ->drawLine(300, $y1, 300, $y1-=48)
                ->drawLine(540, $y1, 540, $y1+=48)
                ->drawText('TVA', 302, $y-=10)
                ->drawText(html_entity_decode($this->getFormattedTotalTax(Core_Model_Language::DEFAULT_CURRENCY), ENT_COMPAT, "UTF-8"), 502, $y)
                ->drawLine(300, $y-=6, 540, $y)
                ->drawText('Total TTC', 302, $y-=10)
                ->drawText(html_entity_decode($this->getFormattedTotal(Core_Model_Language::DEFAULT_CURRENCY), ENT_COMPAT, "UTF-8"), 502, $y)
                ->drawLine(300, $y-=6, 540, $y)
        ;

        return $pdf;
    }

    public function sendEmail($application = null, $sign) {

        return $this;

        if (!$application) {
            $application = $this->getApplication();
        }

        $layout = Zend_Controller_Action_HelperBroker::getStaticHelper('layout')->getLayoutInstance()
            ->loadEmail('sales', 'send_invoice');

        $layout->getPartial('content_email')->setInvoice($this);//->setApplication($application)->setSign($this->getSession()->getSign());
        $content = $layout->render();
        $mail = new Zend_Mail('UTF-8');
        $sender = System_Model_Config::getValueFor("sales_email");
        $name = System_Model_Config::getValueFor("sales_name");

        $mail->createAttachment(
            $this->getPdf()->render(),
            Zend_Mime::TYPE_OCTETSTREAM,
            Zend_Mime::DISPOSITION_ATTACHMENT,
            Zend_Mime::ENCODING_BASE64,
            "invoice.pdf"
        );

        $mail->setBodyHtml($content);
        $mail->setFrom($sender, $name);
        $mail->addTo($sign->getEmail(), $application->getName());
        $mail->setSubject($this->_(''));
        $mail->send();

        return $this;

    }

}
