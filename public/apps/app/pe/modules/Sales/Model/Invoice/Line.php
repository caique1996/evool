<?php


class Sales_Model_Invoice_Line extends Core_Model_Default {

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Sales_Model_Db_Table_Invoice_Line';
        return $this;
    }

    public function save() {

        if($this->getOrderLine()) {
            $this->setOrderLineId($this->getOrderLine()->getId());
        }

        return parent::save();

    }

    public function getQty() {
        $qty = 0;
        if($this->getData('qty')) {
            $qty = round($this->getData('qty'));
        }

        return $qty;
    }
}
