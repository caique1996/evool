<?php

class Sales_Model_Order extends Core_Model_Default {

    protected $_subscription;
    protected $_tax;
    protected $_payment_method;
    protected $_lines = array();

    protected static $_statuses = array(
        "1" => 'Waiting for payment',
        "2" => "Invoiced",
        "-1" => 'Canceled'
    );

    const DEFAULT_STATUS = "1";
    const CANCEL_STATUS = "-1";
    const PAID_STATUS = "2";

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Sales_Model_Db_Table_Order';
        return $this;
    }

    public function findByAppId($app_id) {
        $row = $this->getTable()->findByAppId($app_id);
        $this->_prepareDatas($row);
        return $this;
    }

    public function getSubscription() {
        if (is_null($this->_subscription)) {
            $subscription = new Subscription_Model_Subscription();
            $subscription->find($this->getSubscriptionId(), "subscription_id");
            $this->_subscription = new Subscription_Model_Subscription_Application();
            $this->_subscription->find($this->getAppId());
            $this->_subscription->addData($subscription->getData());
        }
        return $this->_subscription;
    }

    public function getApplication() {
        if(!self::$_application) {
            $application = new Application_Model_Application();
            $application->find($this->getAppId());
            return $application;
        } else {
            return self::$_application;
        }
    }

    public function addLine(Sales_Model_Order_Line $line) {
        $this->_lines[] = $line;
        return $this;
    }

    public function getLines() {
        if (empty($this->_lines)) {
            $line = new Sales_Model_Order_Line();
            $this->_lines = $line->findAll(array('order_id = ?' => $this->getId()));
        }

        return $this->_lines;
    }

    public function calcTotals() {

        $subtotal_excl_tax = 0;
        $total_excl_tax = 0;
        $total_tax = 0;

        foreach($this->getLines() as $line) {
            $subtotal_excl_tax += $line->getPriceExclTax();
            $total_excl_tax += $line->getTotalPriceExclTax();
        }

        // Calcule la TVA
        $tax_rate = $this->getTaxRate();
        $total_tax = $total_excl_tax * ($tax_rate / 100);

        $this->setSubtotalExclTax($subtotal_excl_tax)
            ->setTotalExclTax($total_excl_tax)
            ->setTotalTax($total_tax)
            ->setTotal($total_excl_tax + $total_tax)
        ;

        return $this;
    }

    public function save() {

        if (!$this->getId()) {
            $this->setNumber($this->_getNewNumber());
            if(!$this->getStatus()) $this->setStatusId(self::DEFAULT_STATUS);
        }

        parent::save();

        if (!empty($this->_lines)) {
            foreach ($this->_lines as $line) {
                $line->setOrderId($this->getId())
                    ->save()
                ;
            }
        }
    }

    public function toInvoice() {

        $invoice = new Sales_Model_Invoice();
//        $this->setData("taxes",$this->getTaxes()));
        $invoice->setData($this->getData())
            ->unsNumber()
            ->unsCreatedAt()
            ->setId(null)
        ;

        foreach($this->getLines() as $line) {
            $invoice_line = new Sales_Model_Invoice_Line();
            $invoice_line->setData($line->getData())
                ->setId(null)
            ;

            $invoice->addLine($invoice_line);
        }

        return $invoice;

    }

    public function pay() {

        $this->setPaidAt(Siberian_Date::now()->toString("yyyy-MM-dd HH:mm:ss"))
            ->setStatus(self::PAID_STATUS)
            ->save()
        ;

        if($this->hasToBePaid()) {
            $this->toInvoice()
                ->save();
        }

        return $this;

    }

    public function cancel() {
        $this->setStatusId(self::CANCEL_STATUS)->save();
        return $this;
    }

    public function isRecurrent() {
        foreach($this->getLines() as $line) {
            if($line->getIsRecurrent() == 1) {
                return true;
            }
        }
        return false;
    }

    public function hasToBePaid() {
        return $this->getTotal() > 0;
    }

    public function getTaxes() {
        try {
            $taxes = @unserialize($this->getData("taxes"));
        } catch(Exception $e) {
            $taxes = array();
        }

        if(!$taxes) {
            $taxes = array();
        }
        return $taxes;
    }

    public function getTaxRate() {

        $tax_rate = 0;
        foreach($this->getTaxes() as $tax) {
            $tax_rate += $tax->getRate();
        }

        return $tax_rate;
    }

    public function getDbTaxRate() {
        return parent::getTaxRate();
    }

    protected function _getNewNumber() {

        $number = $this->getTable()->getNextIncrement();
        $number = str_pad($number, 3, '0', STR_PAD_LEFT);
        $now = Siberian_Date::now()->toString('ddMMy');

        return "O-$now-$number";
    }

}
