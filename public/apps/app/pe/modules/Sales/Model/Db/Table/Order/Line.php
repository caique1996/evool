<?php

class Sales_Model_Db_Table_Order_Line extends Core_Model_Db_Table {

    protected $_name    = "sales_order_line";
    protected $_primary = "line_id";
}
