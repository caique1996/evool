<?php
/**
 *
 * Schema definition for 'sales_invoice'
 *
 * Last update: 2016-04-28
 *
 */
$schemas = (!isset($schemas)) ? array() : $schemas;
$schemas['sales_invoice'] = array(
    'invoice_id' => array(
        'type' => 'int(11) unsigned',
        'auto_increment' => true,
        'primary' => true,
    ),
    'admin_id' => array(
        'type' => 'int(11) unsigned',
        'index' => array(
            'key_name' => 'KEY_ADMIN_ADMIN_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => false,
        ),
    ),
    'order_id' => array(
        'type' => 'int(11) unsigned',
        'is_null' => true,
        'index' => array(
            'key_name' => 'KEY_SALES_ORDER_ORDER_ID',
            'index_type' => 'BTREE',
            'is_null' => true,
            'is_unique' => false,
        ),
    ),
    'number' => array(
        'type' => 'varchar(20)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'app_id' => array(
        'type' => 'int(11) unsigned',
        'foreign_key' => array(
            'table' => 'application',
            'column' => 'app_id',
            'name' => 'FK_SALES_INVOICE_APP_ID',
            'on_update' => 'CASCADE',
            'on_delete' => 'NO ACTION',
        ),
        'index' => array(
            'key_name' => 'KEY_APPLICATION_APPLICATION_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => false,
        ),
    ),
    'app_name' => array(
        'type' => 'varchar(30)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'subscription_id' => array(
        'type' => 'int(11) unsigned',
        'foreign_key' => array(
            'table' => 'subscription',
            'column' => 'subscription_id',
            'name' => 'FK_SALES_INVOICE_SUBSCRIPTION_ID',
            'on_update' => 'CASCADE',
            'on_delete' => 'NO ACTION',
        ),
        'index' => array(
            'key_name' => 'KEY_SUBSCRIPTION_SUBSCRIPTION_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => false,
        ),
    ),
    'admin_company' => array(
        'type' => 'varchar(100)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_name' => array(
        'type' => 'varchar(100)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_email' => array(
        'type' => 'varchar(200)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_address' => array(
        'type' => 'text',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_address2' => array(
        'type' => 'varchar(255)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_city' => array(
        'type' => 'varchar(255)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_region' => array(
        'type' => 'varchar(255)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'payment_method' => array(
        'type' => 'varchar(255)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_zip_code' => array(
        'type' => 'varchar(255)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_country' => array(
        'type' => 'varchar(100)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'admin_vat_number' => array(
        'type' => 'varchar(100)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'subtotal_excl_tax' => array(
        'type' => 'decimal(8,4)',
    ),
    'total_excl_tax' => array(
        'type' => 'decimal(8,4)',
    ),
    'tax_rate' => array(
        'type' => 'decimal(5,2)',
        'default' => '0.00',
    ),
    'total_tax' => array(
        'type' => 'decimal(8,4)',
    ),
    'total' => array(
        'type' => 'decimal(8,4)',
    ),
    'created_at' => array(
        'type' => 'datetime',
    ),
);