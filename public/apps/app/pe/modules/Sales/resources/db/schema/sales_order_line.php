<?php
/**
 *
 * Schema definition for 'sales_order_line'
 *
 * Last update: 2016-04-28
 *
 */
$schemas = (!isset($schemas)) ? array() : $schemas;
$schemas['sales_order_line'] = array(
    'line_id' => array(
        'type' => 'int(11) unsigned',
        'auto_increment' => true,
        'primary' => true,
    ),
    'order_id' => array(
        'type' => 'int(11) unsigned',
        'foreign_key' => array(
            'table' => 'sales_order',
            'column' => 'order_id',
            'name' => 'FK_SALES_ORDER_LINE_ORDER_ID',
            'on_update' => 'CASCADE',
            'on_delete' => 'CASCADE',
        ),
        'index' => array(
            'key_name' => 'KEY_SALES_ORDER_ORDER_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => false,
        ),
    ),
    'name' => array(
        'type' => 'varchar(255)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'price_excl_tax' => array(
        'type' => 'decimal(8,4)',
        'is_null' => true,
    ),
    'qty' => array(
        'type' => 'decimal(4,2)',
        'default' => '1.00',
    ),
    'total_price_excl_tax' => array(
        'type' => 'decimal(8,4)',
        'is_null' => true,
    ),
    'is_recurrent' => array(
        'type' => 'tinyint(1)',
        'default' => '0',
    ),
);