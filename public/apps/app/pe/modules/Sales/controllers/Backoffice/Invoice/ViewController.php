<?php

class Sales_Backoffice_Invoice_ViewController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Invoice"),
            "icon" => "fa-file",
        );

        $this->_sendHtml($html);

    }

    public function findAction() {

        $data = array();
        $invoice = new Sales_Model_Invoice();
        $invoice->find($this->getRequest()->getParam("invoice_id"));
        if($invoice->getId()) {
            $invoice->setFormattedSubtotalExclTax($invoice->getFormattedSubtotalExclTax());
            $invoice->setFormattedTotalExclTax($invoice->getFormattedTotalExclTax());
            $invoice->setFormattedTotalTax($invoice->getFormattedTotalTax());
            $invoice->setFormattedTotal($invoice->getFormattedTotal());
            $invoice->setFormattedCreatedAt($invoice->getFormattedCreatedAt());
            $invoice->setAdminAddress(nl2br($invoice->getAdminAddress()));
            $data["invoice"] = $invoice->getData();

            $lines = array();
            foreach($invoice->getLines() as $line) {
                $lines[] = array(
                    "name" => $line->getName(),
                    "formatted_price_excl_tax" => $line->getFormattedPriceExclTax(),
                    "qty" => $line->getQty(),
                    "formatted_total_price_excl_tax" => $line->getFormattedTotalPriceExclTax(),
                );
            }
            $data["invoice"]["lines"] = $lines;
        }

        $data["section_title"] = $this->_("Invoice %s", $invoice->getNumber());

        $data["my_info"] = array(
            "name" => System_Model_Config::getValueFor("company_name"),
            "address" => nl2br(System_Model_Config::getValueFor("company_address")),
            "country" => System_Model_Config::getValueFor("company_country"),
            "phone" => System_Model_Config::getValueFor("company_phone"),
            "vat_number" => System_Model_Config::getValueFor("company_vat_number")
        );

        $this->_sendHtml($data);

    }

}
