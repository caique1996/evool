<?php

class Sales_Backoffice_Invoice_ListController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Invoices"),
            "icon" => "fa-file",
        );

        $this->_sendHtml($html);

    }

    public function findallAction() {

        $invoice = new Sales_Model_Invoice();
        $invoices = $invoice->findAll();
        $data = array();

        foreach($invoices as $invoice) {
            $data[] = array(
                "id" => $invoice->getId(),
                "number" => $invoice->getNumber(),
                "app_name" => $invoice->getAppName(),
                "user_name" => $invoice->getAdminName(),
                "total" => $invoice->getFormattedTotal(),
                "paid_at" => $invoice->getFormattedCreatedAt(Zend_Date::DATETIME_SHORT)
            );
        }

        $this->_sendHtml($data);

    }

}
