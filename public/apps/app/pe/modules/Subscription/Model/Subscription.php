<?php

class Subscription_Model_Subscription extends Core_Model_Default {

    const MONTHLY_FRENQUENCY = "Monthly";
    const YEARLY_FRENQUENCY = "Yearly";

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Subscription_Model_Db_Table_Subscription';
        return $this;
    }

    public function isActive() {
        return $this->getData("is_active");
    }

    public function toOrder() {

        $order = new Sales_Model_Order();
        $admin = $this->getAdmin();
        $tax = new Tax_Model_Tax();
        $tax_rate = null;
        $countries = Siberian_Locale::getEuroZoneTerritoryDatas();
        $store_country = System_Model_Config::getValueFor("company_country");
        $admin_country = $admin->getCountryCode();
        $admin_region = $admin->getRegionCode();
        // If the store is European, the client is European but doesn't have a VAT Number, he pays the store tax
        // if(array_key_exists($store_country, $countries) AND array_key_exists($admin_country, $countries)) {
        //     if(!$admin->getVatNumber()) {
        //         $tax->find($store_country, "country_code");
        //     }
        // } else {
//            $tax->find($admin_country, "country_code");
        // }

        $taxes = array();
        $taxes_collection = $tax->findAll(array("country_code" => $admin_country, "region_code" => $admin_region));
        if(!count($taxes_collection)) {
            $tax->find($admin_country, "country_code");
            $taxes = $tax->getId() ? array($tax) : null;
        } else {
            foreach($taxes_collection as $tax) {
                $taxes[] = $tax;
            }
        }

        $order->setTaxes(@serialize($taxes));

        if($this->getSetupFee()) {
            $qty = 1;
            $line = new Sales_Model_Order_Line();
            $line->setRef($this->getRef())
                ->setName($this->_("Setup Fee"))
                ->setPriceExclTax($this->getSetupFee())
                ->setQty($qty)
                ->setTotalPriceExclTax($this->getSetupFee() * $qty)
            ;

            $order->addLine($line);
        }

        if($this->getRegularPayment()) {
            $qty = 1;
            $name = $this->_("%s Subscription for %s", $this->getTranslatedPaymentFrequency(), $this->getCurrentApplication()->getName());
            $line = new Sales_Model_Order_Line();
            $line->setRef($this->getRef())
                ->setName($name)
                ->setPriceExclTax($this->getRegularPayment())
                ->setQty($qty)
                ->setTotalPriceExclTax($this->getRegularPayment() * $qty)
                ->setIsRecurrent(1)
            ;

            $order->addLine($line);
        }

        /** saving the tax rate in db */
        $order->setTaxRate($order->getTaxRate());

        $order->calcTotals();

        return $order;

    }

    public function getPriceHtml() {

        $html = array();
        $frequency = "%s";
        $has_setup_fees = is_numeric($this->getSetupFee());

        if ($has_setup_fees) {
            $html[] = $this->getFormattedSetupFee();
        }

        if ($this->getRegularPayment()) {

            if($has_setup_fees) {
                $html[] = "+";
            }

            $html[] = $this->getFormattedRegularPayment();

            if($this->getPaymentFrequency() == self::MONTHLY_FRENQUENCY) {
                $frequency = "%s / Month";
            } else if($this->getPaymentFrequency() == self::YEARLY_FRENQUENCY) {
                $frequency = "%s / Year";
            }
        }

        if(!$has_setup_fees AND ! $this->getRegularPayment()) {
            $frequency = "Free";
        }

        return $this->_($frequency, implode($has_setup_fees ? " " : "", $html));

    }

    public function getSetupFee() {
        $setup_fee = $this->getData("setup_fee");
        return $setup_fee > 0 ? $setup_fee : null;
    }

    public function getRegularPayment() {
        $regular_payment = $this->getData("regular_payment");
        return $regular_payment > 0 ? $regular_payment : null;
    }

    public function isFree() {
        return $this->getSetupFee() + $this->getRegularPayment() <= 0;
    }

    public function getTranslatedPaymentFrequency() {
        return $this->_($this->getData("payment_frequency"));
    }

}
