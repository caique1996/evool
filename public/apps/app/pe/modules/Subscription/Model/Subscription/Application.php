<?php

class Subscription_Model_Subscription_Application extends Core_Model_Default {

    protected $_app;
    protected $_subscription;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Subscription_Model_Db_Table_Subscription_Application';
        return $this;
    }

    public function find($id, $field = null) {
        parent::find($id, $field);

        if($this->getId()) {
            $detail = new Subscription_Model_Subscription_Application_Detail();
            $details = $detail->findAll(array("subscription_app_id" => $this->getId()));
            foreach($details as $detail) {
                $this->setData($detail->getCode(), $detail->getValue());
            }
        }

        return $this;
    }

    public function findExpiredSubscriptions($code) {
        return $this->getTable()->findExpiredSubscriptions($code);
    }

    public function findByDetail($code, $value) {
        $row = $this->getTable()->findByDetail($code, $value);
        $this->_prepareDatas($row);
        return $this;
    }

    public function save() {

        parent::save();

        if($payment_data = $this->getPaymentData() AND is_array($payment_data)) {
            $is_recurrent = $payment_data["is_recurrent"];

            if($is_recurrent) {
                foreach ($payment_data as $code => $value) {
                    $detail = new Subscription_Model_Subscription_Application_Detail();
                    $detail->find(array("subscription_app_id" => $this->getId(), "code" => $code));

                    if (!$detail->getId()) {
                        $detail->setSubscriptionAppId($this->getId())
                            ->setCode($code);
                    }

                    $detail->setValue($value)
                        ->save();

                }
            }
        }

        if($this->isActive() == $this->getApplication()->isLocked()) {
            $this->getApplication()
                ->setIsLocked(!$this->isActive())
                ->save()
            ;
        }
    }

    function getSubscriptionsList(){
        return $this->getTable()->getSubscriptionsList();
    }

    public function getSubscription() {
        if(!$this->_subscription) {
            $this->_subscription = new Subscription_Model_Subscription();
            $this->_subscription->find($this->getSubscriptionId());
        }

        return $this->_subscription;
    }

    public function getApplication() {

        if(!$this->_app) {
            $this->_app = new Application_Model_Application();
            $this->_app->find($this->getAppId());
        }

        return $this->_app;
    }

    public function isActive() {
        return (bool) $this->getData("is_active");
    }

    public function create($app_id, $subscription_id) {

        $this->find($app_id, "app_id");

        if($this->getId()) {
            $this->setCreatedAt(Zend_Date::now()->toString("y-MM-dd HH:mm:ss"))
                ->setUpdatedAt(null)
            ;
            $detail = new Subscription_Model_Subscription_Application_Detail();
            $details = $detail->findAll(array("subscription_app_id" => $this->getId()));
            foreach($details as $detail) $detail->delete();
        }

        $this->setSubscriptionId($subscription_id)
            ->setAppId($app_id)
            ->setIsActive(1)
        ;

        $this->update();

        return $this;

    }

    public function update() {

        $subscription = $this->getSubscription();

        if($subscription->getPaymentFrequency()) {

            $expire_at = new Siberian_Date($this->getExpireAt(),"y-MM-dd HH:mm:ss");
            if ($subscription->getPaymentFrequency() == Subscription_Model_Subscription::MONTHLY_FRENQUENCY) {
                $expire_at->addMonth(1);
            } else if ($subscription->getPaymentFrequency() == Subscription_Model_Subscription::YEARLY_FRENQUENCY) {
                $expire_at->addYear(1);
            }

            $this->setExpireAt($expire_at->toString("yyyy-MM-dd HH:mm:ss"));
        } else {
            $this->setExpireAt(null);
        }

        $this->save();

        return $this;

    }

    public function invoice() {

        $invoice = new Sales_Model_Invoice();
        $dummy = new Sales_Model_Invoice();
        $dummy->find(array(
            "subscription_id" => $this->getSubscriptionId(),
            "app_id" => $this->getAppId()
        ));

        foreach($dummy->getLines() as $dummy_line) {
            if($dummy_line->getIsRecurrent()) {
                $dummy_line->setId(null);
                $invoice->addLine($dummy_line);
            }
        }

        $invoice->setData($dummy->getData())
            ->setId(null)
            ->unsNumber()
            ->unsCreatedAt()
            ->calcTotal()
        ;

        $invoice->save();

        return $this;

    }

    public function getDetails() {
        $detail = new Subscription_Model_Subscription_Application_Detail();
        $details = $detail->findAll(array("subscription_app_id" => $this->getSubscriptionAppId()));
        $array_details = array();
        foreach($details as $detail) {
            $array_details[$detail->getCode()] = $detail->getValue();
        }

        return $array_details;
    }
}
