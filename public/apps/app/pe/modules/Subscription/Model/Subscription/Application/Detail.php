<?php

class Subscription_Model_Subscription_Application_Detail extends Core_Model_Default {

    protected $_app;
    protected $_subscription;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Subscription_Model_Db_Table_Subscription_Application_Detail';
        return $this;
    }
}