<?php

class Subscription_ApplicationController extends Application_Controller_Default
{

    protected $_order;

    public function createAction() {

        $admin_can_publish = $this->getSession()->getAdmin()->canPublishThemself();

        if($errors = $this->getApplication()->isAvailableForPublishing($admin_can_publish) AND !$this->getApplication()->isFreeTrialExpired()) {
            $message = $this->_('In order to publish your application, we need:<br />- ');
            $message .= join('<br />- ', $errors);
            $this->getSession()->addError($message);
            $this->redirect("application/customization_publication_infos");
            return $this;
        } else if($this->getApplication()->getSubscription()->isActive()) {
            $this->getSession()->addWarning($this->_("You already have a subscription for this application"));
            $this->redirect("application/customization_publication_infos");
            return $this;
        }

        if($order_id = $this->getSession()->order_id) {
            $order = new Sales_Model_Order();
            $order->find($order_id);
            $order->cancel();
            $this->getSession()->order_id = null;
        }

        $this->loadPartials();

    }

    public function saveaddressAction() {

        $html = array();

        if($data = $this->getRequest()->getPost()) {

            try {

                $admin = $this->getSession()->getAdmin();

                if(empty($data["company"])) {
                    throw new Exception($this->_("Please, enter a company"));
                }
                if(empty($data["address"])) {
                    throw new Exception($this->_("Please, enter an address"));
                }
                if(empty($data["country_code"])) {
                    throw new Exception($this->_("Please, enter a country"));
                }
                if(!empty($data["vat_number"])) {

                    if(!$this->__checkVatNumber($data["vat_number"], $data["country_code"])) {
                        throw new Exception($this->_("Your VAT Number is not valid"));
                    }
                }
                $regions = Siberian_Locale::getRegions();
                if(!empty($data["region_code"])) {
                    if($regions[$data["country_code"]]) {
                        $region = $regions[$data["country_code"]][$data["region_code"]];
                        $region_code = $data["region_code"];
                    }else{
                        $region = null;
                        $region_code = null;
                    }
                }else{
                    $region = null;
                    $region_code = null;
                }
                $admin->setCompany($data["company"])
                    ->setFirstname($data["firstname"])
                    ->setLastname($data["lastname"])
                    ->setAddress($data["address"])
                    ->setAddress2($data["address2"])
                    ->setZipCode($data["zip_code"])
                    ->setCity($data["city"])
                    ->setRegionCode($region_code)
                    ->setRegion($region)
                    ->setCountryCode($data["country_code"])
                    ->setVatNumber($data["vat_number"])
                    ->setPhone($data["phone"])
                    ->save()
                ;

                $html = array("success" => 1);

                if($this->getSession()->subscription_id) {
                    $subscription = new Subscription_Model_Subscription();
                    $subscription->find($this->getSession()->subscription_id);

                    $order_details = $this->_getOrderDetailsHtml($subscription);

                    $html["order_details_html"] = $order_details;

                }

            } catch (Exception $e) {
                $html = array(
                    'error' => 1,
                    'message' => $e->getMessage()
                );
            }

            $this->_sendHtml($html);
        }
    }

    public function setAction() {

        if($data = $this->getRequest()->getPost()) {

            try {

                if(empty($data["subscription_id"])) {
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                $subscription = new Subscription_Model_Subscription();
                $subscription->find($data["subscription_id"]);

                if(!$subscription->getId()) {
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                $order_details = $this->_getOrderDetailsHtml($subscription);

                $this->getSession()->subscription_id = $subscription->getId();

                $html = array(
                    "success" => 1,
                    "order_details_html" => $order_details,
                    "has_to_be_paid" => $this->_subscriptionToOrder($subscription)->hasToBePaid()
                );

            } catch(Exception $e) {
                $html = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($html);

        }

    }

    public function processAction() {

        if($subscription_id = $this->getSession()->subscription_id) {

            try {

                $subscription = new Subscription_Model_Subscription();
                $subscription->find($subscription_id);

                if (!$subscription->getId()) {
                    throw new Exception($this->_("An error occurred while saving. The selected subscription is not valid."));
                }

                $admin = $this->getAdmin();
                $subscription->setCurrentApplication($this->getApplication())
                    ->setAdmin($admin)
                ;
                $order = $subscription->toOrder();
                $order->setStatus(Sales_Model_Order::DEFAULT_STATUS)
                    ->setAppId($this->getApplication()->getId())
                    ->setAppName($this->getApplication()->getName())
                    ->setSubscriptionId($subscription->getId())
                    ->setAdminId($admin->getId())
                    ->setAdminCompany($admin->getCompany())
                    ->setAdminName($admin->getFirstname() . " " . $admin->getLastname())
                    ->setAdminEmail($admin->getEmail())
                    ->setAdminAddress($admin->getAddress())
                    ->setAdminAddress2($admin->getAddress2())
                    ->setAdminCity($admin->getCity())
                    ->setAdminZipCode($admin->getZipCode())
                    ->setAdminRegion($admin->getRegion())
                    ->setAdminCountry($admin->getCountry())
                    ->setAdminPhone($admin->getPhone())
                    ->setPaymentMethod($this->getRequest()->getPost("payment_method"))
                    ->save()
                ;

                $this->getSession()->order_id = $order->getId();

                if($order->hasToBePaid()) {
                    $payment = new Payment_Model_Payment();
                    $params = array(
                        'payment_method' => $this->getRequest()->getPost("payment_method"),
                        'order' => $order
                    );

                    $html = $payment->getPaymentData($params);
                } else {
                    $html = array("url" => $this->getUrl("subscription/application/success"));
                }

            } catch(Exception $e) {
                $html = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($html);

        }

    }

    public function successAction() {

        if($order_id = $this->getSession()->order_id) {

            $order = new Sales_Model_Order();
            $order->find($order_id);

            $payment = new Payment_Model_Payment();

            if($order->hasToBePaid()) {
                $params = array(
                    'payment_method' => $this->getRequest()->getParam("payment_method"),
                    'token' => $this->getRequest()->getParam('token'),
                    'key' => $this->getRequest()->getParam('key'),
                    'order_number' => $this->getRequest()->getParam('order_number')
                );

                if(!$params["payment_method"]) {
                    $success = false;
                } else {
                    $success = $payment->setData($params)->setOrder($order)->success();
                }
            } else {
                $success = array();
            }

            if(!is_array($success)) {

                $this->getSession()->addWarning($this->_("An error occurred while processing the payment. For more information, please feel free to contact us."));
                $this->redirect("subscription/application/create");

            } else {

                $order->pay();
                $subscription = new Subscription_Model_Subscription_Application();
                $subscription->create($order->getAppId(), $order->getSubscriptionId())
                    ->setPaymentData(isset($success["payment_data"]) ? $success["payment_data"] : null)
                    ->setPaymentMethod($payment->getCode())
                    ->save();

                $this->getSession()->subscription_id = null;
                $this->getSession()->order_id = null;

                if ($this->getApplication()->getIsLocked()) {
                    $this->getApplication()->setIsLocked(false)->save();
                }

                $this->loadPartials();
            }

        }
    }

    public function cancelAction() {

        $payment = new Payment_Model_Payment();
        $params = array(
            'payment_method' => $this->getRequest()->getParam("payment_method")
        );
        $cancel = $payment->setData($params)->cancel();
        $this->redirect("subscription/application/create");

    }

    protected function _getOrderDetailsHtml($subscription) {

        $order = $this->_subscriptionToOrder($subscription);
        return $this->getLayout()->addPartial("order_details", "admin_view_default", "subscription/application/create/order_details.phtml")
            ->setTmpOrder($order)
            ->toHtml()
        ;

    }

    private function __checkVatNumber($vatNumber, $countryCode) {

        // Serialize the VAT Number
        $vatNumber = str_replace(array(' ', '.', '-', ',', ', '), '', $vatNumber);
        // Retrieve the country code
        $countryCodeInVat = substr($vatNumber, 0, 2);
        // Retrieve the VAT Number
        $vatNumber = substr($vatNumber, 2);

        // Check the VAT Number syntax
        if (strlen($countryCode) != 2 || is_numeric(substr($countryCode, 0, 1)) || is_numeric(substr($countryCode, 1, 2))) {
            return false;
        }

        // Check if the country code in the VAT Number is the same than the parameter
        if ($countryCodeInVat != $countryCode) {
            return false;
        }

        // Call the webservice
        $client = new SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");
        $params = array('countryCode' => $countryCode, 'vatNumber' => $vatNumber);
        $result = $client->checkVat($params);

        // Return the result
        return $result->valid;
    }

    protected function _subscriptionToOrder($subscription) {

        if(!$this->_order) {
            $subscription->setCurrentApplication($this->getApplication())
                ->setAdmin($this->getSession()->getAdmin());

            $this->_order = $subscription->toOrder();
        }

        return $this->_order;
    }
}
