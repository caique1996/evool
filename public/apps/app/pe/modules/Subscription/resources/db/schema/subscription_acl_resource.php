<?php
/**
 *
 * Schema definition for 'subscription_acl_resource'
 *
 * Last update: 2016-04-28
 *
 */
$schemas = (!isset($schemas)) ? array() : $schemas;
$schemas['subscription_acl_resource'] = array(
    'subscription_resource_id' => array(
        'type' => 'int(11) unsigned',
        'auto_increment' => true,
        'primary' => true,
    ),
    'subscription_id' => array(
        'type' => 'int(11) unsigned',
    ),
    'resource_id' => array(
        'type' => 'int(11) unsigned',
    ),
);