<?php
/**
 *
 * Schema definition for 'subscription_application'
 *
 * Last update: 2016-04-28
 *
 */
$schemas = (!isset($schemas)) ? array() : $schemas;
$schemas['subscription_application'] = array(
    'subscription_app_id' => array(
        'type' => 'int(11) unsigned',
        'auto_increment' => true,
        'primary' => true,
    ),
    'subscription_id' => array(
        'type' => 'int(11) unsigned',
        'foreign_key' => array(
            'table' => 'subscription',
            'column' => 'subscription_id',
            'name' => 'FK_SUBSCRIPTION_SUBSCRIPTION_ID',
            'on_update' => 'CASCADE',
            'on_delete' => 'CASCADE',
        ),
        'index' => array(
            'key_name' => 'IDX_SUBSCRIPTION_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => false,
        ),
    ),
    'app_id' => array(
        'type' => 'int(11) unsigned',
        'unique' => true,
        'foreign_key' => array(
            'table' => 'application',
            'column' => 'app_id',
            'name' => 'FK_APPLICATION_APP_ID',
            'on_update' => 'CASCADE',
            'on_delete' => 'CASCADE',
        ),
        'index' => array(
            'key_name' => 'UNIQUE_APP_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => true,
        ),
    ),
    'payment_method' => array(
        'type' => 'varchar(15)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'is_active' => array(
        'type' => 'tinyint(1)',
        'default' => '0',
    ),
    'expire_at' => array(
        'type' => 'datetime',
        'is_null' => true,
    ),
    'created_at' => array(
        'type' => 'datetime',
    ),
    'updated_at' => array(
        'type' => 'datetime',
    ),
);