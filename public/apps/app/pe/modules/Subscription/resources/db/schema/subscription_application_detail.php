<?php
/**
 *
 * Schema definition for 'subscription_application_detail'
 *
 * Last update: 2016-04-28
 *
 */
$schemas = (!isset($schemas)) ? array() : $schemas;
$schemas['subscription_application_detail'] = array(
    'detail_id' => array(
        'type' => 'int(11) unsigned',
        'auto_increment' => true,
        'primary' => true,
    ),
    'subscription_app_id' => array(
        'type' => 'int(11) unsigned',
        'foreign_key' => array(
            'table' => 'subscription_application',
            'column' => 'subscription_app_id',
            'name' => 'FK_SUBSCRIPTION_APPLICATION_DETAIL_SUBSCRIPTION_APP_ID',
            'on_update' => 'CASCADE',
            'on_delete' => 'CASCADE',
        ),
        'index' => array(
            'key_name' => 'IDX_SUBSCRIPTION_APP_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => false,
        ),
    ),
    'code' => array(
        'type' => 'varchar(50)',
        'charset' => 'utf8',
        'collation' => 'utf8_general_ci',
    ),
    'value' => array(
        'type' => 'varchar(255)',
        'charset' => 'utf8',
        'collation' => 'utf8_general_ci',
    ),
);