<?php
/**
 *
 * Schema definition for 'subscription'
 *
 * Last update: 2016-04-28
 *
 */
$schemas = (!isset($schemas)) ? array() : $schemas;
$schemas['subscription'] = array(
    'subscription_id' => array(
        'type' => 'int(11) unsigned',
        'auto_increment' => true,
        'primary' => true,
    ),
    'name' => array(
        'type' => 'varchar(100)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'description' => array(
        'type' => 'text',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'setup_fee' => array(
        'type' => 'decimal(12,4)',
        'is_null' => true,
    ),
    'payment_frequency' => array(
        'type' => 'varchar(10)',
        'is_null' => true,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'regular_payment' => array(
        'type' => 'decimal(12,4)',
        'is_null' => true,
    ),
    'use_ads' => array(
        'type' => 'tinyint(1)',
        'default' => '0',
    ),
    'is_active' => array(
        'type' => 'tinyint(1)',
        'default' => '1',
    ),
    'created_at' => array(
        'type' => 'datetime',
    ),
    'updated_at' => array(
        'type' => 'datetime',
    ),
);