<?php

class System_Backoffice_Config_TcController extends System_Controller_Backoffice_Default {

    protected $_codes = array("sales_tc");

    public function loadAction() {

        $html = array(
            "title" => $this->_("Terms & Conditions"),
            "icon" => "fa-file-text",
        );

        $this->_sendHtml($html);

    }

}