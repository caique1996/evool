<?php

class Tax_Model_Db_Table_Tax extends Core_Model_Db_Table {

    protected $_name = "tax";
    protected $_primary = "tax_id";

    public function deleteAll() {
        $this->beginTransaction();

        try {

            $this->_db->delete($this->_name);
            $this->commit();
            return true;
        } catch(Exception $e) {
            $this->rollback();
            return false;
        }
    }
}
