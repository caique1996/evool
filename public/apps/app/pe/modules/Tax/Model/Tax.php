<?php

class Tax_Model_Tax extends Core_Model_Default {

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Tax_Model_Db_Table_Tax';
        return $this;
    }

    public function deleteAll() {
        return $this->getTable()->deleteAll();
    }

}
