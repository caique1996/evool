<?php

class Tax_Backoffice_ListController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Taxes"),
            "icon" => "fa-file",
        );

        $this->_sendHtml($html);

    }

    public function findallAction() {

        $tax = new Tax_Model_Tax();
        $taxes = $tax->findAll();
        $data = array("taxes" => array(), "countries" => array());
        $countries = Zend_Registry::get('Zend_Locale')->getTranslationList('Territory', null, 2);
        $regions = Siberian_Locale::getRegions();

        $current_country = null;
        $id = -1;
        foreach($taxes as $tax) {
            if($current_country != $tax->getCountryCode()) {
                $id++;
                $current_country = $tax->getCountryCode();
                $data["taxes"][$id] = array(
                    "id" => $tax->getId(),
                    "country_code" => $tax->getCountryCode(),
                    "country_name" => $countries[$tax->getCountryCode()],
                    "rates" => array(
                        array(
                            "region_code" => $tax->getRegionCode(),
                            "rate" => (float)$tax->getRate(),
                            "name" => $tax->getName()
                        )
                    )
                );
            } else {
                $data["taxes"][$id]["rates"][] = array(
                    "region_code" => $tax->getRegionCode(),
                    "rate" => (float)$tax->getRate(),
                    "name" => $tax->getName()
                );
            }
        }

        $data["countries"] = $countries;
        $data["regions"] = $regions;

        $this->_sendHtml($data);

    }

    public function saveAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {
            try {

                $tax = new Tax_Model_Tax();
                $tax->deleteAll();

                foreach($data["taxes"] as $tax) {

                    if($tax["country_code"]) {
                        foreach($tax["rates"] as $rate) {
                            if($rate["rate"]) {
                                $data_to_save = array(
                                    "country_code" => $tax["country_code"],
                                    "region_code" => $rate["region_code"],
                                    "rate" => $rate["rate"],
                                    "name" => $rate["name"]
                                );

                                $tax_object = new Tax_Model_Tax();
                                $tax_object->addData($data_to_save)->save();
                            }
                        }
                    }
                }

                $data = array(
                    "success" => 1,
                    "message" => $this->_("Info successfully saved")
                );

            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);
        }

    }

}
