<?php

$providers = array(
    array(
        "code" => "paypal",
        "name" => "Paypal",
        "icon" => "fa-paypal",
        "keys" => array(
            "username",
            "password",
            "signature"
        )
    ), array(
        "code" => "2checkout",
        "name" => "2Checkout",
        "icon" => "fa-credit-card",
        "keys" => array(
            "sid",
            "secret",
            "username",
            "password"
        )
    )
);

foreach($providers as $provider_data) {

    $provider = new Api_Model_Provider();
    $provider->find($provider_data["code"], "code");

    if(!$provider->getId()) {

        $provider->setData(array(
            'code' => $provider_data["code"],
            'name' => $provider_data["name"],
            'icon' => $provider_data["icon"]
        ))->save();

        foreach ($provider_data["keys"] as $key) {
            $data = array(
                'provider_id' => $provider->getId(),
                'key' => $key
            );
            $key = new Api_Model_Key();
            $key->setData($data)->save();
        }
    }
}
