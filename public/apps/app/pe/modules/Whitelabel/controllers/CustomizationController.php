<?php

class Whitelabel_CustomizationController extends Admin_Controller_Default {

    public function getstyleAction() {

        if($data = $this->getRequest()->getParams()) {

            try {
                if(!isset($data['white_label_editor_id']) OR $data['white_label_editor_id'] != 0 AND $data['white_label_editor_id'] != $this->getAdmin()->getWhiteLabelEditor()->getId()) {
                    throw new Exception($this->_('An error occurred during process. Please try again later.'));
                }

                $white_label_editor_id = $data['white_label_editor_id'];
                $block = new Template_Model_Block();
                $blocks = $block->findByWhiteLabelEditor($white_label_editor_id);

                $html = array('success' => 1, 'style' => array());
                $notImportant = array('general');
                foreach($blocks as $block) {
                    $html['style'][$block->getCode()]['html'] = $this->getLayout()->addPartial("style_default", "Front_View_Default", "page/layout/css/block.phtml")
                        ->setStyleType($white_label_editor_id == 0 ? 'default' : 'custom')
                        ->setCurrentBlock($block)
                        ->setIsImportant(!in_array($block->getCode(), $notImportant))
                        ->toHtml()
                    ;

                    foreach($block->getData() as $key => $data) {
//                        if(stripos($key, 'use_') !== false AND $block->getData($key) OR in_array($key, array("color", "background_color"))) {
                            $key = str_replace('use_', '', $key);
                            $html['style'][$block->getCode()][$key] = str_replace('#', '', $block->getData($key));
//                        }
                    }
                }

                $logo = "";
                $favicon = "";

                if($white_label_editor_id == 0) {
                    $view = new Core_View_Default();
                    $logo = System_Model_Config::getValueFor("logo");
                    $logo = $logo ? $logo : $view->getImage("header/logo.png");
                    $favicon = System_Model_Config::getValueFor("favicon");
                    $favicon = $favicon ? $favicon : "/favicon.png";
                } else {
                    $logo = $this->getAdmin()->getWhiteLabelEditor()->getLogoUrl();
                    $favicon = $this->getAdmin()->getWhiteLabelEditor()->getFaviconUrl();
                }


                $html['style']['logo'] = $logo;
                $html['style']['favicon'] = $favicon;

            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1,
                );
            }

            $this->getResponse()->setBody(Zend_Json::encode($html))->sendResponse();
            die;

        }

    }

    public function savecolorsAction() {

        if($data = $this->getRequest()->getPost()) {

            try {

                if(empty($data['block_id'])) {
                    throw new Exception($this->_("An error occurred while saving. Please, try again later."));
                }

                $white_label_editor = $this->getAdmin()->getWhiteLabelEditor();

                $block = new Template_Model_Block();
                $block->find($data["block_id"]);

                if(!$block->getId() OR $block->getTypeId() != Template_Model_Block::TYPE_WHITE_LABEL_EDITOR) {
                    throw new Exception("An error occurred while saving. Please, try again later.");
                }

                $data["white_label_editor_id"] = $white_label_editor->getId();
                $data['block_id'] = $block->getId();

                $block->setData($data)
                    ->save()
                ;

                $html = array('success' => '1');
            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage()
                );
            }

            $this->getResponse()->setBody(Zend_Json::encode($html))->sendResponse();
            die;
        }

    }

    public function savelogoAction() {

        if($data = $this->getRequest()->getPost()) {

            try {

                $type = $this->getRequest()->getParam('type');
                $admin = $this->getAdmin();
                $formatted_name = Core_Model_Lib_String::format($admin->getEmail(), true);
                $white_label_editor = $admin->getWhiteLabelEditor();
                $relative = $white_label_editor->getImagePath($type);
                
                if(!is_dir(Core_Model_Directory::getBasePathTo($relative))) {
                    @mkdir(Core_Model_Directory::getBasePathTo($relative), 0777, true);
                }

                $save_path  = "";

                $data['dest_folder'] = $_SERVER['DOCUMENT_ROOT'].$relative;
                if($type == 'favicon') {
                    $data['new_name'] = uniqid() . '.ico';
                }

                $uploader = new Core_Model_Lib_Uploader();
                $file = $uploader->savecrop($data);

                if($type == 'logo') {
                    $white_label_editor->setLogo($file)->save();
                } elseif($type == 'favicon') {
                    $white_label_editor->setFavicon($file)->save();
                }

                $data = array(
                    'success' => 1,
                    'file' =>  $relative.$file
                );

            } catch (Exception $e) {
                $data = array(
                    'error' => 1,
                    'message' => $e->getMessage()
                );
            }

            $this->getLayout()->setHtml(Zend_Json::encode($data));

        }

    }


}
