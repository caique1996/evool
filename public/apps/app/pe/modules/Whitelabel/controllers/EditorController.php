<?php

class Whitelabel_EditorController extends Admin_Controller_Default {

    public function indexAction() {
        $this->loadPartials();
    }

    public function saveAction() {

        if($data = $this->getRequest()->getPost()) {

            try {

                $html = array();
                $editor = $this->getAdmin()->getWhiteLabelEditor();
                if(!$editor->getId()) {
                    $editor->setAdminId($this->getAdmin()->getId());
                }

                if(!empty($data["host"])) {

                    $parts = explode('.', $data['host']);
                    $pattern = "/^((http|https):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
                    if(!empty($data['host']) AND !preg_match($pattern, $data['host']) AND count($parts) <= 3) {
                        throw new Exception($this->_("Please enter a valid url"));
                    }

                    if(!preg_match("/^((http|https):\/\/)/", $data["host"])) {
                        if(stripos($data["host"], "://") !== false) {
                            throw new Exception($this->_("Please enter a valid url"));
                        }
                        $data["host"] = "http://".$data["host"];
                    }

                    $uri = Zend_Uri::factory($data['host']);
                    $host = $uri->getHost();

                    if($host == $this->getRequest()->getHttpHost()) {
                        throw new Exception($this->_("This URL is already used"));
                    }

                    if(!$editor->cnameIsValid($host)) {
                        throw new Exception($this->_('The CNAME for the URL "%s" is not properly set', $host));
                    }

                    $dummy = new Whitelabel_Model_Editor();
                    $dummy->find($host, "host");

                    if($dummy->getId() AND $dummy->getId() != $editor->getId()) {
                        throw new Exception($this->_("This URL is already used"));
                    }

                    $editor->setHost($host);

                    $html = array(
                        "success_message" => $this->_("Info successfully saved"),
                        "message_button" => false,
                        "message_timeout" => 3,
                        "host" => $host
                    );

                } else if(isset($data["is_active"])) {
                    $editor->setIsActive(!empty($data["is_active"]));
                } else {
                    throw new Exception($this->_("An error occurred while saving. Please, try again later."));
                }

                $editor->save();

                $html = array_merge($html, array(
                    "success" => 1,
                    "is_active" => $editor->isActive()
                ));

            } catch(Exception $e) {
                $html = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->getLayout()->setHtml(Zend_Json::encode($html));

        }

    }

}
