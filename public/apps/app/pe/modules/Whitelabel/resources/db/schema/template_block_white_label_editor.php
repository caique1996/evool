<?php
/**
 *
 * Schema definition for 'template_block_white_label_editor'
 *
 * Last update: 2016-04-28
 *
 */
$schemas = (!isset($schemas)) ? array() : $schemas;
$schemas['template_block_white_label_editor'] = array(
    'block_id' => array(
        'type' => 'int(11)',
        'primary' => true,
    ),
    'white_label_editor_id' => array(
        'type' => 'int(11) unsigned',
        'primary' => true,
        'foreign_key' => array(
            'table' => 'whitelabel_editor',
            'column' => 'editor_id',
            'name' => 'FK_TEMPLATE_BLOCK_WHITE_LABEL_EDITOR_WHITE_LABEL_EDITOR_ID',
            'on_update' => 'CASCADE',
            'on_delete' => 'CASCADE',
        ),
        'index' => array(
            'key_name' => 'KEY_WHITE_LABEL_EDITOR_ID',
            'index_type' => 'BTREE',
            'is_null' => false,
            'is_unique' => false,
        ),
    ),
    'color' => array(
        'type' => 'varchar(10)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'color_on_hover' => array(
        'type' => 'varchar(10)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'color_on_active' => array(
        'type' => 'varchar(10)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'background_color' => array(
        'type' => 'varchar(10)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'background_color_on_hover' => array(
        'type' => 'varchar(10)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
    'background_color_on_active' => array(
        'type' => 'varchar(10)',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ),
);