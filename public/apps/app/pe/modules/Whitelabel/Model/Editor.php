<?php

class Whitelabel_Model_Editor extends Core_Model_Default {

    const IMAGE_PATH = "/images/admin/white_label";

    protected $_blocks;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Whitelabel_Model_Db_Table_Editor';
        return $this;
    }

    public function getBlocks() {

        if(empty($this->_blocks)) {
            $block = new Template_Model_Block();
            $this->_blocks = $block->findAll(array('white_label_editor_id' => $this->getId()), 'position ASC');
        }

        return $this->_blocks;
    }

    public function cnameIsValid($url = null) {

        if(!$url AND (!$this->getId() OR !$this->getHost())) return true;

        if(!$url) $url = $this->getHost();

        return Core_Model_Url::checkCname($url);

    }

    public function getBlock($code) {

        $blocks = $this->getBlocks();

        foreach($blocks as $block) {
            if($block->getCode() == $code) return $block;
        }

        return;
    }

    public function setBlocks($blocks) {
        $this->_blocks = $blocks;
        return $this;
    }

    public function isActive() {
        return $this->getId() && $this->getIsActive();
    }

    public function getImagePath($type = "logo") {
        return Core_Model_Directory::getPathTo(self::IMAGE_PATH).DS.$this->getAdminId().DS.$type.DS;
    }

    public function getBaseImagePath($type = "logo") {
        return Core_Model_Directory::getBasePathTo(self::IMAGE_PATH).DS.$this->getAdminId().DS.$type.DS;
    }

    public function getFaviconUrl() {
        return $this->_getImageUrl("favicon");
    }

    public function getLogoUrl() {
        return $this->_getImageUrl("logo");
    }

    protected function _getImageUrl($type) {
        $url = null;
        if(is_file($this->getBaseImagePath($type).$this->getData($type))) {
            $url = $this->getImagePath($type).$this->getData($type);
        }
        return $url;
    }



}