<?php

abstract class Payment_Model_Abstract extends Core_Model_Default {

    protected $_code;

    public function getPaymentData() {
        return array();
    }
    public function success() {
        return false;
    }
    public function manageRecurring() {
        return false;
    }

    public function getCode() {
        return $this->_code;
    }

}