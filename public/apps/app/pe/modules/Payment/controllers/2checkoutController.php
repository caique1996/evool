<?php

class Payment_2checkoutController extends Core_Controller_Default {

    protected $_subscription;
    protected $_admin;

    public function init() {
        parent::init();

        $this->_subscription = new Subscription_Model_Subscription_Application();
        $this->_subscription->findByDetail("order_number",$this->getRequest()->getParam("sale_id"));
        $this->_admin = new Admin_Model_Admin();
        $this->_admin->findByInvoiceApp($this->_subscription->getAppId());
    }

    public function ordercreatedAction() {
        //An order has just been created
        //We use return values to complete admin data

        try {
            $this->_admin->setFirstname($this->getRequest()->getParam("customer_first_name"))
                ->setLastname($this->getRequest()->getParam("customer_last_name"))
                ->setAddress($this->getRequest()->getParam("bill_street_address"))
                ->setAddress2($this->getRequest()->getParam("bill_street_address2"))
                ->setZipCode($this->getRequest()->getParam("bill_postal_code"))
                ->setCity($this->getRequest()->getParam("bill_city"))
                ->setRegion($this->getRequest()->getParam("bill_state"))
                ->setPhone($this->getRequest()->getParam("customer_phone"))
                ->save();
        } catch (Exception $e){
            Zend_Registry::get("logger")->sendException(print_r($e, true), "2checkout_ordercreated_", false);
        }

        die;
    }

    public function fraudstatuschangedAction() {

        if($this->getRequest()->getParam("fraud_status") == "fail" OR $this->getRequest()->getParam("fraud_status") == "wait") {
            try {
                $this->_subscription->setIsActive(0)->save();
            } catch (Exception $e) {
                Zend_Registry::get("logger")->sendException(print_r($e, true), "2checkout_fraudstatuschanged_", false);
            }
        } elseif($this->getRequest()->getParam("fraud_status") == "pass") {
            $this->_subscription->setIsActive(1)->save();
        }
        die;

    }

    public function recurringinstallAction() {
        try {
            $this->_subscription->setIsActive(1)
                ->update()
                ->invoice()
            ;
        } catch (Exception $e) {
            Zend_Registry::get("logger")->sendException(print_r($e, true), "2checkout_recurringinstall_", false);
        }
        die;

    }

    public function recurringinstallfailAction() {
        try {
            $this->_subscription->setIsActive(0)->save();
        } catch (Exception $e) {
            Zend_Registry::get("logger")->sendException(print_r($e, true), "2checkout_recurringinstallfail_", false);
        }
        die;

    }

    public function recurringstoppedAction() {
        try {
            $this->_subscription->setIsActive(0)->save();
        } catch (Exception $e) {
            Zend_Registry::get("logger")->sendException(print_r($e, true), "2checkout_recurringstopped_", false);
        }
        die;

    }

    public function recurringrestartedAction() {
        try {
            $this->_subscription->setIsActive(1)
                ->update()
                ->invoice()
            ;
        } catch (Exception $e) {
            Zend_Registry::get("logger")->sendException(print_r($e, true), "2checkout_recurringrestarted_", false);
        }
        die;

    }



}
