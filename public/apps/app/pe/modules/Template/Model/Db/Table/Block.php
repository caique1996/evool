<?php

class Template_Model_Db_Table_Block extends Core_Model_Db_Table {

    protected $_name = "template_block";
    protected $_primary = "block_id";

    public function findAll($values, $order, $params) {

        $fields = $this->getFields();
        $select = $this->select()
            ->from(array('tb' => $this->_name), array())
        ;

        if(!empty($values['app_id'])) {

            $fields['color'] = new Zend_Db_Expr('IFNULL(tba.color, tb.color)');
            $fields['background_color'] = new Zend_Db_Expr('IFNULL(tba.background_color, tb.background_color)');
            $fields['border_color'] = new Zend_Db_Expr('IFNULL(tba.border_color, tb.border_color)');
            $fields['image_color'] = new Zend_Db_Expr('IFNULL(tba.image_color, tb.image_color)');

            $join = join(' AND ', array(
                'tba.block_id = tb.block_id',
                $this->_db->quoteInto('tba.app_id = ?', $values['app_id'])
            ));

            $select->joinLeft(array('tba' => 'template_block_app'), $join)
                ->setIntegrityCheck(false)
            ;

            if(empty($values["type_id"])) {
                $select->where("tb.type_id = ?", Template_Model_Block::TYPE_APP);
            }

            unset($values['app_id']);
        } else if(array_key_exists("white_label_editor_id", $values)) {

            $white_label_editor_id = !empty($values["white_label_editor_id"]) ? $values["white_label_editor_id"] : 0;

            $fields['color'] = new Zend_Db_Expr('IFNULL(tbwle.color, tb.color)');
            $fields['color_on_hover'] = new Zend_Db_Expr('IFNULL(tbwle.color_on_hover, tb.color_on_hover)');
            $fields['color_on_active'] = new Zend_Db_Expr('IFNULL(tbwle.color_on_active, tb.color_on_active)');
            $fields['background_color'] = new Zend_Db_Expr('IFNULL(tbwle.background_color, tb.background_color)');
            $fields['background_color_on_hover'] = new Zend_Db_Expr('IFNULL(tbwle.background_color_on_hover, tb.background_color_on_hover)');
            $fields['background_color_on_active'] = new Zend_Db_Expr('IFNULL(tbwle.background_color_on_active, tb.background_color_on_active)');

            $join = join(' AND ', array(
                'tbwle.block_id = tb.block_id',
                $this->_db->quoteInto('tbwle.white_label_editor_id = ?', $white_label_editor_id)
            ));

            $select->joinLeft(array('tbwle' => 'template_block_white_label_editor'), $join)
                ->where("tb.type_id = ?", Template_Model_Block::TYPE_WHITE_LABEL_EDITOR)
                ->setIntegrityCheck(false)
            ;
            unset($values['white_label_editor_id']);


        }

        $select->columns($fields);

        if(!empty($values)) {
            foreach($values as $quote => $value) {
                if($value instanceof Zend_Db_Expr) $select->where($value);
                else if(stripos($quote, '?') !== false) $select->where($quote, $value);
                else $select->where($quote . ' = ?', $value);
            }
        }

        if(!empty($params)) {
            if(!empty($params['limit'])) $select->limit($params['limit']);
            if(!empty($params['offset'])) $select->offset($params['offset']);
        }

        if(!empty($order)) {
            $select->order($order);
        }

        $blocks = $this->fetchAll($select);

        $sorted_collection = array();

        foreach($blocks as $block) {
            if(!$block->getParentId()) {
                $sorted_collection[] = $block;
            } else {
                $this->__buildTree($block, $sorted_collection);
            }
        }

        return $sorted_collection;

//        return $this->fetchAll($select);
    }

    public function saveAppBlock($block) {

        $fields = $this->getFields('template_block_app');
        $data = $block->getData();
        foreach($data as $key => $value) {
            if(!in_array($key, $fields)) unset($data[$key]);
        }

        try {
            $this->_db->insert('template_block_app', $data);
        } catch (Exception $e) {
            if($e->getCode() == 23000) {
                $this->_db->update('template_block_app', $data, array('block_id = ?' => $data['block_id'], 'app_id = ?' => $data['app_id']));
            }
        }

    }

    public function saveWhiteLabelEditorBlock($block) {

        $fields = $this->getFields('template_block_white_label_editor');
        $data = $block->getData();
        foreach($data as $key => $value) {
            if(!in_array($key, $fields)) unset($data[$key]);
        }

        try {
            $this->_db->insert('template_block_white_label_editor', $data);
        } catch (Exception $e) {
            if($e->getCode() == 23000) {
                $this->_db->update('template_block_white_label_editor', $data, array('block_id = ?' => $data['block_id'], 'white_label_editor_id = ?' => $data['white_label_editor_id']));
            }
        }

    }

    public function findByDesign($design_id) {

        $select = $this->select()
            ->from(array('td' => 'template_design'), array())
            ->join(array('tdb' => 'template_design_block'), 'tdb.design_id = td.design_id', array('block_id', 'color', 'background_color', 'border_color', 'image_color'))
            ->join(array('tb' => $this->_name), 'tb.block_id = tdb.block_id', array('name', 'code', 'position', 'created_at', 'updated_at'))
            ->where('td.design_id = ?', $design_id)
//            ->order('tdb.position ASC')
            ->setIntegrityCheck(false)
        ;

        return $this->fetchAll($select);
    }

    public function findByWhiteLabelEditor($white_label_editor_id) {

        $cols = array(
            'color' => new Zend_Db_Expr('IFNULL(tbwle.color, tb.color)'),
            'color_on_hover' => new Zend_Db_Expr('IFNULL(tbwle.color_on_hover, tb.color_on_hover)'),
            'color_on_active' => new Zend_Db_Expr('IFNULL(tbwle.color_on_active, tb.color_on_active)'),
            'background_color' => new Zend_Db_Expr('IFNULL(tbwle.background_color, tb.background_color)'),
            'background_color_on_hover' => new Zend_Db_Expr('IFNULL(tbwle.background_color_on_hover, tb.background_color_on_hover)'),
            'background_color_on_active' => new Zend_Db_Expr('IFNULL(tbwle.background_color_on_active, tb.background_color_on_active)'),
            'default_color' => 'tb.color',
            'default_background_color' => 'tb.background_color'
        );

        $select = $this->select()
            ->from(array('tb' => $this->_name), array('block_id', 'code', 'name', 'use_color_on_hover', 'use_color_on_active'))
            ->joinLeft(array('tbwle' => 'template_block_white_label_editor'), 'tbwle.block_id = tb.block_id AND tbwle.white_label_editor_id = '.$white_label_editor_id, $cols)
            ->where('tb.type_id = ?', Template_Model_Block::TYPE_WHITE_LABEL_EDITOR)
            ->order('tb.position ASC')
            ->setIntegrityCheck(false)
        ;

        return $this->fetchAll($select);

    }

    private function __buildTree($block, $parents = array()) {

        foreach($parents as $parent) {
            if($parent->getId() == $block->getParentId()) {
                $parent->addChild($block);
            }
        }

    }

}
