<?php

class Module_Bootstrap {

    static public function init($bootstrap) {
        self::_initApp($bootstrap);
        self::_initWhiteLabelEditor($bootstrap);
    }

    static protected function _initApp($bootstrap) {
        if($bootstrap->_request->isApplication()) {
            $bootstrap->_application = $bootstrap->_request->getApplication();
            Core_Controller_Default::setApplication($bootstrap->_application);
            Core_View_Default::setApplication($bootstrap->_application);
            Core_Model_Default::setApplication($bootstrap->_application);
        }
    }

    static protected function _initWhiteLabelEditor($bootstrap) {

        if($bootstrap->_request->isWhiteLabelEditor()) {
            $white_label_editor = $bootstrap->_request->getWhiteLabelEditor();
            Front_View_Default::setCurrentWhiteLabelEditor($white_label_editor);
        }

    }
}