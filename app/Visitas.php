<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitas extends Model
{
    //protected $fillable
    protected $fillable = ['ip','user_id'];
}
