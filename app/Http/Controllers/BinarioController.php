<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Binario;
use App\User;
use App\extratos;
use App\Pacote;
use App\Referrals;
use App\config;

class BinarioController extends Controller {

    public $hue;
    public $ids;

    public function index() {
        $pacote = new Pacote();
        $pacote->expirados();
        $Bin = new Binario();
        $usr = new User();
        $config = new config();
        $config = $config->getConfig();
        $binario_val = $config['binario_valor'];
        $usuarios = User::where('ativo', 1)->get();
        foreach ($usuarios as $value) {
            echo $value['id'] . '<br>';
            if ($usr->qualificado($value['id'])) {
                echo '---' . $value['id'] . 'esta qualificado<br>';
                $BinarioEsq = $Bin->totalEsquerda($value['id']);
                $BinarioDireita = $Bin->totalDireita($value['id']);
                $pacote = Pacote::where('id', $value['pacote'])->first();
                if ($BinarioDireita > $BinarioEsq and $pacote['binario'] == 1) {
                    $valorBinario = $BinarioEsq * $binario_val;
                    $usr->removePontos($value['id'], $BinarioEsq, 'esquerda');
                    $usr->addSaldo($value['id'], $valorBinario, "Bônus Binário(esquerda)");
                    $usr->removeSaldo(1, $valorBinario - ($valorBinario * 0.1), 'Bônus Binário(' . $value['username'] . ')');
                    $usr->removePontos($value['id'], $BinarioEsq, 'direita');
                    echo '---' . $value['id'] . ' esquerda<br>';
                } else if ($BinarioEsq > $BinarioDireita and $pacote['binario'] == 1) {
                    $valorBinario = $BinarioDireita * $binario_val;
                    $usr->removePontos($value['id'], $BinarioDireita, 'direita');
                    $usr->addSaldo($value['id'], $valorBinario, "Bônus Binário(direita)");
                    $usr->removeSaldo(1, $valorBinario - ($valorBinario * 0.1), 'Bônus Binário(' . $value['username'] . ')');
                    $usr->removePontos($value['id'], $BinarioDireita, 'esquerda');
                    echo '---' . $value['id'] . ' direita<br>';
                }
            }
        }
        return redirect(url('admin/home?infoBin=1'));
    }

    public function divideLucro(Request $request) {
        $pacote = new Pacote();
        $today = date('Y-m-d');
        if ($request->pacote_div == 'todos') {
            $users = User::where('ativo', 1)->get();
        } else {
            $users = User::where('ativo', 1)->where('pacote', $request->pacote_div)->get();
        }

        $valor_simulacao = 0;
        if ($request->simulacao == 1) {
            foreach ($users as $value) {

                if (\Auth::user()->apto_bonus($value['id'])) {
                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {

                        $valor = $request->valor * \Auth::user()->totalCotas($value['id']);
                    } else {
                        $valor = $request->valor * 1;
                    }
                    $desc = "Divisão de lucros";
                    //\Auth::user()->addSaldo($value['id'], $valor, $desc, 12);
                    $valor_simulacao+=$valor;
                }
            }
            echo "Você irá  dividir R$" . number_format($valor_simulacao, 2) . ".Mude a ação para dividir e realize esse procedimento novamente para realizar a divisão";
        } else {
            foreach ($users as $value) {
                if (\Auth::user()->apto_bonus($value['id'])) {
                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                        $valor = $request->valor * \Auth::user()->totalCotas($value['id']);
                    } else {
                        $valor = $request->valor * 1;
                    }
                    $desc = "Divisão de lucros";
                    \Auth::user()->addSaldo($value['id'], $valor, $desc, 12);
                    $valor_simulacao+=$valor;
                }
            }
            extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => (($valor_simulacao) * (-1)), 'beneficiado' => 1, 'tipo' => 12]);
            echo "Operação realizada com sucesso. R$" . number_format($valor_simulacao, 2) . " foram distribuidos!";
        }
    }

}
?>

