<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Password;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectPath = '/painel';
    protected $subject = 'Seu link de redefinição de senha';
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetFormUser(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->showLinkRequestFormUser();
        }

        $email = $request->input('email');

        if (view()->exists('painel.auth.passwords.reset')) {
            return view('painel.auth.passwords.reset')->with(compact('token', 'email'));
        }

        return view('painel.auth.reset')->with('token', $token);
    }

    public function showLinkRequestFormUser()
    {
        if (view()->exists('painel.auth.passwords.email')) {
            return view('painel.auth.passwords.email');
        }

        return view('painel.auth.password');
    }

    public function postEmailUser(Request $request)
    {
        if (\App\config::getConf()['recaptcha_status'] == 'sim') {
            @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . \App\config::getConf()['recaptcha_secret'] . "&response=" . $request->all()['g-recaptcha-response']);
            $res = json_decode($json);
            if ($res->success <> 1) {
                return \Redirect::back()->withErrors(['Captcha inválido!']);
            }
        }
        Password::setDefaultDriver('usersn');
        return $this->sendResetLinkEmail($request);
    }

    public function resetUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect($this->redirectPath)->with('status', trans($response));

            default:
                return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => trans($response)]);
        }
    }

}
