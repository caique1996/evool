<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Pacote;
use App\Referrals;
use App\User;
use App\Visitas;
use Illuminate\Http\Request;
use Validator;
use Mail;

class CadastroController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index2(Request $request) {

        if ($_SERVER['HTTP_HOST'] == 'sistemalax.com') {
            return redirect('http://painel.sistemalax.com/cadastro/');
        }

        $pacotes = Pacote::all();



        return view('painel.auth.register', compact('indicador', 'pacotes'));
    }

    public function index($indicacao, Request $request) {

        if ($_SERVER['HTTP_HOST'] == 'sistemalax.com') {
            return redirect('http://painel.sistemalax.com/cadastro/' . $indicacao);
        }

        $indicador = User::where('username', $indicacao)->orWhere('email', $indicacao)->first();
        $pacotes = Pacote::all();

        if ($indicador && $indicador->ativo == 1) {

            Visitas::create(['ip' => $request->ip(), 'user_id' => $indicador->id]);

            return view('painel.auth.register', compact('indicador', 'pacotes'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = \Input::all();
        if (\App\config::getConf()['recaptcha_status'] == 'sim') {
            @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . \App\config::getConf()['recaptcha_secret'] . "&response=" . $request->all()['g-recaptcha-response']);
            $res = json_decode($json);
            if ($res->success <> 1) {
                return \Redirect::back()->withErrors(['Captcha inválido!']);
            }
        }
        if (User::where('cpf', $data['cpf'])->count() > \App\config::getConf()['cpfCadastros']) {

            return \Redirect::back()->withErrors(['Você excedeu o limite de cadastros por CPF.O número máximo de cadastros por CPF é: ' . \App\config::getConf()['cpfCadastros']]);
        }
        $indicacao = $data['indicador'];
        $indicador = User::where('username', $data['indicador'])->orWhere('email', $data['indicador'])->orWhere('id', $data['indicador'])->first();

        $reffer = new Referrals();

        if (!isset($indicador->id)) {
            return redirect('/cadastro/')->withErrors(['Usuário não encontrado.']);
        } elseif ($indicador && $indicador->ativo == 1) {
            if (Referrals::where('system_id', $indicador->id)->count() == 0) {
                $systemId = ['pai_id' => $indicador->id, 'direcao' => 'esquerda'];
            } else {
                $systemId = $reffer->searchSystemId($indicador->id, $indicador->direcao);
            }
            if ($data['cadastro_exterior'] <> 1) {
                $validator = Validator::make($request->all(), [
                            'name' => 'required|max:255',
                            'email' => 'required|email|max:255|unique:users',
                            'username' => 'required|max:20|unique:users',
                            'password' => 'required|confirmed|min:6',
                            'pacote' => 'required|integer',
                            'cpf' => 'cpf',
                            'endereco' => 'required',
                            'sexo' => 'required',
                            'bairro' => 'required',
                            'cidade' => 'required|integer',
                            'estado' => 'required|integer',
                            'telefone' => 'phone',
                            'nascimento' => 'date|required',
                            'cep' => 'required',
                            'pais' => 'required',
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                            'name' => 'required|max:255',
                            'email' => 'required|email|max:255|unique:users',
                            'username' => 'required|max:20|unique:users',
                            'password' => 'required|confirmed|min:6',
                            'pacote' => 'required|integer',
                            'endereco' => 'required',
                            'sexo' => 'required',
                            'bairro' => 'required',
                            'cidade' => 'required',
                            'estado' => 'required',
                            'telefone' => 'required',
                            'nascimento' => 'date|required',
                            'cep' => 'required',
                            'pais' => 'required',
                ]);
            }

            if ($validator->fails()) {
                return redirect('/cadastro/' . $indicacao)
                                ->withErrors($validator)
                                ->withInput();
            }


            if ($data['cadastro_exterior'] <> 1) {
                $estado = \DB::table('estado')->where('id', $data['estado'])->first();
                if (!isset($estado->nome)) {
                    return redirect('/cadastro/' . $indicacao)->withErrors(['Estado não encontrado.']);
                }
                $cidade = \DB::table('cidade')->where('id', $data['cidade'])->where('estado', $estado->id)->first();
                if (!isset($cidade->nome)) {
                    return redirect('/cadastro/' . $indicacao)->withErrors(['Cidade não encontrada.']);
                }
            }
            // Declara a data! :P
            // Separa em dia, mês e ano

            list($dia, $mes, $ano) = explode('-', $data['nascimento']);

            // Descobre que dia é hoje e retorna a unix timestamp
            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            // Descobre a unix timestamp da data de nascimento do fulano
            $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);

            // Depois apenas fazemos o cálculo já citado :)
            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
            if ($idade < 18) {
                return redirect('/cadastro/' . $indicacao)->withErrors(['Você precisa ser maior de idade para se cadastrar nesse site.']);
            }
            $photo = asset('/img') . '/avatar-' . $data['sexo'] . '.png';
            $tipo_pagamento = Pacote::where('id', $data['pacote'])->first()['tipo_pagamento'];

            if ($tipo_pagamento == 'Gratuito') {
                $ativo = 1;
            } else {
                $ativo = 0;
            }

            if ($data['cadastro_exterior'] <> 1) {

                $user = User::create([
                            'name' => $data['name'],
                            'email' => $data['email'],
                            'username' => $data['username'],
                            'password' => bcrypt($data['password']),
                            'cpf' => $data['cpf'],
                            'endereco' => $data['endereco'],
                            'bairro' => $data['bairro'],
                            'cidade' => $cidade->nome,
                            'sexo' => $data['sexo'],
                            'estado' => $estado->nome,
                            'nascimento' => date('Y-m-d', strtotime($data['nascimento'])),
                            'telefone' => $data['telefone'],
                            'pai_id' => $indicador->id,
                            'direcao' => 'esquerda',
                            'pacote' => $data['pacote'],
                            'total_div' => 12,
                            'photo' => $photo,
                            'n_complemento' => $data['n_complemento'],
                            'complemento' => $data['complemento'],
                            'ativo' => $ativo,
                            'cep' => $data['cep'],
                            'pais' => $data['pais'],
                ]);
            } else {
                $user = User::create([
                            'name' => $data['name'],
                            'email' => $data['email'],
                            'username' => $data['username'],
                            'password' => bcrypt($data['password']),
                            'cpf' => $data['cpf'],
                            'endereco' => $data['endereco'],
                            'bairro' => $data['bairro'],
                            'cidade' => $data['cidade'],
                            'sexo' => $data['sexo'],
                            'estado' => $data['estado'],
                            'nascimento' => date('Y-m-d', strtotime($data['nascimento'])),
                            'telefone' => $data['telefone'],
                            'pai_id' => $indicador->id,
                            'direcao' => 'esquerda',
                            'pacote' => $data['pacote'],
                            'total_div' => 12,
                            'photo' => $photo,
                            'n_complemento' => $data['n_complemento'],
                            'complemento' => $data['complemento'],
                            'ativo' => $ativo,
                            'cep' => $data['cep'],
                            'pais' => $data['pais'],
                ]);
            }
            if ($user) {
                $reffer->user_id = $user->id;
                $reffer->pai_id = $indicador->id;
                $reffer->system_id = $systemId['pai_id'];
                $reffer->direcao = $systemId['direcao'] ? $systemId['direcao'] : $indicador->direcao;
                $reffer->save();
                \DB::table('enderecos')->insert(['user_id' => $user->id, 'pais' => $data['pais'], 'cidade' => $data['cidade'], 'estado' => $data['estado'], 'cep' => @$data['cep'], 'endereco' => $data['endereco']]);
            }

            if (\Auth::login($user)) {


                return redirect('/painel');
            } else {
try{
                $userInfo = User::where('id', \Auth::user()->id)->first();
                session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name']]);
                $dataMail['subject'] = 'Cadastro  efetuado com sucesso!';
                $dataMail['content'] = "<h5>Estamos muito felizes por seu cadastro!</h5>
                    <b>
          Seus dados de acesso são:<br>
          Login: " . $userInfo['username'] . "<br>
          Email: " . $userInfo['email'] . "<br>
          Página de login: <a href='" . url('painel/login') . "'>Login</a>";

                /*Mail::send('painel.auth.emails.email', $dataMail, function ($message) {
                    $message->from(\App\config::getConf()['from_address'], \App\config::getConf()['from_name']);
                    $message->to(session('sendEmail'), session('sendName'))->subject('Cadastro  efetuado com sucesso!');
                });*/
} catch (Exception $exc) {
            return false;
        }




                return redirect('/painel');
            }
        } else {
            abort(404);

            return false;
        }
    }

}
