<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\extratos;
use App\Saque;
use Validator;
use App\config;

class SaquesController extends Controller {

    public function index() {
        $saques = Saque::all();
        $usuarios = new User();
        return view('admin.pages.saque', compact('saques', 'usuarios'));
    }

    public function indexUser() {
        $saques = Saque::where('user_id', \Auth::user()->id);
        $usuarios = new User();
        return view('painel.pages.saque', compact('saques', 'usuarios'));
    }

    public function solicitaSaque() {
        $config = new config();
        $config = $config->getConfig();
        $usr = new User();
        $usr->verificarPin($_GET['pin']);

        if ($config['permitir_saque'] == 'sim') {

            $valor = $_GET['valor'];

            $valor2 = str_replace(array('R', '$', ',', '.'), '', $valor);
            $valor = $this->getAmount($valor);
            $saldo = \Auth::user()->getSaldo();
            $saldo = $this->getAmount($saldo);
            $today = date("Y-m-d");

            if (is_numeric($valor2) and $valor2 >= ($config['saque_minimo'] * 100) and $valor2 <= ($config['saque_max'] * 100)) {
                $dadosbancarios = User::select('agencia', 'banco', 'conta', 'tipo_conta', 'operacao')->where('id', \Auth::user()->id)->first();

                if ($saldo > $valor or $saldo == $valor) {
                    if ($dadosbancarios['banco'] == '' or $dadosbancarios['agencia'] == '' or $dadosbancarios['conta'] == '' or $dadosbancarios['tipo_conta'] == '') {
                        echo 'Preencha a suas informações bancárias.';
                        exit();
                    }
                    $novoSaldo = $saldo - $valor;
                    $dadosConta = "Banco: {$dadosbancarios['banco']} <br>";
                    $dadosConta .= "Agência: {$dadosbancarios['agencia']} <br>";
                    $dadosConta .= "Conta: {$dadosbancarios['conta']} <br>";
                    $dadosConta .= "Tipo: {$dadosbancarios['tipo_conta']} <br>";
                    $dadosConta .= "Operação: {$dadosbancarios['operacao']} <br>";
                    User::where('id', \Auth::user()->id)->update(['saldo' => $novoSaldo]);
                    Saque::create(['valor' => ($valor - ($valor * 0.1)), 'status' => 0, 'user_id' => \Auth::user()->id, 'conta' => $dadosConta]);
                    $usr->addSaldo(1, ($valor * 0.1), 'Taxa administrativa de saque');
                    extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => 'Saque', 'valor' => ($valor) * (-1), 'beneficiado' => \Auth::user()->id]);

                    echo 'Saque Solicitado com sucesso.Pode demorar algum tempo para que o setor financeiro transfira o dinheiro para a sua conta.';
                } else {
                    echo 'Saldo Indisponivel';
                }
            } else {

                echo 'O valor solicitado é maior que o valor máximo permitido ou menor que o valor mínimo permitido.';
            }
        } else {
            echo 'Operação Indisponivel no momento';
        }
    }

    public function store(Request $request) {
        $rules = array(
            'status' => 'required|integer',
            'mensagem' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $data = '';
            if ($request->status == 1) {
                $data = date("Y-m-d H:i:s");
            }


            if (Saque::where('id', $request->id)->update(['data_deposito' => $data, 'status' => $request->status, 'mensagem' => $request->mensagem])) {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Status Atualizado
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
            }
        } else {
            return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro na validação dos dados! Por favor tente novamente.
                 </div>
EOL;
        }
    }

}
