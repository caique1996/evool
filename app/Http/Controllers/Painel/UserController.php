<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Voucher;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\DomCrawler\Form;
use App\config;
use Mail;
use App\Trade;
use Hash;
use App\Http\Controllers\Admin\VoucherController;
use Input;
use Session;
use Redirect;
use App\extratos;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('painel.pages.profile');
    }

    public function acessar($id) {

        if (\Auth::loginUsingId($id)) {
            return redirect('/painel');
        } else {
            return redirect('/admin/usuarios')
                            ->withErrors(['Usuário não encontrado.']);
        }
    }

    public function novachave() {
        $rand = rand(111111, 999999);
        User::where('id', \Auth::user()->id)->update(['pin' => Hash::make($rand)]);
        session(['sendEmail' => \Auth::user()->email, 'sendName' => \Auth::user()->name]);
        Mail::raw('Seu novo código de segurança é ' . $rand, function ($message) {
            $message->from('nao-responda@adsply.com', 'Lax');
            $message->to(session('sendEmail'), session('sendName'))->subject('Código de segurança');
        });
        echo 'O seu código foi enviado com sucesso.';
    }

    public function ativarConta() {
        $post = \Input::all();
        $vouch = new VoucherController();
        $atulizaVoucher = Voucher::where('voucher', $post['ativarVoucher'])
//->where('user_id', \Auth::user()->pai_id)
                ->where('status', 0)
                ->update(['status' => 1, 'activated_id' => \Auth::user()->id]);

        if ($atulizaVoucher) {
            $vouch->addValidadePacote(\Auth::user()->id);
            if (\Auth::user()->update(['ativo' => 1])) {
                return redirect('/painel/home')
                                ->with('success', 'Parabéns! Sua conta foi ativada com sucesso!');
            } else {
                return redirect('/painel/inativo')
                                ->withErrors(['Não foi possivel ativar sua conta. Por favor entre em contato com o Suporte.']);
            }
        } else {
            return redirect('/painel/inativo')
                            ->withErrors(['Voucher não encontrado. Ou já foi utilizado.']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {

        $data = \Input::all();

        $valida = [
            'cpf' => 'cpf',
            'endereco' => 'required',
            'bairro' => 'required',
            'sexo' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            'nascimento' => 'date',
        ];

        if (!empty($data['password'])) {
            $valida = array_merge($valida, ['password' => 'required|confirmed|min:6',]);
        }

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            return redirect('/painel/meus-dados')
                            ->withErrors($validator)
                            ->withInput();
        }
        if (!isset($data['saque'])) {
            $dados = [
                'endereco' => $data['endereco'],
                'bairro' => $data['bairro'],
                'cidade' => $data['cidade'],
                'sexo' => $data['sexo'],
                'estado' => $data['estado'],
                'telefone' => $data['telefone'],
                'banco' => $data['banco'],
                'agencia' => $data['agencia'],
                'conta' => $data['conta'],
                'tipo_conta' => $data['tipo_conta'],
                'operacao' => $data['operacao'],
                'segtitula_cpf' => @$data['segtitula_cpf'],
                'segtitular_nm' => @$data['segtitular_nm'],
            ];
        } else {
            $dados = [
                'segtitula_cpf' => $data['segtitula_cpf'],
                'segtitular_nm' => $data['segtitular_nm'],
                'endereco' => $data['endereco'],
                'bairro' => $data['bairro'],
                'cidade' => $data['cidade'],
                'sexo' => $data['sexo'],
                'estado' => $data['estado'],
                'telefone' => $data['telefone'],
                'banco' => $data['banco'],
                'agencia' => $data['agencia'],
                'conta' => $data['conta'],
                'tipo_conta' => $data['tipo_conta'],
                'operacao' => $data['operacao'],
                'saque' => $data['saque'],
            ];
        }

        if (!empty($data['password'])) {
            $senha = \DB::table('users')->where('id', \Auth::user()->id)->first()->password;
            if (\Hash::check($data['current_password'], $senha)) {
                $dados = array_merge($dados, ['password' => bcrypt($data['password']),]);
            } else {
                return redirect('/painel/meus-dados')
                                ->withErrors(['Senha inválida, tente novamente.']);
            }
        }

        $user = \Auth::user()->update($dados);

        if ($user) {
            return redirect('/painel/meus-dados')
                            ->with('success', 'Perfil Atualizado');
        } else {
            return redirect('/painel/meus-dados')
                            ->withErrors(['Não foi possivel atualizar, tente novamente.']);
        }
    }

    public function buscar(Request $request) {
        $usuario = User::where('id', $request->busca)->orWhere('username', $request->busca)->first();
        if ($usuario) {
            return '<meta http-equiv="refresh" content="0;URL=' . url('painel/minha-rede/' . $usuario->id) . '">';
        } else {
            return redirect('/painel/minha-rede')
                            ->withErrors(['Usuario não encontrado']);
        }
    }

    public function muda_lado(Request $request) {
        $data = \Input::all();
        if ($data['lado'] == 'esquerda' or $data['lado'] == 'direita') {
            \Auth::user()->update(['direcao' => $data['lado']]);
            echo 'ok';
        } else {
            echo 'fail';
        }
    }

    public function view($id) {
        $dados['dados'] = User::where('id', $id)->first();
        $usrTotal = User::where('id', $id)->count();
        return view('admin.pages.usuarios.edit', $dados);
    }

    public function salvar() {
        $data = \Input::all();

        if (env('md_JqHVpdRvx4YV5mxd') == 'md_bo_SZtPY6F5r6EpP23t') {
            $valida = [
                'id' => 'integer|required',
                'name' => 'required|max:255',
                'cpf' => 'cpf',
                'saldo' => 'required',
                'admin' => 'required',
                'pago' => 'required',
                'ativo' => 'required',
                'email' => 'required|email|max:255',
                'direcao' => 'required',
                'pacote' => 'required'
            ];
        } else {
            $valida = [
                'id' => 'integer|required',
                'name' => 'required|max:255',
                'cpf' => 'cpf',
                'saldo' => 'required',
                'admin' => 'required',
                'pago' => 'required',
                'ativo' => 'required',
                'email' => 'required|email|max:255',
                'pacote' => 'required'
            ];
        }

        if (!empty($data['password'])) {
            $valida = array_merge($valida, ['password' => 'required|confirmed|min:6',]);
        }

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            unset($data['password']);
            unset($data['id']);
            unset($data['_token']);
            unset($data['password_confirmation']);
            if (!empty($data['password'])) {
                $data['password'] = bcrypt($data['password']);
            }

            User::where('id', $id)->update($data);
            return <<<EOL
                 <div class="alert alert-success fade in">

                     Usuário Atualizado
                 </div>
EOL;
        }
    }

    public function pin() {
        $data = \Input::all();
        if (\Auth::user()->pin == '') {

            $validator = Validator::make($data, ['pin' => 'required|confirmed|min:6']);

            if ($validator->fails()) {
                return redirect('/painel/home')->withErrors($validator);
            } else {
                User::where('id', \Auth::user()->id)->update(['pin' => bcrypt($data['pin'])]);
                return redirect('/painel/home')->with('success', 'Código de segurança definido com sucesso!');
            }
        } else {
            return redirect('/painel/home')->withErrors(['Você já possui um código de segurança.']);
        }
    }

    public function addSaldo(Request $request) {
        $valor = $request->valor;
        $graduacao = $request->grad_required;


        if (!is_numeric($valor) or ! is_numeric($graduacao)) {
            echo 'Operação não realizada.';
            exit();
        }
        if ($valor == '' or $graduacao == '') {
            echo 'Operação não realizadad.';
            exit();
        }
        $usr = new User();
        $usuarios = User::where('ativo', 1)->where('graduacao', $graduacao)->get();
        foreach ($usuarios as $value) {
            if ($usr->totalCotas($value['id']) >= 5) {
                $usr->addSaldo($value['id'], $valor, 'Bônus Star');
                $usr->removeSaldo(1, $valor, 'Bônus Star(' . $value['username'] . ')');
            }
        }
        echo "Operação realizada com sucesso.";
    }

    public function manage_usr() {
        $usuarios = User::all();
        return view('admin.pages.usuario', compact('usuarios'));
    }

    public function converter_saldo(Request $request) {

        $valor = str_replace('.', '', $request->valor_conver);
        $valor = str_replace(',', '.', $valor);
        $usr = new User();

        $usr->verificarPin($request->pin);
        if (is_numeric($valor) and \Auth::user()->saldo >= $valor and $valor > 0) {
            $usr->removeSaldo(\Auth::user()->id, $valor, 'Conversão de saldo', 13);
            $saldoCarteira = (\Auth::user()->carteira_b) + $valor;
            User::where('id', \Auth::user()->id)->update(['carteira_b' => $saldoCarteira]);
            echo 'Operação realizada com sucesso.';
        } else {
            echo 'Operação não realizada';
        }
    }

    public function ver_user(Request $request) {
        $usr = new User();
        $usr->verificarPin($request->pin, 1);
        $dados = User::where('username', $request->username)->first();
        if (!isset($dados['email'])) {
            echo '<div class="alert alert-danger fade in">
                     O usuário não existe
                 </div>';
        } else if ($dados['ativo'] == 1) {
            echo '<div class="alert alert-danger fade in">
                    O usuário já esta ativo.
                 </div>';
        } else {
            $pacote = \App\Pacote::where('id', $dados['pacote'])->first();
            $dados = 'Nome Completo: ' . $dados['name'] . '<br>' . 'Username: ' . $dados['username'] . "<br>Valor: R$" . $pacote['valor'] . "<br><button  onclick='ativar_user();' class='btn btn-primary' >Ativar Usuário({$dados['name']})</button>";

            echo $dados;
        }
    }

    public function ativar_user(Request $request) {
        $usr = new User();
        $usr->verificarPin($request->pin, 1);
        $dados = User::where('username', $request->username)->first();
        if (!isset($dados['email'])) {
            echo '<div class="alert alert-danger fade in">
                     O usuário não existe
                 </div>';
        } else if ($dados['ativo'] == 1) {
            echo '<div class="alert alert-danger fade in">
                    O usuário já esta ativo.
                 </div>';
        } else {
            $pacote = \App\Pacote::where('id', $dados['pacote'])->first();
            if (\Auth::user()->carteira_b >= $pacote['valor']) {
                $voucher = new VoucherController();
                $voucher->ativarUsr($dados['id']);
                $novoSaldo = \Auth::user()->carteira_b - $pacote['valor'];
                User::where('id', \Auth::user()->id)->update(['carteira_b' => $novoSaldo]);
            } else {
                echo '<div class="alert alert-danger fade in">
                    Saldo insuficiente.
                 </div>';
            }
        }
    }

    private function replaceKeys($oldKey, $newKey, array $input) {
        $return = array();
        foreach ($input as $key => $value) {
            if ($key === $oldKey)
                $key = $newKey;

            if (is_array($value))
                $value = replaceKeys($oldKey, $newKey, $value);

            $return[$key] = $value;
        }
        return $return;
    }

    public function getCidade(Request $request) {
        $id = $request->id;
        $nome = $request->nome;
        $uf = $request->uf;
        if ($request->tp == 2) {
            $data = \DB::table('cidade')->select('nome', 'id')->where('estado', $id)->get();
            $i = 0;
            $json = '';
            foreach ($data as $value) {

                $json[$i]['id'] = $value->id;
                $json[$i]['value'] = $value->nome;
                $i++;
            }
            foreach ($json as $value) {

                echo "<option value='{$value['id']}'>{$value['value']}</option>";
            }
        } else {
            if ($id == '') {
                $estadoId = \DB::table('estado')->select('id')->where('uf', $uf)->first()->id;
                $data = \DB::table('cidade')->select('nome', 'id')->where('nome', $nome)->where('estado', $estadoId)->get();
            } else {
                $data = \DB::table('cidade')->select('nome', 'id')->where('estado', $id)->get();
            }
            $i = 0;
            $json = '';
            foreach ($data as $value) {

                $json[$i]['id'] = $value->id;
                $json[$i]['value'] = $value->nome;
                $i++;
            }
            echo json_encode($json);
        }
    }

    public function getEstados(Request $request) {
        $uf = $request->uf;
        if ($uf == '') {
            $data = \DB::table('estado')->select('nome', 'id')->get();
        } else {
            $data = \DB::table('estado')->select('nome', 'id')->where('uf', $uf)->get();
        }
        $i = 0;
        $json = '';

        foreach ($data as $value) {

            $json[$i]['id'] = $value->id;
            $json[$i]['value'] = $value->nome;
            $i++;
        }
        echo json_encode($json);
    }

    public function mudar_foto(Request $request) {
// getting all of the post data
        $file = array('image' => Input::file('image'));
// setting up rules
        $rules = array('image' => 'required|image',); //mimes:jpeg,bmp,png and for max size max:10000
// doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
// send back to the page with the input data and errors
            return Redirect::to('painel/home')->withInput()->withErrors($validator);
        } else {
// checking file is valid.
            if (Input::file('image')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $photo = asset($destinationPath . '/' . $fileName);
                User::where('id', \Auth::user()->id)->update(['photo' => $photo]);
// sending back with message
                Session::flash('success', 'Foto alterada com sucesso!');
                return Redirect::to('painel/home');
            } else {
// sending back with error message.
                Session::flash('error', 'O upload não foi realizado com sucesso.');
                return Redirect::to('painel/home');
            }
        }
    }

    public function transferir_saldo(Request $request) {

        $valor = str_replace('.', '', $request->valor_transferir);
        $valor = str_replace(',', '.', $valor);
        $username = $request->user;
        $usr = new User();

        $valorRecebe = $valor;

        $usr->verificarPin($request->pin);
        if (is_numeric($valor) and \Auth::user()->saldo >= $valor and $valor > 0) {
            $userBeneficiado = User::where("username", $username)->first();
            $usr->removeSaldo(\Auth::user()->id, $valor, 'Transferência de saldo', 10);
            $today = date("Y-m-d");
            $saldoCarteira = $userBeneficiado['carteira_b'] + $valorRecebe;
            User::where('id', $userBeneficiado['id'])->update(['carteira_b' => $saldoCarteira]);
            extratos::create(['user_id' => \Auth::user()->id, 'data' => $today, 'descricao' => 'Transferência de saldo', 'valor' => $valorRecebe, 'beneficiado' => $userBeneficiado['id'], 'tipo' => 10]);

//$trade = $trade->first();
            /* $userTrade = User::where("username", $trade->username)->first();
              $usr->removeSaldo(\Auth::user()->id, $valor, 'Conversão de saldo');
              $ganhoTrade = $valor * 0.05;
              $usr->addSaldo($userTrade['id'], $ganhoTrade, 'Conversão de saldo');
              $usr->addSaldo(1, $ganhoTrade, 'Conversão de saldo');
              $saldoB = $valor * 0.9;
              $saldoCarteira = (\Auth::user()->carteira_b) + $saldoB;
              User::where('id', \Auth::user()->id)->update(['carteira_b' => $saldoCarteira]); */




            echo 'Operação realizada com sucesso';
        } else {
            echo 'Operação não realizada';
        }
    }

    public function ver_user_transferencia(Request $request) {
        $usr = new User();
        $usr->verificarPin($request->pin, 1);
        $dados = User::where('username', $request->username)->first();
        $valor = str_replace('.', '', $request->valor);
        $valor = str_replace(',', '.', $valor);
        $username = $request->username;

        $valorRecebe = $valor;
        if (!isset($dados['email'])) {
            echo '<div class="alert alert-danger fade in">
                     O usuário não existe
                 </div>';
        } else if ($dados['ativo'] == 0) {
            echo '<div class="alert alert-danger fade in">
                    O usuário  esta inativo.
                 </div>';
        } else {
            $pacote = \App\Pacote::where('id', $dados['pacote'])->first();
            $dados = 'Nome Completo: ' . $dados['name'] . '<br>' . 'Username: ' . $dados['username'] . "<br>Valor a pagar: R$" . $valor . "<br>O usuário irá receber: R$" . $valorRecebe . "<br><button  onclick='transferir();' class='btn btn-primary' >Transferir para {$dados['name']}</button>";
            echo $dados;
        }
    }

    public function pagar_fatura(Request $request) {
        $usr = new User();
        $usr->verificarPin($request->pin);
        $dadosUsr = User::where('username', $request->username)->first();
        if (isset($dadosUsr->id)) {
            $fatura = \DB::table('cotas')->where('id', $request->fatura)->where('status', 0)->where('user_id', $dadosUsr->id)->first();
            if (isset($fatura->id)) {
                $pagamentoInfo = \DB::table('pagamentos')->where('id', $fatura->pagamento_id)->first();
                $pacote = \App\Pacote::where('id', $pagamentoInfo->pacote)->first();
                $pacoteValor = str_replace('.', '', $pacote->valor);
                $pacoteValor = str_replace(',', '.', $pacoteValor);
                if (\Auth::user()->carteira_b >= $pacoteValor) {
                    if (($usr->ativarCota($request->fatura))) {
                        $novoSaldo = (\Auth::user()->carteira_b) - $pacoteValor;
                        User::where('id', \Auth::user()->id)->update(['carteira_b' => $novoSaldo]);
                    }
                } else {
                    echo 'Saldo insuficiente';
                }
            } else {
                echo 'Fatura não encontrada';
            }
        } else {
            echo 'Usuário não encontrado.';
        }
    }

    public function enviarComprovante(Request $request) {
        $formData = $request->all();
// getting all of the post data
        $file = array('image' => Input::file('image'));
// setting up rules
        $rules = array('image' => 'required|image',); //mimes:jpeg,bmp,png and for max size max:10000
// doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
// send back to the page with the input data and errors
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
// checking file is valid.

            if (Input::file('image')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $photo = asset($destinationPath . '/' . $fileName);
                if (@$formData['faturasComprovante'] == '') {
                    $formData['faturasComprovante'] = 0;
                }
                if (@$formData['mensagem'] == '') {
                    $formData['mensagem'] = '';
                }
                \DB::table('comprovantes')->insertGetId(['user_id' => \Auth::user()->id, 'status' => 0, 'data' => date('Y-m-d'), 'mensagem' => @$formData['infoComprovante'], 'fatura_id' => @$formData['faturasComprovante'], 'arquivo_id' => $photo]);


// sending back with message
                Session::flash('success', 'Comprovante enviado  com sucesso!');
                return Redirect::back();
            } else {
// sending back with error message.
                Session::flash('error', 'O upload não foi realizado com sucesso.');
                return Redirect::back();
            }
        }
    }

}
