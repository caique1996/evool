<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pacote;
use App\User;
use PagSeguroConfig;
use PagSeguroConfigWrapper;
use PagSeguroPaymentRequest;
use PagSeguroShippingType;
use App\Pagamentos;
use App\config;

class UpgradeController extends Controller {

    public function index() {
        $pacoteAtual = Pacote::where('id', \Auth::user()->pacote)->first();
        $pacotes = Pacote::where('status', 1)->orderBy('valor', 'ASC')->get();
        $desconto = $pacoteAtual['valor'];
        if (@$_GET['novopagamento'] == 1 and ( $_GET['metodo'] == 1 or $_GET['metodo'] == 2)) {
            $pay = new PagamentoController();
            $res = $pay->gerar_link($_GET['metodo'], $_GET['pacote'], \Auth::user()->id, 'Upgrade');
            if ($res) {
                echo $res;
            }
            //gnt
        } else if (@$_GET['novopagamento'] == 1 and ( $_GET['metodo'] == 3)) {
            $pacote = $_GET['pacote'];
            $hoje = date('d-m-Y');
            $usr = new User();
            $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'Deposito', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => \Auth::user()->id, 'pacote' => $pacote, 'tipo' => 'Upgrade']);
            $usr->criarFatura($pacote, \Auth::user()->id, 0, $idPay, 0);
            return 'ok';
            //gnt
        } else {
            $config = new config();
            $config = $config->getConfig();
            return view('painel.pages.upgrade', compact('pacotes', 'desconto', 'pacoteAtual', 'config'));
        }
    }

}
