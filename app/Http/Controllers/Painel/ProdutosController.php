<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Produtos;
use Model;
use Validator;
use App\Pedidos;

class ProdutosController extends Controller {

    public function index() {

        $busca = \Input::get('busca', null);
        $ctg = \Input::get('ctg', null);

        $perPage = 50;
        if (empty($ctg)) {
            if (empty($busca)) {
                $produtos = Produtos::where('estoque', '>', 0)->where('status', 1)->paginate($perPage);
                $paginate = Produtos::where('estoque', '>', 0)->where('status', 1)->simplePaginate($perPage);
            } else {
                $produtos = Produtos::where('nome', 'like', "%$busca%")->where('estoque', '>', 0)->where('status', 1)->paginate($perPage);
                $paginate = Produtos::where('nome', 'like', "%$busca%")->where('estoque', '>', 0)->where('status', 1)->simplePaginate($perPage);
            }
        } else {
            $produtos = Produtos::where('estoque', '>', 0)->where('status', 1)->where('categoria',$ctg)->paginate($perPage);
            $paginate = Produtos::where('estoque', '>', 0)->where('status', 1)->where('categoria',$ctg)->simplePaginate($perPage);
        }
        return view('painel.pages.produtos', compact('produtos', 'busca', 'paginate'));
    }

    public function edit($id) {
        $dados = Produtos::where('id', $id)->first();
        if ($dados) {
            return view('painel.pages.produtosEdit', compact('dados'));
        } else {
            return redirect('/painel/')
                            ->withErrors(['Produto não encontrado']);
        }
    }

    public function create() {

        return view('painel.pages.produtosAdd');
    }

    public function update(Request $dados) {
        $data = $dados->all();
        $data['preco'] = number_format($this->getAmount($data['preco']), 2);

        $validator = Validator::make($data, [
                    'id' => 'required|integer',
                    'nome' => 'required|max:255',
                    'preco' => 'required|numeric',
                    'peso' => 'required|numeric',
                    'categoria' => 'required|max:100',
                    'estoque' => 'required|integer',
                    'status' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return redirect('admin/produtos/add')
                            ->withErrors($validator)
                            ->withInput();
        }


        // Foi
        if ($dados->file('img')) {
            $int = mt_rand(1262055681, 1262055681);
            $fileName = md5(time() . $int) . "." . $dados->file('img')->getClientOriginalExtension();
            $dados->file('img')->move('uploads', $fileName);
            $data['img'] = 'uploads/' . $fileName;
        }
        $dados = $data;
        foreach ($dados['pre_pacote'] as $key => $value) {
            $dados['pre_pacote'][$key] = $this->getAmount($value);
        }
        $dados['descontos'] = json_encode($dados['pre_pacote']);
        $i = 0;

        unset($dados['pre_pacote']);
        unset($dados['_token']);
        unset($dados['_method']);


        $produto = Produtos::where('id', $dados['id'])->update($dados);
        if ($produto) {
            return redirect()->back()->with('success', 'Produto atualizado com sucesso');
        } else {
            return redirect()->back()->withErrors(['Ocorreu um erro ao salvar!']);
        }
    }

    public function createPost(Request $dados) {
        $data = $dados->all();
        $data['preco'] = $this->getAmount($data['preco']);

        $validator = Validator::make($data, [
                    'nome' => 'required|max:255',
                    'preco' => 'required|numeric',
                    'peso' => 'required|numeric',
                    'img' => 'image',
                    'categoria' => 'required|max:100',
                    'estoque' => 'required|integer',
                    'status' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return redirect('admin/produtos/add')
                            ->withErrors($validator)
                            ->withInput();
        }


        // Foi
        $int = mt_rand(1262055681, 1262055681);

        $fileName = md5(time() . $int) . "." . $dados->file('img')->getClientOriginalExtension();


        $dados->file('img')->move('uploads', $fileName);

        $dados = $data;
        $dados['img'] = 'uploads/' . $fileName;
        foreach ($dados['pre_pacote'] as $key => $value) {
            $dados['pre_pacote'][$key] = $this->getAmount($value);
        }
        $dados['descontos'] = json_encode($dados['pre_pacote']);
        $i = 0;


        unset($dados['pre_pacote']);
        unset($dados['_token']);
        unset($dados['_method']);
        $produto = Produtos::create($dados);
        if ($produto) {
            return redirect()->back()->with('success', 'Produto adicionado com sucesso');
        } else {
            return redirect()->back()->withErrors(['Ocorreu um erro ao salvar!']);
        }
    }

}
