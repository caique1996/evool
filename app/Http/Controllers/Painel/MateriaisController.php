<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Lancamentos;

class LancamentosController extends Controller
{
    public function index()
    {
        return view('painel.pages.materiais');
    }

    public function show($id)
    {
        $materal = Lancamentos::find($id);
        $link = $materal->link;
        $extension = $this->getExtension($link);
        return view('admin.pages.materiais.preview', compact('link', 'extension'));
    }

    private function getExtension($link)
    {
        return pathinfo($link)['extension'];
    }
}
