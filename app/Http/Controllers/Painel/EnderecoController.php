<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Carrinho;
use App\Produtos;
use App\Enderecos;

class EnderecoController extends Controller {

    public function index() {
        $enderecos = Enderecos::where('user_id',\Auth::user()->id)->get();

        if(\Input::has('error')){
            $error = \Input::get('error',null);
        }

        return view('painel.pages.enderecos',compact('enderecos','error'));
    }

    public function add(){

        $dados = array('cep','cidade','endereco','estado','pais');

        foreach ($dados as $key) {

            if(!\Input::has($key)){
                return redirect("/painel/meu-carrinho/endereco");
            }

            // Verifica se o cep é válido
            if ($key == 'cep' && !preg_match('/[0-9]{5,5}([- ]?[0-9]{4})?$/',
                \Request::input($key))){
                $error = '?error=O CEP inserido é inválido, utilize apenas números';
                return redirect("/painel/meu-carrinho/endereco/$error");
            }

            $dados[$key] = \Request::input($key,null);

        }

        $endereco = new Enderecos;
        $endereco->user_id = \Auth::user()->id;
        $endereco->cep = $dados['cep'];
        $endereco->cidade = $dados['cidade'];
        $endereco->endereco = $dados['endereco'];
        $endereco->estado = $dados['estado'];
        $endereco->pais = $dados['pais'];
        $endereco->save();
        return redirect("/painel/meu-carrinho/endereco/");

    }

}
