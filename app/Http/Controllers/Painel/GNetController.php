<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\GNetRepository;
use App\User;
use App\Pacote;
use PagSeguroConfig;
use PagSeguroConfigWrapper;
use PagSeguroPaymentRequest;
use PagSeguroShippingType;
use App\Pagamentos;
use App\Http\Controllers\Painel\PagamentoController;
use App\config;

class GNetController extends Controller {

    public function index() {

        if (@$_GET['novopagamento'] == 1 and ( $_GET['metodo'] == 1 or $_GET['metodo'] == 2 or $_GET['metodo'] == 3)) {
            $pay = new PagamentoController();
            $res = $pay->gerar_link($_GET['metodo'], \Auth::user()->pacote, \Auth::user()->id, 'Ativação de pacote');
            if ($res) {
                echo $res;
            }

            //gnt
        } else {
            if (\Auth::user()->ativo) {
                return redirect('/painel');
            } else {
                $config = new config();
                $config['config'] = $config->getConfig();

                return view('painel.pages.inativo_gnet',$config);
            }
        }
    }

    public function gerarBoleto() {
        $trans = $this->repository->createTransaction();
        $boleto = $this->repository->createBoleto($trans['data']['charge_id']);
        return redirect($boleto['data']['link']);
    }

    public function pagarCartao() {
        $trans = $this->repository->createTransaction();
        $cartao = $this->repository->createCard($trans['data']['charge_id'], \Input::get('payment_token'));
        if (isset($cartao['data']['charge_id'])) {
            return 'Pagamento efetuado, em breve sua conta será ativada.';
        } else {
            return $cartao;
        }
    }

    public function notificacao() {
        header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
        $trans = $this->pagSeguroRepository->notification(\Input::get('notificationCode'));
        return $this->pagSeguroRepository->updateTransaction($trans);
        //var_dump($_POST);
    }

}
