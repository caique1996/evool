<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\PagSeguroRepository;
use Illuminate\Http\Request;
use App\User;
use Iugu;
use App\config;
use App\Pacote;
use App\Pagamentos;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
use Session;

class PagamentoController extends Controller {

    public function gerar_link($metodo, $pacote, $userId, $tipo) {
        $config = new config();
        $config = $config->getConfig();
        $usr = new User();
        $preferencial = 1;
        $userInfo = $usr->userInfo($userId);
        $pacoteInfo = Pacote::where('id', $pacote)->first();
        $vencimento = date('Y-m-d', strtotime("+5 days"));
        $hoje = date('Y-m-d');

        $pacoteValor = str_replace('.', '', $pacoteInfo['valor']);
        $pacoteValor = str_replace(',', '.', $pacoteValor);
        if ($metodo == 1) {
            Iugu::setApiKey($config['iugu_token']);
            $this->invoice = \Iugu_Invoice::create(
                            Array(
                                "email" => $userInfo['email'],
                                "due_date" => $vencimento,
                                "notification_url" => url('notificacao/1'),
                                "return_url" => url('painel/home'),
                                "items" =>
                                Array(
                                    Array(
                                        "description" => $tipo . '-' . $pacoteInfo['nome'],
                                        "quantity" => "1",
                                        "price_cents" => $pacoteValor * 100
                                    )
                                )
                            )
            );
            $retorno = $this->invoice;
            if (isset($retorno['secure_url'])) {
                $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'iugu', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $retorno['id'], 'paymentLink' => $retorno['secure_url'], 'pacote' => $pacote, 'tipo' => $tipo]);
                if ($userInfo['ativo'] == 1) {
                    $preferencial = 1;
                }
                $usr->criarFatura($pacote, $userId, $preferencial, $idPay, 0);
                return $retorno['secure_url'];
            } else {
                return false;
            }
        }//iugu
        if ($metodo == 2) {
            if ($config['gnt_ambiente'] == 'teste') {
                $sandbox = true;
            } else {
                $sandbox = false;
            }
            $this->api = new Gerencianet([
                'client_id' => $config['gnt_client'],
                'client_secret' => $config['gnt_secret'],
                'sandbox' => $sandbox
            ]);

            $body = [
                'items' => [
                    [
                        'name' => $tipo . '-' . $pacoteInfo['nome'],
                        'amount' => 1,
                        'value' => $pacoteValor * 100
                    ]
                ],
                'metadata' => [
                    'custom_id' => "SB-" . strval($userInfo['id']) . "-" . md5(time()),
                    'notification_url' => url('notificacao/2')
                ]
            ];
            $charge = $this->api->createCharge([], $body);
            $params2 = [
                'id' => $charge['data']['charge_id']
            ];
            $vencimento = date('Y-m-d', strtotime("+5 days"));
            $body2 = [
                'payment' => [
                    'banking_billet' => [
                        'expire_at' => strval($vencimento),
                        'customer' => [
                            'name' => $userInfo['name'],
                            'cpf' => str_replace(['.', '-'], '', $userInfo['cpf']),
                            'phone_number' => str_replace(['-'], '', $userInfo['telefone']),
                            'email' => $userInfo['email']
                        ]
                    ]
                ]
            ];
            $retorno = $this->api->payCharge($params2, $body2);
            if (isset($retorno['data']['link'])) {
                $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'gnt', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $charge['data']['custom_id'], 'paymentLink' => $retorno['data']['link'], 'pacote' => $pacote, 'tipo' => $tipo]);
                if ($userInfo['ativo'] == 1) {
                    $preferencial = 1;
                }
                $usr->criarFatura($pacote, $userId, $preferencial, $idPay, 0);
                return $retorno['data']['link'];
            } else {
                return false;
            }
        }//gerencianet
        else if ($metodo == 3) {
            $reference = md5(time()) . \Auth::user()->id . rand(0, 1000);
            $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'bitzpayer', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $reference, 'paymentLink' => url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=$tipo&pacote=$pacote&ref=$reference"), 'pacote' => $pacote, 'tipo' => $tipo]);
            return url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=$tipo&pacote=$pacote&ref=$reference");
        }
    }

    public function notificacao($param) {
        $usr = new User();
        $voucher = new \App\Http\Controllers\Admin\VoucherController;
        if ($param == 1) {

            $data = $_POST['data'];
            $status = $data['status'];
            $id = $data['id'];
            $pagamentoInfo = Pagamentos::where('reference', $id)->first();
            $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
            $tipo = $pagamentoInfo['tipo'];
            if ($status == 'paid' and $pagamentoInfo['status'] == 'Pendente') {
                if ($tipo == 'Ativação de pacote') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                } elseif ($tipo == 'Upgrade') {
                    $voucher->mudarPacote($pagamentoInfo['user_id'], $pagamentoInfo['pacote']);
                } elseif ($tipo == 'Renovação') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                }
                Pagamentos::where('id', $pagamentoInfo['id'])->update(['status' => 'Paga']);
            }
        }//iugu
        if ($param == 2) {
            $not = $_POST['notification'];
            $res = $this->notificationGnt($not)['data'];
            $retorno = array_pop($res);
            $status = $retorno['status']['current'];
            $id = $retorno['custom_id'];

            $pagamentoInfo = Pagamentos::where('reference', $id)->first();
            $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
            $tipo = $pagamentoInfo['tipo'];
            if ($status == 'waiting' and $pagamentoInfo['status'] == 'Pendente') {
                if ($tipo == 'Ativação de pacote') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                } elseif ($tipo == 'Upgrade') {
                    $voucher->mudarPacote($pagamentoInfo['user_id'], $pagamentoInfo['pacote']);
                } elseif ($tipo == 'Renovação') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                }
                Pagamentos::where('id', $pagamentoInfo['id'])->update(['status' => 'Paga']);
            }
            $res['status'] = $status;
            $res['user_id'] = $pagamentoInfo['user_id'];
            $res['tipo'] = $tipo;

            \DB::table('reponse')->insert(['body' => json_encode($res)]);
        }
        //bitpayer
        if ($param == 3) {
            define('MY_SHOP_LOGIN', config::getConf()['bitzpayer_login']);
            define('MY_SHOP_SECRET', config::getConf()['bitzpayer_secret']);
            $shop_secret = MY_SHOP_SECRET;
            $message = 'R' . $_POST['shop_login'] . $_POST['amount'] . $_POST['currency'] . $_POST['signature'] . $_POST['datetime'];
            $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));
            //$signature = 'hue';

            if ($signature === $_POST['signature']) {

                //
                $id = $_POST['signature'];
                $pagamentoInfo = Pagamentos::where('reference', $id)->first();

                $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
                $tipo = $pagamentoInfo['tipo'];

                if (isset($pagamentoInfo['id']) and $pagamentoInfo['status'] != 'Paga') {
                    if ($tipo == 'Ativação de pacote') {
                        $voucher->ativarUsr($pagamentoInfo['user_id']);
                    } elseif ($tipo == 'Upgrade') {
                        $voucher->mudarPacote($pagamentoInfo['user_id'], $pagamentoInfo['pacote']);
                    } elseif ($tipo == 'Renovação') {
                        $voucher->ativarUsr($pagamentoInfo['user_id']);
                    }
                    Pagamentos::where('id', $pagamentoInfo['id'])->update(['status' => 'Paga']);
                }
            }
        }
    }

    public function notificationGnt($token) {
        $config = new config();
        $config = $config->getConfig();
        if ($config['gnt_ambiente'] == 'teste') {
            $sandbox = true;
        } else {
            $sandbox = false;
        }
        $this->api = new Gerencianet([
            'client_id' => $config['gnt_client'],
            'client_secret' => $config['gnt_secret'],
            'sandbox' => $sandbox
        ]);

        $params = [
            'token' => $token
        ];

        try {
            $notification = $this->api->getNotification($params, []);
            return $notification;
        } catch (GerencianetException $e) {
            exit($e->getMessage() . ' - E:' . $e->getCode());
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function retorno($retorno) {
        $usr = new User();
        $voucher = new \App\Http\Controllers\Admin\VoucherController;
        if ($retorno == 1) {
            echo "<center><h1>Pagamento realizado com sucesso.<br><a href='" . env('SITE_URL') . '/painel/home' . "'>Clique aqui para acessar o painel</a></h1></center>'";
        }
        if ($retorno == 2) {
            echo "<center><h1>Ocorreu uma falha no pagamento :-(</h1></center>'";
        }
        if ($retorno == 3) {
            define('MY_SHOP_LOGIN', env("MY_SHOP_LOGIN"));
            define('MY_SHOP_SECRET', env("MY_SHOP_SECRET"));
            $shop_secret = MY_SHOP_SECRET;
            $message = 'R' . $_POST['shop_login'] . $_POST['amount'] . $_POST['currency'] . $_POST['signature'] . $_POST['datetime'];
            $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));

            if ($signature === $_POST['signature']) {

                //
                $id = $_POST['signature'];
                $pagamentoInfo = Pagamentos::where('reference', $id)->first();

                $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
                $tipo = $pagamentoInfo['tipo'];
                echo "Status: " . $pagamentoInfo['status'];
            }
        }
    }

    /* public function notificacao() {



      header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
      //var_dump($_POST);
      $email = env('PAGSEGURO_EMAIL');
      $token = env('PAGSEGURO_TOKEN_SANDBOX');
      $noti = $_GET['not'];

      $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/' . $noti . '?email=' . $email . '&token=' . $token;
      //echo '<br>' . $url . '<br>';
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $transaction = curl_exec($curl);
      curl_close($curl);
      if ($transaction == 'Unauthorized') {
      //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção
      exit; //Mantenha essa linha
      }
      $transaction = simplexml_load_string($transaction);
      $this->pagSeguroRepository->updateTransaction($transaction);
      } */
}
