<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Referrals;
use App\User;
use Illuminate\Http\Request;
use App\Binario;

class RedeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
          \Auth::user()->produto_pacote(\Auth::user()->id);
        $binario = new Binario();
        if (Referrals::where('system_id',\Auth::user()->id)->count() == 0) {
            return redirect('/painel/')
                            ->withErrors(['Nenhum usuário encontrado']);
        }

        return view('painel.pages.rede', compact('binario'));
    }
    public function interna($id) {
        $bin = new Binario();
        $filhos = $bin->getFilhos(\Auth::user()->id);
        if (!$filhos) {
            return redirect('/painel/')
                            ->withErrors(['Nenhum usuário encontrado']);
        } else {
            $perm = array_search($id, $filhos);
            if (!is_numeric($perm)) {
                return redirect('/painel/minha-rede')
                                ->withErrors(['Usuario não encontrado']);
            } else {
                $user_interna = User::where('id', $id)->first();
                if ($user_interna) {
                    return view('painel.pages.rede_interna', compact('user_interna'));
                } else {
                    return redirect('/painel/minha-rede')
                                    ->withErrors(['Usuario não encontrado']);
                }
            }
        }
    }

    public function unilevel() {
        $bin = new Binario();
        $id = \Auth::user()->id;
        $filhos = $bin->getFilhosLevel($id);

        $ordenados = array();
        //gambiara,dps eu errumo
        $key = md5(time() . $id . round(111111, 9999999));
        foreach ($filhos as $value) {
            $data['key'] = $key;
            $data['user'] = $value['user'];
            $data['level'] = $value['level'];
            \DB::table('temp_unilevel')->insert($data);
        }
        $rede = \DB::table('temp_unilevel')->where('key', $key)->orderBy('level')->get();
        \DB::table('temp_unilevel')->where('key', $key)->delete();
        return view('painel.pages.unilevel', compact('rede'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (Referrals::updateDirection($id, \Input::get('direcao'))) {
            return "<div class='alert alert-success'>Usuario ID: <b>$id</b> Atualizado!</div>";
        } else {
            return "<div class='alert alert-danger'>Não foi possivel atualizar o Usuario: <b>$id</b></div>";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
