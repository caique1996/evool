<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Carrinho;
use App\Produtos;
use App\Pedidos;
use PagSeguroConfig;
use PagSeguroConfigWrapper;
use PagSeguroPaymentRequest;
use PagSeguroShippingType;
use App\Enderecos;

class MeusPedidosController extends Controller {

    public function index() {

        $pedidos = Pedidos::where('user_id', \Auth::user()->id)->get()->toArray();


        foreach ($pedidos as $key => $pedido) {
            $produto = Produtos::where('id', Carrinho::where('pedido', $pedido['id'])->first()['product_id'])->first();
            $pedidos[$key]['img'] = $produto['img'];
        }

        return view('painel.pages.meus-pedidos', compact('pedidos'));
    }

    public function todos() {

        $pedidos = Pedidos::where('status', 'Pago')->get()->toArray();


        foreach ($pedidos as $key => $pedido) {
            $produto = Produtos::where('id', Carrinho::where('pedido', $pedido['id'])->first()['product_id'])->first();
            $pedidos[$key]['img'] = $produto['img'];
        }

        return view('painel.pages.todos-pedidos', compact('pedidos'));
    }

    public function add($endereco, $codigo) {
        $endereco = Enderecos::where('id', $endereco)->first();
        $produtos = Carrinho::carrinhoAtual();
        if (!$produtos)
            return redirect("/painel/meus-pedidos");
        // Calcula o subtotal
        $subtotal = 0;
        $pesoTotal = 0;
        $descricao = '';
        $subtotal2 = 0;
        foreach ($produtos as $produto) {

            $quantidade = $produto['quantidade'];
            $preco = $produto['produto']['preco'] * $quantidade;
            if (\Auth::user()->meu_desconto($produto['product_id'])) {
                $subtotal +=\Auth::user()->meu_desconto($produto['product_id']) * $quantidade;
                $descricao.=$produto['produto']['nome'] . '  R$' . number_format(\Auth::user()->meu_desconto($produto['product_id']), 2) . ' Quantidade: ' . $quantidade . " \n";
            } else {
                $subtotal+=$preco;
                $descricao.=$produto['produto']['nome'] . '  R$' . number_format($preco, 2) . ' Quantidade: ' . $quantidade;
            }
            $subtotal2 += $preco;
            $pesoTotal += $quantidade * $produto['produto']['peso'];
        }

        // Calcula o frete do pedido
        $frete = $this->calculaFrete($codigo, $endereco['cep'], $pesoTotal);
        $total = $subtotal + $frete;
        // Cria um novo pedido
        if ($_GET['pagamento'] == 1) {
            if (\Auth::user()->saldo >= $total) {
                \Auth::user()->removeSaldo(\Auth::user()->id, $total, 'Compra', 7);
                foreach ($produtos as $produto) {
                    \Auth::user()->bonus_compra($produto['product_id']);
                }
                $pedido = new Pedidos;
                $pedido->user_id = \Auth::user()->id;
                $pedido->id_endereco = $endereco['id'];
                $pedido->status = "Pago";
                $pedido->preco = $total;
                $pedido->produtos = json_encode($produtos);
                $pedido->date = date("Y-m-d");
                $pedido->save();
                $produ = new Produtos();
                foreach ($produtos as $value) {
                    $produ->add_venda($value['product_id'], $value['quantidade']);
                }
                \App\extratos::create(['user_id' => \Auth::user()->id, 'data' => date('Y-m-d'), 'descricao' => 'Compra<br>' . $descricao, 'valor' => $total, 'beneficiado' => 1, 'tipo' => 7]);
                $carrinhos = Carrinho::where('pedido', 0)->where(
                                'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]);
                return redirect('/painel/home')
                                ->with('success', 'Parabéns! Sua compra foi efetuada com sucesso! Breve entratermos em contato para enviar informações.Fique atento(a) ao seu email.');
            } else {
                return redirect('/painel/meus-pedidos')->withErrors(['Saldo insuficiente.']);
            }
        } elseif ($_GET['pagamento'] == 2) {
            $tipo = 'Compra';
            $pedido = new Pedidos;
            $pedido->user_id = \Auth::user()->id;
            $pedido->id_endereco = $endereco['id'];
            $pedido->status = "Pendente";
            $pedido->preco = $total;
            $pedido->produtos = json_encode($produtos);
            $pedido->date = date("Y-m-d");
            $pedido->save();
            $reference = 'pedido_' . $pedido->id . '_' . \Auth::user()->id;

            Pedidos::where('id', $pedido->id)->update(['id_pag' => $reference]);

            $carrinhos = Carrinho::where('pedido', 0)->where(
                            'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]);
            define('MY_SHOP_LOGIN', env("MY_SHOP_LOGIN"));
            define('MY_SHOP_SECRET', env("MY_SHOP_SECRET"));
            $shop_login = MY_SHOP_LOGIN;
            $shop_secret = MY_SHOP_SECRET;
            $amount = $total;
            $currency = 'BRL';
            $invoice_id = $reference;
            $payment_description = $descricao;
            $signature = '';
            $message = 'S' . $shop_login . $amount . $currency . $invoice_id;
            $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));
            $pacote = $pedido->id;
            $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'bitzpayer', 'status' => 'Pendente', 'date' => date('Y-m-d'), 'user_id' => \Auth::user()->id, 'reference' => $reference, 'paymentLink' => url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=$tipo&pacote=$pacote&ref=$reference"), 'pacote' => $pacote, 'tipo' => $tipo]);
            $usr = new User();
            $usr->criarFatura($pacote, \Auth::user()->id, 0, $idPay,0,1);
            return redirect(url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=$tipo&pacote=$pacote&ref=$reference"));
        }
    }

    public function pagSeguro($total) {
        $id = mt_rand(1262055681, 1262055681);

        $paymentRequest = new PagSeguroPaymentRequest();

        $paymentRequest->addItem(mt_rand(1262055681, 1262055681), "#Seu Pedido", 1, number_format($total, 2));
        $paymentRequest->setSender(
                \Auth::user()->name, \Auth::user()->email, '', '', \Auth::user()->cpf, '');

        $paymentRequest->setCurrency("BRL");
        $reference = md5(\Auth::user()->name . $id . time());
        $paymentRequest->setReference($reference);
        $paymentRequest->setRedirectUrl(env('PAGSEGURO_REDIRECT'));


        $paymentRequest->addParameter('notificationURL', env('PAGSEGURO_NOTIFICATION'));

        $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()

        $checkoutUrl = $paymentRequest->register($credentials);


        return array('id_pag' => mt_rand(1262055681, 1262055681), 'redirect' => $checkoutUrl);
    }

    // Retorna detalhes de um pedido
    public function pedido($id) {

        $pedido = Pedidos::where('id', $id)->first();

        if (!\Auth::user()->isAdmin()) {
            // Verifica se o pedido é do usuário mesmo
            if ($pedido['user_id'] != \Auth::user()->id) {
                return redirect("/painel/meus-pedidos/");
            }
        }

        // Pega o endereco e os produtos do pedido
        $endereco = Enderecos::where('id', $pedido['id_endereco'])->first();
        $produtos = Carrinho::carrinhoPedido($id);

        return view('painel.pages.pedido', compact('produtos', 'endereco', 'pedido'));
    }

    public function edit($id) {

        $dados = Pedidos::where('id', $id)->first();
        return view('admin.pages.editar-pedido', compact('dados'));
    }

    public function update() {
        $data = \Input::all();
        $valida = [
            'id' => 'integer|required',
            'status' => 'required',
            'info' => 'required',
        ];

        $validator = \Validator::make($data, $valida);
        if ($validator->fails()) {
            return redirect('admin/pedido/edit/' . $data['id'])->withErrors($validator)->withInput();
        } else {
            $id = $data['id'];
            unset($data['id']);
            unset($data['_token']);
            unset($data['_method']);
            \DB::table('pedidos')->where('id', $id)->update($data);
            return redirect('admin/pedido/edit/' . $id)->with('success', 'Sucesso!');
        }
    }

}
