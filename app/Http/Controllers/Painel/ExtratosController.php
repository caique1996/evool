<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\extratos;

class ExtratosController extends Controller {

    public function index() {
        $Extratos = extratos::where('beneficiado', 1)->get();

        return view('painel.pages.extratos', compact('Extratos'));
    }

    public function relatorios() {
        $usr = new User();  
      
        $Extratos = extratos::where('beneficiado', 1)->get();

        return view('admin.pages.relatorios', compact('Extratos'));
    }

}
