<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\lancamentos;
use Illuminate\Http\Request;

class LancamentosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $materiais = \App\Lancamentos::all();
        return view('admin.pages.lancamentos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.lancamentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $material = new \App\Lancamentos();
        if ($request->tipo == 'ganho') {
            $idExtrato = \DB::table('extratos')->insertGetId(['user_id' => 1, 'data' => date('Y-m-d'), 'descricao' => $request->nome, 'valor' => $request->valor, 'beneficiado' => 1, 'tipo' => 15]);
        } else {
            $idExtrato = \DB::table('extratos')->insertGetId(['user_id' => 1, 'data' => date('Y-m-d'), 'descricao' => $request->nome, 'valor' => $request->valor, 'beneficiado' => 1, 'tipo' => 14]);
        }
        $material->nome = $request->nome;
        $material->descricao = $request->descricao;
        $material->tipo = $request->tipo;
        $material->valor = $request->valor;
        $material->data = date('Y-m-d');
        $material->extrato_id = $idExtrato;
        if ($material->save()) {

            return <<<EOL
                 <div class="alert alert-success fade in">
                     Lancamento Adicionado
                 </div>
EOL;
        } else {
            return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $materal = \App\Lancamentos::find($id);
        $link = $materal->link;
        $extension = $this->getExtension($link);
        return view('admin.pages.materiais.preview', compact('link', 'extension'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (\App\Lancamentos::destroy($id)) {
            \App\extratos::destroy(\App\Lancamentos::where('id', $id)->first()['extrato_id']);

            return <<<EOL
                 <div class="alert alert-success fade in">
                     Lancamento Removido
                 </div>
EOL;
        } else {
            return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    private function getExtension($link) {
        return pathinfo($link)['extension'];
    }

}
