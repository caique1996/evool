<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pacote;
use Validator;
use App\User;
use App\config;
use Session;
use Redirect;

class PacoteController extends Controller {

    public function index() {

        $usr = new User();

        $pacotes = Pacote::all();
        return view('admin.pages.pacote', compact('pacotes'));
    }

    public function create() {
        return view('admin.pages.pacotes.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $usr = new User();
        $pacote = new Pacote();
        $data = $request->all();
//pega valor
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "nivel{$i}";
            $data[$key] = $this->getAmount($data[$key]);
        }
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "residual_nivel{$i}";
            $data[$key] = $this->getAmount($data[$key]);
        }
        $data['residual_direto'] = $this->getAmount($data['residual_direto']);
        
        $data['valor'] = $this->getAmount($data['valor']);


        $valida = [
            'nome' => 'required',
            'valor' => 'required|numeric',
            'bonus_equiparacao' => 'numeric|required',
            'descricao' => 'required|min:100',
            'tipo_pagamento' => 'required',
            'total_cotas' => 'integer|required',
            'status' => 'integer|required',
            'duracao_meses' => 'integer|required|min:1'
        ];

        //valida indicação
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "nivel{$i}";
            $valida[$key] = "numeric";
        }
//valida pontos
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "pontos{$i}";
            $valida[$key] = "numeric";
        }
        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            unset($data['id']);
            unset($data['_token']);

            $operacao = \DB::table('pacotes')->insert($data);
            //$operacao = Pacote::($data);
            if ($operacao) {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Pacote Adicionado
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function view($id) {
        $dados['dados'] = Pacote::where('id', $id)->first();
        $pacoteTotal = Pacote::where('id', $id)->count();
        return view('admin.pages.pacotes.edit', $dados);
    }

    public function salvar(Request $request) {
        $usr = new User();
        $pacote = new Pacote();
        $data = $request->all();
//pega valor
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "nivel{$i}";
            $data[$key] = $this->getAmount($data[$key]);
        }
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "residual_nivel{$i}";
            $data[$key] = $this->getAmount($data[$key]);
        }

        $data['valor'] = $this->getAmount($data['valor']);


        $valida = [
            'id' => 'required|exists:pacotes',
            'nome' => 'required',
            'valor' => 'required|numeric',
            'bonus_equiparacao' => 'numeric|required',
            'descricao' => 'required|min:100',
            'tipo_pagamento' => 'required',
            'total_cotas' => 'integer|required',
            'status' => 'integer|required',
        ];

        //valida indicação
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "nivel{$i}";
            $valida[$key] = "numeric";
        }
//valida pontos
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            $key = "pontos{$i}";
            $valida[$key] = "numeric";
        }
        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            unset($data['id']);
            unset($data['_token']);
            $operacao = Pacote::where('id', $id)->update($data);
            if ($operacao) {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Pacote Atualizado
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function expirados() {
        $pacote = new Pacote();
        $pacote->expirados();
    }

    public function faturas() {
        $bar = true;
        /* $str = "TEST". ($bar ? 'true' : 'false') ."TEST"; */

        $config = new config();
        $config = $config->getConfig();
        $faturas = \DB::table('faturas')->where('user_id', \Auth::user()->id)->get();
        return view('painel.pages.faturas', compact('faturas', 'config'));
    }

    public function excluir_fatura($id) {
        if (\DB::table('faturas')->where('id', $id)->where('user_id', \Auth::user()->id)->count() > 0) {
            $dados = \DB::table('faturas')->where('id', $id)->first();
            \DB::table('faturas')->where('id', $id)->delete();
            \DB::table('pagamentos')->where('id', $dados->pagamento_id)->delete();
            Session::flash('success', 'Deletado com sucesso!');
            return Redirect::to('painel/faturas');
        } else {
            return redirect('/painel/faturas')->withErrors(['Fatura não encontrada.']);
        }
    }

    //admin

    public function faturas_admin() {
        $config = new config();
        $config = $config->getConfig();
        $faturas = \DB::table('faturas')->get();
        return view('admin.pages.faturas', compact('faturas', 'config'));
    }

    public function liberar_fatura($id) {
        $voucher = new VoucherController();
        $faturaInfo = \DB::table('faturas')->where('id', $id)->first();
        if (isset($faturaInfo->id) and $faturaInfo->status == 0) {
            //ativa a fatura
            $pagamentoInfo = \DB::table('pagamentos')->where('id', $faturaInfo->pagamento_id)->first();
            
            if ($pagamentoInfo->tipo == 'Upgrade') {
                $pacoteInfo=  Pacote::where('id',$pagamentoInfo->pacote)->first();
                $validade = date('Y-m-d', strtotime('+' . $pacoteInfo->duracao_meses . 'month', strtotime('Y-m')));
                $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
                \DB::table('faturas')->where('id', $id)->update(['status' => 1, 'data' => date('Y-m-d'), 'validade' => $validade]);

                $voucher->mudarPacote($faturaInfo->user_id, $pagamentoInfo->pacote);
            } elseif ($pagamentoInfo->tipo == 'Compra') {
                $validade = date('Y-m-d', strtotime('+ 3 days', strtotime(date('Y-m-d'))));
                $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
                \DB::table('faturas')->where('id', $id)->update(['status' => 1, 'data' => date('Y-m-d'), 'validade' => $validade]);
                \App\Pedidos::where('id', $pagamentoInfo->pacote)->update(['status' => 'Pago']);
            }
            return redirect('/admin/faturas')->with('success', 'Operação realizada com sucesso');
        } else {
            return redirect('/admin/faturas')->withErrors(['Fatura não foi encontrada ou já foi paga.']);
        }
    }

    public function excluir_fatura_admin($id) {
        if (\DB::table('faturas')->where('id', $id)->where('user_id', \Auth::user()->id)->count() > 0) {
            $dados = \DB::table('faturas')->where('id', $id)->first();
            \DB::table('faturas')->where('id', $id)->delete();
            \DB::table('pagamentos')->where('id', $dados->pagamento_id)->delete();
            Session::flash('success', 'Deletado com sucesso!');
            return Redirect::to('painel/faturas');
        } else {
            return redirect('/painel/faturas')->withErrors(['Fatura não encontrada.']);
        }
    }

}
