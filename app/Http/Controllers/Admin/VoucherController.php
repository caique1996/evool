<?php

namespace App\Http\Controllers\Admin;

use App\Voucher;
use Illuminate\Http\Request;
use App\User;
use App\Pacote;
use App\extratos;
use App\Referrals;
use App\Pagamentos;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Binario;
use App\config;

class VoucherController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $usuarios = User::all();
        return view('admin.pages.vouchers', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user) {
        $usuario = User::find($user);
        return view('admin.pages.addvoucher', compact('usuario'));
    }

    public function remover($user) {
        $usuario = User::find($user);
        return view('admin.pages.removevoucher', compact('usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, \Faker\Generator $faker) {
        $data = \Input::all();

        $data['user_id'] = $id;

        for ($i = 0; $i < $data['quantidade']; $i++) {
            $data['voucher'] = $faker->sha1;
            $create = Voucher::create($data);
        }

        if ($create) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers Gerados Com Successo!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user) {
        $data = \Input::all();
        for ($i = 0; $i < $data['quantidade']; $i++) {
            $voucher = Voucher::where('user_id', $user)->first();
            if ($voucher) {
                $destroy = Voucher::destroy($voucher->id);
            } else {
                $destroy = true;
                break;
            }
        }

        if ($destroy) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers removidos com sucesso!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    public function addValidadePacote($usr) {
        $dataUs = User::where('id', $usr)->first();
        $pacoteData = Pacote::where('id', $dataUs['pacote'])->first();
        $data = date('Y-m-d');
        $validade = date('Y-m-d', strtotime('+' . $pacoteData['duracao_meses'] . 'month', strtotime($data)));
        $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
        User::where('id', $usr)->update(['validade_pacote' => $validade]);
    }

    public function ativarUsr($id = '') {
        if (@$id == '') {
            $id = @$_GET['id'];
        }
        $idAct = $id;
        $usr = new User();
        $bin = new Binario();
        $dataUs = User::where('id', $id)->first();
        $pacoteUs = Pacote::where('id', $dataUs['pacote'])->first();
        $status = 1;
        if ($status == 0 or $status == 1) {
            
        } else {
            echo 'Operação Não realizada';
            exit();
        }
        if (is_numeric($id) and $dataUs['ativo'] <> 41) {
            // $usr->
            $user = new User();
            $user = User::find($id);
            $user->ativaUser();
            $usr->bonus_indicacao($id);
            $usr->bonus_pontos($id);
            $this->addValidadePacote($id);
            \Auth::user()->produto_pacote($idAct);

            extratos::create(['user_id' => $idAct, 'data' => date('Y-m-d'), 'descricao' => 'Ativação de cadastro', 'valor' => $pacoteUs['valor'], 'beneficiado' => 1, 'tipo' => 5]);

            \App\Landingpages::create(['pilar_1_titulo' => 'Seriedade', 'pilar_1_conteudo' => 'Uma equipe competente, composta de pessoas cujo objetivo é oferecer a melhor oportunidade para você',
                'pilar_2_titulo' => 'Visão', 'pilar_2_conteudo' => 'Qualificar pessoas a serem promotores de benefícios para sua família e para sociedade', 'pilar_3_titulo' => 'Resultado', 'pilar_3_conteudo' => 'Criamos um negócio perfeito, com o objetivo de trazer resultados reais e mudança de vida para todos nossos associado',
                'pilar_4_titulo' => 'Amor',
                'pilar_4_conteudo' => 'Realmente, nós temos paixão pelo que fazemos: oferecer uma mudança real e completa à vida das pessoas '
                , 'video_titulo' => 'A OPORTUNIDADE', 'video_descricao' => 'Com a implementação de dois grandes mercados, iniciamos um sistema de comissionamento inteligente e sustentável, onde oferecemos a oportundiade de transformação financeira e social aos nossos associados. 
Através de nossos produtos, podemos oferecer resultados reais, com segmentações precisas, custo baixo e ótimo custo x benefício aos nossos clientes e parceiros. Somos dententores de uma base de dados com aproximadamente quatro milhões de clientes ativos, que estão localizados em mais de quarenta países ao redor do mundo.',
                'youtube_video' => 'https://www.youtube.com/embed/IJn9Dq0xdKg', 'user_id' => $dataUs['id'], 'email' => $dataUs['email'], 'status' => 1]);
            echo 'Operação  realizada com sucesso!';
        } else {
            echo 'Operação Não realizada';
            exit();
        }
    }

    function mudarPacote($id = '', $pacote = '') {
        if (@$id == '') {
            $id = @$_GET['id'];
        }
        $idAct = $id;
        $usr = new User();
        $bin = new Binario();
        $dataUs = User::where('id', $id)->first();
        $pacoteUs = Pacote::where('id', $dataUs['pacote'])->first();
        $status = 1;
        if ($status == 0 or $status == 1) {
            
        } else {
            echo 'Operação Não realizada';
            exit();
        }
        if (is_numeric($id)) {
            // $usr->
            $user = new User();
            $user = User::find($id);
            $user->ativaUser();
            $usr->bonus_indicacao($id);
            $usr->bonus_pontos($id);
            $this->addValidadePacote($id);
            \Auth::user()->produto_pacote($idAct);

            extratos::create(['user_id' => $idAct, 'data' => date('Y-m-d'), 'descricao' => 'Ativação de cadastro', 'valor' => $pacoteUs['valor'], 'beneficiado' => 1, 'tipo' => 5]);

            \App\Landingpages::create(['pilar_1_titulo' => 'Seriedade', 'pilar_1_conteudo' => 'Uma equipe competente, composta de pessoas cujo objetivo é oferecer a melhor oportunidade para você',
                'pilar_2_titulo' => 'Visão', 'pilar_2_conteudo' => 'Qualificar pessoas a serem promotores de benefícios para sua família e para sociedade', 'pilar_3_titulo' => 'Resultado', 'pilar_3_conteudo' => 'Criamos um negócio perfeito, com o objetivo de trazer resultados reais e mudança de vida para todos nossos associado',
                'pilar_4_titulo' => 'Amor',
                'pilar_4_conteudo' => 'Realmente, nós temos paixão pelo que fazemos: oferecer uma mudança real e completa à vida das pessoas '
                , 'video_titulo' => 'A OPORTUNIDADE', 'video_descricao' => 'Com a implementação de dois grandes mercados, iniciamos um sistema de comissionamento inteligente e sustentável, onde oferecemos a oportundiade de transformação financeira e social aos nossos associados. 
Através de nossos produtos, podemos oferecer resultados reais, com segmentações precisas, custo baixo e ótimo custo x benefício aos nossos clientes e parceiros. Somos dententores de uma base de dados com aproximadamente quatro milhões de clientes ativos, que estão localizados em mais de quarenta países ao redor do mundo.',
                'youtube_video' => 'https://www.youtube.com/embed/IJn9Dq0xdKg', 'user_id' => $dataUs['id'], 'email' => $dataUs['email'], 'status' => 1]);
            echo 'Operação  realizada com sucesso!';
        } else {
            echo 'Operação Não realizada';
            exit();
        }
    }

    function renova($id) {
        $config = new config();
        $config = $config->getConfig();
        if ($config['renovacao_status'] == 'sim') {
            $dataUser = User::where('id', $id)->first();
            $today = date("Y-m-d");
            
            $pacoteData = Pacote::where('id', $dataUser['pacote'])->first();
            //
            $pacoteValor = str_replace('.', '', $pacoteData['valor']);

            $pacoteValor = str_replace(',', '.', $pacoteValor);
            $ganhoLimite = $pacoteValor * $config['renovavao'];
            $saldo = extratos::where('beneficiado', $id)->where('descricao', 'Bônus PLS')->sum('valor');

            if ($saldo >= $ganhoLimite and $dataUser['admin'] <> 1) {

                $novoSaldo = $dataUser['saldo'] - ($pacoteValor * 1.1);
                $data['saldo'] = $novoSaldo;
                User::where('id', $id)->update($data);
                extratos::where('beneficiado', $id)->where('descricao', 'Bônus PLS')->delete();
                $valorRenovacao = ($pacoteValor * 1.1 ) * (-1);
                extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Renovação automática', 'valor' => ($pacoteValor * 1.1), 'beneficiado' => 1]);
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => 'Renovação automática', 'valor' => $valorRenovacao, 'beneficiado' => $id]);
                return true;
            } else {
                //return false;
            }
        } else {
            // return false;
        }
        return false;
    }

    function statusSaque() {
        $id = $_GET['id'];
        if ($id == '' or ! is_numeric($id)) {
            echo 'Erro';
        } else {
            $dataUser = User::where('id', $id)->first();
            if ($dataUser['saque'] == 'ativo') {
                $status = 'inativo';
            } else {
                $status = 'ativo';
            }
            $data['saque'] = $status;
            User::where('id', $id)->update($data);
            echo "Operação realizada com sucesso";
        }
    }

}
