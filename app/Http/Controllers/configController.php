<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\config;
use Validator;
use Symfony\Component\DomCrawler\Form;

class configController extends Controller {

    public function index() {
        $config = config::where('id', 1)->first();
        $config['config'] = $config;
        return view('admin.pages.config', $config);
    }

    public function update(Request $request) {

        $data = \Input::all();
        /*
          Array ( [id] => 1 [site_name] => Seven7club [site_url] => [logo_1] =>
         * [logo_2] => [logo_3] => [binario_valor] => 0.00 [indicacao] => 0.00 [limite_binario] => 6 [smtp_port] => 0 [smtp_host] => [smtp_user] => [smtp_pass] => 
         * [permitir_saque] => [saque_minimo] => [iugu_token] => [iugu_ambiente] => [gnt_status] => [iugu_status] => [deposito_status] =>
         */
        unset($data['_token']);
        unset($data['_method']);
        $valida = [
            'permitir_saque' => 'required|max:6',
            'saque_minimo' => 'required|numeric',
            'saque_max' => 'required|numeric',
            'logo_1' => 'image',
            'logo_2' => 'image',
            'logo_3' => 'image',
            'termos' => 'mimes:pdf,doc'
        ];


        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            return redirect('/admin/config')
                            ->withErrors($validator)
                            ->withInput();
        }
        if (\Input::file('logo_1')) {
            if (\Input::file('logo_1')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = \Input::file('logo_1')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(111119999, 99999999) . '.' . $extension; // renameing image
                \Input::file('logo_1')->move($destinationPath, $fileName); // uploading file to given path
                $logo_1 = asset($destinationPath . '/' . $fileName);
                $data['logo_1'] = $logo_1;
            }
        } else {
            unset($data['logo_1']);
        }
        if (\Input::file('logo_2')) {
            if (\Input::file('logo_2')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = \Input::file('logo_2')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(111119999, 99999999) . '.' . $extension; // renameing image
                \Input::file('logo_2')->move($destinationPath, $fileName); // uploading file to given path
                $logo_1 = asset($destinationPath . '/' . $fileName);
                $data['logo_2'] = $logo_1;
            }
        } else {
            unset($data['logo_2']);
        }
        if (\Input::file('logo_3')) {
            if (\Input::file('logo_3')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = \Input::file('logo_3')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(111119999, 99999999) . '.' . $extension; // renameing image
                \Input::file('logo_3')->move($destinationPath, $fileName); // uploading file to given path
                $logo_1 = asset($destinationPath . '/' . $fileName);
                $data['logo_3'] = $logo_1;
            }
        } else {
            unset($data['logo_3']);
        }
        if (\Input::file('termos')) {
            if (\Input::file('termos')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = \Input::file('termos')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(111119999, 99999999) . '.' . $extension; // renameing image
                \Input::file('termos')->move($destinationPath, $fileName); // uploading file to given path
                $logo_1 = asset($destinationPath . '/' . $fileName);
                $data['termos'] = $logo_1;
            }
        } else {
            unset($data['termos']);
        }
        $dados = $data;


        $conf = config::where('id', 1)->update($dados);

        if ($conf) {

            \Storage::disk('local')->put('config.json', json_encode($dados));
            return redirect('/admin/config')
                            ->with('success', 'Configurações atualizadas');
        } else {
            return redirect('/admin/config')
                            ->withErrors(['Não foi possivel atualizar, tente novamente.']);
        }
    }

}
