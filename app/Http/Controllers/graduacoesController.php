<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\config;
use Validator;
use Symfony\Component\DomCrawler\Form;
use App\graduacoes;
use App\User;

class graduacoesController extends Controller {

    public function index() {
       
        $graduacoes = graduacoes::all();

        return view('admin.pages.graduacoes', compact('graduacoes'));
    }

    public function index2() {

        return view('painel.pages.graduacoes');
    }

    public function create() {
        return view('admin.pages.graduacoes.create');
    }

    public function store(Request $request) {
        $graduacao = new graduacoes();
        $graduacao->name = $request->name;
        $graduacao->status = $request->status;
        $graduacao->pontuacao = $request->pontuacao;
        $graduacao->icone = $request->icone;
        $graduacao->boas_vindas = $request->boas_vindas;
        $graduacao->premios = $request->premios;


        if ($graduacao->save()) {
            return <<<EOL
                 <div class="alert alert-success fade in">
                     Graduação Adicionada
                 </div>
EOL;
        } else {
            return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    public function view($id) {
        $dados['dados'] = graduacoes::where('id', $id)->first();
        $pacoteTotal = graduacoes::where('id', $id)->count();
        return view('admin.pages.graduacoes.edit', $dados);
    }

    public function salvar() {
        $data = \Input::all();

        $valida = [
            'id' => 'integer|required',
            'name' => 'required|max:255',
            'icone' => 'required',
            'status' => 'required|numeric',
            'pontuacao' => 'required|numeric',
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            unset($data['id']);
            unset($data['_token']);


            graduacoes::where('id', $id)->update($data);
            return <<<EOL
                 <div class="alert alert-success fade in">
                     Graduação Atualizada
                 </div>
EOL;
        }
    }

}
