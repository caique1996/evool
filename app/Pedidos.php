<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    public $timestamps = false;
    protected $fillable = array('status','info');
}
