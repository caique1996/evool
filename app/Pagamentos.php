<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagamentos extends Model
{
    protected $fillable = ['id', 'user_id', 'reference', 'code', 'status', 'paymentMethod', 'date', 'lastEventDate', 'paymentLink','pacote','tipo'];

}
