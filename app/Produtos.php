<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model {

    public $timestamps = false;
    protected $fillable = array('pontos_diretos','vendas', 'pontos1', 'pontos2', 'pontos3', 'pontos4', 'pontos5', 'pontos5', 'pontos', 'nome', 'descricao', 'descontos', 'preco', 'peso', 'img', 'categoria', 'estoque', 'status');

    public function add_venda($produto, $qntd_vendas) {
        
        $prod = Produtos::where('id', $produto);
        if ($prod->first()['id']) {
            $total_vendas = $prod->first()['vendas'] + $qntd_vendas;
            $total_estoque = $prod->first()['estoque'] - $qntd_vendas;
            $prod->update(['vendas'=>$total_vendas, 'estoque' => $total_estoque]);
        }
    }

}
