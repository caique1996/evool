<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materiais extends Model
{
    protected $fillable = ['description', 'link', 'download'];
}
