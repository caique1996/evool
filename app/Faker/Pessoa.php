<?php
/**
 * Created by PhpStorm.
 * User: Miller
 * Date: 08/01/2016
 * Time: 00:27
 */

namespace App\Faker;


class Pessoa extends \Faker\Provider\pt_BR\Person
{
    protected static $titleMale = array('');

    protected static $titleFemale = array('');

    protected static $suffix = array('');
}