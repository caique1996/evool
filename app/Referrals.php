<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referrals extends Model {

    protected $fillable = ['user_id', 'system_id', 'pai_id', 'direcao'];

    public function searchSystemId($indicador, $direcao = 'esquerda') {
        $direcao = 'esquerda';
        $procura = true;
        $system_id = $indicador;
        $binario = new Binario();
        $usr = new User();
        if (Referrals::where('system_id', $indicador)->count() < env('N_MATRIZ')) {
            $system_id = $indicador;
        } else {
            $filhos = $binario->getFilhos($indicador);
            if (count($filhos) >= env('N_MATRIZ')) {
                foreach ($filhos as $value) {
                    if (env('N_MATRIZ') > Referrals::where('system_id', $value)->count()) {
                        $system_id = $value;
                        break;
                    }
                }
            }
        }
        return ['pai_id' => $system_id, 'direcao' => $direcao];
    }

    public static function updateDirection($id, $direcao) {
        $refer = new Referrals();
        return $refer->where('user_id', $id)->update(['direcao' => $direcao]);
    }

}
