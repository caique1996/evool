<?php

/**
 * Created by PhpStorm.
 * User: Miller
 * Date: 18/02/2016
 * Time: 21:56
 */

namespace App\Repositories;

use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
use App\config;

class GNetRepository {

    private $api;

    public function __construct() {
        $this->api = new Gerencianet([
            'client_id' => env('GN_CLIENT_ID'),
            'client_secret' => env('GN_CLIENT_SECRET'),
            'sandbox' => env('GN_SANDBOX')
        ]);
    }

    public function createTransaction($pacote, $user,$tipo, $urlnot) {


        $body = [
            'items' => [
                [
                    'name' => 'Seven Brasil - Ativação de Cadastro',
                    'amount' => 1,
                    'value' => 10000
                ]
            ],
            'metadata' => [
                'custom_id' => "SB-" . strval($user) . "-" . md5(time()),
                'notification_url' => $urlnot
            ]
        ];

        return $this->api->createCharge([], $body);
    }

    public function createBoleto($id) {
        $params = [
            'id' => $id
        ];
        $vencimento = date('Y-m-d', strtotime("+5 days"));
        $body = [
            'payment' => [
                'banking_billet' => [
                    'expire_at' => strval($vencimento),
                    'customer' => [
                        'name' => \Auth::user()->name,
                        'cpf' => str_replace(['.', '-'], '', \Auth::user()->cpf),
                        'phone_number' => str_replace(['-'], '', \Auth::user()->telefone),
                        'email' => \Auth::user()->email
                    ]
                ]
            ]
        ];

        return $this->api->payCharge($params, $body);
    }

    public function createCard($id, $paymentToken) {
        $body = [
            'payment' => [
                'credit_card' => [
                    'installments' => 1,
                    'billing_address' => [
                        'street' => \Auth::user()->endereco,
                        'number' => 10,
                        'neighborhood' => \Auth::user()->bairro,
                        'zipcode' => '35400000',
                        'city' => \Auth::user()->cidade,
                        'state' => 'SP',
                    ],
                    'payment_token' => $paymentToken,
                    'customer' => [
                        'name' => \Auth::user()->name,
                        'cpf' => str_replace(['.', '-'], '', \Auth::user()->cpf),
                        'phone_number' => str_replace(['-'], '', \Auth::user()->telefone),
                        'email' => \Auth::user()->email,
                        'birth' => \Auth::user()->nascimento
                    ]
                ]
            ]
        ];
        return $this->api->payCharge(['id' => $id], $body);
    }

    public function notification($token) {
        $params = [
            'token' => $token
        ];

        try {
            $notification = $this->api->getNotification($params, []);
            return $notification;
        } catch (GerencianetException $e) {
            exit($e->getMessage() . ' - E:' . $e->getCode());
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

}
